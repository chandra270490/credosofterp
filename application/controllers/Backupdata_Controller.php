<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class BackupData_Controller extends CI_Controller
{
	public $fileName='k';
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Backupdata_model');
            $this->load->helper('url');
    }
	public function index()
	{
			

		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Backup Data')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			// $data['records'] = $this->Accounts_model->getData();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$this->load->view('BackupData_view');
			$this->load->view('includes/footer');
		}
		else
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  
	public function frmExcel()
	{
		$this->BackupData_model->fromExcel();
		echo "done...";
	} 

	function dbbackup()
	{
      $this->load->dbutil(); 
      $prefs = array( 'format' => 'zip', // gzip, zip, txt 
                               'filename' => 'backup_'.date('d_m_Y_H_i_s').'.sql', 
                                                      // File name - NEEDED ONLY WITH ZIP FILES 
                                'add_drop' => TRUE,
                                                     // Whether to add DROP TABLE statements to backup file
                               'add_insert'=> TRUE,
                                                    // Whether to add INSERT data to backup file 
                               'newline' => "\n"
                                                   // Newline character used in backup file 
                              ); 
         // Backup your entire database and assign it to a variable 
         $backup = $this->dbutil->backup($prefs); 
         // Load the file helper and write the file to your server 
         // $this->load->helper('file');
         // $f='excelfiles/dbbackup_'.date('d_m_Y_H_i_s').'.zip'; 
		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		// force_download('mybackup.gz', $backup); 
		$fileName = 'credo_backup ' . date('Y-m-d @ h-i-s') .'.zip';
		force_download($fileName, $backup); 



     } 




	function backup_Database($hostName,$userName,$password,$DbName,$tables = '*')
	{
	  
	  // CONNECT TO THE DATABASE
	  $con = mysql_connect($hostName,$userName,$password) or die(mysql_error());
	  mysql_select_db($DbName,$con) or die(mysql_error());
	  
	  
	  // GET ALL TABLES
	  if($tables == '*')
	  {
	    $tables = array();
	    $result = mysql_query("SHOW TABLES");
	    while($row = mysql_fetch_row($result))
	    {
	      $tables[] = $row[0];
	    }
	  }
	  else
	  {
	    $tables = is_array($tables) ? $tables : explode(',',$tables);
	  }
	  
	  $return = 'SET FOREIGN_KEY_CHECKS=0;' . "\r\n";
	  $return.= 'SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";' . "\r\n";
	  $return.= 'SET AUTOCOMMIT=0;' . "\r\n";
	  $return.= 'START TRANSACTION;' . "\r\n";
	  
	  $data='';
	  foreach($tables as $table)
	  {
	    $result = mysql_query('SELECT * FROM '.$table) or die(mysql_error());
	    $num_fields = mysql_num_fields($result) or die(mysql_error());
	    
	    $data.= 'DROP TABLE IF EXISTS '.$table.';';
	    $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
	    $data.= "\n\n".$row2[1].";\n\n";
	    // if(substr($table, 0,1)!='v')
	    // {
	    for ($i = 0; $i<$num_fields; $i++) 
	    {
	      while($row = mysql_fetch_row($result))
	      {
	        $data.= 'INSERT INTO '.$table.' VALUES(';
	        for($x=0; $x<$num_fields; $x++) 
	        {
	          $row[$x] = addslashes($row[$x]);
			  $row[$x] = $this->clean($row[$x]); // CLEAN QUERIES
	          if (isset($row[$x])) { 
			  	$data.= '"'.$row[$x].'"' ; 
			  } else { 
			  	$data.= '""'; 
			  }
			  
	          if ($x<($num_fields-1)) { 
			  	$data.= ','; 
			  }
	        }  // end of the for loop 2
	        $data.= ");\n";
	      } // end of the while loop 
	    } // end of the for loop 1
		// }// end of if
		
	    $data.="\n\n\n";
	  }  // end of the foreach*/
	  
	    $return .= 'SET FOREIGN_KEY_CHECKS=1;' . "\r\n";
		$return.= 'COMMIT;';
	  


	  //SAVE THE BACKUP AS SQL FILE
		$this->fileName=$DbName.'-Database-Backup-'.date('Y-m-d @ h-i-s').'.sql';
	  $handle = fopen($this->fileName, 'w+');
	  // $handle = fopen($DbName.'-Database-Backup-'.date('Y-m-d @ h-i-s').'.sql','w+');
	  fwrite($handle,$data);
	  fclose($handle);
	   
	   if($data)
	   		return true;
	   else
			return false;
	 }  // end of the function
	 
	 
	//  CLEAN THE QUERIES
	function clean($str) {
		if(@isset($str)){
			$str = @trim($str);
			if(get_magic_quotes_gpc()) {
				$str = stripslashes($str);
			}
			return mysql_real_escape_string($str);
		}
		else{
			return 'NULL';
		}
	}
}

	
