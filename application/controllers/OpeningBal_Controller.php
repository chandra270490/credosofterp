<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OpeningBal_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Openingbal_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Opening Balance')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			// $data['products'] = $this->Openingbal_model->getProductList();
			$data['ProductCategories'] = $this->Openingbal_model->getDataForProductCategories();
			$data['totalStages'] = $this->Openingbal_model->totalStages();
			$data['stages'] = $this->Openingbal_model->getStages();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('OpeningBal_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function loadData()
	{
		$data['records'] = $this->Openingbal_model->loadData();
		echo json_encode($data);
	}

	// public function getData()
	// {
	// 	$data['ob'] = $this->Openingbal_model->getData();
	// 	echo json_encode($data);
	// }



	public function saveChanges()
	{
		$this->Openingbal_model->saveChanges();

	}


}
