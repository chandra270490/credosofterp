<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseOrderSaveAndPrint_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Purchaseordersaveandprint_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Purchase Order Save and Print') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Purchaseordersaveandprint_model->getPurchaseOrders();
			$data['recordsSaved'] = $this->Purchaseordersaveandprint_model->getPurchaseOrdersSaved();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('PurchaseOrderSaveAndPrint_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getProducts()
	{
		$data['products'] = $this->Purchaseordersaveandprint_model->getProducts();
		echo json_encode($data);
	}

	public function insert()
	{
		$this->Purchaseordersaveandprint_model->insert();
		$this->printToPdf(' ');
	}
	public function reprint()
	{
		// $this->Purchaseordersaveandprint_model->insert();
		$this->printToPdf(' ');
	}

	public function reject()
	{
		$this->Purchaseordersaveandprint_model->reject();
		echo json_encode('$data');
	}

	public function delete()
	{
		$res = $this->Purchaseordersaveandprint_model->checkDependency();
		if( $res == 1)
		{
			echo json_encode("cannot");
		}
		else
		{
			$this->Purchaseordersaveandprint_model->delete();
			// $data['records'] = $this->Purchaseordersaveandprint_model->getDataLimit();
			echo json_encode("data");
		}
	}

	public function printToPdf($arg)
	{
		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Invoice');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg();
		$orgImagePath = $data['org'][0]['imagePath'];
		// $pdf->Write(5, base_url());
		// $pdf->Write(15, $_SERVER['DOCUMENT_ROOT']);
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['tin'] . ', GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Line(0, 73, 210, 73);


		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;

		$data['partyInfo'] = $this->Purchaseordersaveandprint_model->partyInfo($myTableData[0]['partyRowId']);
		
		// $addr = $data['partyInfo'][0]['addr'];
		// $html='<p>'.$addr.'</p>';
		$PoVno = $myTableData[0]['vNo'];
		$PoDt = $myTableData[0]['dt'];
		$partyName = $data['partyInfo'][0]['name'];
		$yr  = date("Y"); 
		$html='<table border="0">
					<tr>
						<td colspan="3" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Purchase Order</td>
					</tr>
					<tr>
						<td colspan="3" align="center" style="height:7mm;"></td>
					</tr>
					<tr>
						<td style="width:95mm;height:7mm;">To,</td>
						<td align="left" style="width:45mm;"></td>
						<td align="right" style="width:48mm;">PO No.: '.$data['org'][0]['shortName'] .'/'. $yr . '/'. $myTableData[0]['vNo'].'</td>
					</tr>
					<tr>
						<td style="height:6mm;font-size: 12pt; font-weight:bold;">'.$data['partyInfo'][0]['name'].'</td>
						<td align="left" style="height:6mm;"></td>
						<td align="right" style="height:6mm;">PO Date: '.$myTableData[0]['dt'].'</td>
					</tr>
					<tr>
						<td style="font-size: 10pt;">'.$data['partyInfo'][0]['addr'].'</td>
						<td style="font-size: 10pt;"></td>
					</tr>
					<tr>
						<td style="height:10mm;font-size: 10pt;">GSTIN: '.$data['partyInfo'][0]['gstIn'].'</td>
						<td style="font-size: 10pt;"></td>
					</tr>

			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$myTableDataProducts = $this->input->post('TableDataProducts');
        $myTableDataProducts = stripcslashes($myTableDataProducts);
        $myTableDataProducts = json_decode($myTableDataProducts,TRUE);
        $myTableRowsProducts = count($myTableDataProducts);
		$r = $myTableRowsProducts;
		$productRows = "";
		for($k=0; $k < $r; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr>";
				$productRows .= "<td style=\"font-size:8pt;\">". $sn ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\">". $myTableDataProducts[$k]['productName'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"right\">". $myTableDataProducts[$k]['rate'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"right\">". $myTableDataProducts[$k]['qty'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"right\">". $myTableDataProducts[$k]['amt'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"right\">". $myTableDataProducts[$k]['discount'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"right\">". $myTableDataProducts[$k]['amtAfterDiscount'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"left\">". $myTableDataProducts[$k]['colour'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"left\">". $myTableDataProducts[$k]['deliveryDt'] ."</td>";
				$productRows .= "<td style=\"font-size:8pt;\" align=\"left\">". $myTableDataProducts[$k]['remarks'] ."</td>";
			$productRows .= "</tr>";
		}
		$html='<table border="1" cellpadding="2">
					<tr>
						<th style="font-size:8pt;width:8mm; color:white; background-color:black;">SN</th>
						<th style="width:35mm; color:white; background-color:black;">Product</th>
						<th style="font-size:8pt;width:17mm; text-align:center; color:white; background-color:black;">Rate</th>
						<th style="font-size:8pt;width:12mm; text-align:center; color:white; background-color:black;">Qty</th>
						<th style="font-size:8pt;width:17mm; text-align:center; color:white; background-color:black;">Amt.</th>
						<th style="font-size:8pt;width:15mm; text-align:center; color:white; background-color:black;">Dis.</th>
						<th style="font-size:8pt;width:17mm; text-align:center; color:white; background-color:black;">Amt.</th>
						<th style="font-size:8pt;width:17mm; text-align:center; color:white; background-color:black;">Colour</th>
						<th style="font-size:8pt;width:20mm; text-align:center; color:white; background-color:black;">D.Dt.</th>
						<th style="font-size:8pt;width:30mm; text-align:center; color:white; background-color:black;">Rem.</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$html='<table border="0">
					<tr>
						<td style="width:65mm;height:7mm;">Total Amt.: '.$myTableData[0]['totalAmt'].'</td>
						<td style="width:65mm;">Total Qty.: '.$myTableData[0]['totalQty'].'</td>
						<td style="width:65mm;">Odr. Validity: '.$myTableData[0]['orderValidity'].'</td>
					</tr>
					<tr>
						<td style="width:65mm;height:7mm;">Payment Terms: '.$myTableData[0]['paymentTerms'].'</td>
						<td style="width:65mm;">Freight: '.$myTableData[0]['freight'].'</td>
						<td style="width:65mm;">Inspection: '.$myTableData[0]['inspection'].'</td>
					</tr>
					<tr>
						<td style="width:65mm;height:7mm;">Delay Clause: '.$myTableData[0]['delayClause'].'</td>
						<td style="width:65mm;">Mode of Despatch: '.$myTableData[0]['modeOfDespatch'].'</td>
						<td style="width:65mm;">Guarantee: '.$myTableData[0]['guarantee'].'</td>
					</tr>
					<tr>
						<td style="width:65mm;height:7mm;">Insurance: '.$myTableData[0]['insurance'].'</td>
						<td colspan="2" style="width:65mm;">Special Note: '.$myTableData[0]['specialNote'].'</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');



		$html='<table border="0">
					<tr>
						<td align="right" style="width:180mm;height:15mm;font-size:11pt;"></td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:15mm;font-size:11pt;">For '.$data['org'][0]['orgName'].'</td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:10mm;font-size:10pt;">Partner/Auth. Signatory</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		// $pdf->Output(FCPATH . '/downloads/PO_'. $dt . ' (' . $tm . ').pdf', 'F');
		$fileName = "".$PoVno."_".$dt."[".$tm."]_".$partyName;
		$pdf->Output(FCPATH . '/downloads/'. $fileName.'.pdf', 'F');
		echo base_url()."/downloads/". $fileName.".pdf";
	}
}
