<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Employees_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Employees')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Employees_model->getDataLimit();
			// $this->load->model('Addressbook_model');
			$data['abList'] = $this->Util_model->getAbList();
			$this->load->model('Departmenttypes_model');
			$data['departments'] = $this->Departmenttypes_model->getDepartmentList();
			$this->load->model('Designationtypes_model');
			$data['designations'] = $this->Designationtypes_model->getDesignationList();
			$data['errorfound'] = "";
			$data['users'] = $this->Employees_model->getUsers();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('Employees_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function insert()
	{		
		$rowId = $this->Employees_model->getRowId(); 
        $rowId = sprintf("%04d", $rowId); 
		$cnt = count(array_filter( $_FILES['images']['name'] ));
		
		for($i=0; $i<$cnt; $i++)
		{
			// $ext = substr($_FILES["images"]["name"][$i],-3,3);
			$ext = pathinfo($_FILES["images"]["name"][$i])['extension'];
			$target =  'bootstrap/images/empdocs/' . $rowId . '_'.$i. '.' . $ext;
	        $tmp=$_FILES['images']['tmp_name'][$i];
	        move_uploaded_file($tmp,$target);
		}
		if($this->Employees_model->checkDuplicate() == 1)
        {
        	echo "Duplicate...";
        }
        else if($this->Employees_model->checkDuplicateUser() == 1)
        {
        	echo "Duplicate User...";
        }
        else
        {
			$this->Employees_model->insert();
			$data['records'] = $this->Employees_model->getDataLimit();
			echo "Done...";
        }
	}

	public function getEmpDocs()
	{
		$data['empDocs'] = $this->Employees_model->getEmpDocs();
		echo json_encode($data);
	}

	public function update()
	{
		// echo $_REQUEST['txtPhotoChange'];
		if($_REQUEST['txtPhotoChange'] == "yes")
        {
        	/////Delete Existing
        	$data['empDocs'] = $this->Employees_model->getEmpDocs4Deletion($_REQUEST['txtHiddenRowId']);
        	if( count($data['empDocs']) > 0)
        	{
	        	for($i=0; $i<count($data['empDocs']); $i++)
	        	{
	        		$fn = $data['empDocs'][$i]['docFileName'];
	        		$tmp = FCPATH ."/bootstrap/images/empdocs/".$fn;
	        		unlink($tmp); //to delete image file
	        	}
        	}
        	// echo $fn;

        	////Uploading New
        	$rowId = $_REQUEST['txtHiddenRowId'];
	        $rowId = sprintf("%04d", $rowId); 
			$cnt = count(array_filter( $_FILES['images']['name'] ));
			// echo $_REQUEST['txtPhotoChange'];
			for($i=0; $i<$cnt; $i++)
			{
				// $ext = substr($_FILES["images"]["name"][$i],-3,3);
			$ext = pathinfo($_FILES["images"]["name"][$i])['extension'];
				
				$target =  'bootstrap/images/empdocs/' . $rowId . '_'.$i. '.' . $ext;
		        $tmp=$_FILES['images']['tmp_name'][$i];
		        move_uploaded_file($tmp,$target);
			}
        }
		if($this->Employees_model->checkDuplicateOnUpdate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else if($this->Employees_model->checkDuplicateOnUpdateUser() == 1)
        {
        	echo "Duplicate User...";
        }
        else
        {
			$this->Employees_model->update();
			// $data['records'] = $this->Employees_model->getDataLimit();
			echo "Done...";
        }
	}

	public function delete()
	{
		$this->Employees_model->delete();
		$data['records'] = $this->Employees_model->getDataLimit();
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Employees_model->getDataAll();
		echo json_encode($data);
	}
	public function loadRecords()
	{
		$data['records'] = $this->Employees_model->getDataLimit();
		echo json_encode($data);
	}
}
