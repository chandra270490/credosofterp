<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RptRoLevel_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Products_model');
            $this->load->model('Rptrolevel_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'RO Level Report')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			// $data['records'] = $this->Products_model->getDataForReport();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('RptRoLevel_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Rptrolevel_model->getDataForReport();
		$data['orderedProducts'] = $this->Rptrolevel_model->orderedProducts();
		echo json_encode($data);
	}

	public function exportData()
	{
		$this->printToPdf('Save');
	}

	public function printToPdf($arg)
	{
		$this->load->library('Pdf');
		$pdf = new Pdf('Landscape', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('RO Level Report');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);
		$orgImagePath = $data['org'][0]['imagePath'];
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		// Line ($x1, $y1, $x2, $y2, $style=array())
		$pdf->Line(0, 73, 210, 73);


		$html='<table border="0">
					<tr>
						<td colspan="2" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">RO Level Report</td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="font-size: 12pt; font-weight:normal;">Category: '. $this->input->post("productCategory") .'</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;


		// // $pdf->writeHTML(100,"dd");


		$productRows = "";
		for($k=0; $k < $r-1; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr nobr=\"true\">";
				$productRows .= "<td>". $sn ."</td>";
				$productRows .= "<td>". $myTableData[$k]['productName'] . "</td>";
				$productRows .= "<td>". $myTableData[$k]['inHandQty'] ."</td>";
				$productRows .= "<td>". $myTableData[$k]['roLevel'] ."</td>";
			$productRows .= "</tr>";
		}
		$html='<table border="1" cellpadding="10">
					<tr>
						<th style="width:20mm; color:white; background-color:black;">S.N.</th>
						<th style="width:100mm; color:white; background-color:black;">Product Name</th>
						<th style="width:30mm; color:white; background-color:black;">Qty In Hand</th>
						<th style="width:30mm; text-align:center; color:white; background-color:black;">RO Level</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');



		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$pdf->Output(FCPATH . '/downloads/ROL_'. $dt . ' (' . $tm . ').pdf', 'F');
		echo base_url()."/downloads/ROL_". $dt . " (" . $tm . ").pdf";
	}
}
