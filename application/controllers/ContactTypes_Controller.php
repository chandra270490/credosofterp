<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactTypes_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Contacttypes_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Contact Types')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Contacttypes_model->getDataLimit();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('ContactTypes_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function insert()
	{
		if($this->Contacttypes_model->checkDuplicate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Contacttypes_model->insert();
			$data['records'] = $this->Contacttypes_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function update()
	{
		if($this->Contacttypes_model->checkDuplicateOnUpdate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->Contacttypes_model->update();
			$data['records'] = $this->Contacttypes_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function delete()
	{
		if($this->Util_model->isDependent('addressbookcontacttypes', 'contactTypeRowId', $this->input->post('rowId')) == 1)
        {
        	$data['dependent'] = "yes";
        	echo json_encode($data);
        }
        else
        {
			$this->Contacttypes_model->delete();
			$data['records'] = $this->Contacttypes_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Contacttypes_model->getDataAll();
		echo json_encode($data);
	}

}
