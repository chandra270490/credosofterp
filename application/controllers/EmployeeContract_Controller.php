<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeContract_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Employeecontract_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Employee Contract Detail')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			// $data['records'] = $this->Util_model->getDataLimitQpo('Q');
			$data['employees'] = $this->Employeecontract_model->getEmpList();
			// $this->load->model('Productcategories_model');
			// $data['productCategories'] = $this->Productcategories_model->getProductCategories();
			// $this->load->model('Placements_model');
			// $data['placements'] = $this->Placements_model->getPlacementList();
			// $this->load->model('Colours_model');
			// $data['colours'] = $this->Colours_model->getColourList();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$this->load->view('EmployeeContract_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getData()
	{
		$data['products'] = $this->Employeecontract_model->getData();
		echo json_encode($data);
	}



	public function saveChanges()
	{
		$this->Employeecontract_model->saveChanges();

	}


}
