<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MoveInGodownApprove_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Moveingodownapprove_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Move in Godown Approve') == 0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Moveingodownapprove_model->getPending();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('MoveInGodownApprove_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	// public function getProducts()
	// {
	// 	$data['products'] = $this->Moveingodownapprove_model->getProducts();
	// 	echo json_encode($data);
	// }

	public function insert()
	{
		$this->Moveingodownapprove_model->insert();
		echo json_encode('$data');
	}

	public function reject()
	{
		$this->Moveingodownapprove_model->reject();
		echo json_encode('$data');
	}
}
