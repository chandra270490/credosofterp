<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rptsessionlog_controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Rptsessionlog_model');
            $this->load->helper('url');
    }
	public function index()
	{
        if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
        {
            if($this->Util_model->getRight($this->session->userRowId,'Session Log')==0)
            {
                // $this->load->view('includes/header4all');
                // $this->load->view('includes/menu');
                // $this->load->view('ErrorUnauthenticateUser_view');
                // $this->load->view('includes/footer');               
                return;
            }
            $this->load->view('includes/header4all');           // with Jumbotron
            $MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);  
			$data['records'] = $this->Rptsessionlog_model->getData();
        	$this->load->view('Rptsessionlog_view');
        	$this->load->view('RptsessionlogTable_view', $data);
	        $this->load->view('includes/footer');
        }
        else
        {

            $this->load->view('includes/header');           // with Jumbotron
            $this->load->model('Login_model');
            $data['org'] = $this->Login_model->getOrgList();
            $this->load->view('login_view', $data);
            $this->load->view('includes/footer');
        }
            
	}    
	public function searchData()
	{
		$data['records'] = $this->Rptsessionlog_model->getData();
		$this->load->view('RptsessionlogTable_view', $data);
	    $this->load->view('includes/footer');
	}  
}
