<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalaryExecutives_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Salaryexecutives_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Calculate Salary (Executives)')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			// $data['parties'] = $this->Salaryexecutives_model->getPartyList();
			$this->load->view('SalaryExecutives_view');
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		if($this->Salaryexecutives_model->checkDuplicate() == 1)
        {
        	
        	$data = "Already Saved...";
        	echo json_encode($data);
        }
        else if($this->Salaryexecutives_model->PooreMahineKiAttendanceMarkHoGayi() == 1)
        {
        	
        	$data = "Attendance not marked for whole month...";
        	echo json_encode($data);
        }
        else
        {
			$data['records'] = $this->Salaryexecutives_model->getDataForReport();
			echo json_encode($data);
		}
	}

	public function saveData()
	{
		$this->Salaryexecutives_model->saveData();
		$this->printToPdf();
	}

	public function printToPdf()
	{
		$this->load->library('Pdf');
		$pdf = new Pdf('Landscape', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Payslip');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(15);
		$pdf->setFooterMargin(12);
		$pdf->setPrintFooter(false);
		// $pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);
		


		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;

		$dt = date("d-m-Y");
		for($k=0; $k < $r; $k++)
		{
			$pdf->AddPage();
			$sn=$k+1;
			$salaryRows = "";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td align=\"center\" colspan=\"4\" style=\"font-size:14pt; font-weight:bold;\">" . $data['org'][0]['orgName'] . "</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td align=\"center\" colspan=\"4\" style=\"font-size:10pt; font-weight:normal; border-bottom:1px dotted grey;\">Payslip (From ". $this->input->post('dtFrom') . " to " . $this->input->post('dtTo') .")</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td style=\"font-weight:bold;\">Employee Name:</td>";
				$salaryRows .= "<td style=\"font-weight:bold;\">". $myTableData[$k]['empName'] ."</td>";
				$salaryRows .= "<td>Employee Code:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['empRowId'] ."</td>";
				// $salaryRows .= "<td align=\"right\">". $myTableData[$k]['rate'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Presents:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['presents'] ."</td>";
				$salaryRows .= "<td>Holidays:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['holidays'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Tours:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['tours'] ."</td>";
				$salaryRows .= "<td>Leaves Taken:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['leavesTaken'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Absents:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['absents'] ."</td>";
				$salaryRows .= "<td>Count Days:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['countDays'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Salary Per Month:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['salPerMonth'] ."</td>";
				$salaryRows .= "<td>H.R.A.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['hra'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Travelling Allow.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['ta'] ."</td>";
				$salaryRows .= "<td>C.A.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['ca'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Medical Allow.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['ma'] ."</td>";
				$salaryRows .= "<td>Salary Calculated:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['salCalculated'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Gross:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['gross'] ."</td>";
				$salaryRows .= "<td>P.F.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['pf'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>E.S.I.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['esi'] ."</td>";
				$salaryRows .= "<td>T.D.S.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['tds'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Net Salary:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['net'] ."</td>";
				$salaryRows .= "<td></td>";
				$salaryRows .= "<td></td>";
			$salaryRows .= "</tr>";


			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td colspan=\"2\" ></td>";
				$salaryRows .= "<td colspan=\"2\" ></td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td colspan=\"2\" >Signature of Employer</td>";
				$salaryRows .= "<td colspan=\"2\" >Signature of Employee</td>";
			$salaryRows .= "</tr>";

			$html='<table border="0" cellpadding="5">'.$salaryRows.'</table>';
			$pdf->writeHTML($html, true, false, true, false, '');

			////////////////// 2nd copy
			$pdf->writeHTML("<p></p>", true, false, true, false, '');
			$pdf->Line(75, 140, 130, 140);

			$sn=$k+1;
			$salaryRows = "";
			
			$salaryRows .= "<tr nobr=\"true\">";
			$salaryRows .= "<td align=\"center\" colspan=\"4\" style=\"font-size:14pt; font-weight:bold;\">" . $data['org'][0]['orgName'] . "</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td align=\"center\" colspan=\"4\" style=\"font-size:10pt; font-weight:normal; \">Payslip (From ". $this->input->post('dtFrom') . " to " . $this->input->post('dtTo') .")</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td align=\"center\" colspan=\"4\" style=\"font-size:10pt; font-weight:normal; border-bottom:1px dotted grey;\">(Office Copy)</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td style=\"font-weight:bold;\">Employee Name:</td>";
				$salaryRows .= "<td style=\"font-weight:bold;\">". $myTableData[$k]['empName'] ."</td>";
				$salaryRows .= "<td>Employee Code:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['empRowId'] ."</td>";
				// $salaryRows .= "<td align=\"right\">". $myTableData[$k]['rate'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Presents:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['presents'] ."</td>";
				$salaryRows .= "<td>Holidays:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['holidays'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Tours:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['tours'] ."</td>";
				$salaryRows .= "<td>Leaves Taken:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['leavesTaken'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Absents:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['absents'] ."</td>";
				$salaryRows .= "<td>Count Days:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['countDays'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Salary Per Month:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['salPerMonth'] ."</td>";
				$salaryRows .= "<td>H.R.A.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['hra'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Travelling Allow.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['ta'] ."</td>";
				$salaryRows .= "<td>C.A.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['ca'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Medical Allow.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['ma'] ."</td>";
				$salaryRows .= "<td>Salary Calculated:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['salCalculated'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Gross:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['gross'] ."</td>";
				$salaryRows .= "<td>P.F.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['pf'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>E.S.I.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['esi'] ."</td>";
				$salaryRows .= "<td>T.D.S.:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['tds'] ."</td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td>Net Salary:</td>";
				$salaryRows .= "<td>". $myTableData[$k]['net'] ."</td>";
				$salaryRows .= "<td></td>";
				$salaryRows .= "<td></td>";
			$salaryRows .= "</tr>";


			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td colspan=\"2\" ></td>";
				$salaryRows .= "<td colspan=\"2\" ></td>";
			$salaryRows .= "</tr>";

			$salaryRows .= "<tr nobr=\"true\">";
				$salaryRows .= "<td colspan=\"2\" >Signature of Employer</td>";
				$salaryRows .= "<td colspan=\"2\" >Signature of Employee</td>";
			$salaryRows .= "</tr>";

			$html='<table border="0" cellpadding="5">'.$salaryRows.'</table>';
			$pdf->writeHTML($html, true, false, true, false, '');
		}


		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$pdf->Output(FCPATH . '/downloads/PS_'. $dt . ' (' . $tm . ').pdf', 'F');
		echo base_url()."/downloads/PS_". $dt . " (" . $tm . ").pdf";
	}




	public function exportData()
	{
		$this->printToExcel();
	}

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:H3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Calculated Salary');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:H4');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));



		$objPHPExcel->getActiveSheet()->getStyle('A6:AF6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$cellRange1 = "A" . (6) . ":" . "AF" . (6);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		$noOfDays = $this->input->post('noOfDays');
		$i = 6;
		for($k=0; $k < $r; $k++)
		{
			for($c=0; $c<count($myTableData[$k]); $c++)
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $i, $myTableData[$k][$c]);
			}
			// $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
		 	$i++;
		}
		$r=$i-1;

		$cellRange1 = "A" . ($i) . ":" . "AF" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	// $objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (6) . ":" . "AF" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(11);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);	
		for ($ii = 'D'; $ii != $objPHPExcel->getActiveSheet()->getHighestColumn(); $ii++) 
		{
		    $objPHPExcel->getActiveSheet()->getColumnDimension($ii)->setWidth(11);
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(0);	

	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// $objWriter->save("excelfiles/$acname$branch.xls");
		$objWriter->save("excelfiles/tmp.xls");
		echo base_url()."excelfiles/tmp.xls";

	}
}
