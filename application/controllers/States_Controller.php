<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class States_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('States_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'States')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->States_model->getDataLimit();
			$this->load->model('Countries_model');
			$data['countries'] = $this->Countries_model->getCountryList();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('States_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function insert()
	{
		if($this->States_model->checkDuplicate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->States_model->insert();
			$data['records'] = $this->States_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function update()
	{
		if($this->States_model->checkDuplicateOnUpdate() == 1)
        {
        	$data = "Duplicate record...";
        	echo json_encode($data);
        }
        else
        {
			$this->States_model->update();
			$data['records'] = $this->States_model->getDataLimit();
			echo json_encode($data);
        }
	}

	public function delete()
	{
		if($this->Util_model->isDependent('districts', 'stateRowId', $this->input->post('rowId')) == 1)
        {
        	$data['dependent'] = "yes";
        	echo json_encode($data);
        }
        else
        {
			$this->States_model->delete();
			$data['records'] = $this->States_model->getDataLimit();
			echo json_encode($data);
		}
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->States_model->getDataAll();
		echo json_encode($data);
	}

}
