<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductStatusAtDesp_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Productstatusatdesp_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Product Status (Desp. Stage)')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['products'] = $this->Productstatusatdesp_model->getProducts4CheckBox();
			$data['godowns4table'] = $this->Productstatusatdesp_model->getGodowns4table();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$data['ff'] = 'gg';
			$this->load->view('ProductStatusAtDesp_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getProductList()
	{
		$data['products'] = $this->Productstatusatdesp_model->getProductList();
		echo json_encode($data);
	}
	
	public function getData()
	{
		$data['records'] = $this->Productstatusatdesp_model->getData();
		echo json_encode($data);
	}

	
	public function exportDataExcel()
	{
		$this->printToExcel();
	}
	
	

	public function printToExcel()
	{

		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/Q.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/Q.xls');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['org'][0]['orgName']);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(16)->getColor()->setRGB('0000FF');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4']);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(false)->setSize(10)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:H3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Product Status (Desp. Stage)');
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('000000');;

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:H4');
		// $objPHPExcel->getActiveSheet()->setCellValue('A4', "From " . $this->input->post('dtFrom') . " To " . $this->input->post('dtTo'));


		// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:E6');

		

		// $objPHPExcel->getActiveSheet()->getStyle('F7:E7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// $cellRange1 = "A" . (8) . ":" . "E" . (8);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		// $myTableData = $this->input->post('TableData');
  //       $myTableData = stripcslashes($myTableData);
  //       $myTableData = json_decode($myTableData,TRUE);
  //       $myTableRows = count($myTableData);
		// $r = $myTableRows;
		// $productRows = "";
		// $i = 8;
		// $sn=0;
		// for($k=0; $k < $r; $k++)
		// {
		// 	// if($k == 1)
		// 	// {

		// 	// }
		// 	// else
		// 	{
		// 	    $myCol = 0;
		// 	    // $sn = $k + 1;
		// 	    if($sn==0)
		// 	    	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['sn']);
		// 		else
		// 			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $sn);
			 	
		// 	    $myCol = $myCol + 1;
		// 	 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['productName']);
		// 	    $myCol = $myCol + 1;
		// 	 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['qtyAtDesp']);
		// 	    $myCol = $myCol + 1;
		// 	 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['orders']);
		// 	    $myCol = $myCol + 1;
		// 	 	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($myCol, $i, $myTableData[$k]['diff']);

		// 	 	$sn++;
		// 	 	$i++;
		// 	 }
		// }
		////////// table heading
		$myTableData = $this->input->post('TableDataHeader');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);

        $myTableRows = count($myTableData);
		$r = $myTableRows;
		// $noOfDays = $this->input->post('noOfDays');
		$i = 6;
		for($k=0; $k < $r; $k++)
		{
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($k, $i, $myTableData[$k]);
		}

		//////////END - Table Heading

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;
		// $noOfDays = $this->input->post('noOfDays');
		$i = 7;
		for($k=1; $k < $r; $k++)
		{
			for($c=0; $c<count($myTableData[$k]); $c++)
			{
				$newValue = htmlspecialchars($myTableData[$k][$c]);
				$newValue = str_replace("&amp;", "&", $newValue);
				// str_replace("world","Peter","Hello world!");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $i, $newValue ); //htmlspecialchars
			}
			// $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
		 	$i++;
		}
		$r=$i-1;
		// $r=$i-1;
		// $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'Total');
		// $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '=SUM(D8:D'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '=SUM(E8:E'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F8:F'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '=SUM(G8:G'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H8:H'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '=SUM(I8:I'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J8:J'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K8:K'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=SUM(L8:L'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '=SUM(M8:M'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '=SUM(N8:N'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '=SUM(O8:O'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '=SUM(P8:P'.$r.')');
		// $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=SUM(Q8:Q'.$r.')');

		// $cellRange1 = "A" . ($i) . ":" . "E" . ($i);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 // 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$cellRange2 = "A" . (7) . ":" . "H" . ($i);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange2)->getFont()->setSize(10);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(0);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);	
		


	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// $objWriter->save('php://output');	///to download without ajax call like hyperlink
		// $objWriter->save("excelfiles/$acname$branch.xls");
		$objWriter->save("excelfiles/Q.xls");
		echo base_url()."excelfiles/Q.xls";
	}
}
