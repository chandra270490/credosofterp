<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalesReturn_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Salesreturn_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Sales Return')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['records'] = $this->Salesreturn_model->getDataLimit();
			$data['orderTypes'] = $this->Salesreturn_model->getOrderTypeList();
			$data['parties'] = $this->Util_model->getPartyList();
			// $data['references'] = $this->Salesreturn_model->getReferenceList();
			// $data['orderTypes'] = $this->Salesreturn_model->getOrderTypeList();
			$this->load->model('Productcategories_model');
			$data['productCategories'] = $this->Productcategories_model->getProductCategories();
			$this->load->model('Placements_model');
			$data['placements'] = $this->Placements_model->getPlacementList();
			$this->load->model('Colours_model');
			$data['colours'] = $this->Colours_model->getColourList();
			$data['errorfound'] = "";
			$data['stages'] = $this->Salesreturn_model->getStages();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('SalesReturn_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getProductList()
	{
		$data['products'] = $this->Salesreturn_model->getProductList();
		echo json_encode($data);
	}

	public function getColours()
	{
		$this->load->model('Colours_model');
		$data['colours'] = $this->Colours_model->getColourListRefresh();
		echo json_encode($data);
	}

	public function getReturnDetial()
	{
		$data['srDetail'] = $this->Salesreturn_model->getReturnDetial();
		echo json_encode($data);
	}
	
	public function insert()
	{
		$this->Salesreturn_model->insert();
		$this->printToPdf('Save');
	}

	public function update()
	{
		$this->Salesreturn_model->update();
		$this->printToPdf('Update');
	}

	public function reprint()
	{
		// $this->Purchaseordersaveandprint_model->insert();
		$this->printToPdfReprint(' ');
	}

	public function printToPdf($arg)
	{
		$rowId = -10;
		if($arg == "Update")
		{
			$rowId = $this->input->post('globalrowid');
		}
		else if($arg == "Save")
		{
			$rowId = $this->Salesreturn_model->getInvNo();
		}
		$data['ci'] = $this->Salesreturn_model->getci($rowId);

		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Sales Return');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 
		$data['org'] = $this->Util_model->getOrg();
		$orgImagePath = $data['org'][0]['imagePath'];
		// $pdf->Write(5, 'CodeIgniter TCPDF Integration');
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/logolt.jpg" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		// Line ($x1, $y1, $x2, $y2, $style=array())
		$pdf->Line(0, 80, 210, 80);

		$addr = ($this->input->post('addr'));
		$textlines = explode("\n", $addr);
		$cnt = count($textlines);
		$addrNew = "";
		for($c=0; $c<$cnt; $c++)
		{
			$addrNew .= $textlines[$c] . "<br />";
		}

		$html='<table border="0">
					<tr>
						<td colspan="2" align="center" style="height:6mm; "></td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Sales Return</td>
					</tr>
					<tr>
						<td style="width:95mm;height:10mm;">Date: '.$this->input->post('vDt').'</td>
						<td align="right" style="width:90mm;">SR.No.: '.$data['ci'][0]['vNo'].'</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;">To,</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;font-size: 12pt; font-weight:bold;">'.$data['ci'][0]['prefixType']. ' ' .$data['ci'][0]['name'].'</td>
					</tr>
					<tr>
						<td colspan="2" style="height:10mm;font-size: 10pt;">'.$addrNew.'</td>
					</tr>
					<tr>
						<td style="height:6mm;font-size: 10pt;"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;font-size: 10pt;"></td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
		$r = $myTableRows;


		$productRows = "";
		for($k=0; $k < $r; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr>";
				$productRows .= "<td>". $sn ."</td>";
				$productRows .= "<td>". $myTableData[$k]['productName'] ."</td>";
				// $productRows .= "<td>". $myTableData[$k]['placement'] ."</td>";
				$productRows .= "<td>". $myTableData[$k]['colour'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['qty'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['rate'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableData[$k]['amt'] ."</td>";
				$productRows .= "<td>". $myTableData[$k]['remarks'] ."</td>";
			$productRows .= "</tr>";
		}
		$html='<table border="1" cellpadding="5">
					<tr>
						<th style="width:12mm; color:white; background-color:black;">S.N.</th>
						<th style="width:35mm; color:white; background-color:black;">Product</th>
						<th style="width:25mm; color:white; background-color:black;">Colour</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Qty</th>
						<th style="width:20mm; text-align:center; color:white; background-color:black;">Rate</th>
						<th style="width:30mm; text-align:center; color:white; background-color:black;">Amt.</th>
						<th style="width:50mm; color:white; background-color:black;">Remarks</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$col1="";
		$col2="";
		$col3="";
		$col4="";
		$col5="";		
		$col3 .= 'Total Amt.:' . '<br>';	
		// $col4 .=  '<br>';	
		$col5 .= $this->input->post('totalAmt') . '<br>';	
		if($this->input->post('discountAmt') == 0) 
		{
			$printDiscountTitle="";
			$printDiscountPer="";
			$printDiscountAmt="";
		}
		else
		{
			$printDiscountTitle = "Discount Amt.:";
			$printDiscountPer = $this->input->post('discountAmt').' ('. $this->input->post('discountPer')  .'%)' ;
			$printDiscountAmt = $this->input->post('totalAfterDiscount');
			$col3 .= $printDiscountTitle . ' [' . $printDiscountPer . ']<br>';
			// $col4 .= $printDiscountPer . '<br>';
			$col5 .= $printDiscountAmt . '<br>';
		}

		if($this->input->post('vatPer') == 0) 
		{
			$printVatTitle="";
			$printVatPer="";
			$printVatAmt="";
		}
		else
		{
			$printVatTitle = "VAT:";
			$printVatPer = $this->input->post('vatPer').'%';
			$printVatAmt = $this->input->post('vatAmt');
			$col3 .= $printVatTitle . ' (' . $printVatPer . ')<br>';
			// $col4 .= $printVatPer . '<br>';
			$col5 .= $printVatAmt . '<br>';			
		}

		if($this->input->post('sgstPer') == 0) 
		{
			$printSgstTitle="";
			$printSgstPer="";
			$printSgstAmt="";
		}
		else
		{
			$printSgstTitle = "SGST:";
			$printSgstPer = $this->input->post('sgstPer').'%';
			$printSgstAmt = $this->input->post('sgstAmt');
			$col3 .= $printSgstTitle . ' (' . $printSgstPer . ')<br>';
			$col5 .= $printSgstAmt . '<br>';			
		}
		if($this->input->post('cgstPer') == 0) 
		{
			$printCgstTitle="";
			$printCgstPer="";
			$printCgstAmt="";
		}
		else
		{
			$printCgstTitle = "CGST:";
			$printCgstPer = $this->input->post('cgstPer').'%';
			$printCgstAmt = $this->input->post('cgstAmt');
			$col3 .= $printCgstTitle . ' (' . $printCgstPer . ')<br>';
			$col5 .= $printCgstAmt . '<br>';			
		}
		if($this->input->post('igstPer') == 0) 
		{
			$printIgstTitle="";
			$printIgstPer="";
			$printIgstAmt="";
		}
		else
		{
			$printIgstTitle = "IGST:";
			$printIgstPer = $this->input->post('igstPer').'%';
			$printIgstAmt = $this->input->post('igstAmt');
			$col3 .= $printIgstTitle . ' (' . $printIgstPer . ')<br>';
			$col5 .= $printIgstAmt . '<br>';			
		}
		$col3 .= 'Net Amt.:';
		$col5 .= $this->input->post('net');	

		$html='<table border="0" cellpadding="5">
					<tr>
							<td  align="left" style="width:50mm;height:7mm;font-size: 10pt;">' . $col1 . '</td>
							<td  align="left" style="width:30mm;height:7mm;font-size: 10pt;">' . $col2 .'</td>
							<td align="right" style="width:75mm;height:7mm;font-size: 10pt;">' . $col3 . '</td>
							<td align="right" style="width:33mm;height:7mm;font-size: 10pt;">' . $col5 .'</td>
					</tr>

					<tr>
						<td align="right" colspan="4" style="height:7mm;font-size: 10pt;">'. $this->input->post('inWords') .'</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$html='<table border="0">
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">1. All prices are for delivery F.O.R./Ajmer/Destination.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">2. VAT and Other duties will be extra.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">3. if the purchaser fails to take the delivery of the goods from the transport he shall be liable to bear all the expenses for damage of goods etc.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">4. ____50___% payment will be received by sending the dispatch documents through Bank and balance at the time of delivery.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">5. All disputes are subject to Ajmer Jurisdiction only.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">6. In case payments are not made within the stipulated period interest at 18% p.a. will be charged.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">7. Prices are based on present market rates of raw material and subject to </td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:10mm;font-size:10pt;">For ' . $data['org'][0]['orgName'] . '</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$partyName = $data['ci'][0]['name'];
		$pdf->Output(FCPATH . '/downloads/SR_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		echo base_url()."downloads/SR_". $dt . " (" . $tm . ") " . $partyName . ".pdf";
	}

	

	public function delete()
	{
		$this->Salesreturn_model->delete();
		$data['records'] = $this->Salesreturn_model->getDataLimit();
		echo json_encode($data);
	}

	public function loadAllRecords()
	{
		$data['records'] = $this->Salesreturn_model->getDataAll();
		echo json_encode($data);
	}
	public function loadLimitedRecords()
	{
		$data['records'] = $this->Salesreturn_model->getDataLimit();
		echo json_encode($data);
	}

	public function getProducts()
	{
		$data['products'] = $this->Salesreturn_model->getProducts();
		echo json_encode($data);
	}

	public function printToPdfReprint($arg)
	{
		$myTableData = $this->input->post('TableData');
        $myTableData = stripcslashes($myTableData);
        $myTableData = json_decode($myTableData,TRUE);
        $myTableRows = count($myTableData);
        $rowId = $myTableData[0]['vNo'];

        $this->load->model('Purchaseordersaveandprint_model');
		$data['partyInfo'] = $this->Purchaseordersaveandprint_model->partyInfo($myTableData[0]['partyRowId']);
		$CiDt = $myTableData[0]['dt'];
		$partyName = $data['partyInfo'][0]['name'];
		// $rowId = -10;
		
		// $data['ci'] = $this->Salesreturn_model->getci($rowId);

		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Sales Return');
		// $pdf->SetHeaderMargin(1);
		$pdf->SetPrintHeader(false);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(12);
		$pdf->SetAutoPageBreak(true, 15); //2nd arg is margin from footer
		$pdf->SetAuthor('Suri');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();

		$pdf->SetFont('', '', 10, '', true); 

		$data['org'] = $this->Util_model->getOrg(); /////////////////////////ORG INFO

		$orgImagePath = $data['org'][0]['imagePath'];
		// $pdf->Write(5, 'CodeIgniter TCPDF Integration');
		$html='<table border="0">
					<tr>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/logolt.jpg" /></td>
						<td style="width:90mm;"></td>
						<td style="width:50mm;"><img src="'.FCPATH.'/bootstrap/images/'. $orgImagePath .'" width="293px" height="200px" /></td>
					</tr>
					<tr>
						<td colspan="2" style="font-size:14pt; font-weight:bold;">' . $data['org'][0]['orgName'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['add1'] . $data['org'][0]['add2'] . $data['org'][0]['add3'] . $data['org'][0]['add4'] . '</td>
					</tr>
					<tr>
						<td colspan="2">' . $data['org'][0]['mobile1'] . $data['org'][0]['email'] . '</td>
					</tr>
					<tr>
						<td colspan="2">GSTIN: ' . $data['org'][0]['gstIn'] . '</td>
					</tr>
			</table>';

		$pdf->writeHTML($html, true, false, true, false, '');
		// Line ($x1, $y1, $x2, $y2, $style=array())
		$pdf->Line(0, 80, 210, 80);


		$html='<table border="0">
					<tr>
						<td colspan="2" align="center" style="height:6mm; "></td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="height:10mm; font-size: 14pt; font-weight:bold;">Sales Return</td>
					</tr>
					<tr>
						<td style="width:95mm;height:10mm;">Date: '.$CiDt.'</td>
						<td align="right" style="width:90mm;">SR.No.: '.$rowId.'</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;">To,</td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;font-size: 12pt; font-weight:bold;">'.$partyName.'</td>
					</tr>
					<tr>
						<td colspan="2" style="height:10mm;font-size: 10pt;">'.$data['partyInfo'][0]['addr'].'</td>
					</tr>
					<tr>
						<td style="height:6mm;font-size: 10pt;"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:6mm;font-size: 10pt;"></td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$myTableDataProducts = $this->input->post('TableDataProducts');
        $myTableDataProducts = stripcslashes($myTableDataProducts);
        $myTableDataProducts = json_decode($myTableDataProducts,TRUE);
        // $myTableDataProducts = count($myTableDataProducts);
		$r = count($myTableDataProducts);


		$productRows = "";
		for($k=0; $k < $r; $k++)
		{
			$sn=$k+1;
			$productRows .= "<tr>";
				$productRows .= "<td>". $sn ."</td>";
				$productRows .= "<td>". $myTableDataProducts[$k]['productName'] ."</td>";
				// $productRows .= "<td>". $myTableData[$k]['placement'] ."</td>";
				$productRows .= "<td>". $myTableDataProducts[$k]['colourName'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableDataProducts[$k]['qty'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableDataProducts[$k]['rate'] ."</td>";
				$productRows .= "<td align=\"right\">". $myTableDataProducts[$k]['amt'] ."</td>";
				$productRows .= "<td>". $myTableDataProducts[$k]['remarks'] ."</td>";
			$productRows .= "</tr>";
		}
		$html='<table border="1" cellpadding="5">
					<tr>
						<th style="width:12mm; color:white; background-color:black;">S.N.</th>
						<th style="width:35mm; color:white; background-color:black;">Product</th>
						<th style="width:25mm; color:white; background-color:black;">Colour</th>
						<th style="width:15mm; text-align:center; color:white; background-color:black;">Qty</th>
						<th style="width:20mm; text-align:center; color:white; background-color:black;">Rate</th>
						<th style="width:30mm; text-align:center; color:white; background-color:black;">Amt.</th>
						<th style="width:50mm; color:white; background-color:black;">Remarks</th>
					</tr>'.$productRows.
			'</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$col1="";
		$col2="";
		$col3="";
		$col4="";
		$col5="";		
		$col3 .= 'Total Amt.:' . '<br>';	
		// $col4 .=  '<br>';	
		$col5 .= $myTableData[0]['totalAmt'] . '<br>';	
		if($myTableData[0]['disPer'] == 0) 
		{
			$printDiscountTitle="";
			$printDiscountPer="";
			$printDiscountAmt="";
		}
		else
		{
			$printDiscountTitle = "Discount Amt.:";
			$printDiscountPer = $myTableData[0]['disAmt'].' ('. $myTableData[0]['disPer']  .'%)' ;
			$printDiscountAmt = $myTableData[0]['amtAfterDis'];
			$col3 .= $printDiscountTitle . ' [' . $printDiscountPer . ']<br>';
			// $col4 .= $printDiscountPer . '<br>';
			$col5 .= $printDiscountAmt . '<br>';
		}

		// if($this->input->post('vatPer') == 0) 
		// {
		// 	$printVatTitle="";
		// 	$printVatPer="";
		// 	$printVatAmt="";
		// }
		// else
		// {
		// 	$printVatTitle = "VAT:";
		// 	$printVatPer = $this->input->post('vatPer').'%';
		// 	$printVatAmt = $this->input->post('vatAmt');
		// 	$col3 .= $printVatTitle . ' (' . $printVatPer . ')<br>';
		// 	// $col4 .= $printVatPer . '<br>';
		// 	$col5 .= $printVatAmt . '<br>';			
		// }

		if($myTableData[0]['sgstPer'] == 0) 
		{
			$printSgstTitle="";
			$printSgstPer="";
			$printSgstAmt="";
		}
		else
		{
			$printSgstTitle = "SGST:";
			$printSgstPer = $myTableData[0]['sgstPer'].'%';
			$printSgstAmt = $myTableData[0]['sgstAmt'];
			$col3 .= $printSgstTitle . ' (' . $printSgstPer . ')<br>';
			$col5 .= $printSgstAmt . '<br>';			
		}
		if($myTableData[0]['cgstPer'] == 0) 
		{
			$printCgstTitle="";
			$printCgstPer="";
			$printCgstAmt="";
		}
		else
		{
			$printCgstTitle = "CGST:";
			$printCgstPer = $myTableData[0]['cgstPer'].'%';
			$printCgstAmt = $myTableData[0]['cgstAmt'];
			$col3 .= $printCgstTitle . ' (' . $printCgstPer . ')<br>';
			$col5 .= $printCgstAmt . '<br>';			
		}
		if($myTableData[0]['igstPer'] == 0) 
		{
			$printIgstTitle="";
			$printIgstPer="";
			$printIgstAmt="";
		}
		else
		{
			$printIgstTitle = "IGST:";
			$printIgstPer = $myTableData[0]['igstPer'].'%';
			$printIgstAmt = $myTableData[0]['igstAmt'];
			$col3 .= $printIgstTitle . ' (' . $printIgstPer . ')<br>';
			$col5 .= $printIgstAmt . '<br>';			
		}
		$col3 .= 'Net Amt.:';
		$col5 .= $myTableData[0]['net'];	

		$html='<table border="0" cellpadding="5">
					<tr>
							<td  align="left" style="width:50mm;height:7mm;font-size: 10pt;">' . $col1 . '</td>
							<td  align="left" style="width:30mm;height:7mm;font-size: 10pt;">' . $col2 .'</td>
							<td align="right" style="width:75mm;height:7mm;font-size: 10pt;">' . $col3 . '</td>
							<td align="right" style="width:33mm;height:7mm;font-size: 10pt;">' . $col5 .'</td>
					</tr>

					<tr>
						<td align="right" colspan="4" style="height:7mm;font-size: 10pt;">'. $this->input->post('inWords') .'</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');


		$html='<table border="0">
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">1. All prices are for delivery F.O.R./Ajmer/Destination.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">2. VAT and Other duties will be extra.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">3. if the purchaser fails to take the delivery of the goods from the transport he shall be liable to bear all the expenses for damage of goods etc.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">4. ____50___% payment will be received by sending the dispatch documents through Bank and balance at the time of delivery.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">5. All disputes are subject to Ajmer Jurisdiction only.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">6. In case payments are not made within the stipulated period interest at 18% p.a. will be charged.</td>
					</tr>
					<tr>
						<td style="width:180mm;height:6mm;font-size:8pt;">7. Prices are based on present market rates of raw material and subject to </td>
					</tr>
					<tr>
						<td align="right" style="width:180mm;height:10mm;font-size:10pt;">For ' . $data['org'][0]['orgName'] . '</td>
					</tr>
			</table>';
		$pdf->writeHTML($html, true, false, true, false, '');

		$dt = date("Y_m_d");
		date_default_timezone_set("Asia/Kolkata");
		$tm = date("H_i_s");
		$partyName = "ss"; //$data['ci'][0]['name'];
		$pdf->Output(FCPATH . '/downloads/SR_'. $dt . ' (' . $tm . ') ' . $partyName .'.pdf', 'F');
		echo base_url()."downloads/SR_". $dt . " (" . $tm . ") " . $partyName . ".pdf";
	}
}
