<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MailLog_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Maillog_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Mail Log')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			// $this->load->model('Colours_model');
			$data['contactTypes'] = $this->Maillog_model->getContactTypes();
			$data['referencers'] = $this->Maillog_model->getReferencers();
			$data['mailTypes'] = $this->Maillog_model->getMailTypes();
			$this->load->view('MailLog_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function showData()
	{
		$data['records'] = $this->Maillog_model->getDataForReport();
		echo json_encode($data);
	}

	public function saveData()
	{
		$this->Maillog_model->saveData();
		echo json_encode('ss');
	}

	public function exportData()
	{
		set_time_limit(0);
		$data['org'] = $this->Util_model->getOrg($this->session->orgRowId);

		$this->load->library('Excel');
		//////////// Copying blank file
		copy('excelfiles/Q_blank.xls', 'excelfiles/tmp1.xls');

		// Create new PHPExcel object
		$objPHPExcel = PHPExcel_IOFactory::load('excelfiles/tmp1.xls');
		$objPHPExcel->setActiveSheetIndex(0);


		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'S.N.');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Name');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Email');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Prev 1');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Prev 2');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Prev 3');


		$cellRange1 = "A" . (1) . ":" . "F" . (1);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 	// $objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(true);

		$data['records'] = $this->Maillog_model->getDataForReport();
		$row = 2;
		// $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 10, count($data['records']));
		for ($i=0; $i < count($data['records']) ; $i++) 
		{ 
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i+$row, $i+1);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i+$row, $data['records'][$i]['name']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i+$row, $data['records'][$i]['email1']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i+$row, $data['records'][$i]['prev1']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i+$row, $data['records'][$i]['prev2']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i+$row, $data['records'][$i]['prev3']);
		}
		$cellRange1 = "A" . (1) . ":" . "F" . ($i+1);
	 	$objPHPExcel->getActiveSheet()->getStyle($cellRange1)->getFont()->setBold(false)->setSize(10);


		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);	


	 	////// Page Setup
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()
		    ->getPageSetup()
		    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setTop(0.75);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()
		    ->getPageMargins()->setBottom(0.75);
	 	////// Page Setup Ends Here

		$dt = date("Y_m_d");
		$tm = date("H_i_s");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save("excelfiles/tmpEmailLog_" . $dt . ' (' . $tm . ') ' . ".xls");
		echo base_url()."excelfiles/tmpEmailLog_" . $dt . ' (' . $tm . ') ' . ".xls";


	}
}
