<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LeaveApproval_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Leaveapproval_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Leave Approval')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			$data['parties'] = $this->Util_model->getAbList();
			$data['users'] = $this->Leaveapproval_model->getUsers();
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);

			$this->load->view('LeaveApproval_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
            $this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	public function getDetail()
	{
		$data['detail'] = $this->Leaveapproval_model->getDetail();
		$data['lastApproved'] = $this->Leaveapproval_model->getLastApproved($data['detail'][0]['userRowId']);
		echo json_encode($data);
	}

	public function approve()
	{
		$this->Leaveapproval_model->approve();
		// $data['lastApproved'] = $this->Leaveapproval_model->getLastApproved($data['detail'][0]['userRowId']);
		// echo json_encode($data);
		echo "Done...";
	}

	public function reject()
	{
		$this->Leaveapproval_model->reject();
		echo "Done...";
	}

	// public function update()
	// {
	// 	$this->Leaveapproval_model->update();
	// 	$data['records'] = $this->Leaveapproval_model->getDataLimit();
	// 	echo json_encode($data);
	// }


	// public function delete()
	// {
	// 	$this->Leaveapproval_model->delete();
	// 	$data['records'] = $this->Leaveapproval_model->getDataLimit();
	// 	echo json_encode($data);
	// }

	// public function loadAllRecords()
	// {
	// 	$data['records'] = $this->Leaveapproval_model->getDataAll();
	// 	echo json_encode($data);
	// }
	// // public function loadLimitedRecords()
	// // {
	// // 	$data['records'] = $this->Util_model->getDataLimitQpo('Q');
	// // 	echo json_encode($data);
	// // }




}
