<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailer_Controller extends CI_Controller
{
	public function __construct()
    {
            parent::__construct();
            $this->load->model('Emailer_model');
            $this->load->helper('form');
            $this->load->helper('url');
    }
	public function index()
	{
		if ($this->session->isLogin===True && $this->session->session_id != '') /*if logged in*/
		{
			if($this->Util_model->getRight($this->session->userRowId,'Email')==0)
			{
				$this->load->view('includes/header4all');
				$MenuRights['mr'] = $this->Util_model->getUserRights();
				$this->load->view('includes/menu4admin', $MenuRights);
				$this->load->view('ErrorUnauthenticateUser_view');
				$this->load->view('includes/footer');				
				return;
			}
			// $data['records'] = $this->Emailer_model->getDataLimit();
			$data['errorfound'] = "";
			$this->load->view('includes/header4all');
			$MenuRights['mr'] = $this->Util_model->getUserRights();
			$this->load->view('includes/menu4admin', $MenuRights);
			$this->load->view('Emailer_view', $data);
			$this->load->view('includes/footer');
		}
		else 	/* if not logged in */	
		{
            $this->load->view('includes/header');           // with Jumbotron
        	$this->load->model('Login_model');
        	$data['org'] = $this->Login_model->getOrgList();
			$this->load->view('login_view', $data);
	        $this->load->view('includes/footer');
		}
	}  

	function send()
	{
        ///// upload attachment 1
		$this->load->library('upload');
		$att = $_FILES["txtAttachment"]["name"];
		$lastId=1;
		if ($att) 
		{
			$conf = array(
							'upload_path' =>  "./bootstrap/images/emailatt",
							'allowed_types' => "*",
							'file_name' =>	'att' . $lastId,
							'overwrite' => TRUE,
						 );
			$this->upload->initialize($conf);
        	$this->upload->do_upload('txtAttachment');
			$attdata = $this->upload->data();

			$uploadedFileName = $attdata['raw_name'] . $attdata['file_ext'];
			// $updata1['courseImage'] = $imgdata['raw_name'] . $imgdata['file_ext'];
			// $this->Admin_model->update_data_by_id($updata1, $lastId, 'courseId' ,$tblnm); //$data,$id,$fldnm,$tblnm
		}
		else
		{
			// $this->session->set_flashdata('feedback',"You have not insert New Image");
			// $this->session->set_flashdata('feedback_class', 'alert-warning');
		}
        ///// END - upload attachment 1
        ///// upload attachment 2
		$att2 = $_FILES["txtAttachment2"]["name"];
		$lastId=2;
		if ($att2) 
		{
			$conf = array(
							'upload_path' =>  "./bootstrap/images/emailatt",
							'allowed_types' => "*",
							'file_name' =>	'att' . $lastId,
							'overwrite' => TRUE,
						 );
			$this->upload->initialize($conf);
        	$this->upload->do_upload('txtAttachment2');
			$attdata = $this->upload->data();

			$uploadedFileName2 = $attdata['raw_name'] . $attdata['file_ext'];
			// $updata1['courseImage'] = $imgdata['raw_name'] . $imgdata['file_ext'];
			// $this->Admin_model->update_data_by_id($updata1, $lastId, 'courseId' ,$tblnm); //$data,$id,$fldnm,$tblnm
		}
		else
		{
			// $this->session->set_flashdata('feedback',"You have not insert New Image");
			// $this->session->set_flashdata('feedback_class', 'alert-warning');
		}
        ///// END - upload attachment 2
        ///// upload attachment 3
		$att3 = $_FILES["txtAttachment3"]["name"];
		$lastId=3;
		if ($att3) 
		{
			$conf = array(
							'upload_path' =>  "./bootstrap/images/emailatt",
							'allowed_types' => "*",
							'file_name' =>	'att' . $lastId,
							'overwrite' => TRUE,
						 );
			$this->upload->initialize($conf);
        	$this->upload->do_upload('txtAttachment3');
			$attdata = $this->upload->data();

			$uploadedFileName3 = $attdata['raw_name'] . $attdata['file_ext'];
			// $updata1['courseImage'] = $imgdata['raw_name'] . $imgdata['file_ext'];
			// $this->Admin_model->update_data_by_id($updata1, $lastId, 'courseId' ,$tblnm); //$data,$id,$fldnm,$tblnm
		}
		else
		{
			// $this->session->set_flashdata('feedback',"You have not insert New Image");
			// $this->session->set_flashdata('feedback_class', 'alert-warning');
		}
        ///// END - upload attachment 3

        // Load PHPMailer library
        $this->load->library('phpmailer_lib');


        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        $from = $this->input->post('txtFrom');
        $password = $this->input->post('txtPassword');
        
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'ssl://smtp.gmail.com';
        $mail->SMTPAuth = true;
        // $mail->Username = 'reetlekhyani@gmail.com';
        // $mail->Password = 'Rauni2009';
        $mail->Username = $from;
        $mail->Password = $password;
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;
        
        // $mail->setFrom('reetlekhyani@gmail.com', 'Mail Tester');
        $mail->setFrom($this->input->post('txtFrom'), 'Mail Tester');
        // $mail->addReplyTo('info@example.com', 'CodexWorld');
        
        // Add a recipient
        $mail->addAddress( $this->input->post('txtTo') );
        // $mail->addAddress('chandalekhyani@gmail.com');
        // $mail->addAddress('dhupialekhyani@gmail.com');
        
        // IMP - Add cc or bcc (bahut sare address ho to BCC hi use karna h warna TO m zyada nahi jate)
        $mail->addCC($this->input->post('txtCc'));
		$toEmailAddresses = explode(",", $this->input->post('txtBcc'));
		for ($i=0; $i <count($toEmailAddresses); $i++)
       	{
        	$mail->addBCC( trim( strtolower( $toEmailAddresses[$i] ) ) );
		}
        // $mail->addBCC( $this->input->post('txtTo') );
        
        // Email subject
        $mail->Subject = $this->input->post('txtSubject');
        
        // Set email format to HTML
        $mail->isHTML(true);
        
        // Email body content
        // $mail->AddEmbeddedImage('./uploads/images/Untitled.jpg', 'Untitled');
        // $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
        //     <p>This is a test email sending using SMTP mail server with PHPMailer.</p> <img src='cid:Untitled' /> <br><h2>-Surendra Lekhyani </h2>";
        // $mail->Body = $mailContent;
            
        $body = $this->input->post('txtBody');
        ///// replacing img tag src with cid
        // get all img tags
	    preg_match_all('/<img.*?>/', $body, $matches);

	    if (!isset($matches[0])) return;
	    // foreach tag, create the cid and embed image
	    $i = 1;
	    foreach ($matches[0] as $img)
	    {
	        // make cid
	        $id = 'img'.($i++);
	        // replace image web path with local path
	        fwrite($myfile,"\n -   img:   -" . $img);
	        preg_match('/src="(.*?)"/', $img, $m);
	        preg_match('/style="(.*?)"/', $img, $sty); //// suri line
	        if (!isset($m[1])) continue;
	        $arr = parse_url($m[1]);
	        // if (!isset($arr['host']) || !isset($arr['path']))continue;
	        // // add
	        $mail->AddEmbeddedImage('./uploads/images/'.basename($arr['path']), $id);
	        // $body = str_replace($img, '<img alt="" src="cid:'.$id.'" style="border: none;" />', $body); 
	        $body = str_replace($img, '<img alt="" src="cid:'.$id.'" style="'.$sty[1].'" />', $body); 
	    }
        ///// END - replacing img tag src with cid
        $mail->Body = $body;


        // $mail->Body = $this->input->post('txtBody');
        $mail->AddAttachment( './bootstrap/images/emailatt/'. $uploadedFileName );
        $mail->AddAttachment( './bootstrap/images/emailatt/'. $uploadedFileName2 );
        $mail->AddAttachment( './bootstrap/images/emailatt/'. $uploadedFileName3 );
        // $mail->Body = $_POST['txtBody'];
        // Send email
        if(!$mail->send()){
        	$this->session->set_flashdata('feedback',"Error: Message could not be sent.: " . $mail->ErrorInfo);
			$this->session->set_flashdata('feedback_class', 'alert-danger');
            // echo 'Message could not be sent.';
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
        	$this->session->set_flashdata('feedback',"Message has been sent");
			$this->session->set_flashdata('feedback_class', 'alert-success');
            // echo 'Message has been sent';
        }
        return redirect('Emailer_Controller');
    }



	// public function insert()
	// {
	// 	if($this->Emailer_model->checkDuplicate() == 1)
 //        {
 //        	$data = "Duplicate record...";
 //        	echo json_encode($data);
 //        }
 //        else
 //        {
	// 		$this->Emailer_model->insert();
	// 		$data['records'] = $this->Emailer_model->getDataLimit();
	// 		echo json_encode($data);
 //        }
	// }

	// public function update()
	// {
	// 	if($this->Emailer_model->checkDuplicateOnUpdate() == 1)
 //        {
 //        	$data = "Duplicate record...";
 //        	echo json_encode($data);
 //        }
 //        else
 //        {
	// 		$this->Emailer_model->update();
	// 		$data['records'] = $this->Emailer_model->getDataLimit();
	// 		echo json_encode($data);
 //        }
	// }

	// public function delete()
	// {
	// 	if($this->Util_model->isDependent('qpodetail', 'colourRowId', $this->input->post('rowId')) == 1)
 //        {
 //        	$data['dependent'] = "yes";
 //        	echo json_encode($data);
 //        }
 //        else
 //        {
	// 		$this->Emailer_model->delete();
	// 		$data['records'] = $this->Emailer_model->getDataLimit();
	// 		echo json_encode($data);
	// 	}
	// }

	// public function loadAllRecords()
	// {
	// 	$data['records'] = $this->Emailer_model->getDataAll();
	// 	echo json_encode($data);
	// }

}
