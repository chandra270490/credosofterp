<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='OpeningBal_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
		  // var sn=1;
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          // cell.style.display="none";

	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].stageRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(2);
	          // cell.style.display="none";
	          cell.innerHTML = records[i].stageName;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].opQty;
	          cell.style.color = "red";
	          cell.contentEditable = "true";
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		// $("#tbl1 tr").on("click", highlightRow);
			
	}

	function loadData()
	{	
		productRowId = $("#cboProduct").val();
		if(productRowId == "-1")
		{
			alertPopup("Select product...", 8000);
			$("#cboProduct").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/getData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productRowId': productRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['ob']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	if( $(tr).find('td:eq(1)').text() > 0 )
	    	{
	        	TableData[i]=
	        	{
		            "stageRowId" : $(tr).find('td:eq(1)').text()
		            , "opQty" : $(tr).find('td:eq(3)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveChanges()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		productRowId = $("#cboProduct").val();
		if(productRowId == "-1")
		{
			alertPopup("Select product...", 8000);
			$("#cboProduct").focus();
			return;
		}
		// var party = $("#cboParty option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/saveChanges',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'productRowId': productRowId
						},
				'success': function(data)
				{
					alert("Changes saved...");
					window.location.href=data;
				}
		});
		
	}

</script>
<div class="container">
	<div class="row">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Opening Balance</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					
					<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Name:</label>";
							echo form_dropdown('cboProduct',$products, '-1',"class='form-control' id='cboProduct'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Load Detail' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:350px; overflow:auto;">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none1;'>S.N.</th>
						<th style='display:none;'>stageRowId</th>
					 	<th>Stage</th>
						<th style='display:none1;'>Op. Bal.</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='saveChanges();' value='Save Changes' id='btnSaveChanges' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
</div>





<script type="text/javascript">


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );



</script>