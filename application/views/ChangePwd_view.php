<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div style="margin-top:20px;">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style='border:1px solid gray; border-radius:10px; padding: 10px;box-shadow: 5px 5px #d3d3d3;background-color:#fffaf0'>
		<h2 class="text-center" style='margin-top:0px'>Change Password</h2>
		<?php

			// $attributes[] = array("class"=>"form-control" );

			$this->load->helper('form');		// it will load 'form' so that we will be able to use form_open() etc.
			// echo validation_errors(); 
			echo form_open('Changepwd_Controller/checkLogin');		// checklogin will be called on submit button.

			echo form_label('Old Password', '');
			echo form_input('txtOldPassword', set_value('txtOldPassword'),"placeholder='', class='form-control' autofocus");
			echo form_error('txtOldPassword');
			echo form_label('New Password', '');
			echo form_password('txtPassword', set_value('txtPassword'),"placeholder='', class='form-control'");
			echo form_error('txtPassword');
			echo form_label('Repeat Password', '');
			echo form_password('txtRepeatPassword', set_value('txtRepeatPassword'),"placeholder='', class='form-control'");
			echo form_error('txtRepeatPassword');


			echo "<br>";
			echo "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-4'>";
			echo form_submit('btnSubmit', 'Submit',"class='btn btn-danger btn-block'");
			echo "</div>";
			echo form_close();
		?>

	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("input[type=password], input[type=text]").val("");
	});


</script>