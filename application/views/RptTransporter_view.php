<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptTransporter_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].despatchRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].despatchRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].despatchRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].despatchDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].transporter;
	          // cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].remarks;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].prGrNo;
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		$("#tbl1 tr").on('click', showDetail);
			
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}


		partyRowId = $("#cboParty").val();
		// var prGrNo = $("#txtPrGrNo").val().trim();
		// if( prGrNo == "")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/showData',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'partyRowId': partyRowId
								, 'dtFrom': dtFrom
								, 'dtTo': dtTo
								
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(JSON.stringify(data));
								setTable(data['records']) 
								alertPopup('Records loaded...', 4000);
						}
					}
			});
		}
		// else
		// {
		// 	$.ajax({
		// 			'url': base_url + '/' + controller + '/showDataPrGr',
		// 			'type': 'POST',
		// 			'dataType': 'json',
		// 			'data': {
		// 						'prGrNo': prGrNo
		// 						, 'dtFrom': dtFrom
		// 						, 'dtTo': dtTo
								
		// 					},
		// 			'success': function(data)
		// 			{
		// 				if(data)
		// 				{
		// 					// alert(JSON.stringify(data));
		// 						setTable(data['records']) 
		// 						alertPopup('Records loaded...', 4000);
		// 				}
		// 			}
		// 	});
		// }
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "despatchRowId" : $(tr).find('td:eq(2)').text()
		            , "vType" : $(tr).find('td:eq(3)').text()
		            , "vNo" :$(tr).find('td:eq(4)').text()
		            , "vDt" :$(tr).find('td:eq(5)').text()
		            , "partyRowId" :$(tr).find('td:eq(6)').text()
		            , "partyName" :$(tr).find('td:eq(7)').text()
		            , "transporter" :$(tr).find('td:eq(8)').text()
		            , "remarks" :$(tr).find('td:eq(9)').text()
		            , "prGrNo" :$(tr).find('td:eq(10)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		var party = $("#cboParty option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'party': party
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
	function storeTblValuesDetail()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "orderNo" : $(tr).find('td:eq(0)').text()
		            , "orderDate" : $(tr).find('td:eq(1)').text()
		            , "orderType" :$(tr).find('td:eq(2)').text()
		            , "category" :$(tr).find('td:eq(3)').text()
		            , "product" :$(tr).find('td:eq(4)').text()
		            , "colour" :$(tr).find('td:eq(5)').text()
		            , "qty" :$(tr).find('td:eq(6)').text()
		            , "challanNo" :$(tr).find('td:eq(7)').text()
		            , "invNo" :$(tr).find('td:eq(8)').text()
		        }
	        	i++; 
	        
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportDetailData()
	{	
		var TableData;
		TableData = storeTblValuesDetail();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No detail to print...", 8000);
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		var party = $("#cboParty option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportDataDetail',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'party': party
							, 'globalVno': globalVno
							, 'globalVdt': globalVdt
							, 'globalPartyName': globalPartyName
							, 'globalTransporter': globalTransporter
							, 'globalRemarks': globalRemarks
							, 'globalPrGrNo': globalPrGrNo
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Transporter-Wise Report</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});

						    // Set the Current Date-50 as Default
						    dt=new Date();
     					    dt.setDate(dt.getDate() - 50);
   		 					$("#dtFrom").val(dateFormat(dt));
						</script>					
		          	</div>
		          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtTo").val(dateFormat(new Date()));
						</script>					
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Party:</label>";
							echo form_dropdown('cboParty',$parties, '-1',"class='form-control' id='cboParty'");
		              	?>
		          	</div>

					<!-- <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>PR/GR No.</label>";
							echo "<input type='text'  id='txtPrGrNo' class='form-control'>";
		              	?>
		          	</div> -->

					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th style='display:none;' width="50" class="text-center">Delete</th>
						<th style='display:none;'>despatchRowid</th>
					 	<th style='display:none;'>V. Type</th>
					 	<th style='display:none1;'>V.No.</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th>Transporter</th>
					 	<th>Remarks</th>
					 	<th>PR/Gr #</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tblProducts'>
				 <thead>
					 <tr>
					 	<th>Order No</th>
					 	<th>Order Dt.</th>
					 	<th>Order Type</th>
					 	<th>Product Category</th>
					 	<th>Product</th>
					 	<th>Colour</th>
					 	<th>Desp. Qty</th>
					 	<th>Challan No.</th>
					 	<th>Inv. No.</th>
					 	<!-- <th>DespDetailRowId</th>
					 	<th>DespRowId</th> -->
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportDetailData();' value='Export Detail' id='btnLoadDetail' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>
</div>





<script type="text/javascript">
		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );


	// $(document).ready(function()
	// {
	//     $("#tbl1 tr").on('click', showDetail);
	// });
	var globalVno, globalVdt, globalPartyName, globalTransporter,globalRemarks, globalPrGrNo 
	function showDetail()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		despatchRowId = $(this).closest('tr').children('td:eq(2)').text();
		// alert(despatchRowId);
		globalVno = $(this).closest('tr').children('td:eq(4)').text();
		globalVdt = $(this).closest('tr').children('td:eq(5)').text();
		globalPartyName = $(this).closest('tr').children('td:eq(7)').text();
		globalTransporter = $(this).closest('tr').children('td:eq(8)').text();
		globalRemarks = $(this).closest('tr').children('td:eq(9)').text();
		globalPrGrNo = $(this).closest('tr').children('td:eq(10)').text();
		// alert(globalStatus);
		$.ajax({
			'url': base_url + '/RptTransporter_Controller/getProducts',
			'type': 'POST', 
			'data':{'rowid':despatchRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert( JSON.stringify(data) );
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          // cell.innerHTML = data['products'][i].vType + "-" + data['products'][i].vNo;
		          cell.innerHTML = data['products'][i].one;
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].two;
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].three;
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].four;
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].five;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].six;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].seven;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].eight;
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].nine;
		          // var cell = row.insertCell(9);
		          // cell.innerHTML = data['products'][i].nine;
		          // var cell = row.insertCell(10);
		          // cell.innerHTML = data['products'][i].ten;
		        }
		        $("#tblProducts tr").on("click", highlightRow);	
			}
		});

	}

	// var tblRowsCount;
	function storeTblValues4InvNo()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	        	TableData[i]=
	        	{
		            "despatchRowId" : $(tr).find('td:eq(8)').text()
	        	}   
	        	i++; 
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

</script>