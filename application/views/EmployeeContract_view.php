<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='EmployeeContract_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  // $("#tbl1").empty();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "x";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].catRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(2);
	          // cell.style.display="none";
	          cell.innerHTML = records[i].productCategory;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].productRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].productName;
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].contractRate;
	          cell.contentEditable = "true";
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		// myDataTable.destroy();
		// $(document).ready( function () {
	 //    myDataTable=$('#tbl1').DataTable({
		//     paging: false,
		//     iDisplayLength: -1,
		//     aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// });
		// } );

		// $("#tbl1 tr").on("click", highlightRow);
			
	}

	function loadData()
	{	
		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 4000);
			$("#cboEmp").focus();
			return;
		}

		productCategory = $("#cboProductCategories").val();
		if(productCategory == "-1")
		{
			alertPopup("Select Product Category...", 4000);
			$("#cboProductCategories").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/getData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'empRowId': empRowId
							, 'productCategory': productCategory
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['products']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "catRowId" : $(tr).find('td:eq(1)').text()
		            , "productRowId" : $(tr).find('td:eq(3)').text()
		            , "contractRate" :$(tr).find('td:eq(5)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveChanges()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		empRowId = $("#cboEmp").val();
		if(empRowId == "-1")
		{
			alertPopup("Select employee...", 8000);
			$("#cboEmp").focus();
			return;
		}
		// var party = $("#cboParty option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/saveChanges',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'empRowId': empRowId
						},
				'success': function(data)
				{
					alert("Changes saved...");
					window.location.href=data;
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Employee Contract Detail</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Employee Name:</label>";
							echo form_dropdown('cboEmp',$employees, '-1',"class='form-control' id='cboEmp'");
		              	?>
		          	</div>
		          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
							echo form_dropdown('cboProductCategories',$productCategories, '-1',"class='form-control' id='cboProductCategories'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Load Detail' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>rowid</th>
						<th style='display:none;'>catRowId</th>
					 	<th>Category</th>
						<th style='display:none;'>productRowId</th>
					 	<th>Product Name</th>
					 	<th>Contract Rate</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='saveChanges();' value='Save Changes' id='btnSaveChanges' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
</div>





<script type="text/javascript">


		// $(document).ready( function () {
		//     myDataTable = $('#tbl1').DataTable({
		// 	    paging: false,
		// 	    iDisplayLength: -1,
		// 	    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		// 	});
		// } );



</script>