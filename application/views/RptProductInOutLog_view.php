<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptProductInOutLog_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = records[i].ledgerPRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].vType;

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].vNo;

	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].dt));

	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].qtyIn;

	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].qtyOut;

	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].toName;

	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].user;
	  	  }

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}




	function storeTblHeaders()
	{
		var header = Array();
		// alert('ff');
		$("#tbl1 tr th").each(function(i, v){
		        header[i] = $(this).text();
		})

		return header;
	}

	var tblRowsCount=0;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text().replace(/(\r\n|\n|\r)/gm,", ");
		    }); 
		})
	    return data;
	}


	function exportData()
	{	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		var productCategoryRowId=$("#cboProductCategories").val();
		if(productCategoryRowId == "-1")
		{
			alertPopup("Select product category...", 8000);
			return;
		}
		productCategoryName = $("#cboProductCategories option:selected").text();
		var productRowId=$("#cboProducts").val();
		if(productRowId == "-1")
		{
			alertPopup("Select product...", 8000);
			return;
		}
		productName = $("#cboProducts option:selected").text();


		// alert();
		// return;
		var TableDataHeader;
		TableDataHeader = storeTblHeaders();
		TableDataHeader = JSON.stringify(TableDataHeader);
		// alert(JSON.stringify(TableDataHeader));
		// return;

		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		// alert(TableData.length);
		// return;
		if(TableData.length <= 35)  //35 because of 'No data available in table'
		{
			alertPopup("Nothing to export...", 8000);
			// $("#cboProducts").focus();
			return;
		}


		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'productCategoryName': productCategoryName
							, 'productName': productName
							, 'TableData': TableData
							, 'TableDataHeader': TableDataHeader
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

	function showData()
	{	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		var productCategoryRowId=$("#cboProductCategories").val();
		if(productCategoryRowId == "-1")
		{
			alertPopup("Select product category...", 8000);
			return;
		}
		var productRowId=$("#cboProducts").val();
		if(productRowId == "-1")
		{
			alertPopup("Select product...", 8000);
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/getData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'productRowId': productRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']);
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px'>Product In/Out Log</h3>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>From:</label>";
						echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtFrom" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});

					    // Set the Current Date-50 as Default
					    dt=new Date();
 					    dt.setDate(dt.getDate() - 50);
		 					$("#dtFrom").val(dateFormat(dt));
					</script>	
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>To:</label>";
						echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtTo" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dtTo").val(dateFormat(new Date()));
					</script>		          	
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
						echo form_dropdown('cboProductCategories',$productCategories, '-1', "class='form-control' id='cboProductCategories'");
	              	?>				
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						$products="--- Select ---";
						echo "<label style='color: black; font-weight: normal;'>Products:</label>";
						echo form_dropdown('cboProducts',$products, '-1', "class='form-control' id='cboProducts'");
	              	?>
	          	</div>
			</div>

			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
				</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='showData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:400px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>ledgerRowId</th>
					 	<th style='display:none1;'>V.Type</th>
					 	<th style='display:none1;'>V.No</th>
					 	<th>Dt</th>
					 	<th>In</th>
					 	<th>Out</th>
					 	<th>To/From</th>
					 	<th>User</th>
					 </tr>
				 </thead>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 10px;" >
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
		</div>	
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
	</div>
</div>


<script type="text/javascript">


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );



      $(document).ready(function(){
        $("#cboProductCategories").change(function(){
          var productCategoryRowId = $("#cboProductCategories").val();
          if(productCategoryRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductList',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productCategoryRowId': productCategoryRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                  // alert(data);
                  $('#cboProducts').empty();
                  if(data['products'] != null) 
                  {
                    var options = "<option value='-1' rate='0' productLength='0' productWidth='0' productHeight='0' uom='-1' roLevel='0'>" + "--- Select ---" + "</option>";
                    for(var i=0; i<data['products'].length; i++)
                    {
                      options += "<option value=" + data['products'][i].productRowId + " rate=" + data['products'][i].productRate + " productLength=" + data['products'][i].productLength + " productWidth=" + data['products'][i].productWidth + " productHeight=" + data['products'][i].productHeight + " uom=" + data['products'][i].uom +  " roLevel=" + data['products'][i].roLevel + ">" + data['products'][i].productName + "</option>";
                    }
                    $('#cboProducts').append(options);

                  }
                  else
                  {
                  }

                }
              }
          });
        });
      });
              


</script>