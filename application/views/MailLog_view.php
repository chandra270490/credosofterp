<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='MailLog_Controller';
	var base_url='<?php echo site_url();?>';

	
</script>
<style type="text/css">
	#style-1::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		border-radius: 1px;
		/*margin-left: 50px;*/
		background-color: #F5F5F5;
	}

	#style-1::-webkit-scrollbar
	{
		width: 12px;
		/*margin: -20px;*/
		background-color: #F5F5F5;
	}

	#style-1::-webkit-scrollbar-thumb
	{
		border-radius: 1px;
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
		background-color: lightgrey;
	}
</style>

<script type="text/javascript">

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);

	          var cell = row.insertCell(0);
	          cell.innerHTML = "<input type='checkbox' class='chkRecord' id="+records[i].abRowId + " value=" + records[i].abRowId + "></input>"
	          cell.style.textAlign = 'left';

	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].abRowId;
	          cell.style.display="none";
	          // cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].name;
	          cell.className = 'clsName';
	          var cell = row.insertCell(3);
	          cell.style.display="none";
	          
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].prev1;
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].prev2;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].prev3;
	          var cell = row.insertCell(7);
	          cell.style.display="none";
	          
	  	  }


	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
		    initComplete: function () {
		            this.api().columns().every( function () {
		                var column = this;

		                var select = $('<select><option value=""></option></select>')
		                    .appendTo( $(column.footer()).empty() )
		                    .on( 'change', function () {
		                        var val = $.fn.dataTable.util.escapeRegex(
		                            $(this).val()
		                        );
		 
		                        column
		                            .search( val ? '^'+val+'$' : '', true, false )
		                            .draw();
		                    } );
		 
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append( '<option value="'+d+'">'+d+'</option>' )
		                } );

		                
		            } );
		        }
		});
		} );

		// $("#tbl1 tr").on("click", highlightRow);
		$("#chkAllRecords").on('click', selectAllWhenHeadClicked);
		$(".chkRecord").on('click', chkSelectAllRecords);
		$(".clsName").on('click', chkOnOffName);
			
	}

	function loadData()
	{	
		var refRowId="";
		$('input:checked.chkReferencer').each(function() 
		{
			refRowId = $(this).val() + "," + refRowId;
		});
		if(refRowId == "")
		{
			alertPopup("Select Reference...", 8000);
			return;
		}
		refRowId = refRowId.substr(0, refRowId.length-1);

		var contactTypeRowId="";
		$('input:checked.chkContactTypes').each(function() 
		{
			contactTypeRowId = $(this).val() + "," + contactTypeRowId;
		});
		if(contactTypeRowId == "")
		{
			alertPopup("Select Contact Type...", 8000);
			return;
		}
		contactTypeRowId = contactTypeRowId.substr(0, contactTypeRowId.length-1);

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'refRowId': refRowId
							, 'contactTypeRowId': contactTypeRowId
						},
				'success': function(data)
				{
					// console.log(data);
					// if(data)
					// {
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
					// }
				}
		});
	}


	var checkedRows=0;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0, j=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if(j>0)
	    	// {
	    		if($(tr).find('td:eq(0)').find('input[type=checkbox]').is(':checked'))
	    		{
		        	TableData[i]=
		        	{
			            "abRowId" : $(tr).find('td:eq(1)').text()
			            , "prev3" : $(tr).find('td:eq(6)').text().substr(0,10)
		        	}   
		        	i++; 
		    	}
		    		
	    }); 
	    checkedRows = i;
	    // TableData.shift();  // first row will be heading - so remove
	    return TableData;
	}

	function save()
	{	
		// var abRowId="";
		// $('input:checked.chkRecord').each(function() 
		// {
		// 	abRowId = $(this).val() + "," + abRowId;
		// });
		// if(abRowId == "")
		// {
		// 	alertPopup("Select Contact...", 8000);
		// 	return;
		// }
		// abRowId = abRowId.substr(0, abRowId.length-1);
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// console.log(TableData);
		// return;
		
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}

		mailTypeRowId = $("#cboMailType").val();
		if(mailTypeRowId == "-1")
		{
			alertPopup("Invalid Mail Type...", 5000);
			return;
		}
		remarks = $("#txtRemarks").val().trim();
		if(remarks == "")
		{
			alertPopup("Enter remarks...", 5000);
			return;
		}


		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dt': dt
							, 'mailTypeRowId': mailTypeRowId
							, 'remarks': remarks
						},
				'success': function(data)
				{
					// console.log(data);
					// if(data)
					// {
						// setTable(data['records']) 
						alertPopup('Saved... Page will be reloaded...', 4000);
						location.reload();
					// }
				}
		});
	}

</script>


<div class="container-fluid" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
	<div class="row">
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style=''>
			<?php
				// echo "<label style='color: black; font-weight: normal;'>Date:</label>";
				echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='11'");
          	?>
          	<script>
				$( "#dt" ).datepicker({
					dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
				});
			    // Set the Current Date as Default
				$("#dt").val(dateFormat(new Date()));
			</script>
		</div>
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style=''>
			<h3 class="text-center" style='margin-top:-20px'>Mail Log</h3>
		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style=''>
		</div>
	</div>
	<div class="row" style="margin-top: 20px;">
		<!-- Referencer -->
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style=''>
			<div class="row" id="style-1" class="" style='background:rgba(128,128,128,.2);padding:0 10px;height:350px;overflow:auto;margin: 1px; font-size: 10pt;'>
				<span class="btn" style='margin-left:-10px;'>
					<input type="checkbox" id="chkAllReferencer">
					<label for="chkAllReferencer" style="font-weight:bold;color:black;">All Referencers</label>
				</span>
				<?php
					foreach ($referencers as $key => $value) 
					{
				?>
						<div class="checkbox1" style='margin-left: 0px;'>
							<input type="checkbox" class="chkReferencer" txt='<?php echo $value;?>' id='p<?php echo $key;?>' value='<?php echo $key;?>'></input>
							<label  style="color: black;" for='p<?php echo $key;?>'><?php echo $value;?></label>
						</div>
				<?php
					}
				?>
			</div>
		</div>

		<!-- Contact Types -->
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style=''>
			<div class="row" id="style-1" class="" style='background:rgba(128,128,128,.2);padding:0 10px;height:350px;overflow:auto;margin: 1px;'>
				<span class="btn" style='margin-left:-10px;'>
					<input type="checkbox" id="chkAllContactTypes">
					<label for="chkAllContactTypes" style="font-weight:bold;color:black;">All Contact Types</label>
				</span>
				<?php
					foreach ($contactTypes as $key => $value) 
					{
				?>
						<div class="checkbox1" style='margin-left: 0px;'>
							<input type="checkbox" class="chkContactTypes" txt='<?php echo $value;?>' id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
							<label  style="color: black;" for='<?php echo $key;?>'><?php echo $value;?></label>
						</div>
				<?php
					}
				?>
			</div>
			<div class="row">
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="margin-top: 12px;">
					<?php
						// echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary btn-block'>";
	              	?>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" style=''>
			<div id="divTable" style="border:1px solid lightgray; padding: 10px;height:400px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th><input type="checkbox" id="chkAllRecords"></th>
					 	<th style='display:none;'>abRowId</th>
					 	<th>Name</th>
					 	<th style='display:none;'>Email1</th>
					 	<th>Prev 1 Mail</th>
					 	<th>Prev 2 Mail</th>
					 	<th>Prev 3 Mail</th>
					 	<th style='display:none;'>History</th>
					 </tr>
				 </thead>
				 <tfoot>
					 <tr>
					 	<th><input type="checkbox" id="chkAllRecords"></th>
					 	<th style='display:none;'>abRowId</th>
					 	<th>Name</th>
					 	<th style='display:none;'>Email1</th>
					 	<th>Prev 1 Mail</th>
					 	<th>Prev 2 Mail</th>
					 	<th>Prev 3 Mail</th>
					 	<th style='display:none;'>History</th>
					 </tr>
				 </tfoot>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row" style='margin-top:15px; background: lightgrey; padding: 10px 10px;'>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>Mail Type:</label>";
				echo form_dropdown('cboMailType',$mailTypes, '-1',"class='form-control' id='cboMailType'");
          	?>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
				echo form_input('txtRemarks', '', "class='form-control' id='txtRemarks' maxlength='100'");
          	?>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='save();' value='Save Log' id='btnShow' class='btn btn-danger btn-block'>";
          	?>
		</div>
	</div>

	<div class="row" style="margin-top: 20px;">
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="margin-top: 12px;">
			<?php
				// echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='exportData();' value='Export Contacts of Above Table' id='btnExport' class='btn btn-primary btn-block'>";
          	?>
		</div>
	</div>
</div>





<script type="text/javascript">
		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
		    initComplete: function () {
		            this.api().columns().every( function () {
		                var column = this;
		                
		                var select = $('<select><option value=""></option></select>')
		                    .appendTo( $(column.footer()).empty() )
		                    .on( 'change', function () {
		                        var val = $.fn.dataTable.util.escapeRegex(
		                            $(this).val()
		                        );
		 
		                        column
		                            .search( val ? '^'+val+'$' : '', true, false )
		                            .draw();
		                    } );
		 
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append( '<option value="'+d+'">'+d+'</option>' )
		                } );

		                
		            } );
		        }

		});
		} );


	// add multiple select / deselect functionality
	$("#chkAllContactTypes").click(function () {
		// alert();
		  $('.chkContactTypes').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkContactTypes').bind('click', chkSelectAll);
	function chkSelectAll()
	{
		var x = parseInt($(".chkContactTypes").length);
		var y = parseInt($(".chkContactTypes:checked").length);
		if( x == y ) 
		{
			$("#chkAllContactTypes").prop("checked", true);
		} 
		else 
		{
			$("#chkAllContactTypes").prop("checked", false);
		}
	}


	// add multiple select / deselect functionality
	function selectAllWhenHeadClicked()
	{
		$('.chkRecord').prop('checked', this.checked);
	}
	function chkSelectAllRecords()
	{
		var x = parseInt($(".chkRecord").length);
		var y = parseInt($(".chkRecord:checked").length);
		if( x == y ) 
		{
			$("#chkAllRecords").prop("checked", true);
		} 
		else 
		{
			$("#chkAllRecords").prop("checked", false);
		}
	}
	function chkOnOffName()
	{
		x = $(this).parent().find("td:eq(0)").find('input[type=checkbox]').is(':checked');
		if( x== false)
		{
			$(this).parent().find("td:eq(0)").find('input[type=checkbox]').prop('checked', true);
		}
		else
		{
			$(this).parent().find("td:eq(0)").find('input[type=checkbox]').prop('checked', false);
		}
		// $('.chkRecord').prop('checked', this.checked);
	}
	
</script>


<!-- refrencers -->
<script type="text/javascript">
	// add multiple select / deselect functionality
	$("#chkAllReferencer").click(function () {
		// alert();
		  $('.chkReferencer').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkReferencer').bind('click', chkSelectAllReferncer);
	function chkSelectAllReferncer()
	{
		var x = parseInt($(".chkReferencer").length);
		var y = parseInt($(".chkReferencer:checked").length);
		if( x == y ) 
		{
			$("#chkAllReferencer").prop("checked", true);
		} 
		else 
		{
			$("#chkAllReferencer").prop("checked", false);
		}
	}
</script>

<script type="text/javascript">
	function exportData()
	{
		var refRowId="";
		$('input:checked.chkReferencer').each(function() 
		{
			refRowId = $(this).val() + "," + refRowId;
		});
		if(refRowId == "")
		{
			alertPopup("Select Reference...", 8000);
			return;
		}
		refRowId = refRowId.substr(0, refRowId.length-1);

		var contactTypeRowId="";
		$('input:checked.chkContactTypes').each(function() 
		{
			contactTypeRowId = $(this).val() + "," + contactTypeRowId;
		});
		if(contactTypeRowId == "")
		{
			alertPopup("Select Contact Type...", 8000);
			return;
		}
		contactTypeRowId = contactTypeRowId.substr(0, contactTypeRowId.length-1);

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'refRowId': refRowId
							, 'contactTypeRowId': contactTypeRowId
							, 'abType': 'abType'
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
	}
</script>