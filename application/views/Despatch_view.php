<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Despatch_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          // cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].despatchRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");
	          cell.className = "delRecord";

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].despatchRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].despatchDt));
	          // cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].transporterRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].transporter;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].prGrNo;
	          // cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].remarks;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;	
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].vehicleNo;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].driverName;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].driverContactNo;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].driverLicenceNo;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].vehicleInsuranceNo;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].vNo;	
	  	  }


	  	$('.editRecord').bind('click', editThis);
	  	$('.delRecord').bind('click', setGlobalDespatchVno);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}



	function deleteRecord(rowId)
	{
		if($("#btnSave").val() == "Update")
		{
			msgBoxError("Error", "Cannot delete, updation is in process...");
			return;
		}
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId, 'globalDespatchVno': globalDespatchVno},
				'success': function(data){
					// alert(JSON.stringify(data));
					if(data)
					{
						if( data == "cannot")
						{
							// alertPopup('Cannot cancel, dependent rows exists...', 7000);
							msgBoxError("Error", "Cannot delete, dependent rows exists...");
						}
						else
						{
							setTable(data['records']);
							// alertPopup('Record deleted...', 4000);
							msgBoxDone("Done","Record deleted...");
							blankControls();
							$("#txtLetterNo").focus();
							$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
						}
					}
				}
			});
	}
	

	var tblRowsCount;
	var depatchMoreThanPending = 0;
	var invaliGodownSelected = 0;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	if( parseFloat($(tr).find('td:eq(9)').text()) > 0 )
	    	{
	    		if( parseFloat($(tr).find('td:eq(9)').text()) > parseFloat($(tr).find('td:eq(8)').text()) )
	    		{
	    			depatchMoreThanPending = 1;
	    		}
	    		if( ($(tr).find('td:eq(20)').text()) == "" || ($(tr).find('td:eq(20)').text()) == "-1" )
	    		{
	    			invaliGodownSelected = 1;
	    		}
	        	TableData[i]=
	        	{
		            "qpoRowId" : $(tr).find('td:eq(0)').text()
		            , "qpoDetailRowId" : $(tr).find('td:eq(1)').text()
		            , "vNo" :$(tr).find('td:eq(2)').text()
		            , "productRowId" :$(tr).find('td:eq(5)').text()
		            , "productAmt" :$(tr).find('td:eq(4)').text()
		            , "placementRowId" :$(tr).find('td:eq(5)').text()
		            , "despatchQty" :$(tr).find('td:eq(9)').text()
		            , "pendingNow" :$(tr).find('td:eq(10)').text()
		            , "colourRowId" :$(tr).find('td:eq(13)').text()
		            , "despatchDetailRowId" :$(tr).find('td:eq(19)').text()
		            , "godownAbRowId" :$(tr).find('td:eq(20)').text()
		            , "godownName" :$(tr).find('td:eq(21)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i;
	    return TableData;
	}


	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount <= 0)
		{
			// alertPopup("All desp. qty zero...", 8000);
			// $("#cboParty").focus();
			msgBoxError("Error", "All desp. qty zero...");
			return;
		}
		if( depatchMoreThanPending == 1)
		{
			// alertPopup("Despatching more than pending...", 8000);
			msgBoxError("Error", "Despatching more than pending...");
			depatchMoreThanPending = 0;
			return;
		}
		if( invaliGodownSelected == 1)
		{
			// alertPopup("Invalid godown selected...", 8000);
			msgBoxError("Error", "Invalid godown selected...");
			invaliGodownSelected = 0;
			return;
		}

		partyRowId = $("#cboParty").val();
		if(partyRowId == "-1")
		{
			// alertPopup("Select party...", 8000);
			msgBoxError("Error", "Select party...");
			$("#cboParty").focus();
			return;
		}

		transporterRowId = $("#cboTransporter").val();
		if(transporterRowId == "-1")
		{
			// alertPopup("Select transporter...", 8000);
			msgBoxError("Error", "Select transporter...");
			$("#cboTransporter").focus();
			return;
		}

		var despatchDt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			// alertPopup("Invalid date...", 5000);
			msgBoxError("Error", "Invalid date...");
			$("#dt").focus();
			return;
		}
		var prGrNo = $("#txtPrGrNo").val();
		var remarks = $("#txtRemarks").val();
		var vehicleNo = $("#txtVehicleNo").val();
		var driverName = $("#txtDriverName").val();
		var driverContactNo = $("#txtDriverContactNo").val();
		var driverLicenceNo = $("#txtDriverLicenceNo").val();
		var vehicleInsuranceNo = $("#txtVehicleInsuranceNo").val();

		if($("#btnSave").val() == "Save")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'partyRowId': partyRowId
								, 'despatchDt': despatchDt
								, 'transporterRowId': transporterRowId
								, 'prGrNo': prGrNo
								, 'remarks': remarks
								, 'vehicleNo': vehicleNo
								, 'driverName': driverName
								, 'driverContactNo': driverContactNo
								, 'driverLicenceNo': driverLicenceNo
								, 'vehicleInsuranceNo': vehicleInsuranceNo
								, 'TableData': TableData
							},
					'success': function(data)
					{
						// alert(JSON.stringify(data));
						if( data == "Minus")
						{
							msgBoxError("error",'Cannot insert, In hand qty will be -ve...');
						}
						else
						{
							setTable(data['records']); ///loading records in tbl1
							// alertPopup('Record saved...', 4000);
							msgBoxDone("Done","Record saved...");
							blankControls();
							$("#dt").val(dateFormat(new Date()));
							$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert(globalDespatchVno);
			// return;
			// alert(JSON.stringify(TableData));
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'despatchDt': despatchDt
								, 'transporterRowId': transporterRowId
								, 'prGrNo': prGrNo
								, 'remarks': remarks
								, 'vehicleNo': vehicleNo
								, 'driverName': driverName
								, 'driverContactNo': driverContactNo
								, 'driverLicenceNo': driverLicenceNo
								, 'vehicleInsuranceNo': vehicleInsuranceNo
								, 'globalDespatchVno': globalDespatchVno								
								, 'TableData': TableData
							},
					'success': function(data)
					{
						// alert(data);
						if( data == "Minus")
						{
							// alertPopup('Cannot update, Pending qty will be -ve... <br><br>OR Available qty is less...', 11000);
							msgBoxError("Error", "Cannot update, Pending qty will be -ve... OR Available qty is less...");
						}
						else
						{
							setTable(data['records']); ///loading records in tbl1
							// alertPopup('Record updated...', 8000);
							msgBoxDone("Done","Record updated...");
							blankControls();
							$("#dt").val(dateFormat(new Date()));
							$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
							$("#btnSave").val("Save");
						}
							
					}
			});
		}
		$("#btnShow").attr("disabled", false);
	}

	function loadAllRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}

</script>
<div class="container-fluid" style="width:100%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Despatch</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
	              	?>
	              	<script>
						$( "#dt" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dt").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						// $parties=9;
						echo "<label style='color: black; font-weight: normal;'>Party Name:</label>";
						// echo form_dropdown('cboParty',$parties, '-1', "class='form-control' id='cboParty'");
	              	?>
	              	<select id="cboParty" class="form-control">
		              <option value="-1" addr="" mobile1="" mobile2="" townName="">--- Select ---</option>
		              <?php
		                foreach ($parties as $row) 
		                {
		              ?>
		              <option value=<?php echo $row['partyRowId']; ?>  addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" ><?php echo $row['name']; ?></option>
		              <?php
		                }
		              ?>
		            </select>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='loadData();' value='Show Orders' id='btnShow' class='btn form-control' style='background-color: #FF5733; color:white;'>";
	              	?>

	          	</div>
			</div>

			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:0 dashed lightgray; padding: 10px;height:200px; overflow:auto;">
					<table class='table table-bordered' id='tblProducts' style="table-layout: fixed; " width="100%">
						<tr>
						 	<th width="50" class="text-center" style='display:none;'>QpoRowId</th>
							<th width="150" style='display:none;'>QpoDetailRowId</th>
						 	<th width="50" style='display:none1;'>O.No.</th>
						 	<th style='display:none;'>ProductCatRowId</th>
						 	<th style='display:none;'>ProductCat</th>
						 	<th style='display:none;'>ProductRowId</th>
						 	<th width="150">Product</th>
						 	<th width="70">O.Qty</th>
						 	<th width="70">P.Qty</th>
						 	<th width="70">Des.Qty</th>
						 	<th width="70">P.Qty Now</th>
						 	<th width="70">Rate</th>
						 	<th width="70">Amt</th>
						 	<th  width="70" style='display:none;'>ColourRowId</th>
						 	<th width="70">Colour</th>
						 	<th width="150">Remarks</th>
						 	<th width="70">Order Type</th>
						 	<th width="140">Order Dt.</th>
						 	<th width="140">Commit. Dt.</th>
						 	<th style='display:none;'>Desp. Det. RowId</th>
						 	<th width="40">Godown Ab RowId</th>
						 	<th width="140">Godown</th>
						</tr>
					</table>
				</div>
			</div>

			<div class="row" style="margin-top:10px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						// $transporters=92;
						echo "<label style='color: black; font-weight: normal;'>Transporter:</label>";
						echo form_dropdown('cboTransporter',$transporters, '-1', "class='form-control' id='cboTransporter'");
	              	?>
	          	</div>
	          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>PR/GR No.:</label>";
						echo form_input('txtPrGrNo', '', "class='form-control' id='txtPrGrNo' style='text-transform: capitalize;' maxlength=20 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			
			<div class="row" style="margin-top:10px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Vehicle No.:</label>";
						echo form_input('txtVehicleNo', '', "class='form-control' id='txtVehicleNo' style='text-transform: capitalize;' maxlength=20 autocomplete='off'");
	              	?>
	          	</div>
	          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Driver Name:</label>";
						echo form_input('txtDriverName', '', "class='form-control' id='txtDriverName' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
	              	?>
	          	</div>
			</div>	

			<div class="row" style="margin-top:10px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Driver Contact No.:</label>";
						echo form_input('txtDriverContactNo', '', "class='form-control' id='txtDriverContactNo' style='text-transform: capitalize;' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
	          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Driver Licence No.:</label>";
						echo form_input('txtDriverLicenceNo', '', "class='form-control' id='txtDriverLicenceNo' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			
			<div class="row" style="margin-top:10px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Vehicle Insurance No.:</label>";
						echo form_input('txtVehicleInsuranceNo', '', "class='form-control' id='txtVehicleInsuranceNo' style='text-transform: capitalize;' maxlength=40 autocomplete='off'");
	              	?>
	          	</div>
	          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
						echo form_input('txtRemarks', '', "class='form-control' id='txtRemarks' style='text-transform: capitalize1;' maxlength=255 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			
			<div class="row" style="margin-top:10px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Delete</th>
						<th style='display:none;'>despatchRowid</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th style='display:none;'>TransporterRowId</th>
					 	<th>Transporter</th>
					 	<th>PR/GR No.</th>
					 	<th>Remarks</th>
					 	<th>V.No.</th>
					 	<th>Veh. #</th>
					 	<th>Driver</th>
					 	<th>Contact#</th>
					 	<th>Lic. #</th>
					 	<th>Veh. Ins. #</th>
						<th width="100">VNo</th>					 	
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 //// print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['despatchRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: red;cursor: pointer;cursor: hand;" class="delRecord text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['despatchRowId']."</td>";
						 	$vdt = strtotime($row['despatchDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td style='display:none;'>".$row['transporterRowId']."</td>";
						 	echo "<td>".$row['transporter']."</td>";
						 	echo "<td>".$row['prGrNo']."</td>";
						 	echo "<td>".$row['remarks']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
						 	echo "<td>".$row['vehicleNo']."</td>";
						 	echo "<td>".$row['driverName']."</td>";
						 	echo "<td>".$row['driverContactNo']."</td>";
						 	echo "<td>".$row['driverLicenceNo']."</td>";
						 	echo "<td>".$row['vehicleInsuranceNo']."</td>";
						 	echo "<td>".$row['vNo']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-7 col-sm-7 col-md-7 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10%">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	var globalDespatchVno=0;
	function delrowid(rowid)
	{
		globalrowid = rowid;
		
	}
	$('.delRecord').bind('click', setGlobalDespatchVno);
	function setGlobalDespatchVno()
	{
		globalDespatchVno = $(this).closest('tr').children('td:eq(16)').text();
		// alert(globalDespatchVno);
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();
		// alert(globalrowid);

		$("#dt").val($(this).closest('tr').children('td:eq(3)').text());
		$("#cboParty").val($(this).closest('tr').children('td:eq(4)').text());
		$("#cboTransporter").val($(this).closest('tr').children('td:eq(6)').text());
		// $('#cboParty').trigger('change');
		$("#txtPrGrNo").val($(this).closest('tr').children('td:eq(8)').text());
		$("#txtRemarks").val($(this).closest('tr').children('td:eq(9)').text());
		$("#txtVehicleNo").val($(this).closest('tr').children('td:eq(11)').text());
		$("#txtDriverName").val($(this).closest('tr').children('td:eq(12)').text());
		$("#txtDriverContactNo").val($(this).closest('tr').children('td:eq(13)').text());
		$("#txtDriverLicenceNo").val($(this).closest('tr').children('td:eq(14)').text());
		$("#txtVehicleInsuranceNo").val($(this).closest('tr').children('td:eq(15)').text());
		globalDespatchVno = $(this).closest('tr').children('td:eq(16)').text();
		// return;
		/////Setting Product Detail
		// $('input:checked').each(function() {
		// 	$(this).removeAttr('checked');
		// });
		$.ajax({
			'url': base_url + '/Despatch_Controller/getProductsFromDespatch',
			'type': 'POST', 
			'data':{'rowid':globalrowid},
			'dataType': 'json',
			'success':function(data)
			{
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        // alert(data['products'].length);
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          // cell.innerHTML =  data['products'][i].rowId;
		          cell.innerHTML =  data['products'][i].qpoRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].qpoDetailRowId; ///qpoDetailRowId
		          cell.style.display = "none";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].vNo; // vNo
		          // // // cell.style.display = "none";
		          var cell = row.insertCell(3);
		          cell.innerHTML = ''; //data['products'][i].productCategoryRowId ;
		          cell.style.display = "none";
		          var cell = row.insertCell(4);
		          cell.innerHTML = ''; //data['products'][i].productCategory;
		          cell.style.display = "none";
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].productName;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].qty;
		          cell.style.textAlign = "right";
		          // // cell.style.display = "none";
		          var cell = row.insertCell(8);
		          cell.innerHTML = ''; //data['products'][i].pendingQty;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].despatchQty;
		          cell.style.textAlign = "right";
		          cell.style.color = "red";
		          cell.setAttribute("contentEditable", true);
		          // cell.className = "classDespatchQty";
		          // // cell.style.display = "none";
		          var cell = row.insertCell(10);
		          cell.innerHTML = ''; //data['products'][i].pendingQty;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(11);
		          cell.innerHTML = ''; //data['products'][i].rate;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(12);
		          cell.innerHTML = ''; //data['products'][i].amt;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(13);
		          cell.innerHTML = data['products'][i].colourRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(14);
		          cell.innerHTML = ''; //data['products'][i].colourName;
		          var cell = row.insertCell(15);
		          cell.innerHTML = ''; //data['products'][i].remarks;;
		          var cell = row.insertCell(16);
		          cell.innerHTML = ''; //data['products'][i].orderType;
		          var cell = row.insertCell(17);
		          cell.innerHTML = ''; //dateFormat(new Date(data['products'][i].vDt));
		          var cell = row.insertCell(18);
		          cell.innerHTML = ''; //dateFormat(new Date(data['products'][i].commitmentDate));
		          var cell = row.insertCell(19);
		          cell.innerHTML = data['products'][i].rowId; //despatch Detail RowId
		          cell.style.display = "none";

		          var cell = row.insertCell(20);
		          cell.innerHTML = data['products'][i].godownAbRowId;
		          var cell = row.insertCell(21);
		          cell.innerHTML = "";
		          cell.setAttribute("contentEditable", true);
		          cell.className = "clsGodown";
		        }	
		        $('.classDespatchQty').bind('keyup', doQtyCalculation);
			}
		});
		/////END - Setting Product Detail

		// $("#txtLetterNo").focus();

		// var netInWords = number2text( parseFloat( $("#txtNet").val() ) );
	 //  	$("#txtWords").val( netInWords );
	 	$("#btnShow").attr("disabled", true);
		$("#btnSave").val("Update");
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );

	function loadData()
	{
		var partyRowId = $("#cboParty").val();
		if(partyRowId == "-1")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}
		$.ajax({
			'url': base_url + '/Despatch_Controller/getProducts',
			'type': 'POST', 
			'data':{'partyRowId':partyRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(JSON.stringify(data))
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML =  data['products'][i].qpoRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].rowId; ///qpoDetailRowId
		          cell.style.display = "none";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].vNo;
		          // // cell.style.display = "none";
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].productCategoryRowId ;
		          cell.style.display = "none";
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].productCategory;
		          cell.style.display = "none";
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].productName;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].qty;
		          cell.style.textAlign = "right";
		          // cell.style.display = "none";
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].pendingQty;
		          cell.style.textAlign = "right";
		          cell.style.color = "blue";
		          var cell = row.insertCell(9);
		          cell.innerHTML = 0;
		          cell.style.textAlign = "right";
		          cell.style.color = "red";
		          cell.setAttribute("contentEditable", true);
		          cell.className = "classDespatchQty";
		          // cell.style.display = "none";
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['products'][i].pendingQty;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(11);
		          cell.innerHTML = data['products'][i].rate;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(12);
		          cell.innerHTML = data['products'][i].amt;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(13);
		          cell.innerHTML = data['products'][i].colourRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(14);
		          cell.innerHTML = data['products'][i].colourName;
		          var cell = row.insertCell(15);
		          cell.innerHTML = data['products'][i].remarks;;
		          var cell = row.insertCell(16);
		          cell.innerHTML = data['products'][i].orderType;
		          var cell = row.insertCell(17);
		          cell.innerHTML = dateFormat(new Date(data['products'][i].vDt));
		          var cell = row.insertCell(18);
		          if( data['products'][i].commitmentDate == null)
		          {
		          	cell.innerHTML = "";
		          }
		          else
		          {
			          cell.innerHTML = dateFormat(new Date(data['products'][i].commitmentDate));
			      }
			      var cell = row.insertCell(19);
		          cell.innerHTML = "";
		          cell.style.display = "none";

		          var cell = row.insertCell(20);
		          cell.innerHTML = "";
		          var cell = row.insertCell(21);
		          cell.innerHTML = "";
		          cell.setAttribute("contentEditable", true);
		          cell.className = "clsGodown";
		        }	
		        $('.classDespatchQty').bind('keyup', doQtyCalculation);
		        // $('.clsGodown').bind('keyup', bindItem);
		        bindItem();
			}
		});
	}

	function doQtyCalculation()
	{
	    // var col = $(this).parent().children().index($(this));
	    // var row = $(this).parent().parent().children().index($(this).parent());

	    // var pendingQty = $('#myTable').find('tr:eq('+ (row) +')').find('td:eq(' + (2) + ')').text();
		// alert();
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		var pendingQty = $(this).closest('tr').children('td:eq(8)').text();
		var despatchQty = $(this).closest('tr').children('td:eq(9)').text();
		var pendingNow = parseFloat(pendingQty) - parseFloat(despatchQty);
		pendingNow = pendingNow.toFixed(2);
		$(this).closest('tr').children('td:eq(10)').text( pendingNow );
		// alert(pendingQty);
	}


	// $(document).ready(function(){
	// 	$(function () {
	//         // $('#tblProducts').Scrollable({
	//         //     ScrollHeight: 100
	//         // });
	//     });
	// });



			function bindItem()
		  	{
		  		var select = false;
			  	var defaultText = "";
			    $( ".clsGodown" ).focus(function(){ 
		  			select = false; 
		  			defaultText = $(this).text();
		  		});


				var jSonArray = '<?php echo json_encode($godowns); ?>';
				// jSonArray.push({"name":"Desp. Stage","abRowId":"-2"});

				var obj = JSON.parse(jSonArray);
				// obj['jSonArray'].push({"name":"Desp. Stage","abRowId":"-2"});
				obj.push({"name":"Desp. Stage","abRowId":"-2"});
				jSonArray = JSON.stringify(obj);
				// alert(jSonArray);

				var availableTags = $.map(JSON.parse(jSonArray), function(obj){
							return{
									label: obj.name,
									abRowId: obj.abRowId,
							}
					});

				    $(function() {
			        $( ".clsGodown" ).autocomplete({
			            source: function(request, response) {
			                
			                var aryResponse = [];
			                var arySplitRequest = request.term.split(" ");
			                // alert(JSON.stringify(arySplitRequest));
			                for( i = 0; i < availableTags.length; i++ ) {
			                    var intCount = 0;
			                    for( j = 0; j < arySplitRequest.length; j++ ) {
			                        regexp = new RegExp(arySplitRequest[j], 'i');
			                        var test = JSON.stringify(availableTags[i].label.toLowerCase()).match(regexp);

			                        
			                        if( test ) {
			                            intCount++;
			                        } else if( !test ) {
			                        intCount = arySplitRequest.length + 1;
			                        }
			                        if ( intCount == arySplitRequest.length ) {
			                            aryResponse.push( availableTags[i] );
			                        }
			                    };
			                }
			                response(aryResponse);
			            },
			            select: function (event, ui) {
				      	select = true;
				      	var selectedObj = ui.item; 
					    var abRowId = ui.item.abRowId;
					    var rowIndex = $(this).parent().index();
					    $("#tblProducts").find("tr:eq(" + rowIndex + ")").find("td:eq(20)").text( abRowId );
			        	}

			        }).blur(function() {
				    	var rowIndex = $(this).parent().index();
					    var newText = $("#tblProducts").find("tr:eq(" + rowIndex + ")").find("td:eq(21)").text(); 
					    // $("#txtAddress").val(defaultText + "  " + newText);
					    // alert(select);
						  if( !select && !(defaultText == newText)) 
						  {
						  	$("#tblProducts").find("tr:eq(" + rowIndex + ")").find("td:eq(21)").css("color", "red");
						  	$("#tblProducts").find("tr:eq(" + rowIndex + ")").find("td:eq(20)").text( '-1' );
						  }
						  else
						  {
						  	$("#tblProducts").find("tr:eq(" + rowIndex + ")").find("td:eq(21)").css("color", "black");
						  }
						});	///////////
			        // alert();
			    });





		  	}
</script>