<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptProducts_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 var burl = '<?php echo base_url();?>';
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.display="none";
	          cell.style.textAlign = "center";
	          cell.style.color='green';
	          cell.setAttribute("onmouseover", "this.style.color='lightgray'");
	          cell.setAttribute("onmouseout", "this.style.color='green'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
			  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
			  cell.style.display="none";
	          cell.style.textAlign = "center";
	          cell.style.color='red';
	          cell.setAttribute("onmouseover", "this.style.color='lightgray'");
	          cell.setAttribute("onmouseout", "this.style.color='red'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].productRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].productRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].productCategoryRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].productCategory;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].productLength;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].productHeight;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].productWidth;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].uom;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].productRate;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].imagePath;
	          cell.style.display="none";
	          var cell = row.insertCell(12);
	          if( records[i].imagePathThumb != "N" )
	          {
				  var pic = burl + "bootstrap/images/products/" + records[i].imagePathThumb;
		          cell.innerHTML = "<img id=\'" + records[i].productRowId + "\' src=\'" + pic + "\' width='60px' alt='' />";
	      	  }
	      	  else
	      	  {
	      	  	  cell.innerHTML = "<img src='' width='60px' alt='' />";
	      	  }
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].imagePathThumb;
	          cell.style.display="none";
	  	  }


		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);	
	}


	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	

		productCategoryRowId = $("#cboProductCategories").val();
		if(productCategoryRowId == "-1")
		{
			alertPopup("Select category...", 8000);
			$("#cboProductCategories").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productCategoryRowId': productCategoryRowId
						},
				'success': function(data)
				{
					if(data)
					{
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "productRowId" : $(tr).find('td:eq(2)').text()
		            , "productName" : $(tr).find('td:eq(3)').text()
		            , "productCategoryRowId" :$(tr).find('td:eq(4)').text()
		            , "productCategory" :$(tr).find('td:eq(5)').text()
		            , "length" :$(tr).find('td:eq(6)').text()
		            , "height" :$(tr).find('td:eq(7)').text()
		            , "width" :$(tr).find('td:eq(8)').text()
		            , "uom" :$(tr).find('td:eq(9)').text()
		            , "rate" :$(tr).find('td:eq(10)').text()
		            , "imagePath" :$(tr).find('td:eq(11)').text()
		            , "imagePathThumb" :$(tr).find('td:eq(13)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}


		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
							//// setTable(data['records']) ///loading records in tbl1
							// alertPopup('Record saved...', 4000);
							window.location.href=data;
							// blankControls();
							// $("#txtLetterNo").focus();
							// $("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
							// if(exportIn == "W")
							// {
							// 	loadLimitedRecords();
							// }
					}
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Products List</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
							echo form_dropdown('cboProductCategories',$productCategories, '-1',"class='form-control' id='cboProductCategories'");
		              	?>
		          	</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none;' width="50" class="text-center">Delete</th>
						<th style='display:none;'>productRowId</th>
					 	<th>Product Name</th>
						<th style='display:none;'>productCategoryRowId</th>
					 	<th>P.Category Name</th>
					 	<th>Length</th>
					 	<th>Height</th>
					 	<th>Width</th>
					 	<th>UOM</th>
					 	<th>Rate</th>
					 	<th style='display:none;'>imagePath</th>
					 	<th>image</th>
					 	<th style='display:none;'>imagePathThumb</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

	</div>
</div>





<script type="text/javascript">


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );




	$(document).ready(function() {
	   // $("#cboProductCategories").append('<option value="ALL">ALL</option>');
	   var opt = "<option value='ALL'>ALL</option>";
	   var idx=2;
	   $(opt).insertBefore("#cboProductCategories option:nth-child(" + idx + ")");
	  });
</script>