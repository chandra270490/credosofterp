<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Attendance_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 // alert(records[0].naya);
	      if( records[0].duty == null )
	      {
		      var table = document.getElementById("tbl1");
		      for(i=0; i<records.length; i++)
		      {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);


		          var cell = row.insertCell(0);
		          cell.innerHTML = i+1;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(1);
		          cell.innerHTML = records[i].empRowId;
		          cell.style.backgroundColor="#F0F0F0";
		          // cell.style.display="none";
		      

		          var cell = row.insertCell(2);
		          // cell.style.display="none";
		          cell.innerHTML = records[i].name;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(3);
		          cell.innerHTML = "P";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";

		          var cell = row.insertCell(4);
		          cell.innerHTML = "9:00 AM";
		          cell.setAttribute("contentEditable", true);
		          // cell.style.display="none";

		          var cell = row.insertCell(5);
		          cell.innerHTML = "5:30 PM";
		          cell.setAttribute("contentEditable", true);
		          

		          var cell = row.insertCell(6);
		          cell.innerHTML = "00:00";
		          cell.setAttribute("contentEditable", true);
		          
		          // cell.style.display="none";

		          var cell = row.insertCell(7);
		          cell.innerHTML = "---Select---";
		          cell.setAttribute("contentEditable", true);

		          var cell = row.insertCell(8);
		          cell.innerHTML = "";
		          cell.setAttribute("contentEditable", true);
		  	  }
	  	  }
	  	  else ///// if already marked attendance
	  	  {
	  	  	// alert("Already marked");
		      var table = document.getElementById("tbl1");
		      for(i=0; i<records.length; i++)
		      {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);


		          var cell = row.insertCell(0);
		          cell.innerHTML = i+1;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(1);
		          cell.innerHTML = records[i].empRowId;
		          // cell.style.display="none";
		          cell.style.backgroundColor="#F0F0F0";
		      

		          var cell = row.insertCell(2);
		          // cell.style.display="none";
		          cell.innerHTML = records[i].name;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(3);
		          cell.innerHTML = "P";
		          cell.setAttribute("contentEditable", true);
		          cell.style.display="none";

		          var cell = row.insertCell(4);
		          cell.innerHTML = records[i].tmIn;
		          cell.setAttribute("contentEditable", true);
		          // cell.style.display="none";

		          var cell = row.insertCell(5);
		          cell.innerHTML = records[i].tmOut;
		          cell.setAttribute("contentEditable", true);
		          

		          var cell = row.insertCell(6);
		          cell.innerHTML = records[i].extraHours;
		          cell.setAttribute("contentEditable", true);
		          
		          // cell.style.display="none";

		          var cell = row.insertCell(7);
		          cell.innerHTML = records[i].duty;
		          cell.setAttribute("contentEditable", true);

		          var cell = row.insertCell(8);
		          cell.innerHTML = records[i].remarks;
		          cell.setAttribute("contentEditable", true);
		  	  }
	  	  }
	  	  $('td').on("click focus", setDropDown);
	}

	function loadData()
	{	
		// alert();
		// return;
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}

		// alert(dt);
		// return;
		$.ajax({
			'url': base_url + '/' + controller + '/showData',
			'type': 'POST',
			'dataType': 'json',
			'data': {
						'dt': dt
					},
			'success': function(data)
			{
				if(data)
				{
					// alert(JSON.stringify(data));
					$("#tbl1").find("tr:gt(0)").remove();
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
				}
			}
		});
		
	}


	var tblRowsCount;
	var dutySelected;
	var falseExtra = 0;
	var falseExtraRow = 0;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    dutySelected = 1;
	    var timeFormat = /^([0-9]{2})\:([0-9]{2})$/;
	    
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	extra = $(tr).find('td:eq(6)').text();
	    	var x = timeFormat.test(extra);
	    	// alert(row);
	    	if( x == false && row > 0 )
	    	{
	    		falseExtra = 1;
	    		falseExtraRow = row+1;
	    		return false;
	    	}
        	TableData[i]=
        	{
	            "empRowId" : $(tr).find('td:eq(1)').text()
	            , "tmIn" :$(tr).find('td:eq(4)').text()
	            , "tmOut" :$(tr).find('td:eq(5)').text()
	            , "extraHours" :$(tr).find('td:eq(6)').text()
	            , "duty" :$(tr).find('td:eq(7)').text()
	            , "remarks" :$(tr).find('td:eq(8)').text()
        	}   
        	i++; 
        	if( $(tr).find('td:eq(7)').text() =="---Select---" )
        	{
        		dutySelected = 0;
        		return false;
        	}
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		if ( falseExtra == 1 )
		{
			alertPopup("Extra Hrs. Invlaid Format at row..." + falseExtraRow, 8000);
			falseExtra = 0;
			return;
		}
		// alert(JSON.stringify(TableData));
		// return;
		if( dutySelected == 0 )
		{
			alertPopup("Select all duties...", 8000);
			// $("#cboProducts").focus();
			return;
		}
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("Nothing to save...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dt': dt
						},
				'success': function(data)
				{
					alert('Changes saved...');
					location.reload();
				}
		});
		
	}

</script>
<div class="acontainer">
	
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
				<h2 class="text-center" style='margin-top:-20px'>Mark Attendance.</h2>
				<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
						<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Date:</label>";
								echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
			              	?>
			              	<script>
								$( "#dt" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    // Set the Current Date as Default
								$("#dt").val(dateFormat(new Date()));
							</script>					
			          	</div>
			          	
						<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
								echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
			              	?>
			          	</div>
			          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			          	</div>
				
				</form>
			</div>

			<div class="row" style="margin-top: 20px;">
				<style>
			      table, th, td{border:1px solid gray; padding: 7px;}
			    </style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:500px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
								<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="40" style='display:none1;'>Emp Code</th>
							 	<th width="150" style='display:none1;'>Emp. Name</th>
							 	<th width="50" style='display:none;'>P / A</th>
							 	<th width="100" style='display:none1;'>In</th>
							 	<th width="100">Out</th>
							 	<th width="100">Extra Hrs.(<span style="color:red;">hh:mm</span>)</th>
							 	<th width="100">Duty</th>
							 	<th width="100">Remarks</th>
							 </tr>
						 <tbody>

						 </tbody>
						</table>
					</div>
			</div>

			<div class="row" style="margin-top:20px;margin-bottom:20px;" >
				<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
				</div>

				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<input type='button' onclick='saveData();' value='Save Changes' id='btnLoadAll' class='btn btn-primary form-control'>";
			      	?>
				</div>
			</div>

		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>

<div id="divTmIn" style="display: none;">
  <select class='form-control' id='cboTmIn'> </select>
</div>

<div id="divTmOut" style="display: none;">
  <select class='form-control' id='cboTmOut'> </select>
</div>

<div id="divDuty" style="display: none;">
  <select class='form-control' id='cboDuty'>
  	<option value="---Select---">---Select---</option>
  	<option value="Present">Present</option>
  	<option value="Absent">Absent</option>
  	<option value="Leave">Leave</option>
  	<option value="Night Shift">Night Shift</option>
  	<option value="Mid-Night">Mid-Night</option>
  	<option value="Tour">Tour</option>
  	<option value="Holiday">Holiday</option>
  </select>
</div>

<script type="text/javascript">
	row4Bold = -1;
	var col1 = -1;
	var row1 = -1;
	var colO1 = -1;
	var rowO1 = -1;
	var colDuty1 = -1;
	var rowDuty1 = -1;
	$('td').on("click", setDropDown);
	function setDropDown()
	{
		var col = $(this).parent().children().index($(this));
		var row = $(this).parent().parent().children().index($(this).parent());
	    var xPos = Math.floor($(this).offset().left + 3);
	    var yPos = Math.floor($(this).offset().top + 1);
	    var cellWidth=$(this).innerWidth();
	    var cellHeight=$(this).innerHeight() - 20;
	    var existingCellValue=$('#tbl1').find('tr:eq('+ (row) +')').find('td:eq(' + (col) + ')').text();
	    // alert(existingCellValue);
	    ///Doing Formatting of Name normal
	    var nm = $('#tbl1').find('tr:eq('+ (row4Bold) +')').find('td:eq(' + (2) + ')').text();
	    $('#tbl1').find('tr:eq('+ (row4Bold) +')').find('td:eq(' + (2) + ')').html(nm);

	    ///Doing Formatting of Name Bold
	    var nm = $('#tbl1').find('tr:eq('+ (row) +')').find('td:eq(' + (2) + ')').text();
	    $('#tbl1').find('tr:eq('+ (row) +')').find('td:eq(' + (2) + ')').html('<font color=red>' + nm + '</font>');
	    row4Bold = row;

	    ////for Time In
		if( col == 4 )
		{
			if(row != row1)
			{
				hideCboIn();
			}
		    col1=col;
		    row1=row;
		    $('#divTmIn').css({'top':yPos,'left':xPos, 'position':'absolute', 'border':'0px solid black', 'padding':'0px'});
        	$('#divTmIn').css({'width': cellWidth, 'height': cellHeight, 'margin-left': '0px' });
        	$('#divTmIn').show();
        	$('#cboTmIn').val(existingCellValue);	
        	$('#cboTmIn').focus();
		}
		else if(col != 4 && row != row1)
		{
			hideCboIn();
		}
		else if(col != col1)
		{
			hideCboIn();
		}

	    ////for Time OUT
		if( col == 5 )
		{
			if(row != row1)
			{
				hideCboIn();
			}
		    colO1=col;
		    rowO1=row;
		    $('#divTmOut').css({'top':yPos,'left':xPos, 'position':'absolute', 'border':'0px solid black', 'padding':'0px'});
        	$('#divTmOut').css({'width': cellWidth, 'height': cellHeight, 'margin-left': '0px' });
        	$('#divTmOut').show();
        	$('#cboTmOut').val(existingCellValue);	
        	$('#cboTmOut').focus();
		}
		else if(col != 5 && row != rowO1)
		{
			hideCboOut();
		}
		else if(col != colO1)
		{
			hideCboOut();
		}

		////for Duty
		if( col == 7 )
		{
			if(row != rowDuty1)
			{
				hideCboDuty();
			}
		    colDuty1=col;
		    rowDuty1=row;
		    $('#divDuty').css({'top':yPos,'left':xPos, 'position':'absolute', 'border':'0px solid black', 'padding':'0px'});
        	$('#divDuty').css({'width': cellWidth, 'height': cellHeight, 'margin-left': '0px' });
        	$('#divDuty').show();
        	$('#cboDuty').val(existingCellValue);	
        	$('#cboDuty').focus();
		}
		else if(col != 7 && row != rowDuty1)
		{
			hideCboDuty();
		}
		else if(col != col1)
		{
			hideCboDuty();
		}


	}

	function hideCboIn()
	{
		if( row1 != -1)
		{
			$('#tbl1').find('tr:eq('+ (row1) +')').find('td:eq(' + (col1) + ')').html($("#cboTmIn option:selected").text());
		}
		$('#divTmIn').hide();	
		row1 = -1;
		col1 = -1;
	}
	function hideCboOut()
	{
		if( rowO1 != -1)
		{
			$('#tbl1').find('tr:eq('+ (rowO1) +')').find('td:eq(' + (colO1) + ')').html($("#cboTmOut option:selected").text());
		}
		$('#divTmOut').hide();	
		rowO1 = -1;
		colO1 = -1;
	}
	function hideCboDuty()
	{
		if( rowDuty1 != -1)
		{
			$('#tbl1').find('tr:eq('+ (rowDuty1) +')').find('td:eq(' + (colDuty1) + ')').html($("#cboDuty option:selected").text());
		}
		$('#divDuty').hide();	
		rowDuty1 = -1;
		colDuty1 = -1;
	}

	$('#cboTmIn').keydown( function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 9) 
        {
          e.preventDefault();
          if(e.shiftKey) 
          {
          	if(row1==1)   //in case of 1st row
            {
            	// alert("1");
            	$('#tbl1').find('tr:eq('+ (1) +')').find('td:eq(' + (4) + ')').focus();
            }
            else
            {
            	// alert("n");
            	$('#tbl1').find('tr:eq('+ (row1-1) +')').find('td:eq(' + (8) + ')').focus();
            }
          }
          else
          {
                $('#tbl1').find('tr:eq('+ (row1) +')').find('td:eq(' + (5) + ')').focus();
          }
            // hideCboIn();
        }
    });

    $('#cboTmIn').change( function(e) 
    {    
    	$('#tbl1').find('tr:eq('+ (row1) +')').find('td:eq(' + (col1) + ')').html($("#cboTmIn option:selected").text());
         // hideCboIn();
    });

    $('#cboTmIn').blur( function(e) 
    {    
    	$('#divTmIn').hide();
    });

	$('#cboTmOut').keydown( function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 9) 
        {
          e.preventDefault();
          if(e.shiftKey) 
          {
            {
            	$('#tbl1').find('tr:eq('+ (rowO1) +')').find('td:eq(' + (4) + ')').focus();
            }
          }
          else
          {
                $('#tbl1').find('tr:eq('+ (rowO1) +')').find('td:eq(' + (6) + ')').focus();
          }
            // hideCboIn();
        }
    });

    $('#cboTmOut').change( function(e) 
    {    
    	$('#tbl1').find('tr:eq('+ (rowO1) +')').find('td:eq(' + (colO1) + ')').html($("#cboTmOut option:selected").text());
    });

    $('#cboTmOut').blur( function(e) 
    {    
    	$('#divTmOut').hide();
    });

	$('#cboDuty').keydown( function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 9) 
        {
          e.preventDefault();
          if(e.shiftKey) 
          {
            {
            	$('#tbl1').find('tr:eq('+ (rowDuty1) +')').find('td:eq(' + (6) + ')').focus();
            }
          }
          else
          {
                $('#tbl1').find('tr:eq('+ (rowDuty1) +')').find('td:eq(' + (8) + ')').focus();
          }
            // hideCboIn();
        }
    });

    $('#cboDuty').change( function(e) 
    {    
    	$('#tbl1').find('tr:eq('+ (rowDuty1) +')').find('td:eq(' + (colDuty1) + ')').html($("#cboDuty option:selected").text());
    });

    $('#cboDuty').blur( function(e) 
    {    
    	$('#divDuty').hide();
    });

	function populate(selector) 
	{
		// alert();
	    var select = $(selector);
	    var hours, minutes, ampm;
	    ////////// 1425 is minutes in a day
	    for(var i = 0; i <= 1425; i += 15){
	        hours = Math.floor(i / 60);
	        minutes = i % 60;
	        if (minutes < 10){
	            minutes = '0' + minutes; // adding leading zero
	        }
	        ampm = hours % 24 < 12 ? 'AM' : 'PM';
	        hours = hours % 12;
	        if (hours === 0){
	            hours = 12;
	        }
	        select.append($('<option></option>')
	            .attr('value', hours + ':' + minutes + ' ' + ampm)
	            .text(hours + ':' + minutes + ' ' + ampm)); 
	    }
	}
	populate('#cboTmIn');
	populate('#cboTmOut');



</script>