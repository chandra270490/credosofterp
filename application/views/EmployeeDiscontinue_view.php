<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='EmployeeDiscontinue_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].colourRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");
	          cell.style.display="none";

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].empRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].name;
	          // cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = dateFormat(new Date(records[i].doj));
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].discontinueDt));

	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].discontinueReason;
	          // cell.contentEditable = "true";
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}

	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						// if( data['dependent'] == "yes" )
						// {
						// 	alertPopup('Record can not be deleted... Dependent records exist...', 8000);
						// }
						// else
						{
							setTable(data['records'])
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#txtColourName").focus();
						}
					}
				}
			});
	}


	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						$("#cboEmp").focus();
						$("#dtDOL").val(dateFormat(new Date()));
						
					}
				}
			});
	}

	function saveData()
	{	

		
		var dtDOL = $("#dtDOL").val().trim();
		dtOk = testDate("dtDOL");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtDOL").focus();
			return;
		}
		var reason = $("#txtReason").val();
		// var party = $("#cboParty option:selected").text();
		if($("#btnSave").val() == "Save")
		{
			empRowId = $("#cboEmp").val();
			if(empRowId == "-1")
			{
				alertPopup("Select employee...", 8000);
				$("#cboEmp").focus();
				return;
			}
			$.ajax({
					'url': base_url + '/' + controller + '/saveData',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'empRowId': empRowId
								, 'dtDOL': dtDOL
								, 'reason': reason
							},
					'success': function(data)
					{
						if( data['attendanceLagaiHaiKya'] == "Yes" )
						{
							myAlert('Record can not be fixed... Attendance marked after this date...');
						}
						else
						{
							setTable(data['records']); ///loading records in tbl1
							alertPopup('Record saved...', 4000);
							blankControls();
							$("#dtDOL").val(dateFormat(new Date()));
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert("update");
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'dtDOL': dtDOL
								, 'reason': reason
							},
					'success': function(data)
					{
						if(data)
						{
							// // alert(data);
							// if(data == "Duplicate record...")
							// {
							// 	alertPopup("Duplicate record...", 4000);
							// 	$("#txtColourName").focus();
							// }
							// else
							{
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								blankControls();
								$("#btnSave").val("Save");
								$("#cboEmp").focus();
								$("#dtDOL").val(dateFormat(new Date()));
								$("#cboEmp").prop("disabled", false);
							}
						}
							
					}
			});
		}
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Employee Discontinue</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Employee Name:</label>";
							echo form_dropdown('cboEmp',$employees, '-1',"class='form-control' id='cboEmp'");
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>DOL:</label>";
							echo form_input('dtDOL', '', "class='form-control' placeholder='' id='dtDOL' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtDOL" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtDOL").val(dateFormat(new Date()));
						</script>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Reason:</label>";
							echo form_input('txtReason', '', "class='form-control' placeholder='' id='txtReason' maxlength='100'");
		              	?>
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>Edit</th>
						<th style='display:none;'>Del</th>
						<th style='display:none;'>empRowId</th>
					 	<th>Name</th>
					 	<th>DOJ</th>
						<th>DOL</th>
						<th>DOL Reason</th>
					 </tr>
				 </thead>
				 <tbody>
				 <?php
				 	foreach ($records as $row) 
						{
						 	$rowId = $row['empRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="display:none; color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
							 	   <td style="display:none; color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';

						 	echo "<td style='display:none;'>".$row['empRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	if($row['doj'] == "")
						 	{
						 		echo "<td>".''."</td>";
						 	}
						 	else
						 	{
							 	$vdt = strtotime($row['doj']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
							}

						 	if($row['discontinueDt'] == "")
						 	{
						 		echo "<td>".''."</td>";
						 	}
						 	else
						 	{
							 	$vdt = strtotime($row['discontinueDt']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
						 	}
						 	echo "<td>".$row['discontinueReason']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
			?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
</div>


		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">SB</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>




<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		doj = $(this).closest('tr').children('td:eq(4)').text();
		dol = $(this).closest('tr').children('td:eq(5)').text();
		reason = $(this).closest('tr').children('td:eq(6)').text();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();
		// alert(globalrowid);
		$("#cboEmp").prop("disabled", true);
		$("#dtDOL").val(dol);
		$("#txtReason").val(reason);
		$("#btnSave").val("Update");
	}

	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );



</script>