<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!--  Author: SL -->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>masala/js/jquery.stickytable.min.js"></script>
<link rel='stylesheet' href='<?php  echo base_url();  ?>masala/css/jquery.stickytable.min.css'>
 -->
<script type="text/javascript">
	var controller='OpeningBal_Controller';
	var base_url='<?php echo site_url();?>';
	var totalStages = '<?php echo $totalStages;?>';
	var flagCol = 0;
	var rowsCount = 0;
	function setTable(records)
	{
		 // alert(JSON.stringify(executives));
		  // $("#tbl1").empty();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);

	          var cell = row.insertCell(0);
	          cell.innerHTML = i;
	          cell.style.backgroundColor="#F0F0F0";
	          // cell.className = " sticky-cell";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].productRowId;
	          // cell.className = " sticky-cell";
	          cell.style.backgroundColor="#F0F0F0";
	          cell.style.display = "none";
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].productName;
	          // cell.className = " sticky-cell";
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].productCategoryRowId;
	          // cell.className = " sticky-cell";
	          cell.style.display = "none";
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].productCategory;
	          // cell.className = " sticky-cell";
	          cell.style.display = "none";
	          cell.style.backgroundColor="#F0F0F0";
	          // alert(totalStages);
	          var index = [];
				// build the index
				for (var x in records[i]) {
				   index.push(x);
				}
			// alert(index);
	          for (j = 1; j <= totalStages; j++ ) 
			  {
			  	var cell = row.insertCell(j+4);
			  	cell.innerHTML = records[i][index[j+3]] ;
			  	if(i>0)
			  	{
			  		cell.setAttribute("contentEditable", true);
			  	}
			  	// cell.innerHTML = [index[j+3]] ;
			  }

			  
			  var cell = row.insertCell(j+4);
			  if(i>0)
			  {
				  cell.innerHTML = "0";
				  cell.style.backgroundColor="#F0F0F0";
				  // cell.style.display = "none";
				  flagCol = j+4;
				  // alert(flagCol);
			  }
			  else
			  {
			  	  cell.innerHTML = "1";
				  cell.style.backgroundColor="#F0F0F0";
				  // cell.style.display = "none";
			  }

			  /////hiding 1st row of StageRowId
			  $("#tbl1").find("tr:eq(1)").hide();
	  	  }

	  	  

	  	  $("#tbl1 tr td").on("keyup", function(e){
	  	  	if ( (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) ) 
	  	  	{
	  	  		var rowIndex = $(this).parent().index();
	  	  		$("#tbl1").find("tr:eq("+ rowIndex + ")").find("td:eq("+ flagCol +")").text(1);
	  	  		$("#tbl1").find("tr:eq("+ rowIndex + ")").find("td:eq("+ flagCol +")").css({'color':'red', 'background':'yellow'});
	  	  		rowCount();
	  	  	}

	  	  });

	  	///////Following function to add select TD text on FOCUS
	  	$("#tbl1 tr td").on("focus", function(){
	  		// alert($(this).text());
	  		 var range, selection;
			  if (document.body.createTextRange) {
			    range = document.body.createTextRange();
			    range.moveToElementText(this);
			    range.select();
			  } else if (window.getSelection) {
			    selection = window.getSelection();
			    range = document.createRange();
			    range.selectNodeContents(this);
			    selection.removeAllRanges();
			    selection.addRange(range);
			  }
	  	}); 	
	}

	function rowCount()
	{
		var c=0;
		$("#tbl1 tr").each(function(i, v){
		    if($(this).find("td:eq("+ flagCol +")").text() == '1' )
		    {
		    	c++;
		    }	
		});
		// alert(c);
		if(c>45)
		{
			alert("Enough changes done for this time...");
		}
	}

	var tblRowsCount=0;
	function storeTblValues()
	{
	 	var data = Array();
	 	var p =0 ;
	 	tblRowsCount=0;
		$("#tbl1 tr").each(function(i, v){
		    data[p] = Array();
		    if($(this).find("td:eq("+ flagCol +")").text() == '1' )
		    {
			    $(this).children('td').each(function(ii, vv){
			        data[p][ii] = $(this).text();
			    });
			    p++; 
			    tblRowsCount++;
			}
		})

	    return data;
	}	

	
	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		//// TableData = JSON.stringify(TableData);
		// alert(TableData);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/saveChanges',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
						},
				'success': function(data)
				{
					// alert(data);
					alertPopup("Changes Saved...", 8000);
					$("#tbl1").find("tr:gt(0)").remove();
				},
				'error': function(data)
				{
					alert(data);
				}
		});
	}

	function loadData()
	{
		// alert(rowId);
		var productCategoryRowId = $("#cboProductCategory").val();
		if(productCategoryRowId == "-1")
		{
			alertPopup("Select product category...", 8000);
			$("#cboProductCategory").focus();
			return;
		}
		// alert(productCategoryRowId);
		$.ajax({
			'url': base_url + '/' + controller + '/loadData',
			'type': 'POST',
			'dataType': 'json',
			'data': {'productCategoryRowId': productCategoryRowId},
			'success': function(data)
			{
				// alert(JSON.stringify(data));
				if(data)
				{
					setTable(data['records']);
				}
			}
		});
	}
</script>
<div class="container-fluid" style="margin-top: 10px; width:90%">
	<div class="row"style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style=''>
			<h3 class="text-center" style='margin-top:-20px'>Opening Balance</h3>
			<h5 class="text-center" style='color:red;'>(Max. 50 products at a time) <span style="color:green;">(Dont work on MOBILE PHONE in this module)</span></h5>
			<div class="row" style="margin-top:1px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="margin-top: 10px;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Product Group:</label>";
						echo form_dropdown('cboProductCategory',$ProductCategories, '-1', "class='form-control' id='cboProductCategory'");
					?>
				</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="margin-top: 10px;">
					<label style='color: black; font-weight: normal;'>&nbsp;</label>
					<button id="btnLoadRecords" onclick="loadData();" class='btn btn-primary btn-block'>Load Records</button>
				</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="margin-top: 10px;">
				</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="margin-top: 10px;">
					<label style='color: black; font-weight: normal;'>&nbsp;</label>
					<button id="btnSave" onclick="saveData();" class='btn btn-primary btn-block'>Save Changes</button>
		      	</div>
			</div>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
	<div class="row" style="margin-top: 20px;">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 sticky-table sticky-headers sticky-ltr-cells" id="divTable" style="overflow:auto; height:400px;border-radius:25px;box-shadow:5px 5px #d3d3d3">
			<table style="table-layout: fixed1; border: 1px solid lightgrey;" id='tbl1' class="table table-bordered">
	           <thead>
			 	  <?php 
						///loading price types
					echo "<tr>";
					echo "<th style='background-color:#F0F0F0;'>S.N.</th>";
					echo "<th style='display:none;background-color:#F0F0F0;'>Product RowId</th>";
					echo "<th style='background-color:#F0F0F0;'>Product Name</th>";
					echo "<th style='display:none;background-color:#F0F0F0;'>Category RowId</th>";
					echo "<th style='display:none;background-color:#F0F0F0;'>Category Name</th>";
					foreach ($stages as $row) 
					{
						echo "<th style='text-align:left;background-color:#F0F0F0;'>".$row['stageName']."</th>";
					}
					echo "<th style='display:none1;background-color:#F0F0F0;'>Flag</th>";

					echo "</tr>";
				  ?>
				</thead>
          </table>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>

