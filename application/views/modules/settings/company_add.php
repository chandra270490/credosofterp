<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Company Add</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Add Company</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/settingsc/company_entry">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Franchisor Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_fra_name" name="comp_fra_name" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_email" name="comp_email" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_name" name="comp_name" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Logo</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" id="comp_logo" name="comp_logo" size="33" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Website</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_website" name="comp_website" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_phone" name="comp_phone" 
                                value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address Line 1</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_addr1" name="comp_addr1" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address Line2</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_addr2" name="comp_addr2" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">City / District</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_city" name="comp_city" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">State / Province</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="comp_state" name="comp_state" required>
                                    <?php
                                        $sql_state = "select * from state_mst";
                                        $qry_state = $this->db->query($sql_state);
                                        
                                        foreach($qry_state->result() as $row){
                                            $state_name = $row->state_name;
                                    ?>
                                    <option value="<?php echo $state_name; ?>"><?php echo $state_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Postal Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comp_postcode" name="comp_postcode" 
                                value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="comp_country" name="comp_country" required>
                                    <?php
                                        $sql_country = "select * from country_mst";
                                        $qry_country = $this->db->query($sql_country);
                                        
                                        foreach($qry_country->result() as $row){
                                            $country_name = $row->country_name;
                                    ?>
                                    <option value="<?php echo $country_name; ?>"><?php echo $country_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//select 2 box
$( function(){
    $("#comp_country").select2();	
    $("#comp_state").select2();	
});

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>