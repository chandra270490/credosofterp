<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Update Statistics</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Update Statistics</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/reportsc/stat_updt_entry">
                        <div class="form-group">
                            <div class="table-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered" id="item_tbl">
                                            <thead>
                                                <tr style="font-weight:bold">
                                                    <th>Store Name</th>
                                                    <th>Sales</th>
                                                    <th>Customers</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $sql_stores = "select * from store_mst where store_enabled='1'";
                                                $qry_stores = $this->db->query($sql_stores);
                                                foreach($qry_stores->result() as $row){
                                                    $store_no = $row->store_no;
                                                    $store_name = $row->store_name;
                                            ?>
                                                <tr>
                                                    <td>
                                                        <?=$store_name;?>
                                                        <input type="hidden" class="form-control" id="store_no" name="store_no[]" value="<?=$store_no;?>">
                                                        <input type="hidden" class="form-control" id="store_name" name="store_name[]" value="<?=$store_name;?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" id="sales" name="sales[]" value="0" onkeypress="return isNumberKey(event);">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" id="customers" name="customers[]"  value="0" onkeypress="return isNumberKey(event);">
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>   
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>