<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Products List</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <!-- Add Row Button -->    
    <!-- View Records -->
    <div class="row">
        <div class="col-lg-12">
        	<table class="table table-bordered">
                <!-- ListHead Starts -->
                <thead>
                    <tr>
                        <th>SNo</th>
                        <th>Product Id</th>
                        <th>Product Name</th>
                        <th>Product Category</th>
                    </tr>
                </thead>
                <!-- ListHead Ends -->
                <!-- ListBody Starts -->
                <tbody>
                    <?php
                        $sql_tbl_val = "SELECT * FROM products_mst where prod_cat_name != '' order by prod_cat_name";
                        $qry_tbl_val = $this->db->query($sql_tbl_val);

                        $sno=0;
                        foreach($qry_tbl_val->result() as $row){
                            $sno++;
                    ?>
                    <tr>
                        <td><?php echo $sno; ?></td>
                        <td><?php echo $row->prod_id; ?></td>
                        <td><?php echo $row->prod_name; ?></td>
                        <td><?php echo $row->prod_cat_name; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <!-- ListBody Ends -->
            </table>
        </div>
    </div>
  </section>
</section>