<?php $this->load->helper("franchisees_inq"); ?>
<style>
.info-box {
    min-height: 140px;
    margin-bottom: 30px;
    padding: 20px;
    color: white;
    -webkit-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
    box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
}

.green-bg {
    color: #fff;
    background: #86bc24;
    background-color: rgb(134, 188, 36);
    background-color: #86bc24;
    border-radius: 5px;
}

.info-box .count {
    margin-top: 20px;
    font-size: 20px;
    font-weight: 700;
}

.info-box .title {
    font-size: 14px;
    text-transform: uppercase;
    font-weight: 600;
}
</style>
<section id="main-content">
  <section class="wrapper">
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Inquiries Dashboard</h3>
        </div>
    </div><br>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Fresh Inquiry">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Fresh Inquiry'); ?></div>
          <div class="title">Fresh Inquiry</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Under Followup">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Under Followup'); ?></div>
          <div class="title">Under Followup</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Under Process">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Under Process'); ?></div>
          <div class="title">Under Process</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Converted">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Converted'); ?></div>
          <div class="title">Converted</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Lost">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Lost'); ?></div>
          <div class="title">Lost</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Dropped">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Dropped'); ?></div>
          <div class="title">Dropped</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->

  </section>
</section>