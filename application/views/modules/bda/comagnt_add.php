<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Add Commission Agent</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Add Commission Agent</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" 
                    action="<?php echo base_url(); ?>index.php/bdac/comagnt_entry">
                    <div class="form-group required">
                            <label class="col-sm-2 control-label">Agent Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_name" name="comagnt_name" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_phone" name="comagnt_phone" 
                                value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_email" name="comagnt_email" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_compname" name="comagnt_compname" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Address Line 1</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_addr1" name="comagnt_addr1" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Address Line2</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_addr2" name="comagnt_addr2" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">City / District</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_city" name="comagnt_city" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">State / Province</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="comagnt_state" name="comagnt_state" required>
                                    <?php
                                        $sql_state = "select * from state_mst";
                                        $qry_state = $this->db->query($sql_state);
                                        
                                        foreach($qry_state->result() as $row){
                                            $state_name = $row->state_name;
                                    ?>
                                    <option value="<?php echo $state_name; ?>"><?php echo $state_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Postal Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_postcode" name="comagnt_postcode" 
                                value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="comagnt_country" name="comagnt_country" required>
                                    <?php
                                        $sql_country = "select * from country_mst";
                                        $qry_country = $this->db->query($sql_country);
                                        
                                        foreach($qry_country->result() as $row){
                                            $country_name = $row->country_name;
                                    ?>
                                    <option value="<?php echo $country_name; ?>"><?php echo $country_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Adhar No.</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_adhar" name="comagnt_adhar" 
                                value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">PAN No.</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_pan" name="comagnt_pan" 
                                value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Bank Account Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_bankacname" name="comagnt_bankacname" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Bank Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_bankname" name="comagnt_bankname" 
                                value="" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Bank Account No.</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_accountno" name="comagnt_accountno" 
                                value="" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Bank IFSC Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="comagnt_ifsc" name="comagnt_ifsc" 
                                value="" required>
                            </div>
                        </div>                        

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//select 2 box
$( function(){
    $("#bda_country").select2();	
    $("#bda_state").select2();	
});

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>