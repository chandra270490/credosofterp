<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='SalesReturn_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display = 'none';

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          // cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].ciRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].ciRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].ciDt));
	          // cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].totalAmt;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].discountPer;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].discountAmt;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].totalAfterDiscount;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].vatPer;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].vatAmt;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].sgstPer;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].sgstAmt;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].cgstPer;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].cgstAmt;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].igstPer;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].igstAmt;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].net;
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].totalQty;	
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].roundAmt;	
	          var cell = row.insertCell(21);
	          cell.innerHTML = records[i].cpe;	
	          var cell = row.insertCell(22);
	          cell.innerHTML = records[i].vNo;	
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
        $("#tbl1 tr").find("td:gt(1)").on('click', showDetails);
		
	}




	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						setTable(data['records']);
						alertPopup('Record deleted...', 4000);
						blankControls();
						$("#cboParty").focus();
						$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
					}
				}
			});
	}
	

	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "productRowId" : $(tr).find('td:eq(1)').text()
		            , "productName" : $(tr).find('td:eq(3)').text()
		            , "rate" :$(tr).find('td:eq(4)').text()
		            , "qty" :$(tr).find('td:eq(5)').text()
		            , "amt" :$(tr).find('td:eq(6)').text()
		            , "colourRowId" :$(tr).find('td:eq(7)').text()
		            , "colour" :$(tr).find('td:eq(8)').text()
		            , "remarks" :$(tr).find('td:eq(9)').text()
		            , "stageRowId" :$(tr).find('td:eq(10)').text()
		            , "stageName" :$(tr).find('td:eq(11)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i-1;
	    return TableData;
	}


	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		if(partyRowId == "-1")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}

		var vDt = $("#dtS").val().trim();
		// if(vDt !="")
		// {
			dtOk = testDate("dtS");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtS").focus();
				return;
			}
		// }
		var returnType = $("#cboReturnTypes option:selected").text();

		totalAmt = $("#txtTotalAmt").val();
		discountPer = $("#txtDiscountPer").val();
		discountAmt = $("#txtDiscountAmt").val();
		totalAfterDiscount = $("#txtTotalAfterDiscount").val();
		vatPer = $("#txtVatPer").val();
		vatAmt = $("#txtVatAmt").val();
		sgstPer = $("#txtSgstPer").val();
		sgstAmt = $("#txtSgstAmt").val();
		cgstPer = $("#txtCgstPer").val();
		cgstAmt = $("#txtCgstAmt").val();
		igstPer = $("#txtIgstPer").val();
		igstAmt = $("#txtIgstAmt").val();
		net = $("#txtNet").val();
		totalQty = $("#txtTotalQty").val();
		addr = $("#txtAddress").val().trim();
		inWords = $("#txtWords").val().trim();

		exportIn = $("#cboExport").val();
		// alert(exportIn);
		
		if(exportIn == "-1")
		{
			alertPopup("Select export in...", 8000);
			$("#cboExport").focus();
			return;
		}

		if($("#btnSave").val() == "Save")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					// 'dataType': 'json',
					'data': {
								'partyRowId': partyRowId
								, 'returnType': returnType
								, 'vDt': vDt
								, 'totalQty': totalQty
								, 'totalAmt': totalAmt
								, 'discountPer': discountPer
								, 'discountAmt': discountAmt
								, 'totalAfterDiscount': totalAfterDiscount
								, 'vatPer': vatPer
								, 'vatAmt': vatAmt
								, 'sgstPer': sgstPer
								, 'sgstAmt': sgstAmt
								, 'cgstPer': cgstPer
								, 'cgstAmt': cgstAmt
								, 'igstPer': igstPer
								, 'igstAmt': igstAmt
								, 'net': net
								, 'addr': addr
								, 'inWords': inWords
								, 'exportIn': exportIn
								, 'TableData': TableData
							},
					'success': function(data)
					{
						if(data)
						{
								alertPopup('Record saved...', 4000);
								window.location.href=data;
								blankControls();
								$("#cboParty").focus();
								$("#tblProducts").find("tr:gt(0)").remove();  //empty except 
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert(JSON.stringify(TableData));
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					// 'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'partyRowId': partyRowId
								, 'vDt': vDt
								, 'totalQty': totalQty
								, 'totalAmt': totalAmt
								, 'discountPer': discountPer
								, 'discountAmt': discountAmt
								, 'totalAfterDiscount': totalAfterDiscount
								, 'vatPer': vatPer
								, 'vatAmt': vatAmt
								, 'sgstPer': sgstPer
								, 'sgstAmt': sgstAmt
								, 'cgstPer': cgstPer
								, 'cgstAmt': cgstAmt
								, 'igstPer': igstPer
								, 'igstAmt': igstAmt
								, 'net': net
								, 'addr': addr
								, 'inWords': inWords
								, 'exportIn': exportIn
								, 'TableData': TableData
							},
					'success': function(data)
					{
						if(data)
						{
								alertPopup('Record updated...', 4000);
								window.location.href=data;
								blankControls();
								$("#cboParty").focus();
								$("#tblProducts").find("tr:gt(0)").remove(); // empty except 1st (head) 	
						}
						$("#cboProducts").attr("disabled", false);
						$("#cboStage").attr("disabled", false);

					}
			});
		}
	}

	function loadAllRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}
	function loadLimitedRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadLimitedRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						// alertPopup('Records loaded...', 4000);
						// blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}
</script>
<div class="container-fluid" style="width: 100%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px'>Sales Return</h3>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="row">
						<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Date:</label>";
								echo form_input('dtS', '', "class='form-control' placeholder='' id='dtS' maxlength='10'");
			              	?>
			              	<script>
								$( "#dtS" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    // Set the Current Date as Default
								$("#dtS").val(dateFormat(new Date()));
							</script>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Party Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>";
			              	?>
					        <select id="cboParty" class="form-control">
					              <option value="-1" addr="" mobile1="" mobile2="" townName="" stateName="">--- Select ---</option>
					              <?php
					                foreach ($parties as $row) 
					                {
					              ?>
					              <option value=<?php echo $row['partyRowId']; ?>  addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" stateName="<?php echo $row['stateName']; ?>" ><?php echo $row['name']; ?></option>
					              <?php
					                }
					              ?>
						    </select>
				            <script type="text/javascript">
					            $(document).ready(function()
					            {
					              $("#cboParty").change(function()
					              {
								        $("#txtAddress").val($('option:selected', '#cboParty').attr('addr'));
								        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('townName') + " [" + $('option:selected', '#cboParty').attr('stateName') + "]");
								        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('mobile1'));
								        $("#txtAddress").val( $("#txtAddress").val() + ", " + $('option:selected', '#cboParty').attr('mobile2'));

								        if( $('option:selected', '#cboParty').attr('stateName').toUpperCase() == "RAJASTHAN" )
								        {
								        	$("#txtSgstPer").val("9");
								        	$("#txtCgstPer").val("9");
								        	$("#txtIgstPer").val("0");
								        }
								        else
								        {
								        	$("#txtSgstPer").val("0");
								        	$("#txtCgstPer").val("0");
								        	$("#txtIgstPer").val("18");
								        }
								        doTotals();
					              });
					            });
					        </script>	
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Return Type:</label>";
								echo form_dropdown('cboReturnTypes',$orderTypes, '-1', "class='form-control' id='cboReturnTypes'");
			              	?>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Address:</label>";
						echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px;' id='txtAddress'  maxlength='255' value=''");
	              	?>
				</div>
			</div>

			<hr />
			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" >
					<?php
						echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
						echo form_dropdown('cboProductCategories',$productCategories, '-1', "class='form-control' id='cboProductCategories'");
	              	?>

	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						$products="--- Select ---";
						echo "<label style='color: black; font-weight: normal;'>Products:</label>";
						echo form_dropdown('cboProducts',$products, '-1', "class='form-control' id='cboProducts'");
	              	?>
	              	<script type="text/javascript">
			            $(document).ready(function()
			            {
			              $("#cboProducts").change(function()
			              {
						        $("#txtRate").val($('option:selected', '#cboProducts').attr('rate'));
						        $("#txtSize").val($('option:selected', '#cboProducts').attr('productLength'));
						        $("#txtSize").val( $("#txtSize").val() + ' x ' + $('option:selected', '#cboProducts').attr('productWidth') );
						        $("#txtSize").val( $("#txtSize").val() + ' x ' + $('option:selected', '#cboProducts').attr('productHeight') );
						        $("#txtSize").val( $("#txtSize").val() + ' ' + $('option:selected', '#cboProducts').attr('uom') );
			              });
			            });
			        </script>	
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Rate:</label>";
						echo '<input type="number" step="0" name="txtRate" value="0" placeholder="" class="form-control" maxlength="20" id="txtRate" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Qty:</label>";
						echo '<input type="number" step="1" name="txtQty" value="1" placeholder="" class="form-control" maxlength="20" id="txtQty" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Amt.:</label>";
						echo '<input type="number" step="1" name="txtAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtAmt" />';
	              	?>
	          	</div>	          	
			</div>

			<div class="row" style="margin-top:15px;">

				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Size:</label>";
						echo '<input type="text" disabled name="txtSize" placeholder="" class="form-control" maxlength="40" id="txtSize" />';
	              	?>
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						$loc = base_url() . "index.php/Colours_Controller";
						echo "<a target='_blank' href=". $loc ."> <label style='color: black;'>Colour:</a></label> &nbsp;&nbsp;<label id='lblRefreshColours' class='glyphicon glyphicon-refresh' style='color: green; font-weight: normal;'></label>";
						echo form_dropdown('cboColour',$colours, '-1', "class='form-control' id='cboColour'");
	              	?>
	          	</div>
				
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						// $placements=9;
						echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
						echo '<input type="text" name="txtRemarks" placeholder="" class="form-control" maxlength="255" id="txtRemarks" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Return Stage:</label>";
					?>
					<select id="cboStage" class="form-control">
		              <option value="-1" weightage="">--- Select ---</option>
		              <?php
		                foreach ($stages as $row) 
		                {
		              ?>
		              <option value=<?php echo $row['stageRowId']; ?>  weightage="<?php echo $row['weightage']; ?>" ><?php echo $row['stageName']; ?></option>
		              <?php
		                }
		              ?>
		              <option value="-2" weightage="">NONE</option>
		              <option value="-3" weightage="">DAMAGED</option>
		              <option value="-4" weightage="">ADD IN CREDO STOCK</option>
	            	</select>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='confirm();' value='Confirm Product' id='btnConfirm' class='btn form-control' style='background-color: #FF5733; color:white;'>";
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:0;">
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
				</div>
				
			</div>


			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:0 dashed lightgray; padding: 10px;height:200px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
					<table class='table table-bordered' id='tblProducts'>
						<tr>
						 	<th  width="50" class="text-center">Delete</th>
							<th style='display:none;'>productRowId</th>
						 	<th style='display:none;'>Product Category</th>
						 	<th>Product</th>
						 	<th>Rate</th>
						 	<th>Qty</th>
						 	<th>Amt</th>
						 	<th style='display:none;'>ColourRowId</th>
						 	<th>Colour</th>
						 	<th>Remarks</th>
						 	<th style='display:none;'>StageRowId</th>
						 	<th>Stage</th>
						</tr>
					</table>
				</div>
			</div>

			<hr />
			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Amt.:</label>";
						echo '<input type="number" disabled name="txtTotalAmt" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Qty.:</label>";
						echo '<input type="number" disabled name="txtTotalQty" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalQty" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Discount (%):</label>";
						echo '<input type="number" step="1" name="txtDiscountPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtDiscountPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Discount Amt.:</label>";
						echo '<input type="number" step="1" name="txtDiscountAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtDiscountAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total After Discount.:</label>";
						echo '<input type="number" disabled name="txtTotalAfterDiscount" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalAfterDiscount" />';
	              	?>
	          	</div>
	          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>SGST(%):</label>";
						echo '<input type="number" step="1" name="txtSgstPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtSgstPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>SGST Amt.:</label>";
						echo '<input type="number" disabled name="txtSgstAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtSgstAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>CGST(%):</label>";
						echo '<input type="number" step="1" name="txtCgstPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtCgstPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>CGST Amt.:</label>";
						echo '<input type="number" disabled name="txtCgstAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtCgstAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>IGST(%):</label>";
						echo '<input type="number" step="1" name="txtIgstPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtIgstPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>IGST Amt.:</label>";
						echo '<input type="number" disabled name="txtIgstAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtIgstAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Net Amt.:</label>";
						echo '<input type="number" disabled name="txtNet" value="" placeholder="" class="form-control" maxlength="20" id="txtNet" />';
	              	?>
	          	</div>
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>In Words:</label>";
						echo '<input type="text" disabled name="txtWords" value="" placeholder="" class="form-control" id="txtWords" />';
	              	?>
	          	</div>
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						$types = array();
						// $types['-1'] = '--- Select ---';
						// $types['W'] = "Word";
						$types['P'] = "PDF";
						echo "<label style='color: black; font-weight: normal;'>Export in: <span style='color: red;'>*</span></label>";
						echo form_dropdown('cboExport', $types, 'P', "class='form-control' id='cboExport'");
					?> 
	          	</div>
			</div>
		
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="display: none;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>VAT (%):</label>";
						echo '<input type="number" step="1" name="txtVatPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtVatPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12" style="display: none;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>VAT Amt.:</label>";
						echo '<input type="number" disabled name="txtVatAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtVatAmt" />';
	              	?>
	          	</div>
	          	<!-- NAYA TAX -->
				
			</div>

		
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
	          	</div>
				
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th style='display:none1;' width="50" class="text-center">Delete</th>
						<th style='display:none;'>ciRowid</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th>Total Amt</th>
					 	<th>Dis. Per.</th>
					 	<th>Dis. Amt.</th>
					 	<th>Amt. After Dis.</th>
					 	<th>VAT %</th>
					 	<th>VAT Amt</th>
					 	<th>SGST %</th>
					 	<th>SGST Amt</th>
					 	<th>CGST %</th>
					 	<th>CGST Amt</th>
					 	<th>IGST %</th>
					 	<th>IGST Amt</th>
					 	<th>Net</th>
					 	<th>Total Qty.</th>
					 	<th>Round</th>
					 	<th>CPE</th>
					 	<th>vNo</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
					 // print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['ciRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="display:none;color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td  style="display:none1; color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none;'>".$row['ciRowId']."</td>";
						 	$vdt = strtotime($row['ciDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['totalAmt']."</td>";
						 	echo "<td>".$row['discountPer']."</td>";
						 	echo "<td>".$row['discountAmt']."</td>";
						 	echo "<td>".$row['totalAfterDiscount']."</td>";
						 	echo "<td>".$row['vatPer']."</td>";
						 	echo "<td>".$row['vatAmt']."</td>";
						 	echo "<td>".$row['sgstPer']."</td>";
						 	echo "<td>".$row['sgstAmt']."</td>";
						 	echo "<td>".$row['cgstPer']."</td>";
						 	echo "<td>".$row['cgstAmt']."</td>";
						 	echo "<td>".$row['igstPer']."</td>";
						 	echo "<td>".$row['igstAmt']."</td>";
						 	echo "<td>".$row['net']."</td>";
						 	echo "<td>".$row['totalQty']."</td>";
						 	echo "<td>".$row['roundAmt']."</td>";
						 	echo "<td>".$row['cpe']."</td>";
						 	echo "<td>".$row['vNo']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
					<table class='table table-stripped' id='tblDetail'>
						<tr>
						 	<th style='display:none;' class="text-center">Delete</th>
							<th style='display:none;'>productRowId</th>
						 	<th style='display:none;'>Product Category</th>
						 	<th>Product</th>
						 	<th>Rate</th>
						 	<th>Qty</th>
						 	<th>Amt</th>
						 	<th style='display:none;'>ColourRowId</th>
						 	<th>Colour</th>
						 	<th>Remarks</th>
						 	<th style='display:none;'>StageRowId</th>
						 	<th>Stage</th>
						</tr>
					</table>
				</div>
		</div>
	</div>

	<div class="row" style="margin-top:20px; margin-bottom: 20px;" >
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<input type='button' onclick='reprint();' value='Reprint' id='btnLoadAll' class='btn  btn-danger form-control'>";
	      	?>
		</div>
	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();
		// alert(globalrowid);

		$("#dtS").val($(this).closest('tr').children('td:eq(3)').text());
		$("#cboParty").val($(this).closest('tr').children('td:eq(4)').text());
		$('#cboParty').trigger('change');
		$("#txtTotalAmt").val($(this).closest('tr').children('td:eq(6)').text());
		$("#txtDiscountPer").val($(this).closest('tr').children('td:eq(7)').text());
		$("#txtDiscountAmt").val($(this).closest('tr').children('td:eq(8)').text());
		$("#txtTotalAfterDiscount").val($(this).closest('tr').children('td:eq(9)').text());
		$("#txtVatPer").val($(this).closest('tr').children('td:eq(10)').text());
		$("#txtVatAmt").val($(this).closest('tr').children('td:eq(11)').text());
		$("#txtSgstPer").val($(this).closest('tr').children('td:eq(12)').text());
		$("#txtSgstAmt").val($(this).closest('tr').children('td:eq(13)').text());
		$("#txtCgstPer").val($(this).closest('tr').children('td:eq(14)').text());
		$("#txtCgstAmt").val($(this).closest('tr').children('td:eq(15)').text());
		$("#txtIgstPer").val($(this).closest('tr').children('td:eq(16)').text());
		$("#txtIgstAmt").val($(this).closest('tr').children('td:eq(17)').text());
		$("#txtNet").val($(this).closest('tr').children('td:eq(18)').text());
		$("#txtTotalQty").val($(this).closest('tr').children('td:eq(19)').text());
		// $("#txtRound").val($(this).closest('tr').children('td:eq(14)').text());
		var cpe = $(this).closest('tr').children('td:eq(20)').text();
		if ( cpe == "Y" )
		{
			$("#chkCpe").prop("checked", true);
		}
		else
		{
			$("#chkCpe").prop("checked", false);
		}

		var netInWords = number2text( parseFloat( $("#txtNet").val() ) );
	  	$("#txtWords").val( netInWords );
	  	// alert(globalrowid);
		$.ajax({
			'url': base_url + '/SalesReturn_Controller/getProducts',
			'type': 'POST', 
			'data':{'rowid':globalrowid},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(JSON.stringify(data));
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          // cell.innerHTML =""onClick='editProduct(this);'
		          cell.innerHTML =  "<span  style='color: green;cursor: pointer;cursor: hand;' onmouseover=\"this.style.color=\'lightgray\';\"  onmouseout=\"this.style.color=\'green\';\" class='editThisProduct glyphicon glyphicon-pencil'></span>";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].productCategory;
		          cell.style.display = "none";
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].productName + " (" + data['products'][i].productLength + " x " + data['products'][i].productWidth + " x " + data['products'][i].productHeight + " " + data['products'][i].uom + ")";
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].rate;
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].qty;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].amt;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].colourRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].colourName;
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].remarks;
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['products'][i].stageRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(11);
		          cell.innerHTML = data['products'][i].stageName;
		        }	
		        i=0;
		        $('#tblProducts tr').each(function (i) 
                {
                  if(i>0) //Excluding header row 
                  {
                    $("td", this).eq(0).bind("click", editProduct);
                  }
                }); 
			}
		});


		// $("#btnShow").prop('disabled', true);
		$("#cboParty").prop('disabled', true);
		$("#cboProducts").attr("disabled", true);
		$("#cboStage").attr("disabled", true);
		$("#btnSave").val("Update");
		
	}

	// $('.editThisProduct').bind('click', editProduct);
	var editingProductFlag = 0;
	var editingProductRowId = 0;
	function editProduct()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		editingProductRowId = $(this).closest('tr').children('td:eq(1)').text();
		productCategoryRowId = $(this).closest('tr').children('td:eq(2)').text();

		// $("#cboProducts").val( $(this).closest('tr').children('td:eq(1)').text() );
		$("#txtRate").val( $(this).closest('tr').children('td:eq(4)').text() );
		$("#txtQty").val( $(this).closest('tr').children('td:eq(5)').text() );
		$("#txtAmt").val( $(this).closest('tr').children('td:eq(6)').text() );
		$("#cboColour").val( $(this).closest('tr').children('td:eq(7)').text() );
		$("#txtRemarks").val( $(this).closest('tr').children('td:eq(9)').text() );
		$("#cboStage").val( $(this).closest('tr').children('td:eq(10)').text() );
		editingProductFlag = 1;

		// $("#cboProducts").val( productRowId );
		// alert(productCategoryRowId);
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );



      $(document).ready(function(){
        $("#cboProductCategories").change(function(){
          var productCategoryRowId = $("#cboProductCategories").val();
          if(productCategoryRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductList',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productCategoryRowId': productCategoryRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                  // alert(data);
                  $('#cboProducts').empty();
                  if(data['products'] != null) 
                  {
                    var options = "<option value='-1' rate='0' productLength='0' productWidth='0' productHeight='0' uom='-1'>" + "--- Select ---" + "</option>";
                    for(var i=0; i<data['products'].length; i++)
                    {
                      options += "<option value=" + data['products'][i].productRowId + " rate=" + data['products'][i].productRate + " productLength=" + data['products'][i].productLength + " productWidth=" + data['products'][i].productWidth + " productHeight=" + data['products'][i].productHeight + " uom=" + data['products'][i].uom + ">" + data['products'][i].productName + "</option>";
                    }
                    $('#cboProducts').append(options);

                  }
                  else
                  {
                  }

                }
              }
          });
        });
      });
              

	$(document).ready(function()
    {
      $("#txtRate").on('keyup change', calcAmt);
      $("#txtQty").on('keyup change', calcAmt);
      $("#cboProducts").on('change', calcAmt);
      $("#txtDiscountPer").on('keyup change', doDiscount);
      $("#txtDiscountAmt").on('keyup change', doDiscountOnAmtChange);
      $("#txtVatPer").on('keyup change', doTax);
      $("#txtSgstPer").on('keyup change', doTax);
      $("#txtCgstPer").on('keyup change', doTax);
      $("#txtIgstPer").on('keyup change', doTax);
      // $("#txtAdvance").on('keyup change', doAdvance);

    });
    function calcAmt()
    {
      	var rate = $("#txtRate").val();
      	var qty = $("#txtQty").val();
      	var amt = (rate * qty);
	    $("#txtAmt").val(amt.toFixed(2));
    }


	  function confirm() 
	  {
	  	if( editingProductFlag == 1)
	  	{
	  	  var rate = $("#txtRate").val();
	      var qty = $("#txtQty").val();
	      var amt = $("#txtAmt").val();
	      var colourRowId = $("#cboColour").val();
	      if(colourRowId == "-1")
	      {
	      	alertPopup("Select colour...", 6000);
	      	$("#cboColour").focus();
	      	return;
	      }
	      var colour = $("#cboColour option:selected").text();
	      var stageRowId = $("#cboStage").val();
	      if(stageRowId == "-1")
	      {
	      	alertPopup("Select stage...", 6000);
	      	$("#cboStage").focus();
	      	return;
	      }
	      var stageName = $("#cboStage option:selected").text();
	      var remarks = $("#txtRemarks").val().trim();
	      $('#tblProducts tr').each(function(row, tr)
		  {
		    	if ( parseInt($(tr).find('td:eq(1)').text()) == parseInt(editingProductRowId) )
		    	{
		    		$(tr).find('td:eq(4)').text(rate);
		    		$(tr).find('td:eq(5)').text(qty);
		    		$(tr).find('td:eq(6)').text(amt);
		    		$(tr).find('td:eq(7)').text(colourRowId);
		    		$(tr).find('td:eq(8)').text(colour);
		    		$(tr).find('td:eq(9)').text(remarks);
		    		$(tr).find('td:eq(10)').text(stageRowId);
		    		$(tr).find('td:eq(11)').text(stageName);
			    }
		   }); 
	      // $("#cboProductCategories").val('-1');
	      $("#cboProducts").val('-1');
	      $("#txtRate").val('0');
	      $("#txtQty").val('0');
	      $("#txtAmt").val('0');
	      $("#cboColour").val('-1');
	      $("#txtRemarks").val('');
	      $("#cboStage").val('-1');

	  	}
	  	else /////////While NOT editing product
	  	{
	      var productCategoryRowId = $("#cboProductCategories").val();
	      if(productCategoryRowId == "-1")
	      {
	      	alertPopup("Select product category...", 6000);
	      	$("#cboProductCategories").focus();
	      	return;
	      }

	      var productRowId = $("#cboProducts").val();
	      if(productRowId == "-1")
	      {
	      	alertPopup("Select product...", 6000);
	      	$("#cboProducts").focus();
	      	return;
	      }

	      var flag=0;
		  $('#tblProducts tr').each(function(row, tr)
		  {
		    	if ( parseInt($(tr).find('td:eq(1)').text()) == parseInt(productRowId) )
		    	{
		    		flag=1;
			    	
			    }
		   }); 
		  if( flag == 1)
		  {
		  	alertPopup("This product already added...", 6000);
			$("#cboProducts").focus();
			return;
		  }


	      var productCategory = $("#cboProductCategories option:selected").text();
	      var product = $("#cboProducts option:selected").text();
	      var productSize = ' (' + $("#txtSize").val() + ')'; 
	      var rate = $("#txtRate").val();
	      var qty = $("#txtQty").val();
	      var amt = $("#txtAmt").val();
	      var colourRowId = $("#cboColour").val();
	      if(colourRowId == "-1")
	      {
	      	alertPopup("Select colour...", 6000);
	      	$("#cboColour").focus();
	      	return;
	      }
	      var colour = $("#cboColour option:selected").text();
	      var stageRowId = $("#cboStage").val();
	      if(stageRowId == "-1")
	      {
	      	alertPopup("Select stage...", 6000);
	      	$("#cboStage").focus();
	      	return;
	      }
	      var stageName = $("#cboStage option:selected").text();
	      var remarks = $("#txtRemarks").val().trim();

	      var table = document.getElementById("tblProducts");
	      var newRowIndex = table.rows.length;
	      var row = table.insertRow(newRowIndex);
	      var cell0 = row.insertCell(0);
	      var cell1 = row.insertCell(1);
	      cell1.style.display="none";
	      var cell2 = row.insertCell(2);
	      cell2.style.display="none";
	      var cell3 = row.insertCell(3);
	      var cell4 = row.insertCell(4);
	      var cell5 = row.insertCell(5);
	      var cell6 = row.insertCell(6);
	      var cell7 = row.insertCell(7);
	      cell7.style.display="none";
	      var cell8 = row.insertCell(8);
	      var cell9 = row.insertCell(9);
	      var cell10 = row.insertCell(10);
	      cell10.style.display="none";
	      var cell11 = row.insertCell(11);

	      cell0.innerHTML = "<span onClick='delTableRowProducts(this);' style='color: lightgray;cursor: pointer;cursor: hand;' onmouseover=\"this.style.color=\'red\';\"  onmouseout=\"this.style.color=\'lightgray\';\" class='glyphicon glyphicon-remove'></span>";
	      cell1.innerHTML = productRowId;
	      cell2.innerHTML = productCategory;
	      cell3.innerHTML = product + productSize;
	      cell4.innerHTML = rate;
	      cell5.innerHTML = qty;
	      cell6.innerHTML = amt;
	      cell7.innerHTML = colourRowId;
	      cell8.innerHTML = colour;
	      cell9.innerHTML = remarks;
	      cell10.innerHTML = stageRowId;
	      cell11.innerHTML = stageName;

	      // $("#cboProductCategories").val('-1');
	      $("#cboProducts").val('-1');
	      $("#txtRate").val('0');
	      $("#txtQty").val('0');
	      $("#txtAmt").val('0');
	      $("#cboColour").val('-1');
	      $("#cboStage").val('-1');
	      $("#txtRemarks").val('');

	      $("#cboProductCategories").focus();
	    }

	    doTotals();
	  }

	  function delTableRowProducts(x)
	  {
	      var rowToDelete = x.parentNode.parentNode.rowIndex
	      var table = document.getElementById("tblProducts");
	      table.deleteRow(rowToDelete);
	      doTotals();
	  }

	  function doTotals()
	  {
	  	var totalAmt=0;
	  	var totalQty=0;
	  	$('#tblProducts tr').each(function(row, tr)
	    {
	    	if ( isNaN(parseFloat($(tr).find('td:eq(6)').text())) == false )
	    	{
		    	totalAmt += parseFloat($(tr).find('td:eq(6)').text());
		    	totalQty += parseFloat($(tr).find('td:eq(5)').text());
		    }
	    }); 
	    // alert(totalAmt);
	    $("#txtTotalAmt").val(totalAmt.toFixed(2));
	    $("#txtTotalQty").val(totalQty.toFixed(2));
	    doDiscount()
	  }
	  function doDiscount()
	  {
	  	var disPer = $("#txtDiscountPer").val();
	  	var totalAmt = $("#txtTotalAmt").val();
	  	var disAmt = totalAmt * disPer / 100;
	  	$("#txtDiscountAmt").val(disAmt.toFixed(2));
	  	var totalAfterDiscount = totalAmt - disAmt;
	  	$("#txtTotalAfterDiscount").val(totalAfterDiscount.toFixed(2));
	  	doTax();
	  }

	  function doDiscountOnAmtChange()
	  {
	  	var disAmt = $("#txtDiscountAmt").val();
	  	var totalAmt = $("#txtTotalAmt").val();
	  	var disPer = disAmt * 100 / totalAmt;
	  	$("#txtDiscountPer").val(disPer.toFixed(2));
	  	var totalAfterDiscount = totalAmt - disAmt;
	  	$("#txtTotalAfterDiscount").val(totalAfterDiscount.toFixed(2));
	  	doTax();
	  }

	  function doTax()
	  {
	  	var vatPer = $("#txtVatPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var vatAmt = totalAfterDiscount * vatPer / 100;
	  	$("#txtVatAmt").val(vatAmt.toFixed(2));

	  	var sgstPer = $("#txtSgstPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var sgstAmt = totalAfterDiscount * sgstPer / 100;
	  	$("#txtSgstAmt").val(sgstAmt.toFixed(2));
	  	
	  	var cgstPer = $("#txtCgstPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var cgstAmt = totalAfterDiscount * cgstPer / 100;
	  	$("#txtCgstAmt").val(cgstAmt.toFixed(2));
	  	
	  	var igstPer = $("#txtIgstPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var igstAmt = totalAfterDiscount * igstPer / 100;
	  	$("#txtIgstAmt").val(igstAmt.toFixed(2));

	  	var net = parseFloat(totalAfterDiscount) + parseFloat(vatAmt) + parseFloat($("#txtSgstAmt").val()) + parseFloat($("#txtCgstAmt").val()) + parseFloat($("#txtIgstAmt").val());

	  	$("#txtNet").val(net.toFixed(2));
	  	var netInWords = number2text( parseFloat( $("#txtNet").val() ) ) ;
	  	// alert(netInWords);
	  	$("#txtWords").val( netInWords );
	  	// doAdvance();
	  }



	  $(document).ready(function()
	  {
        $("#lblRefreshColours").click(function()
        {
          $.ajax({
              'url': base_url + '/' + controller + '/getColours',
              'type': 'POST',
              'dataType': 'json',
              'data': {'x': 'x'},
              'success': function(data)
              {
                if(data)
                {
                  $('#cboColour').empty();
                  if(data['colours'] != null) 
                  {
                    var options = "<option value='-1'>" + "--- Select ---" + "</option>";
                    for(var i=0; i<data['colours'].length; i++)
                    {
                      options += "<option value=" + data['colours'][i].colourRowId + ">" + data['colours'][i].colourName + "</option>";
                    }
                    $('#cboColour').append(options);
                  }
                }
              }
          });
        });
      });



	function number2text(value) {
	    var fraction = Math.round(frac(value)*100);
	    var f_text  = "";

	    if(fraction > 0) {
	        f_text = "AND "+convert_number(fraction)+" PAISE";
	    }

	    return convert_number(value)+" RUPEE "+f_text+" ONLY";
	}

	function frac(f) {
	    return f % 1;
	}

	function convert_number(number)
	{
	    if ((number < 0) || (number > 999999999)) 
	    { 
	        return "NUMBER OUT OF RANGE!";
	    }
	    var Gn = Math.floor(number / 10000000);  /* Crore */ 
	    number -= Gn * 10000000; 
	    var kn = Math.floor(number / 100000);     /* lakhs */ 
	    number -= kn * 100000; 
	    var Hn = Math.floor(number / 1000);      /* thousand */ 
	    number -= Hn * 1000; 
	    var Dn = Math.floor(number / 100);       /* Tens (deca) */ 
	    number = number % 100;               /* Ones */ 
	    var tn= Math.floor(number / 10); 
	    var one=Math.floor(number % 10); 
	    var res = ""; 

	    if (Gn>0) 
	    { 
	        res += (convert_number(Gn) + " CRORE"); 
	    } 
	    if (kn>0) 
	    { 
	            res += (((res=="") ? "" : " ") + 
	            convert_number(kn) + " LAKH"); 
	    } 
	    if (Hn>0) 
	    { 
	        res += (((res=="") ? "" : " ") +
	            convert_number(Hn) + " THOUSAND"); 
	    } 

	    if (Dn) 
	    { 
	        res += (((res=="") ? "" : " ") + 
	            convert_number(Dn) + " HUNDRED"); 
	    } 


	    var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX","SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN","FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN","NINETEEN"); 
	var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY","SEVENTY", "EIGHTY", "NINETY"); 

	    if (tn>0 || one>0) 
	    { 
	        if (!(res=="")) 
	        { 
	            res += " AND "; 
	        } 
	        if (tn < 2) 
	        { 
	            res += ones[tn * 10 + one]; 
	        } 
	        else 
	        { 

	            res += tens[tn];
	            if (one>0) 
	            { 
	                res += ("-" + ones[one]); 
	            } 
	        } 
	    }

	    if (res=="")
	    { 
	        res = "zero"; 
	    } 
	    return res;
	}	  

	$(document).ready(function() {
        $("#tbl1 tr").find("td:gt(1)").on('click', showDetails);
		
	});


	function showDetails()
      {
      	// alert();
	    $("#tblDetail").find("tr:gt(0)").remove();
	    ciRowId = $(this).parent().find("td:eq(2)").text();
	    globalCiRowIdForReprint = $(this).parent().find("td:eq(2)").text();
	    // globalPiRowIdForReprint = $(this).parent().find("td:eq(4)").text();
	    globalvDt = $(this).parent().find("td:eq(3)").text();
	    // alert(ciRowId);
	    // return;
	    $.ajax({
				'url': base_url + '/' + controller + '/getReturnDetial',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'ciRowId': ciRowId
						},
				'success': function(data)
				{
					// alert( JSON.stringify(data) );
					// $("#tblDetail").empty();
					$("#tblDetail").find("tr:gt(0)").remove();
	      			var table = document.getElementById("tblDetail");
					for(i=0; i<data['srDetail'].length; i++)
				      {
				          newRowIndex = table.rows.length;
				          row = table.insertRow(newRowIndex);


				          var cell = row.insertCell(0);
				          cell.innerHTML = "";
				          cell.style.display="none";
				          var cell = row.insertCell(1);
				          cell.innerHTML = "";
				          cell.style.display="none";
				          var cell = row.insertCell(2);
				          cell.innerHTML = "";
				          cell.style.display="none";
				          var cell = row.insertCell(3);
				          cell.innerHTML = data['srDetail'][i].productName;
				          var cell = row.insertCell(4);
				          cell.innerHTML = data['srDetail'][i].rate;
				          var cell = row.insertCell(5);
				          cell.innerHTML = data['srDetail'][i].qty;
				          var cell = row.insertCell(6);
				          cell.innerHTML = data['srDetail'][i].amt;
				          var cell = row.insertCell(7);
				          cell.innerHTML = "";
				          cell.style.display="none";
				          var cell = row.insertCell(8);
				          cell.innerHTML = data['srDetail'][i].colourName;
				          var cell = row.insertCell(9);
				          cell.innerHTML = data['srDetail'][i].remarks;
				          var cell = row.insertCell(10);
				          cell.style.display="none";
				          cell.innerHTML = "";
				          var cell = row.insertCell(11);
				          cell.innerHTML = data['srDetail'][i].stageName;
				  	  }						
				}
				
		});
      }


    function storeTblValuesReprint()
	{
	    var TableData = new Array();
	    var i=0;
	    // alert(globalPoRowId);
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	if( parseInt($(tr).find('td:eq(2)').text()) == parseInt(globalCiRowIdForReprint) )
	    	{
	        	TableData[i]=
	        	{
		            "ciRowId" : $(tr).find('td:eq(2)').text()
		            , "dt" : $(tr).find('td:eq(3)').text()
		            , "partyRowId" : $(tr).find('td:eq(4)').text()
		            , "totalAmt" : $(tr).find('td:eq(6)').text()
		            , "disPer" : $(tr).find('td:eq(7)').text()
		            , "disAmt" : $(tr).find('td:eq(8)').text()
		            , "amtAfterDis" : $(tr).find('td:eq(9)').text()
		            , "sgstPer" : $(tr).find('td:eq(12)').text()
		            , "sgstAmt" : $(tr).find('td:eq(13)').text()
		            , "cgstPer" : $(tr).find('td:eq(14)').text()
		            , "cgstAmt" : $(tr).find('td:eq(15)').text()
		            , "igstPer" : $(tr).find('td:eq(16)').text()
		            , "igstAmt" : $(tr).find('td:eq(17)').text()
		            , "net" : $(tr).find('td:eq(18)').text()
		            , "totalQty" : $(tr).find('td:eq(19)').text()
		            , "vNo" : $(tr).find('td:eq(22)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i-1;
	    return TableData;
	}

	function storeTblValuesProductsReprint()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblDetail tr').each(function(row, tr)
	    {
	        	TableData[i]=
	        	{
		            "productName" : $(tr).find('td:eq(3)').text()
		            , "rate" : $(tr).find('td:eq(4)').text()
		            , "qty" : $(tr).find('td:eq(5)').text()
		            , "amt" : $(tr).find('td:eq(6)').text()
		            , "colourName" : $(tr).find('td:eq(8)').text()
		            , "remarks" : $(tr).find('td:eq(9)').text()
		            , "stageName" : $(tr).find('td:eq(11)').text()
	        	}   
	        	i++; 
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    return TableData;
	}

    function reprint()
	{	
		var TableData;
		TableData = storeTblValuesReprint();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		var TableDataProducts;
		TableDataProducts = storeTblValuesProductsReprint();
		TableDataProducts = JSON.stringify(TableDataProducts);
		// alert(JSON.stringify(TableDataProducts));
		// return;

		ciRowId = globalCiRowIdForReprint;


		$.ajax({
				'url': base_url + '/' + controller + '/reprint',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'ciRowId': ciRowId
							, 'TableData': TableData
							, 'TableDataProducts': TableDataProducts
						},
				'success': function(data)
				{
					 window.location.href=data;
				}
				
		});
	}
</script>