<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='SalaryContract_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      var totAmt=0;
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);

	          var cell = row.insertCell(0);
	          // cell.style.display="none";
	          cell.innerHTML = i+1;
	          var cell = row.insertCell(1);
	          // cell.style.display="none";
	          cell.innerHTML = records[i].empRowId;
	          var cell = row.insertCell(2);
	          // cell.style.display="none";
	          cell.innerHTML = records[i].empName;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].amt;
	          totAmt += parseFloat(records[i].amt);

	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].due;

	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].paid;

	          var cell = row.insertCell(6);
	          cell.innerHTML = parseFloat(records[i].amt) + parseFloat(records[i].due) - parseFloat(records[i].paid);

	          var cell = row.insertCell(7);
	          cell.innerHTML = "0";
	          cell.setAttribute("contentEditable", true);
	          cell.className = "classPayingNow";

	          var cell = row.insertCell(8);
	          cell.innerHTML = parseFloat(records[i].amt) + parseFloat(records[i].due) - parseFloat(records[i].paid);

	          var cell = row.insertCell(9);
	          cell.innerHTML = "Show Detail";
	          cell.style.textAlign = "center";
	          cell.style.color='blue';
	          // cell.setAttribute("onmouseover", "this.style.color='green'");
	          // cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "showDetail";
	  	  }

	  	  $("#txtTotalAmt").val(totAmt);
		// $("#tbl1 tr").on("click", highlightRow);
		// $("#tbl1 tr").on('click', showDetail);
		$('.showDetail').bind('click', showDetail);
		$('.classPayingNow').bind('keyup', doDueCalculation);
			
	}

	function doDueCalculation()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		var bal = $(this).closest('tr').children('td:eq(6)').text();
		var payingNow = $(this).closest('tr').children('td:eq(7)').text();
		var dueNow = parseFloat(bal) - parseFloat(payingNow);
		dueNow = dueNow.toFixed(2);
		$(this).closest('tr').children('td:eq(8)').text( dueNow );
		// alert(pendingQty);
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}


		var empRowId="";
		$('input:checked.chkEmployees').each(function() 
		{
			empRowId = $(this).val() + "," + empRowId;
		});
		if(empRowId == "")
		{
			alertPopup("Select employee...", 8000);
			return;
		}
		empRowId = empRowId.substr(0, empRowId.length-1);
		// alert(empRowId);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'empRowId': empRowId
							, 'dt': dt
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "sn" : $(tr).find('td:eq(0)').text()
		            , "empRowId" : $(tr).find('td:eq(1)').text()
		            , "amtCalculated" :$(tr).find('td:eq(3)').text()
		            , "prevDues" :$(tr).find('td:eq(4)').text()
		            , "alreadyPaid" :$(tr).find('td:eq(5)').text()
		            , "bal" :$(tr).find('td:eq(6)').text()
		            , "payingNow" :$(tr).find('td:eq(7)').text()
		            , "dueNow" :$(tr).find('td:eq(8)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		// alert(tblRowsCount);
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dt': dt
						},
				'success': function(data)
				{
					alert("Changes Saved...");
					location.reload();
					if(data)
					{
						
						// window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Calculate Salary (Contract)</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllEmployees">
							<label for="chkAllEmployees" style="font-weight:bold;color:black;">All Employees
						</span>
						
						<?php
							foreach ($employees as $key => $value) 
							{
						?>
								<div class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkEmployees" txt='<?php echo $value;?>' id='<?php echo "P".$key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo  "P".$key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<div class="row">
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>Calculate Upto:</label>";
									echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
				              	?>
				              	<script>
									$( "#dt" ).datepicker({
										dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
									});
								    // Set the Current Date as Default
									$("#dt").val(dateFormat(new Date()));
								</script>					
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
									echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
				              	?>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
					 <tr>
						<th style='display:none1;'>S.N.</th>
					 	<th style='display:none1;'>Emp Code</th>
					 	<th>Employee</th>
					 	<th>Amt</th>
					 	<th>Prev. Due</th>
					 	<th>Paid</th>
					 	<th>Bal</th>
					 	<th>Now Paying</th>
					 	<th>Due Now</th>
					 	<th></th>
					 </tr>
				</table>
			</div>

			<div class="row" style="margin-top:45px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Amt.:</label>";
						echo form_input('txtTotalAmt', '', "class='form-control' placeholder='' id='txtTotalAmt' maxlength='10' disabled");
	              	?>
	          	</div>
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
				</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	      	
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
			<div id="divTableDetail" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:400px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3;margin-bottom:5%">
					<table class='table table-hover' id='tblDetail'>
						 <tr>
							<th style='display:none1;'>S.N.</th>
						 	<th style='display:none1;'>Date</th>
						 	<th style='display:none;'>ProductRowID</th>
						 	<th>Product</th>
						 	<th style='display:none;'>StageRowId</th>
						 	<th>Direction</th>
						 	<th>Qty</th>
						 	<th>Rate</th>
						 	<th>Amt</th>
						 	<th>&nbsp;</th>
						 </tr>
					</table>
				</div>
			</div>

		</div>




		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>





<script type="text/javascript">



	// add multiple select / deselect functionality
	$("#chkAllEmployees").click(function () {
		// alert();
		  $('.chkEmployees').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkEmployees').bind('click', chkSelectAllEmployees);
	function chkSelectAllEmployees()
	{
		var x = parseInt($(".chkEmployees").length);
		var y = parseInt($(".chkEmployees:checked").length);
		if( x == y ) 
		{
			$("#chkAllEmployees").prop("checked", true);
		} 
		else 
		{
			$("#chkAllEmployees").prop("checked", false);
		}
	}	







	var globalEmpRowId; 
	function showDetail()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalEmpRowId = $(this).closest('tr').children('td:eq(1)').text();
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}
		// alert(globalEmpRowId);
		$.ajax({
			'url': base_url + '/SalaryContract_Controller/getDetail',
			'type': 'POST', 
			'data':{'globalEmpRowId':globalEmpRowId, 'dt':dt},
			'dataType': 'json',
			'success':function(data)
			{
				$("#tblDetail").find("tr:gt(0)").remove(); //// empty first
				// alert(JSON.stringify(data));
		        var table = document.getElementById("tblDetail");
		        for(i=0; i<data['records'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML = i+1;
		          // cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = dateFormat(new Date(data['records'][i].dt));
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['records'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['records'][i].productName;
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['records'][i].stageRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['records'][i].direction;
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['records'][i].moveQty;
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['records'][i].contractRate;
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['records'][i].amt;
		        }
		        $("#tblDetail tr").on("click", highlightRow);	
			}
		});

	}
</script>