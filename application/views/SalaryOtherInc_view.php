<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='SalaryOtherInc_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
	      if( records[0].remarks == null )  ////if NOT already saved on that date
	      {
		      var table = document.getElementById("tbl1");
		      for(i=0; i<records.length; i++)
		      {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);


		          var cell = row.insertCell(0);
		          cell.innerHTML = i+1;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(1);
		          cell.innerHTML = records[i].empRowId;
		          cell.style.backgroundColor="#F0F0F0";
		          // cell.style.display="none";
		      

		          var cell = row.insertCell(2);
		          // cell.style.display="none";
		          cell.innerHTML = records[i].name;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(3);
		          cell.innerHTML = "0";
		          cell.setAttribute("contentEditable", true);
		          cell.style.textAlign = "right";
		          // cell.style.display="none";

		          var cell = row.insertCell(4);
		          cell.innerHTML = "";
		          cell.setAttribute("contentEditable", true);
		          // cell.style.display="none";

		  	  }
	  	  }
	  	  else ///// if already saved
	  	  {
	  	  	
		      var table = document.getElementById("tbl1");
		      for(i=0; i<records.length; i++)
		      {
		          newRowIndex = table.rows.length;
		          row = table.insertRow(newRowIndex);


		          var cell = row.insertCell(0);
		          cell.innerHTML = i+1;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(1);
		          cell.innerHTML = records[i].empRowId;
		          cell.style.backgroundColor="#F0F0F0";
		          // cell.style.display="none";
		      

		          var cell = row.insertCell(2);
		          // cell.style.display="none";
		          cell.innerHTML = records[i].name;
		          cell.style.backgroundColor="#F0F0F0";

		          var cell = row.insertCell(3);
		          cell.innerHTML = records[i].amt;
		          cell.style.textAlign = "right";
		          cell.setAttribute("contentEditable", true);
		          

		          var cell = row.insertCell(4);
		          cell.innerHTML = records[i].remarks;
		          cell.setAttribute("contentEditable", true);
		          // cell.style.display="none";

		  	  }
	  	  }
	  	  // $('td').on("click focus", setDropDown);
	}

	function loadData()
	{	
		// alert();
		// return;
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}

		// alert(dt);
		// return;
		$.ajax({
			'url': base_url + '/' + controller + '/showData',
			'type': 'POST',
			'dataType': 'json',
			'data': {
						'dt': dt
					},
			'success': function(data)
			{
				if(data)
				{
					// alert(JSON.stringify(data));
					$("#tbl1").find("tr:gt(0)").remove();
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
				}
			}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    
	    $('#tbl1 tr').each(function(row, tr)
	    {
        	TableData[i]=
        	{
	            "empRowId" : $(tr).find('td:eq(1)').text()
	            , "amt" :$(tr).find('td:eq(3)').text()
	            , "remarks" :$(tr).find('td:eq(4)').text()
        	}   
        	i++; 
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("Nothing to save...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dt': dt
						},
				'success': function(data)
				{
					alert('Changes saved...');
					location.reload();
				}
		});
		
	}

</script>
<div class="acontainer" >
	
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
				<h2 class="text-center" style='margin-top:-20px'>Salary Incentive (Others)</h2>
				<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
						<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Date:</label>";
								echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
			              	?>
			              	<script>
								$( "#dt" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    // Set the last of previous month
								var date = new Date();
								var lastDay = new Date(date.getFullYear(), date.getMonth() , 0);
								$("#dt").val(dateFormat(lastDay));
							</script>					
			          	</div>
			          	
						<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
								echo "<input type='button' onclick='loadData();' value='Load Employees' id='btnShow' class='btn btn-primary form-control'>";
			              	?>
			          	</div>
			          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			          	</div>
				
				</form>
			</div>

			<div class="row" style="margin-top: 20px;">
				<style>
			      table, th, td{border:1px solid gray; padding: 7px;}
			    </style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:450px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
								<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="40" style='display:none1;'>Emp Code</th>
							 	<th width="150" style='display:none1;'>Emp. Name</th>
							 	<th width="100" style='display:none1;'>Amount</th>
							 	<th width="200">Remarks</th>
							 </tr>
						 <tbody>

						 </tbody>
						</table>
					</div>
			</div>

			<div class="row" style="margin-top: 20px; margin-bottom:20px;" >
				<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
				</div>

				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<input type='button' onclick='saveData();' value='Save Changes' id='btnSaveChanges' class='btn btn-primary form-control'>";
			      	?>
				</div>
			</div>

		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>


