<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='SalaryMonthly_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  // $("#tbl1").empty();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].empRowId;
	          // cell.style.display="none";
	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].empName;
	          var cell = row.insertCell(3);
	          // cell.style.display="none";
	          cell.innerHTML = records[i].basicSal;
	          var cell = row.insertCell(4);
	          // var sal = records[i].salary;
	          // sal = parseFloat(sal).toFixed(2);
	          cell.innerHTML = parseFloat(records[i].salary).toFixed(2);
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].night;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].tour;
	          // cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].attendance;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].nashta;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].absent;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].other;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].gross;
	          cell.style.color="green";
	          // // cell.style.display="none";
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].esi;
	          var cell = row.insertCell(13);
	          cell.innerHTML = parseFloat(records[i].due).toFixed(2);
	          var cell = row.insertCell(14);
	          cell.innerHTML = parseFloat(records[i].netSal).toFixed(2);
	          cell.style.color="green";
	          // cell.style.display="none";

	          var cell = row.insertCell(15);
	          cell.innerHTML = parseFloat(records[i].netPayble).toFixed(2);
	          cell.style.color="red";
	          var cell = row.insertCell(16);
	          cell.innerHTML = "0"; //parseFloat(records[i].netPayble).toFixed(2);
	          // cell.setAttribute("contentEditable", true);
	          cell.className = "classPayingNow";
	          cell.style.color="blue";
	          cell.style.display="none";
	          var cell = row.insertCell(17);
	          cell.innerHTML = "0";
	          // cell.setAttribute("contentEditable", true);
	          cell.style.display="none";
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].mins;
	          // cell.style.display="none";
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].extraMins;
	          // cell.style.display="none";
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].salPerMin;
	          // cell.style.display="none";
	          var cell = row.insertCell(21);
	          var totHrs = parseInt((parseFloat(records[i].mins) + parseFloat(records[i].extraMins)) / 60);
	          var totHrs1 = (parseFloat(records[i].mins) + parseFloat(records[i].extraMins)) % 60;
	          cell.innerHTML = totHrs + "." + totHrs1;
	          // cell.style.display="none";
	  	  }

		$('.classPayingNow').bind('keyup', doDueCalculation);
		$("#tbl1 tr").on("click", highlightRowAlag);
	}

	function doDueCalculation()
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		var netPayble = $(this).closest('tr').children('td:eq(15)').text();
		var payingNow = $(this).closest('tr').children('td:eq(16)').text();
		var dueNow = parseFloat(netPayble) - parseFloat(payingNow);
		dueNow = dueNow.toFixed(2);
		$(this).closest('tr').children('td:eq(17)').text( dueNow );
		// alert(pendingQty);
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		// alert(dtTo);

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data == "Already Saved...")
					{
						alertPopup("Already saved...", 8000);
						$("#tbl1").find("tr:gt(0)").remove();
					}
					else if(data == "Attendance not marked for whole month...")
					{
						alertPopup("Attendance not marked for whole month...", 9000);
						$("#tbl1").find("tr:gt(0)").remove();
					}
					else
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				},
				'error': function(jqXHR, exception)
				{
					document.write(jqXHR.responseText);
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "empRowId" : $(tr).find('td:eq(1)').text()
		            , "empName" : $(tr).find('td:eq(2)').text()
		            , "salPerMonth" :$(tr).find('td:eq(3)').text()
		            , "salCalculated" :$(tr).find('td:eq(4)').text()
		            , "nightAllow" :$(tr).find('td:eq(5)').text()
		            , "tourAllow" :$(tr).find('td:eq(6)').text()
		            , "attendanceAllow" :$(tr).find('td:eq(7)').text()
		            , "breakfastAllow" :$(tr).find('td:eq(8)').text()
		            , "absent" :$(tr).find('td:eq(9)').text()
		            , "otherInc" :$(tr).find('td:eq(10)').text()
		            , "gross" :$(tr).find('td:eq(11)').text()
		            , "esi" :$(tr).find('td:eq(12)').text()
		            , "prevDues" :$(tr).find('td:eq(13)').text()
		            , "net" :$(tr).find('td:eq(14)').text()
		            , "netPayble" :$(tr).find('td:eq(15)').text()
		            , "payingNow" :$(tr).find('td:eq(16)').text()
		            , "dueNow" :$(tr).find('td:eq(17)').text()
		            , "mins" :$(tr).find('td:eq(18)').text()
		            , "extraMins" :$(tr).find('td:eq(19)').text()
		            , "salPerMin" :$(tr).find('td:eq(20)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No rows to save...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		var dt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}
		

		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'dt': dt
							
						},
				'success': function(data)
				{
					// alert(data);
					// if(data == "Already Saved...")
					// {
					// 	alertPopup("Already saved...", 8000);
					// }
					// else
					// {
						window.location.href=data;
					// }
				}
		});
	}


	function storeTblValues1()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text();
		    }); 
		})

	    return data;
	}

	function exportData()
	{	
		// alert(noOfDays);
		// return;
		var TableData;
		TableData = storeTblValues1();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}
</script>
<div class="acontainer">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12"style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
			<h1 class="text-center" style='margin-top:-20px'>Calculate Salary (Monthly)</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From:</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the 1st of previous month
							var date = new Date();
							var firstDay = new Date(date.getFullYear(), date.getMonth()-1, 1);
							$("#dtFrom").val(dateFormat(firstDay));
							// var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
						</script>					
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To:</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the last of previous month
							var date = new Date();
							var lastDay = new Date(date.getFullYear(), date.getMonth() , 0);
							$("#dtTo").val(dateFormat(lastDay));
						</script>					
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Current Date:</label>";
							echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
		              	?>
		              	<script>
							$( "#dt" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2017:2050"
							});
							$("#dt").val(dateFormat(new Date()));
						</script>	
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
							 	<td width="40" style='display:none1;font-weight: bold;'>S.N.</td>
							 	<td width="50" style='display:none1;font-weight: bold;'>Emp Code</td>
							 	<td width="150" style='font-weight: bold;'>Emp Name</td>
							 	<td width="80" style='font-weight: bold;'>Sal/Month</td>
							 	<td width="80" style='font-weight: bold;'>Sal Calc.</td>
							 	<td width="80" style='font-weight: bold;'>Night A.</td>
							 	<td width="80" style='font-weight: bold;'>Tour A.</td>
							 	<td width="80" style='font-weight: bold;'>Att. A.</td>
							 	<td width="80" style='font-weight: bold;'>Brk/Lnch A.</td>
							 	<td width="80" style='font-weight: bold;'>Absent.</td>
							 	<td width="80" style='font-weight: bold;'>Other Inc.</td>
							 	<td width="80" style='font-weight: bold;'>Gross</td>
							 	<td width="80" style='font-weight: bold;'>ESI</td>
							 	<td width="80" style='font-weight: bold;'>Prev. Due</td>
							 	<td width="80" style='font-weight: bold;display:none1;'>Net</td>
							 	<td width="80" style='font-weight: bold;'>Net Payble</td>
							 	<td width="80" style='font-weight: bold;display:none;'>Paying Now</td>
							 	<td width="80" style='font-weight: bold;display:none;'>Due Now</td>
							 	<td width="80" style='font-weight: bold;'>Total Mins.</td>
							 	<td width="80" style='font-weight: bold;'>Extra Mins.</td>
							 	<td width="80" style='font-weight: bold;'>Sal/Min.</td>
							 	<td width="80" style='font-weight: bold;'>Total Hrs.</td>
							 </tr>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>



	<div class="row" style="margin-top:30px;" >
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='exportData();' value='Export Data to Excel' id='btnExportData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<?php
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
				echo "<input type='button' onclick='saveData();' value='Save & Print' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
				echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
	      	?>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->

	</div>
</div>





<script type="text/javascript">


</script>