<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">


<script type="text/javascript">
	var controller='Emailer_Controller';
	var base_url='<?php echo site_url();?>';

	// function setTable(records)
	// {
	// 	 // alert(JSON.stringify(records));
	// 	  $("#tbl1").empty();
	//       var table = document.getElementById("tbl1");
	//       for(i=0; i<records.length; i++)
	//       {
	//           newRowIndex = table.rows.length;
	//           row = table.insertRow(newRowIndex);


	//           var cell = row.insertCell(0);
	//           cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	//           cell.style.textAlign = "center";
	//           cell.style.color='lightgray';
	//           cell.setAttribute("onmouseover", "this.style.color='green'");
	//           cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	//           cell.className = "editRecord";

	//           var cell = row.insertCell(1);
	// 			  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	//           cell.style.textAlign = "center";
	//           cell.style.color='lightgray';
	//           cell.setAttribute("onmouseover", "this.style.color='red'");
	//           cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	//           cell.setAttribute("onclick", "delrowid(" + records[i].colourRowId +")");
	//           // data-toggle="modal" data-target="#myModal"
	//           cell.setAttribute("data-toggle", "modal");
	//           cell.setAttribute("data-target", "#myModal");

	//           var cell = row.insertCell(2);
	//           cell.style.display="none";
	//           cell.innerHTML = records[i].colourRowId;
	//           var cell = row.insertCell(3);
	//           cell.innerHTML = records[i].colourName;
	//           var cell = row.insertCell(4);
	//           cell.innerHTML = records[i].remarks;
	//   	  }


	//   	$('.editRecord').bind('click', editThis);

	// 	myDataTable.destroy();
	// 	$(document).ready( function () {
	//     myDataTable=$('#tbl1').DataTable({
	// 	    paging: false,
	// 	    iDisplayLength: -1,
	// 	    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

	// 	});
	// 	} );

	// 	$("#tbl1 tr").on("click", highlightRow);
	// 	// $('#tbl1 tr').each(function(){
	// 	// 	$(this).bind('click', highlightRow);
	// 	// });
			
	// }
	// function deleteRecord(rowId)
	// {
	// 	// alert(rowId);
	// 	$.ajax({
	// 			'url': base_url + '/' + controller + '/delete',
	// 			'type': 'POST',
	// 			'dataType': 'json',
	// 			'data': {'rowId': rowId},
	// 			'success': function(data){
	// 				if(data)
	// 				{
	// 					if( data['dependent'] == "yes" )
	// 					{
	// 						alertPopup('Record can not be deleted... Dependent records exist...', 8000);
	// 					}
	// 					else
	// 					{
	// 						setTable(data['records'])
	// 						alertPopup('Record deleted...', 4000);
	// 						blankControls();
	// 						$("#txtColourName").focus();
	// 					}
	// 				}
	// 			}
	// 		});
	// }
	
	function send()
	{	
		bcc = $("#txtTo").val().trim();
		subject = $("#txtSubject").val().trim();
		// body = $("#txtBody").html().trim();
		var body = CKEDITOR.instances['txtBody'].getData();
		// console.log(body);
		// return;
		// if(colourName == "")
		// {
		// 	alertPopup("Colour name can not be blank...", 8000);
		// 	$("#txtColourName").focus();
		// 	return;
		// }

			$.ajax({
					'url': base_url + '/' + controller + '/send',
					'type': 'POST',
					// 'dataType': 'json',
					'data': {
								'bcc': bcc
								, 'subject': subject
								, 'body': body
							},
					'success': function(data)
					{
						alertPopup("Mail Sent..." + data, 4000);
						// if(data == "Duplicate record...")
						// {
						// 	alertPopup("Duplicate record...", 4000);
						// 	$("#txtColourName").focus();
						// }
						// else
						// {
						// 	setTable(data['records']) ///loading records in tbl1

						// 	alertPopup('Record saved...', 4000);
						// 	blankControls();
						// 	$("#txtColourName").focus();
						// 	// location.reload();
						// }
					}
			});
	}

	// function loadAllRecords()
	// {
	// 	// alert(rowId);
	// 	$.ajax({
	// 			'url': base_url + '/' + controller + '/loadAllRecords',
	// 			'type': 'POST',
	// 			'dataType': 'json',
	// 			'success': function(data)
	// 			{
	// 				if(data)
	// 				{
	// 					setTable(data['records'])
	// 					alertPopup('Records loaded...', 4000);
	// 					blankControls();
	// 					$("#txtColourName").focus();
	// 				}
	// 			}
	// 		});
	// }
</script>

<div class="container-fluid" style="width: 90%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
	  <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
		<h3 class="text-center" style='margin-top:-20px'>Email</h3>
						<?php if( $feedback = $this->session->flashdata('feedback')): 
                              $feedback_class = $this->session->flashdata('feedback_class');
                        ?>
                        <div class="alert aler-dismissible <?= $feedback_class ?>">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?= $feedback ?>
                        </div>
                        <?php endif ?>
        <form class="form-sample" method="post" enctype="multipart/form-data" action="<?php echo base_url('index.php/Emailer_Controller/send');?>">
        	  
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>From:</label>";
						echo form_input('txtFrom', 'reetlekhyani@gmail.com', "class='form-control' id='txtFrom'");
	              	?>
	          	</div>
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Password:</label>";
	              	?>
	              	<input class="form-control" type="Password" name="txtPassword" id="txtPassword" maxlength="20" />
	          	</div>
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "change security setting in case of gmail account (from ac.)<br>";
						echo "<label style='color: red; font-weight: normal;'>'Less secure app access' turn it on</label>";
	              	?>
	          	</div>
	        </div>
			<div class="row" style="margin-top:5px;">
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>To:</label>";
						echo form_input('txtTo', 'surendralekhyani@gmail.com', "class='form-control' id='txtTo'");
	              	?>
	          	</div>
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Cc:</label>";
						echo form_input('txtCc', '', "class='form-control' id='txtCc'");
	              	?>
	          	</div>
	          	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>BCC: (use this to send many ac.)</label>";
						echo form_input('txtBcc', 'dhupialekhyani@gmail.com', "class='form-control' id='txtBcc'");
	              	?>
	          	</div>
			</div>
			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Subject:</label>";
						echo form_input('txtSubject', 'mail Sub.', "class='form-control'  id='txtSubject' maxlength=255 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Body:</label>";
						// echo form_input('txtBody', 'Demo mail', "class='form-control'  id='txtBody'");
	              	?>
	              	<textarea class="form-control ckeditor" name="txtBody" id='txtBody' >Demo mail..</textarea>

	          	</div>
			</div>
			

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <input type="file" class="form-control" id="txtAttachment" name="txtAttachment" />
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <input type="file" class="form-control" id="txtAttachment2" name="txtAttachment2" />
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <input type="file" class="form-control" id="txtAttachment3" name="txtAttachment3" />
	          	</div>
				
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<input type='submit' value='Send' id='btnSend' class='btn btn-primary btn-block'>";
	              	?>
	          	</div>
			</div>
			
		</form>
	  </div>
	</div>
</div>



<script type="text/javascript">
	// var globalrowid;
	// function delrowid(rowid)
	// {
	// 	globalrowid = rowid;
	// }

	// $('.editRecord').bind('click', editThis);
	// function editThis(jhanda)
	// {
	// 	rowIndex = $(this).parent().index();
	// 	colIndex = $(this).index();
	// 	colourName = $(this).closest('tr').children('td:eq(3)').text();
	// 	remarks = $(this).closest('tr').children('td:eq(4)').text();
	// 	globalrowid = $(this).closest('tr').children('td:eq(2)').text();

	// 	$("#txtColourName").val(colourName);
	// 	$("#txtRemarks").val(remarks);
	// 	$("#txtColourName").focus();
	// 	$("#btnSave").val("Update");
	// }


	// 	$(document).ready( function () {
	// 	    myDataTable = $('#tbl1').DataTable({
	// 		    paging: false,
	// 		    iDisplayLength: -1,
	// 		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

	// 		});
	// 	} );
</script>

  <script src="<?php echo base_url('texteditor/ckeditor/ckeditor.js');?>"></script>
