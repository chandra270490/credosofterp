<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
	label{color:black;}
</style>

<script type="text/javascript">
	var controller='NotificationHierarchy_Controller';
	var base_url='<?php echo site_url();?>';

	function loaddata()
	{	
		var str="";
		var users4notification="";
		$('#divUsers4notification input:checked').each(function() {
			users4notification = $(this).val() + "," + users4notification;
		});

		var forModule = $("#cboType").val();
		if(forModule == '-1')
		{
			myAlert('Please Select Module');
			return;	
		}

		var OcboUsers = document.getElementById("cboUsers");
		var userRowId = OcboUsers.options[OcboUsers.selectedIndex].value;
		if(userRowId == -1)
		{
			myAlert('Please Select User');
			return;	
		}
		if(users4notification=="")
		{
			myAlert('Please check Atleast one user');
			return;
		}

		arr = users4notification.split(",");
		arr = arr.slice(0,arr.length-1);
		str = arr.reverse().join(",");

		$.ajax
		(
			{
				'url': base_url + '/' + controller + '/insert',
				'type': 'POST', 
				'data': {'forModule' : forModule, 'userRowId' : userRowId, 'users4notification' : str},
				'success': function(data){
					// alert(data);
					// alertPopup("Changes saved...!!!", 5000);
					location.reload();
				}
			}
		);

	 	blankcontrol();
	}

	function blankcontrol()
	{
		document.getElementById("cboUsers").selectedIndex = "0";
		$('#tree input:checked').each(function() {
			$(this).removeAttr('checked');
		});		 
	}
	$(document).ready(function(){
		$("#cboType").change(function(){
			alreadySavedRights();
		});

		$("#cboUsers").change(function(){
			alreadySavedRights();
		});
	});

	function alreadySavedRights()
	{
		var forModule = $("#cboType").val();
		if(forModule == '-1')
		{
			myAlert('Please Select Module');
			return;	
		}

		$.ajax({
        'url': base_url + '/NotificationHierarchy_Controller/getRights',
        'data': {'uid':$("#cboUsers").val(), 'forModule' : forModule},
        'type': 'POST', 
        'success': function(data)
	        {
	        	// alert(data);
				arr = data.split(",");
				arr = arr.slice(0,arr.length-1);
				arr = arr.reverse();
				 $('.chk').prop('checked', false);
	            $('input[type="checkbox"]').each(function(){
					for(j=0;j<arr.length;j++)
					{
						if($(this).val() === arr[j])
						{
							$(this).prop("checked",true);
							break;
						}
						else
						{
							$(this).removeAttr("checked");
						}
					}
	            });
	        }
	    });
	}
</script>


<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
	<h3 class="text-center" style='margin-top:0px'>Notification Hierarchy</h3>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?php
				$attributes[] = array("class"=>"form-control" );
				$this->load->helper('form');
				echo validation_errors(); 
				echo form_open('Notificationhierarchy_model/insertRights', "onsubmit='return(false);'");
				$types = array();
				$types['-1'] = '--- Select ---';
				$types['Leave'] = "Leave";
				$types['Purchase'] = "Purchase";
				echo "<label style='color: black; font-weight: normal;'>For What: <span style='color: red;'></span></label>";
				echo form_dropdown('cboType', $types, '-1', "class='form-control' id='cboType'");
			?> 
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style=''>
			
			<?php
				
				$arr = array();
				foreach ($users as $row)
				{
	        		$arr[$row['rowid']]= $row['uid'];
				}
				$temp["-1"] = "--- SELECT USER ---";
				$users = $temp+$arr;
				echo "<label style='color: black; font-weight: normal;'>Select User: <span style='color: red;'></span></label>";
				echo form_dropdown('uid',$users,"-1","class='form-control' id='cboUsers'");
			?>
			<br/>
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style='background:rgba(128,128,128,.2);padding:10px; margin-left:-10px;border-radius:5px;margin-top:1px;height:295px;overflow:auto'>
			<ul id="tree">			
				<li>
					<label style1="color:black;">
						<!-- <input type="checkbox" class="rights_menu" value="Tools" /> -->
						<b>Notification to Users</b>
					</label>
					<?php
						foreach ($users4notification as $key => $value) 
						{
					?>
					<div id="divUsers4notification" class="checkbox" style="margin-left:40px;">
						<input class="chk" type="checkbox" id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
						<label  style="color: black; margin-left:-10px;" for='<?php echo $key;?>'><?php echo $value;?></label>
					</div>
					<?php
						}
					?>

				</li>
			</ul>
		</div>

		<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>bootstrap/checktree/js/jquery-checktree.js"></script>
		<script>
			$('#tree').checktree();
		</script>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:15px;">
				<?php
					// echo "<div class='col-lg-1 col-md-1 col-sm-1'></div>";
					echo "<input type='button' onclick='loaddata();' value='Save' id='btnSave' class='btn btn-primary btn-block'>";
					echo form_close();
				?>
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable" style="border:1px solid lightgray; padding: 10px;height:400px; overflow:auto; margin-top: 20px;">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none;'>rowid</th>
					 	<th style='display:none;'>UserRowId</th>
						<th style='display:none1;'>User</th>
					 	<th style='display:none;'>userRowIdMgr</th>
						<th style='display:none1;'>Mgr</th>
					 	<th>For Module</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						foreach ($records as $row) 
						{
						 	$rowId = $row['rowId'];
						 	echo "<tr>";						//onClick="editThis(this);
						 	echo "<td style='display:none;'>".$row['rowId']."</td>";
						 	echo "<td style='display:none;'>".$row['userRowId']."</td>";
						 	echo "<td style='display:none1;'>".$row['userName']."</td>";
						 	echo "<td style='display:none;'>".$row['userRowIdMgr']."</td>";
						 	echo "<td style='display:none1;'>".$row['mgr']."</td>";
						 	echo "<td>".$row['forModule']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>
	</div>