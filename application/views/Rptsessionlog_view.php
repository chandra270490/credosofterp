<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Rptsessionlog_controller';
	var base_url='<?php echo site_url();?>';

	function loaddata()
	{	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			myAlert("Pls enter valid FROM date...")
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			myAlert("Pls enter valid TO date...")
			return;
		}


		if(document.getElementById("btnSearch").value == "Search")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/searchData',
					'type': 'POST',
					'data': {'dtFrom' : dtFrom, 'dtTo' : dtTo},
					'success': function(data){
						// alert(data);
						var container = $('#container');
						if(data){
							container.html(data);
						}
					}
			});
		}

	}

	// Function to Change the Default Date Format
	function dateFormat(dt)
	{
		// alert(dt);
		var mnth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
		return (dt.getDate()<=9?"0"+dt.getDate():dt.getDate()) + "-" + mnth[dt.getMonth()] + "-" + dt.getFullYear();
	}

</script>
	<div class="row" style="margin-top: 30px">
	<div class="col-lg-1 col-md-1 col-sm-1 col-xs-0">
	</div>
	<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style='border:1px solid lightgray; border-radius:10px; padding: 10px;'>
		<h3 class="text-center" style='margin-top:0px'>Session Log</h3>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<?php
					$this->load->helper('form');		
					echo validation_errors(); 
					echo form_open('Rptsessionlog_controller', "onsubmit='return(false);'");

					echo "<label style='color: black; font-weight: normal;'>From:</label>";
					echo "<input type='text' id='dtFrom' placeholder='Date' class='form-control dtp'>";
				?>
				<script>
					// set Datepicker of all Elements whose class is `dtp`
	    			$( "#dtFrom" ).datepicker({
						dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2014:2100"
					});

				    // Set the Current Date - n days as Default
				    dt=new Date();
				    dt.setDate(dt.getDate() - 1);
					$("#dtFrom").val(dateFormat(dt));				
				</script>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<?php
					echo "<label style='color: black; font-weight: normal;'>To:</label>";
					echo "<input type='text' id='dtTo' placeholder='Date' class='form-control dtp'>";
				?>
				<script>
					// set Datepicker of all Elements whose class is `dtp`
	    			$( "#dtTo" ).datepicker({
						dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2014:2100"
					});

				    // Set the Current Date as Default
					$("#dtTo").val(dateFormat(new Date()));				
				</script>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<?php
					echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
					echo "<input type='button' onclick='loaddata();' value='Search' id='btnSearch' class='btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
					echo form_close();
				?>
			</div>
		</div>
		
	</div>
	<div class="col-lg-1 col-md-1 col-sm-1 col-xs-0">
	</div>
</div>
	<hr>






	
	
<script type="text/javascript">


</script>	
