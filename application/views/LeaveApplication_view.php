<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='LeaveApplication_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(records.length);
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          // cell.style.display="none";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].leaveApplicationRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].leaveApplicationRowId
	          cell.style.display="none";

	          var cell = row.insertCell(3);
	          cell.innerHTML =  records[i].userRowId;
	          cell.style.display="none";

	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].uid;
	          // cell.style.display="none";

	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].dtFrom));
	          // cell.style.display="none";

	          var cell = row.insertCell(6);
	          cell.innerHTML = dateFormat(new Date(records[i].dtTo));

	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].totalDays;

	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].reason;

	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].approved;
	          // cell.style.display="none";

	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
	}

	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						if( data['dependent'] == "yes" )
						{
							alertPopup('Record can not be deleted... Dependent records exist...', 8000);
						}
						else
						{
							setTable(data['records'])
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#dtFrom").val(dateFormat(new Date()));
							$("#dtTo").val(dateFormat(new Date()));
							$('#dtFrom').trigger('change');
							$("#txtReason").focus();
						}
					}
				}
			});
	}

	function saveData()
	{	
		
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		var totalDays = $("#txtTotalDays").val().trim();

		var reason = $("#txtReason").val().trim();
		if(reason == "")
		{
			alertPopup("Enter reason...", 5000);
			$("#txtReason").focus();
			return;
		}

		if($("#btnSave").val() == "Save")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'dtFrom': dtFrom
								, 'dtTo': dtTo
								, 'totalDays': totalDays
								, 'reason': reason
								
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 5000);
								$("#txtReason").focus();
							}
							else if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 5000);
								$("#txtReason").focus();
							}
							else
							{
								// alert(data['records'].length);
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 5000);
								// window.location.href=data;
								blankControls();
								$("#dtFrom").val(dateFormat(new Date()));
								$("#dtTo").val(dateFormat(new Date()));
								$('#dtFrom').trigger('change');
								$("#txtReason").focus();
								var uid='<?php echo $this->session->userid; ?>';
								$("#txtUser").val( uid );
							}
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {	
								'globalrowid': globalrowid
								, 'dtFrom': dtFrom
								, 'dtTo': dtTo
								, 'totalDays': totalDays
								, 'reason': reason
								
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Session out...")
							{
								alertPopup("Session expired...", 4000);
								$("#txtReason").focus();
							}
							else if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 5000);
								$("#txtReason").focus();
							}
							else
							{
								// alert(data['records'].length);
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								// window.location.href=data;
								blankControls();
								$("#dtFrom").val(dateFormat(new Date()));
								$("#dtTo").val(dateFormat(new Date()));
								$('#dtFrom').trigger('change');
								$("#txtReason").focus();
								var uid='<?php echo $this->session->userid; ?>';
								$("#txtUser").val( uid );
								$("#btnSave").val("Save");
							}
						}
					}
			});
		}
	}


</script>
<div class="acontainer">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
			<h1 class="text-center" style='margin-top:-20px'>Leave Application</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>From:</label>";
								echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
			              	?>
			              	<script>
								$( "#dtFrom" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    $("#dtFrom").val(dateFormat(new Date()));
							</script>	
						</div>
						<div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>To:</label>";
								echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
			              	?>
			              	<script>
								$( "#dtTo" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    $("#dtTo").val(dateFormat(new Date()));
							</script>	
						</div>
						<div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Total Days:</label>";
								echo form_input('txtTotalDays', '', "class='form-control' placeholder='' id='txtTotalDays' maxlength='10' disabled='yes'");
			              	?>
						</div>
		          	</div>
		          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Reason:</label>";
							echo form_textarea('txtReason', '', "class='form-control' placeholder='' style='resize:none;height:150px;' id='txtReason' maxlength=500 value=''");
		              	?>				
		          	</div>
					
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>User ID:</label>";
								echo form_input('txtUser', $this->session->userid, "class='form-control' id='txtUser' style='' disabled='yes' ");
			              	?>
		              	</div>
						<div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">
		              	</div>
						<div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" style="margin-top: 50px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
								echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
			              	?>
		              	</div>
		          	</div>
				</div>
				<div class="row" style="margin-top:50px;" >
					
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;">
						<table class='table table-hover' id='tbl1'>
						 <thead>
							 <tr>
							 	<th  width="50" class="editRecord text-center">Edit</th>
							 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
								<th style='display:none;'>LARowid</th>
							 	<th style='display:none;'>UserRowId</th>
							 	<th>User</th>
							 	<th>From</th>
							 	<th>To</th>
							 	<th>Days</th>
							 	<th>Reason</th>
							 	<th>Approved</th>
							 </tr>
						 </thead>
						 <tbody>
							 <?php 
							 // // print_r($records);
								foreach ($records as $row) 
								{
								 	$rowId = $row['leaveApplicationRowId'];
								 	echo "<tr>";						//onClick="editThis(this);
									echo '<td style="color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
										   <td  style="display:none1; color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
								 	echo "<td style='display:none;'>".$row['leaveApplicationRowId']."</td>";
								 	echo "<td style='display:none;'>".$row['userRowId']."</td>";
								 	echo "<td>".$row['uid']."</td>";
								 	$vdt = strtotime($row['dtFrom']);
									$vdt = date('d-M-Y', $vdt);
								 	echo "<td>".$vdt."</td>";
								 	$vdt = strtotime($row['dtTo']);
									$vdt = date('d-M-Y', $vdt);
								 	echo "<td>".$vdt."</td>";
								 	echo "<td>".$row['totalDays']."</td>";
								 	echo "<td>".$row['reason']."</td>";
								 	echo "<td>".$row['approved']."</td>";
									echo "</tr>";
								}
							 ?>
						 </tbody>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>


<div class="modal" id="myModal" role="dialog">
	<div class="modal-dialog modal-sm">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	      <h4 class="modal-title">WSS</h4>
	    </div>
	    <div class="modal-body">
	      <p>Are you sure <br /> Delete this record..?</p>
	    </div>
	    <div class="modal-footer">
	      <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
	      <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
	    </div>
	  </div>
	</div>
</div>

<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#txtUser").val( $(this).closest('tr').children('td:eq(4)').text() );
		$("#dtFrom").val( $(this).closest('tr').children('td:eq(5)').text() );
		$("#dtTo").val( $(this).closest('tr').children('td:eq(6)').text() );
		$("#txtTotalDays").val( $(this).closest('tr').children('td:eq(7)').text() );
		$("#txtReason").val( $(this).closest('tr').children('td:eq(8)').text() );
		$("#txtReason").focus();
		$("#btnSave").val("Update");
	}


</script>


<script type="text/javascript">

	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});

		$('#dtFrom').trigger('change');
	} );


	///////////////////////////////////////////////////////////////////////

	Date.prototype.addDays = function(days) {
       var dat = new Date(this.valueOf())
       dat.setDate(dat.getDate() + days);
       return dat;
   }

   function getDates(startDate, stopDate) {
      var dateArray = new Array();
      var currentDate = startDate;

      while (currentDate <= stopDate) {
        dateArray.push(currentDate)
        currentDate = currentDate.addDays(1);
      }
      return dateArray;
    }


	$('#dtFrom, #dtTo').change( function(e) 
    {    
    	// alert();
    	var dateArray = getDates( new Date($("#dtFrom").val().trim()), (new Date( $("#dtTo").val().trim() )) );
	    
	    noOfDays=0;

		for (i = 0; i < dateArray.length; i ++ ) 
		{
            noOfDays++;
		}
		// alert(dateArray.length);
		$("#txtTotalDays").val(noOfDays);
    });

	///////////////////////////////////////////////////////////////////////
</script>