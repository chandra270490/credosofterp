<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptPurchaseProductWise_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		 // alert(records.length);
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";
	          cell.style.display="none";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.style.display="none1";
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].poRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");
	          cell.style.display="none";

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].poRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].vType;
	          // cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].vNo;
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = dateFormat(new Date(records[i].billDt));
	          // cell.innerHTML = dateFormat(new Date(records[i].vDt));
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].poDetailRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].productRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].receivedQty;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].rate;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].amt;
	          
	  	  }

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
		    fixedHeader: true
		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		// $("#tbl1 tr").on('click', showDetail);
			
	}

	function loadData()
	{	
		$("#tbl1").empty();
		var productRowId="";
		$('input:checked.chkProducts').each(function() 
		{
			productRowId = $(this).val() + "," + productRowId;
		});
		if(productRowId == "")
		{
			alertPopup("Select product...", 8000);
			return;
		}
		productRowId = productRowId.substr(0, productRowId.length-1);

		var partyRowId="";
		$('input:checked.chkParties').each(function() 
		{
			partyRowId = $(this).val() + "," + partyRowId;
		});
		if(partyRowId == "")
		{
			alertPopup("Select party...", 8000);
			return;
		}
		partyRowId = partyRowId.substr(0, partyRowId.length-1);

		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		// alert(partyRowId);
		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productRowId': productRowId
							, 'partyRowId': partyRowId
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						console.log(data);
						setTable(data['records']) 
						alertPopup('Records loaded...', 4000);
					}
				}
		});
	}



</script>
<div class="container-fluid" style="width: 95%">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h3 class="text-center" style='margin-top:-20px'>Purchase Report (Product Wise)</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				
				<div class="row" style="margin-top:2px;">
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<input type="text" id="txtSearchProduct" class="form-control" placeholder="Filter Products Here" style='margin: 2px'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllProducts">
							<label class="lblChkProducts" for="chkAllProducts" style="font-weight:bold;color:black;">All Products
						</span>
						
						<?php
							foreach ($products as $key => $value) 
							{
						?>
								<div class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkProducts" txt='<?php echo $value;?>' id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
									<label class="lblChkProducts" style="color: black;" for='<?php echo $key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<input type="text" id="txtSearchParty" class="form-control" placeholder="Filter Party Here" style='margin: 2px'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllParties">
							<label class="lblChkParty" for="chkAllParties" style="font-weight:bold;color:black;">All Parties
						</span>
						
						<?php
							foreach ($parties as $key => $value) 
							{
						?>
								<div class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkParties" txt='<?php echo $value;?>' id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
									<label class="lblChkParty" style="color: black;" for='<?php echo $key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From (Ref Bill Dt.):</label>";
							echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtFrom" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});

						    // Set the Current Date-50 as Default
						    // dt=new Date();
     					//     dt.setDate(dt.getDate() - 50);
   		 				// 	$("#dtFrom").val(dateFormat(dt));

   		 					// Set the Current Date as Default
							$("#dtFrom").val(dateFormat(new Date()));
						</script>					
		          	</div>
		          	<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To (Ref Bill Dt.):</label>";
							echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
		              	?>
		              	<script>
							$( "#dtTo" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dtTo").val(dateFormat(new Date()));
						</script>					
		          	</div>
					
					
					<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;box-shadow:5px 5px #d3d3d3;border-radius:25px">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th style='display:none;' width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>poRowid</th>
					 	<th style='display:none1;'>V. Type</th>
					 	<th style='display:none1;'>V.No.</th>
					 	<th style='display:none1;'>Dt.</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th style='display:none;'>poDetailRowid</th>
					 	<th style='display:none;'>ProductRowId</th>
					 	<th>Product</th>
					 	<th>R.Qty</th>
					 	<th>Rate</th>
					 	<th>Amt</th>

					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>


</div>





<script type="text/javascript">
		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});

			$("#txtSearchProduct").on('keyup', searchProductInList)
			$("#txtSearchParty").on('keyup', searchPartyInList)
		} );

		function searchProductInList()
		{
			txt = $(this).val().trim().toLowerCase();
			// var value = $(this).val().toLowerCase();
		    $(".lblChkProducts").filter(function() {
		      $(this).parent().toggle($(this).text().toLowerCase().indexOf(txt) > -1)
		    });
		}
		function searchPartyInList()
		{
			txt = $(this).val().trim().toLowerCase();
			// var value = $(this).val().toLowerCase();
		    $(".lblChkParty").filter(function() {
		      $(this).parent().toggle($(this).text().toLowerCase().indexOf(txt) > -1)
		    });
		}


	// add multiple select / deselect functionality
	$("#chkAllProducts").click(function () {
		// alert();
		  $('.chkProducts').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkProducts').bind('click', chkSelectAll);
	function chkSelectAll()
	{
		var x = parseInt($(".chkProducts").length);
		var y = parseInt($(".chkProducts:checked").length);
		if( x == y ) 
		{
			$("#chkAllProducts").prop("checked", true);
		} 
		else 
		{
			$("#chkAllProducts").prop("checked", false);
		}
	}

	// add multiple select / deselect functionality
	$("#chkAllParties").click(function () {
		// alert();
		  $('.chkParties').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkParties').bind('click', chkSelectAllParties);
	function chkSelectAllParties()
	{
		var x = parseInt($(".chkParties").length);
		var y = parseInt($(".chkParties:checked").length);
		if( x == y ) 
		{
			$("#chkAllParties").prop("checked", true);
		} 
		else 
		{
			$("#chkAllParties").prop("checked", false);
		}
	}
</script>