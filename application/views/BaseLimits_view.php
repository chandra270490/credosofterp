<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='BaseLimits_Controller';
	var base_url='<?php echo site_url();?>';

	
	
	function saveData()
	{	
		pfBase = $("#txtPfBase").val().trim();
		if(isNaN(pfBase) == true)
		{
			alertPopup("Enter valid PF Base Limit...", 8000);
			$("#txtPfBase").focus();
			return;
		}
		pfEmployee = $("#txtPfEmployee").val().trim();
		if(isNaN(pfEmployee) == true)
		{
			alertPopup("Enter valid PF Employee Contri...", 8000);
			$("#txtPfEmployee").focus();
			return;
		}
		pfEmployer = $("#txtPfEmployer").val().trim();
		if(isNaN(pfEmployer) == true)
		{
			alertPopup("Enter valid PF Employer Contri...", 8000);
			$("#txtPfEmployer").focus();
			return;
		}
		esiBase = $("#txtEsiBase").val().trim();
		if(isNaN(esiBase) == true)
		{
			alertPopup("Enter valid ESI Base Limit...", 8000);
			$("#txtEsiBase").focus();
			return;
		}
		esiEmployee = $("#txtEsiEmployee").val().trim();
		if(isNaN(esiEmployee) == true)
		{
			alertPopup("Enter valid ESI Employee Contri...", 8000);
			$("#txtEsiEmployee").focus();
			return;
		}
		esiEmployer = $("#txtEsiEmployer").val().trim();
		if(isNaN(esiEmployer) == true)
		{
			alertPopup("Enter valid ESI Employer Contri...", 8000);
			$("#txtEsiEmployer").focus();
			return;
		}
		if($("#btnSave").val() == "Save Changes")
		{
			// alert("save");
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					// 'dataType': 'json',
					'data': {
								'pfBase': pfBase
								, 'pfEmployee': pfEmployee
								, 'pfEmployer': pfEmployer
								, 'esiBase': esiBase
								, 'esiEmployee': esiEmployee
								, 'esiEmployer': esiEmployer
							},
					'success': function(data)
					{
						alert("Changes Saved...");
						location.reload();
					}
			});
		}
	}


</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Base Limits</h1>
			<div class="row" style="margin-top:25px;">
				<?php 
					$pfBase = 0;
					$pfEmployee = 0;
					$pfEmployer = 0;
					$esiBase = 0;
					$esiEmployee = 0;
					$esiEmployer = 0;
					if( count($records) > 0 )
					{
						$pfBase = $records[0]['pfBase'];
						$pfEmployee = $records[0]['pfEmployee'];
						$pfEmployer = $records[0]['pfEmployer'];
						$esiBase = $records[0]['esiBase'];
						$esiEmployee = $records[0]['esiEmployee'];
						$esiEmployer = $records[0]['esiEmployer'];
					}
				?>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>PF Base Limit (Rs.):</label>";
						echo form_input('txtPfBase', $pfBase, "class='form-control' autofocus id='txtPfBase' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>PF Employee Contri.(%): &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>";
						echo form_input('txtPfEmployee', $pfEmployee, "class='form-control' id='txtPfEmployee' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>PF Employer Contri.(%):</label>";
						echo form_input('txtPfEmployer', $pfEmployer, "class='form-control' id='txtPfEmployer' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>ESI Base Limit (Rs.):</label>";
						echo form_input('txtEsiBase', $esiBase, "class='form-control' id='txtEsiBase' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>ESI Employee Contri.(%):</label>";
						echo form_input('txtEsiEmployee', $esiEmployee, "class='form-control' id='txtEsiEmployee' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
	          	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>ESI Employer Contri.(%):</label>";
						echo form_input('txtEsiEmployer', $esiEmployer, "class='form-control' id='txtEsiEmployer' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
			</div>

			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
				</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" style="margin-bottom:5%">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save Changes' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>
</div>




<script type="text/javascript">

</script>