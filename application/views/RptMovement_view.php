<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptMovement_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      var totQty=0;
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);

	          var cell = row.insertCell(0);
	          // cell.style.display="none";
	          cell.innerHTML = i+1;
	          var cell = row.insertCell(1);
	          cell.style.display="none";
	          cell.innerHTML = records[i].rowId;
	          var cell = row.insertCell(2);
	          // cell.style.display="none";
	          cell.innerHTML = dateFormat(new Date(records[i].dt));
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].empRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].name;
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].productRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].productName;
	          // cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].fromStage;
	          cell.style.display="none";
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].fromStageName;
	          // cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].toStage;
	          cell.style.display="none";
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].toStageName;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].qty;
	          totQty += parseInt(records[i].qty);
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].weightage;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].direction;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].remarks;
	  	  }

	  	  $("#txtTotalQty").val(totQty);
	  	// $('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		var empRowId="";
		$('input:checked.chkEmployees').each(function() 
		{
			empRowId = $(this).val() + "," + empRowId;
		});
		if(empRowId == "")
		{
			alertPopup("Select employee...", 8000);
			return;
		}
		empRowId = empRowId.substr(0, empRowId.length-1);
		// alert(empRowId);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'empRowId': empRowId
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']) 
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "sn" : $(tr).find('td:eq(0)').text()
		            , "dt" : $(tr).find('td:eq(2)').text()
		            , "emp" :$(tr).find('td:eq(4)').text()
		            , "productName" :$(tr).find('td:eq(6)').text()
		            , "fromStage" :$(tr).find('td:eq(8)').text()
		            , "toStage" :$(tr).find('td:eq(10)').text()
		            , "qty" :$(tr).find('td:eq(11)').text()
		            , "weightage" :$(tr).find('td:eq(12)').text()
		            , "direction" :$(tr).find('td:eq(13)').text()
		            , "remarks" :$(tr).find('td:eq(14)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		// alert(tblRowsCount);
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}

		empRowId = $("#cboEmp").val();
		var emp = $("#cboEmp option:selected").text();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'emp': emp
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Stage Movement Log</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllEmployees">
							<label for="chkAllEmployees" style="font-weight:bold;color:black;">All Employees
						</span>
						
						<?php
							foreach ($employees as $key => $value) 
							{
						?>
								<div class="checkbox1" style='margin-left:-30px;'>
									<input type="checkbox" class="chkEmployees" txt='<?php echo $value;?>' id='<?php echo "P".$key;?>' value='<?php echo $key;?>'></input>
									<label  style="color: black;" for='<?php echo  "P".$key;?>'><?php echo $value;?></label>
								</div>
						<?php
							}
						?>
					</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<div class="row">
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>From:</label>";
									echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
				              	?>
				              	<script>
									$( "#dtFrom" ).datepicker({
										dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
									});
								    // Set the Current Date as Default
									$("#dtFrom").val(dateFormat(new Date()));
								</script>					
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>To:</label>";
									echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
				              	?>
				              	<script>
									$( "#dtTo" ).datepicker({
										dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
									});
								    // Set the Current Date as Default
									$("#dtTo").val(dateFormat(new Date()));
								</script>					
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
								<?php
									echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
									echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
				              	?>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:400px; overflow:auto;box-shadow:5px 5px #d3d3d3;border-radius:25px">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
						<th style='display:none1;'>S.N.</th>
					 	<th style='display:none;'>rowId</th>
					 	<th style='display:none1;'>Date</th>
					 	<th style='display:none;'>empRowId</th>
					 	<th>Employee</th>
					 	<th style='display:none;'>ProductRowId</th>
					 	<th>Product</th>
					 	<th style='display:none;'>fromStageRowId</th>
					 	<th>From Stage</th>
					 	<th style='display:none;'>toStageRowId</th>
					 	<th>To Stage</th>
					 	<th>Qty</th>
					 	<th>Weightage</th>
					 	<th>Direction</th>
					 	<th>Remarks</th>
					 </tr>
				 </thead>
				 <tbody>

				 </tbody>
				</table>
			</div>

			<div class="row" style="margin-top:45px;">
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Qty.:</label>";
						echo form_input('txtTotalQty', '', "class='form-control' placeholder='' id='txtTotalQty' maxlength='10' disabled");
	              	?>
	          	</div>
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
				</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='exportData();' value='Export In Excel' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	
	              	?>
	          	</div>
			</div>

		</div>




		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>





<script type="text/javascript">


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );




	// $(document).ready(function() {
	//    // $("#cboProductCategories").append('<option value="ALL">ALL</option>');
	//    var opt = "<option value='ALL'>ALL</option>";
	//    var idx=2;
	//    $(opt).insertBefore("#cboProductCategories option:nth-child(" + idx + ")");
	//   });

	// add multiple select / deselect functionality
	$("#chkAllEmployees").click(function () {
		// alert();
		  $('.chkEmployees').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkEmployees').bind('click', chkSelectAllEmployees);
	function chkSelectAllEmployees()
	{
		var x = parseInt($(".chkEmployees").length);
		var y = parseInt($(".chkEmployees:checked").length);
		if( x == y ) 
		{
			$("#chkAllEmployees").prop("checked", true);
		} 
		else 
		{
			$("#chkAllEmployees").prop("checked", false);
		}
	}	
</script>