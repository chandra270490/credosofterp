<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Invoice_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          if( records[i].deleted == "N")
	          {
		          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
		          cell.style.textAlign = "center";
		          cell.style.color='lightgray';
		          cell.setAttribute("onmouseover", "this.style.color='green'");
		          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
		          cell.className = "editRecord";
	      	  }
	      	  else
	      	  {
		          cell.innerHTML = "<span class='glyphicon glyphicon-pencil1'></span>";
		          cell.style.textAlign = "center";
		          cell.style.color='lightgray';
		          cell.setAttribute("onmouseover", "this.style.color='green'");
		          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
		          cell.className = "editRecord1";
	      	  }

	          var cell = row.insertCell(1);
	          if( records[i].deleted == "N")
	          {
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
		          cell.style.textAlign = "center";
		          cell.style.color='lightgray';
		          // cell.style.display="none";
		          cell.setAttribute("onmouseover", "this.style.color='red'");
		          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
		          cell.setAttribute("onclick", "delrowid(" + records[i].ciRowId +")");
		          // data-toggle="modal" data-target="#myModal"
		          cell.setAttribute("data-toggle", "modal");
		          cell.setAttribute("data-target", "#myModal");
		      }
		      else
	      	  {
	      	  	  cell.innerHTML = "Cancelled";
	      	  	  cell.style.color='blue';
	      	  }
	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].ciRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = dateFormat(new Date(records[i].ciDt));
	          // cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].name;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].totalAmt;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].discountPer;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].discountAmt;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].totalAfterDiscount;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].vatPer;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].vatAmt;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].sgstPer;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].sgstAmt;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].cgstPer;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].cgstAmt;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].igstPer;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].igstAmt;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].net;
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].totalQty;	
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].roundAmt;	
	          var cell = row.insertCell(21);
	          cell.innerHTML = records[i].cpe;	
	          var cell = row.insertCell(22);
	          cell.innerHTML = records[i].vType + "-" + records[i].vNo;	
	          var cell = row.insertCell(23);
	          cell.innerHTML = records[i].otherExpName;	
	          var cell = row.insertCell(24);
	          cell.innerHTML = records[i].otherExpAmt;	
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}



	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblProducts tr').each(function(row, tr)
	    {
	    	if( $(tr).find('td:eq(0)').find('input[type=checkbox]').is(':checked') && row>0)
    		{
	        	TableData[i]=
	        	{
		            "qpoRowId" : $(tr).find('td:eq(1)').text()
		            , "qpoDetailRowId" : $(tr).find('td:eq(2)').text()
		            , "odrNo" :$(tr).find('td:eq(3)').text()
		            , "productRowId" :$(tr).find('td:eq(6)').text()
		            , "productName" :$(tr).find('td:eq(7)').text()
		            , "qty" :$(tr).find('td:eq(9)').text()
		            , "rate" :$(tr).find('td:eq(10)').text()
		            , "amt" :$(tr).find('td:eq(11)').text()
		            , "odrDt" :$(tr).find('td:eq(16)').text()
		            , "despatchDetailRowId" :$(tr).find('td:eq(19)').text()
		            , "ciDetailRowId" :$(tr).find('td:eq(21)').text()
		            , "hsn" :$(tr).find('td:eq(23)').text()
		            , "letterNo" :$(tr).find('td:eq(25)').text()
		            
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i;
	    return TableData;
	}


	function saveData()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount <= 0)
		{
			alertPopup("no rows selected...", 8000);
			$("#cboParty").focus();
			return;
		}

		partyRowId = $("#cboParty").val();
		if(partyRowId == "-1")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}

		var ciDt = $("#dt").val().trim();
		dtOk = testDate("dt");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dt").focus();
			return;
		}
		deliveryAddress = $("#tblProducts").find("tr:eq(1)").find("td:eq(22)").text();
		// alert(deliveryAddress);
		// return;

		totalAmt = $("#txtTotalAmt").val();
		discountPer = $("#txtDiscountPer").val();
		discountAmt = $("#txtDiscountAmt").val();
		totalAfterDiscount = $("#txtTotalAfterDiscount").val();
		vatPer = $("#txtVatPer").val();
		vatAmt = $("#txtVatAmt").val();
		sgstPer = $("#txtSgstPer").val();
		sgstAmt = $("#txtSgstAmt").val();
		cgstPer = $("#txtCgstPer").val();
		cgstAmt = $("#txtCgstAmt").val();
		igstPer = $("#txtIgstPer").val();
		igstAmt = $("#txtIgstAmt").val();
		roundAmt = $("#txtRound").val();
		otherExpName = $("#txtOtherExpName").val().trim();
		otherExpAmt = $("#txtOtherExpAmt").val();
		net = $("#txtNet").val();
		totalQty = $("#txtTotalQty").val();
		addr = $("#txtAddress").val().trim();
		inWords = $("#txtWords").val().trim();
		exportIn = $("#cboExport").val();

		var cpe = $("#chkCpe").prop("checked");
		// alertPopup(cpe, 4000);
		// return;
		if( cpe == true)
		{
			cpe = "Y";
		}
		else
		{
			cpe = "N";
		}

		if(exportIn == "-1")
		{
			alertPopup("Select export in...", 8000);
			$("#cboExport").focus();
			return;
		}

		if($("#btnSave").val() == "Save")
		{
			var ms = $("#txtPrefix").val();
			var gstIn = $("#txtGstIn").val();
			if(ms == "M/S" && gstIn == "")
			{
				alertPopup("Can not save without GSTIN", 8000);
				// $("#cboExport").focus();
				return;
			}
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					// 'dataType': 'json',
					'data': {
								'partyRowId': partyRowId
								, 'ciDt': ciDt
								, 'totalQty': totalQty
								, 'totalAmt': totalAmt
								, 'discountPer': discountPer
								, 'discountAmt': discountAmt
								, 'totalAfterDiscount': totalAfterDiscount
								, 'vatPer': vatPer
								, 'vatAmt': vatAmt
								, 'sgstPer': sgstPer
								, 'sgstAmt': sgstAmt
								, 'cgstPer': cgstPer
								, 'cgstAmt': cgstAmt
								, 'igstPer': igstPer
								, 'igstAmt': igstAmt
								, 'otherExpName': otherExpName
								, 'otherExpAmt': otherExpAmt
								, 'roundAmt': roundAmt
								, 'net': net
								, 'cpe': cpe
								, 'addr': addr
								, 'inWords': inWords
								, 'exportIn': exportIn
								, 'TableData': TableData
								, 'deliveryAddress': deliveryAddress
							},
					'success': function(data)
					{
						if(data)
						{
								//// setTable(data['records']) ///loading records in tbl1
								alertPopup('Record saved...', 4000);
								window.location.href=data;
								blankControls();
								$("#cboParty").focus();
								$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
								if(exportIn == "W")
								{
									loadLimitedRecords();
								}
						}
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert(JSON.stringify(TableData));
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					// 'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'ciDt': ciDt
								, 'totalQty': totalQty
								, 'totalAmt': totalAmt
								, 'discountPer': discountPer
								, 'discountAmt': discountAmt
								, 'totalAfterDiscount': totalAfterDiscount
								, 'vatPer': vatPer
								, 'vatAmt': vatAmt
								, 'sgstPer': sgstPer
								, 'sgstAmt': sgstAmt
								, 'cgstPer': cgstPer
								, 'cgstAmt': cgstAmt
								, 'igstPer': igstPer
								, 'igstAmt': igstAmt
								, 'otherExpName': otherExpName
								, 'otherExpAmt': otherExpAmt
								, 'roundAmt': roundAmt
								, 'net': net
								, 'cpe': cpe
								, 'addr': addr
								, 'inWords': inWords
								, 'exportIn': exportIn
								, 'TableData': TableData
								, 'deliveryAddress': deliveryAddress
							},
					'success': function(data)
					{
						if(data)
						{
								alertPopup('Record updated...', 4000);
								window.location.href=data;
								blankControls();
								$("#btnShow").prop('disabled', true);
								$("#cboParty").prop('disabled', true);
								$("#cboParty").focus();
								$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
								$("#btnSave").val("Save");
								if(exportIn == "W")
								{
									loadLimitedRecords();
								}
						}
					}
			});
		}
	}


	function loadAllRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}
	function loadLimitedRecords()
	{
		$.ajax({
				'url': base_url + '/' + controller + '/loadLimitedRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						// alertPopup('Records loaded...', 4000);
						// blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}

	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						setTable(data['records']);
						alertPopup('Record deleted...', 4000);
						blankControls();
						$("#txtLetterNo").focus();
						$("#tblProducts").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
					}
				}
			});
	}
	
</script>
<div class="container-fluid" style="width: 95%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Invoice</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
	              	?>
	              	<script>
						$( "#dt" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the Current Date as Default
						$("#dt").val(dateFormat(new Date()));
					</script>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						// $parties=9;
						echo "<label style='color: black; font-weight: normal;'>Party Name:</label>";
						// echo form_dropdown('cboParty',$parties, '-1', "class='form-control' id='cboParty'");
	              	?>
			        <select id="cboParty" class="form-control">
			              <option value="-1" addr="" mobile1="" mobile2="" townName="" stateName="">--- Select ---</option>
			              <?php
			                foreach ($parties as $row) 
			                {
			              ?>
			              <option value=<?php echo $row['partyRowId']; ?>  addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" stateName="<?php echo $row['stateName']; ?>"  pin="<?php echo $row['pin']; ?>" ><?php echo $row['name']; ?></option>
			              <?php
			                }
			              ?>
				    </select>
		            <script type="text/javascript">
			            $(document).ready(function()
			            {
			              $("#cboParty").change(function()
			              {
						        $("#txtAddress").val($('option:selected', '#cboParty').attr('addr'));
						        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('townName') + " - " + $('option:selected', '#cboParty').attr('pin') + " [" + $('option:selected', '#cboParty').attr('stateName') + "]");
						        // $("#txtAddress").val( $("#txtAddress").val() + " - " + $('option:selected', '#cboParty').attr('pin'));

						        $("#txtAddress").val( $("#txtAddress").val() + "\r\n" + $('option:selected', '#cboParty').attr('mobile1'));
						        $("#txtAddress").val( $("#txtAddress").val() + ", " + $('option:selected', '#cboParty').attr('mobile2'));

						        if( $('option:selected', '#cboParty').attr('stateName').toUpperCase() == "RAJASTHAN" )
						        {
						        	$("#txtSgstPer").val("9");
						        	$("#txtCgstPer").val("9");
						        	$("#txtIgstPer").val("0");
						        }
						        else
						        {
						        	$("#txtSgstPer").val("0");
						        	$("#txtCgstPer").val("0");
						        	$("#txtIgstPer").val("18");
						        }
						        doTotals();
			              });
			            });
			        </script>	
	          	</div>
	          	<div style="display:none;">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Address:</label>";
						echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px;' id='txtAddress'  maxlength='255' value=''");
	              	?>
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label>";
						echo "<input type='button' onclick='loadData();' value='Show Orders' id='btnShow' class='btn form-control' style='background-color: #FF5733; color:white;'>";
	              	?>

	          	</div>
			</div>

			<div class="row" style="margin-top:10px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:0 dashed lightgray; padding: 10px;height:200px; overflow:auto;">
					<table class='table table-bordered' id='tblProducts' style="table-layout: fixed; " width="100%">
						<tr>
						 	<th width="50" style='display:none1;'><input type='checkbox' id='chkHead' name='chkHead'/></th>
						 	<th width="50" class="text-center" style='display:none;'>QpoRowId</th>
							<th style='display:none;'>QpoDetailRowId</th>
						 	<th width="50" style='display:none1;'>O.No.</th>
						 	<th style='display:none;'>ProductCatRowId</th>
						 	<th style='display:none;'>ProductCat</th>
						 	<th style='display:none;'>ProductRowId</th>
						 	<th width="150">Product</th>
						 	<th width="70">Odr.Qty</th>
						 	<th width="70">Des.Qty</th>
						 	<th width="70">Rate</th>
						 	<th width="70">Amt</th>
						 	<th style='display:none;'>ColourRowId</th>
						 	<th width="70">Colour</th>
						 	<th width="150">Remarks</th>
						 	<th width="100">Order Type</th>
						 	<th width="140">Order Dt.</th>
						 	<th width="140">Commit. Dt.</th>
						 	<th style='display:none;' width="140">DespRowId</th>
						 	<th style='display:none1;' width="140">DespDetailRowId</th>
						 	<th style='display:none1;' width="140">CiRowId</th>
						 	<th style='display:none1;' width="140">CiDetailRowId</th>
						 	<th style='display:none1;' width="140">D. Addr.</th>
						 	<th style='display:none1;' width="140">HSN</th>
						 	<th style='display:none1;' width="140">OdrDiscount</th>
						 	<th style='display:none1;' width="140">PO No.</th>
						</tr>
					</table>
				</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Amt.:</label>";
						echo '<input type="number" disabled name="txtTotalAmt" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total Qty.:</label>";
						echo '<input type="number" disabled name="txtTotalQty" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalQty" />';
	              	?>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;</label><br />";
						echo '<input type="checkbox"  name="chkCpe" class="" id="chkCpe" />';
						echo "<label for='chkCpe' style='color: black; font-weight: normal;'> &nbsp; Children's Playground Equipments</label>";
	              	?>
	          	</div>
			</div>
			
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Discount (%):</label>";
						echo '<input type="number" step="1" name="txtDiscountPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtDiscountPer" />';
	              	?>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Discount Amt.:</label>";
						echo '<input type="number" step="1" name="txtDiscountAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtDiscountAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Total After Discount.:</label>";
						echo '<input type="number" disabled name="txtTotalAfterDiscount" value="" placeholder="" class="form-control" maxlength="20" id="txtTotalAfterDiscount" />';
	              	?>
	          	</div>
			</div>
		
			<div class="row" style="margin-top:15px;">
			<!-- PURANA MAMLA -->
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>VAT (%):</label>";
						echo '<input type="number" step="1" name="txtVatPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtVatPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>VAT Amt.:</label>";
						echo '<input type="number" disabled name="txtVatAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtVatAmt" />';
	              	?>
	          	</div>

	          	<!-- NAYA TAX -->
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>SGST(%):</label>";
						echo '<input type="number" step="1" name="txtSgstPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtSgstPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>SGST Amt.:</label>";
						echo '<input type="number" disabled name="txtSgstAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtSgstAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>CGST(%):</label>";
						echo '<input type="number" step="1" name="txtCgstPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtCgstPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>CGST Amt.:</label>";
						echo '<input type="number" disabled name="txtCgstAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtCgstAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>IGST(%):</label>";
						echo '<input type="number" step="1" name="txtIgstPer" value="0" placeholder="" class="form-control" maxlength="20" id="txtIgstPer" />';
	              	?>
	          	</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>IGST Amt.:</label>";
						echo '<input type="number" disabled name="txtIgstAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtIgstAmt" />';
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Other Exp. Name:</label>";
						echo '<input type="text" name="txtOtherExpName" class="form-control" maxlength="40" id="txtOtherExpName" />';
	              	?>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Other Exp. Amt.:</label>";
						echo '<input type="number" name="txtOtherExpAmt" value="0" placeholder="" class="form-control" maxlength="20" id="txtOtherExpAmt" />';
	              	?>
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Round:</label>";
						echo '<input type="number" name="txtRound" value="0" placeholder="" class="form-control" maxlength="20" id="txtRound" />';
	              	?>
	          	</div>
			</div>

		
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Net Amt.:</label>";
						echo '<input type="number" disabled name="txtNet" value="" placeholder="" class="form-control" maxlength="20" id="txtNet" />';
	              	?>
	          	</div>
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>In Words:</label>";
						echo '<input type="text" disabled name="txtWords" value="" placeholder="" class="form-control" id="txtWords" />';
	              	?>
	          	</div>
			</div>


			<div class="row" style="margin-top:15px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
								$types = array();
								$types['-1'] = '--- Select ---';
								$types['W'] = "Word";
								$types['P'] = "PDF";
								echo "<label style='color: black; font-weight: normal;'>Export in: <span style='color: red;'>*</span></label>";
								echo form_dropdown('cboExport', $types, 'P', "class='form-control' id='cboExport'");
							?> 
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>

			
			<!-- <div class="row" style="margin-top:10px;">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
	          	</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div> -->


		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

		<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  style='display:none1;' width="50" class="text-center">Cancel</th>
						<th style='display:none;'>ciRowid</th>
					 	<th>Dt</th>
					 	<th style='display:none;'>PartyRowId</th>
					 	<th>Party</th>
					 	<th>Total Amt</th>
					 	<th>Dis. Per.</th>
					 	<th>Dis. Amt.</th>
					 	<th>Amt. After Dis.</th>
					 	<th>VAT %</th>
					 	<th>VAT Amt</th>
					 	<th>SGST %</th>
					 	<th>SGST Amt</th>
					 	<th>CGST %</th>
					 	<th>CGST Amt</th>
					 	<th>IGST %</th>
					 	<th>IGST Amt</th>
					 	<th>Net</th>
					 	<th>Total Qty.</th>
					 	<th>Round</th>
					 	<th>CPE</th>
					 	<th>V.No.</th>
					 	<th>Other Exp. Name</th>
					 	<th>Other Exp. Amt</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						foreach ($records as $row) 
						{
						 	$rowId = $row['ciRowId'];
						 	echo "<tr>";	
						 	if( $row['deleted'] == 'N')
							{					//onClick="editThis(this);
								echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>';
								echo '<td  style="display:none1; color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
							}
							else
							{
								echo '<td style="color: lightgray;cursor1: pointer;cursor: hand1;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil1"></span></td>';
								echo '<td style="color: blue;" class="text-center" >Cencelled</td>';
							}
						 	echo "<td style='display:none;'>".$row['ciRowId']."</td>";
						 	$vdt = strtotime($row['ciDt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['partyRowId']."</td>";
						 	echo "<td>".$row['name']."</td>";
						 	echo "<td>".$row['totalAmt']."</td>";
						 	echo "<td>".$row['discountPer']."</td>";
						 	echo "<td>".$row['discountAmt']."</td>";
						 	echo "<td>".$row['totalAfterDiscount']."</td>";
						 	echo "<td>".$row['vatPer']."</td>";
						 	echo "<td>".$row['vatAmt']."</td>";
						 	echo "<td>".$row['sgstPer']."</td>";
						 	echo "<td>".$row['sgstAmt']."</td>";
						 	echo "<td>".$row['cgstPer']."</td>";
						 	echo "<td>".$row['cgstAmt']."</td>";
						 	echo "<td>".$row['igstPer']."</td>";
						 	echo "<td>".$row['igstAmt']."</td>";
						 	echo "<td>".$row['net']."</td>";
						 	echo "<td>".$row['totalQty']."</td>";
						 	echo "<td>".$row['roundAmt']."</td>";
						 	echo "<td>".$row['cpe']."</td>";
						 	echo "<td>".$row['vType']."-".$row['vNo']."</td>";
						 	echo "<td>".$row['otherExpName']."</td>";
						 	echo "<td>".$row['otherExpAmt']."</td>";
							echo "</tr>";
						}
					?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-7 col-sm-7 col-md-7 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div>

	</div>
</div>

<div id="partyInfo">
	<input type="text" id="txtPrefix" disabled />
	<input type="text" id="txtGstIn" disabled />
</div>


		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();
		// alert(globalrowid);

		$("#dt").val($(this).closest('tr').children('td:eq(3)').text());
		$("#cboParty").val($(this).closest('tr').children('td:eq(4)').text());
		$('#cboParty').trigger('change');
		$("#txtTotalAmt").val($(this).closest('tr').children('td:eq(6)').text());
		$("#txtDiscountPer").val($(this).closest('tr').children('td:eq(7)').text());
		$("#txtDiscountAmt").val($(this).closest('tr').children('td:eq(8)').text());
		$("#txtTotalAfterDiscount").val($(this).closest('tr').children('td:eq(9)').text());
		$("#txtVatPer").val($(this).closest('tr').children('td:eq(10)').text());
		$("#txtVatAmt").val($(this).closest('tr').children('td:eq(11)').text());
		$("#txtSgstPer").val($(this).closest('tr').children('td:eq(12)').text());
		$("#txtSgstAmt").val($(this).closest('tr').children('td:eq(13)').text());
		$("#txtCgstPer").val($(this).closest('tr').children('td:eq(14)').text());
		$("#txtCgstAmt").val($(this).closest('tr').children('td:eq(15)').text());
		$("#txtIgstPer").val($(this).closest('tr').children('td:eq(16)').text());
		$("#txtIgstAmt").val($(this).closest('tr').children('td:eq(17)').text());
		$("#txtOtherExpName").val($(this).closest('tr').children('td:eq(23)').text());
		$("#txtOtherExpAmt").val($(this).closest('tr').children('td:eq(24)').text());
		$("#txtNet").val($(this).closest('tr').children('td:eq(18)').text());
		$("#txtTotalQty").val($(this).closest('tr').children('td:eq(19)').text());
		$("#txtRound").val($(this).closest('tr').children('td:eq(20)').text());
		var cpe = $(this).closest('tr').children('td:eq(15)').text();
		if ( cpe == "Y" )
		{
			$("#chkCpe").prop("checked", true);
		}
		else
		{
			$("#chkCpe").prop("checked", false);
		}

		var netInWords = number2text( parseFloat( $("#txtNet").val() ) );
	  	$("#txtWords").val( netInWords );

		$.ajax({
			'url': base_url + '/Invoice_Controller/getProductsFromCi',
			'type': 'POST', 
			'data':{'rowid':globalrowid},
			'dataType': 'json',
			'success':function(data)
			{
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        // alert(data['products'].length);
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML = "<input type='checkbox' class='chk' 'checked' id='chk' name='chk'/>"; 
		          // cell.style.display = "none";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['products'][i].ciRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].ciDetailRowId; 
		          cell.style.display = "none";
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].odrNo;  //earlier qpoRowId
		          // cell.style.display = "none";
		          var cell = row.insertCell(4);
		          cell.innerHTML = ''; 
		          cell.style.display = "none";
		          var cell = row.insertCell(5);
		          cell.innerHTML = ''; 
		          cell.style.display = "none";
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].productName;
		          // cell.style.textAlign = "right";
		          // // cell.style.display = "none";
		          var cell = row.insertCell(8);
		          cell.innerHTML = ''; //data['products'][i].pendingQty;
		          // cell.style.textAlign = "right";
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].qty;
		          cell.style.textAlign = "right";
		          // cell.style.color = "red";
		          // cell.setAttribute("contentEditable", true);
		          // cell.className = "classDespatchQty";
		          // // cell.style.display = "none";
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['products'][i].rate;
		          cell.style.textAlign = "right";
		          cell.style.color = "red";
		          cell.className = "classRate";
		          cell.setAttribute("contentEditable", true);
		          var cell = row.insertCell(11);
		          cell.innerHTML = data['products'][i].amt;
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(12);
		          cell.innerHTML = ''; 
		          cell.style.display = "none";
		          var cell = row.insertCell(13);
		          cell.innerHTML = ''; 
		          var cell = row.insertCell(14);
		          cell.innerHTML = ''; 
		          var cell = row.insertCell(15);
		          cell.innerHTML = ''; 
		          var cell = row.insertCell(16);
		          cell.innerHTML = dateFormat(new Date(data['products'][i].vDt));
		          var cell = row.insertCell(17);
		          cell.innerHTML = ''; 
		          var cell = row.insertCell(18);
		          cell.innerHTML = '';
		          cell.style.display = "none";
		          var cell = row.insertCell(19);
		          cell.innerHTML = data['products'][i].despatchDetailRowId;
		          cell.style.display = "none1";
		          var cell = row.insertCell(20);
		          cell.innerHTML = data['products'][i].ciRowId;
		          var cell = row.insertCell(21);
		          cell.innerHTML = data['products'][i].ciDetailRowId;
		          var cell = row.insertCell(22);
		          cell.innerHTML = data['products'][i].deliveryAddress;
		          var cell = row.insertCell(23);
		          cell.innerHTML = data['products'][i].hsn;
		          var cell = row.insertCell(24);
		          cell.innerHTML = "";
		          var cell = row.insertCell(25);
		          cell.innerHTML = data['products'][i].letterNo;
		        }	
		        $('.classRate').bind('keyup', doCalculation);
		        $('.chk').prop('checked', true);
			}
		});


		$("#btnShow").prop('disabled', true);
		$("#cboParty").prop('disabled', true);

		$("#btnSave").val("Update");
	}


	$(document).ready( function () {
	    myDataTable = $('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
	} );

	function loadData()
	{
		var partyRowId = $("#cboParty").val();
		if(partyRowId == "-1")
		{
			alertPopup("Select party...", 8000);
			$("#cboParty").focus();
			return;
		}
		$.ajax({
			'url': base_url + '/Invoice_Controller/getProducts',
			'type': 'POST', 
			'data':{'partyRowId':partyRowId},
			'dataType': 'json',
			'success':function(data)
			{
				// alert(JSON.stringify(data))
				$("#tblProducts").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblProducts");
		        for(i=0; i<data['products'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
				  cell.innerHTML = "<input type='checkbox' class='chk' id='chk' name='chk'/>";
		          var cell = row.insertCell(1);
		          cell.innerHTML =  data['products'][i].qpoRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['products'][i].qpoDetailRowId; ///qpoDetailRowId
		          cell.style.display = "none";
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['products'][i].odrNo; //odrNo
		          // // // cell.style.display = "none";
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['products'][i].productCategoryRowId ;
		          cell.style.display = "none";
		          var cell = row.insertCell(5);
		          cell.innerHTML = data['products'][i].productCategory;
		          cell.style.display = "none";
		          var cell = row.insertCell(6);
		          cell.innerHTML = data['products'][i].productRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(7);
		          cell.innerHTML = data['products'][i].productName;
		          var cell = row.insertCell(8);
		          cell.innerHTML = data['products'][i].odrQty; ///odr qty
		          cell.style.textAlign = "right";
		          // cell.style.display = "none";
		          var cell = row.insertCell(9);
		          cell.innerHTML = data['products'][i].despatchQty;
		          cell.style.textAlign = "right";
		          // cell.style.color = "blue";
		          var cell = row.insertCell(10);
		          cell.innerHTML = data['products'][i].rate;
		          cell.style.textAlign = "right";
		          cell.style.color = "red";
		          cell.setAttribute("contentEditable", true);
		          cell.className = "classRate";
		          // cell.style.display = "none";
		          var cell = row.insertCell(11);
		          cell.innerHTML = (data['products'][i].despatchQty * data['products'][i].rate).toFixed(2);
		          cell.style.textAlign = "right";
		          var cell = row.insertCell(12);
		          cell.innerHTML = data['products'][i].colourRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(13);
		          cell.innerHTML = data['products'][i].colourName;
		       //    cell.style.textAlign = "right";
		          var cell = row.insertCell(14);
		          cell.innerHTML = data['products'][i].remarks;
		          // cell.style.display = "none";
		          var cell = row.insertCell(15);
		          cell.innerHTML = data['products'][i].orderType;
		          var cell = row.insertCell(16);
		          cell.innerHTML = dateFormat(new Date(data['products'][i].odrDt));
		          var cell = row.insertCell(17);
		          if( data['products'][i].commitmentDate == null)
		          {
		          	cell.innerHTML = "";
		          }
		          else
		          {
			          cell.innerHTML = dateFormat(new Date(data['products'][i].commitmentDate));
			      }
		          var cell = row.insertCell(18);
		          cell.innerHTML = data['products'][i].despatchRowId;
		          cell.style.display = "none";
		          var cell = row.insertCell(19);
		          cell.innerHTML = data['products'][i].despatchDetailRowId;
		          cell.style.display = "none1";
		          var cell = row.insertCell(20);
		          cell.innerHTML = '';
		          // cell.style.display = "none";
		          var cell = row.insertCell(21);
		          cell.innerHTML = '';
		          // cell.style.display = "none";
		          var cell = row.insertCell(22);
		          cell.innerHTML = data['products'][i].deliveryAddress;
		          var cell = row.insertCell(23);
		          cell.innerHTML = data['products'][i].hsn;
		          var cell = row.insertCell(24);
		          cell.innerHTML = data['products'][i].discountPer;
		          var cell = row.insertCell(25);
		          cell.innerHTML = data['products'][i].letterNo;

		          $("#txtDiscountPer").val(data['products'][i].discountPer);
		        }	
		        $('.classRate').bind('keyup', doCalculation);
		        $('.chk').bind('click', chkSelectAll);
		        $('.chk').bind('click', doTotals);
		        $('#chkHead').bind('click', doTotals);
				// doTotals();

				$("#txtPrefix").val( data['gstIn'][0].prefixType );
				$("#txtGstIn").val( data['gstIn'][0].gstIn );
			}
		});
	}

	$(document).ready(function()
    {
      $("#txtDiscountPer").on('keyup change', doDiscount);
      $("#txtDiscountAmt").on('keyup change', doDiscountOnAmtChange);
      $("#txtVatPer").on('keyup change', doTax);
      $("#txtSgstPer").on('keyup change', doTax);
      $("#txtCgstPer").on('keyup change', doTax);
      $("#txtIgstPer").on('keyup change', doTax);
      $("#txtOtherExpAmt").on('keyup change', doOtherExp);
      $("#txtRound").on('keyup change', doRound);

    });

	function doCalculation()
	{
	    // var col = $(this).parent().children().index($(this));
	    // var row = $(this).parent().parent().children().index($(this).parent());

	    // var pendingQty = $('#myTable').find('tr:eq('+ (row) +')').find('td:eq(' + (2) + ')').text();
		// alert();
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		var qty = $(this).closest('tr').children('td:eq(9)').text();
		var rate = $(this).closest('tr').children('td:eq(10)').text();
		var amt = parseFloat(qty) * parseFloat(rate);
		amt = amt.toFixed(2);
		$(this).closest('tr').children('td:eq(11)').text( amt );
		// $("#txtDiscountPer").val( $(this).closest('tr').children('td:eq(24)').text() );
		doTotals();

	}


	  function doTotals()
	  {
	  	var totalAmt=0;
	  	var totalQty=0;
		// $("#txtDiscountPer").val( $(this).closest('tr').children('td:eq(24)').text() );
	  	
	  	$('#tblProducts tr').each(function(row, tr)
	    {
	    	if( $(tr).find('td:eq(0)').find('input[type=checkbox]').is(':checked') && row>0)
	    	{
		    	if ( isNaN(parseFloat($(tr).find('td:eq(11)').text())) == false )
		    	{
			    	totalAmt += parseFloat($(tr).find('td:eq(11)').text());
			    	totalQty += parseFloat($(tr).find('td:eq(9)').text());
			    }
			}
	    }); 
	    // alert(totalAmt);
	    $("#txtTotalAmt").val(totalAmt.toFixed(2));
	    $("#txtTotalQty").val(totalQty.toFixed(2));
	    doDiscount();
	  }
	  function doDiscount()
	  {
	  	var disPer = $("#txtDiscountPer").val();
	  	var totalAmt = $("#txtTotalAmt").val();
	  	var disAmt = totalAmt * disPer / 100;
	  	$("#txtDiscountAmt").val(disAmt.toFixed(2));
	  	var totalAfterDiscount = totalAmt - disAmt;
	  	$("#txtTotalAfterDiscount").val(totalAfterDiscount.toFixed(2));
	  	doTax();
	  }

	  function doDiscountOnAmtChange()
	  {
	  	var disAmt = $("#txtDiscountAmt").val();
	  	var totalAmt = $("#txtTotalAmt").val();
	  	var disPer = disAmt * 100 / totalAmt;
	  	$("#txtDiscountPer").val(disPer.toFixed(2));
	  	var totalAfterDiscount = totalAmt - disAmt;
	  	$("#txtTotalAfterDiscount").val(totalAfterDiscount.toFixed(2));
	  	doTax();
	  }

	  function doTax()
	  {
	  	var vatPer = $("#txtVatPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var vatAmt = totalAfterDiscount * vatPer / 100;
	  	$("#txtVatAmt").val(vatAmt.toFixed(2));

	  	var sgstPer = $("#txtSgstPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var sgstAmt = totalAfterDiscount * sgstPer / 100;
	  	$("#txtSgstAmt").val(sgstAmt.toFixed(2));
	  	
	  	var cgstPer = $("#txtCgstPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var cgstAmt = totalAfterDiscount * cgstPer / 100;
	  	$("#txtCgstAmt").val(cgstAmt.toFixed(2));
	  	
	  	var igstPer = $("#txtIgstPer").val();
	  	var totalAfterDiscount = $("#txtTotalAfterDiscount").val();
	  	var igstAmt = totalAfterDiscount * igstPer / 100;
	  	$("#txtIgstAmt").val(igstAmt.toFixed(2));
	  	
	  	doOtherExp();
	  }

	  function doOtherExp()
	  {
  	
	  	doRound();
	  }

	  function doRound()
	  {
	  	var net = parseFloat($("#txtTotalAfterDiscount").val()) + parseFloat($("#txtVatAmt").val()) + parseFloat($("#txtSgstAmt").val()) + parseFloat($("#txtCgstAmt").val()) + parseFloat($("#txtIgstAmt").val())+ parseFloat($("#txtOtherExpAmt").val());
	  	$("#txtNet").val(net.toFixed(2));
	  	var roundAmt = $("#txtRound").val();
	  	net = parseFloat($("#txtNet").val()) + parseFloat($("#txtRound").val());
	  	$("#txtNet").val(  net.toFixed(2) );
	  	var netInWords = number2text( parseFloat( $("#txtNet").val() ) ) ;
	  	// alert(netInWords);
	  	$("#txtWords").val( netInWords );
	  }



	function number2text(value) {
	    var fraction = Math.round(frac(value)*100);
	    var f_text  = "";

	    if(fraction > 0) {
	        f_text = "AND "+convert_number(fraction)+" PAISE";
	    }

	    return convert_number(value)+" RUPEE "+f_text+" ONLY";
	}

	function frac(f) {
	    return f % 1;
	}

	function convert_number(number)
	{
	    if ((number < 0) || (number > 999999999)) 
	    { 
	        return "NUMBER OUT OF RANGE!";
	    }
	    var Gn = Math.floor(number / 10000000);  /* Crore */ 
	    number -= Gn * 10000000; 
	    var kn = Math.floor(number / 100000);     /* lakhs */ 
	    number -= kn * 100000; 
	    var Hn = Math.floor(number / 1000);      /* thousand */ 
	    number -= Hn * 1000; 
	    var Dn = Math.floor(number / 100);       /* Tens (deca) */ 
	    number = number % 100;               /* Ones */ 
	    var tn= Math.floor(number / 10); 
	    var one=Math.floor(number % 10); 
	    var res = ""; 

	    if (Gn>0) 
	    { 
	        res += (convert_number(Gn) + " CRORE"); 
	    } 
	    if (kn>0) 
	    { 
	            res += (((res=="") ? "" : " ") + 
	            convert_number(kn) + " LAKH"); 
	    } 
	    if (Hn>0) 
	    { 
	        res += (((res=="") ? "" : " ") +
	            convert_number(Hn) + " THOUSAND"); 
	    } 

	    if (Dn) 
	    { 
	        res += (((res=="") ? "" : " ") + 
	            convert_number(Dn) + " HUNDRED"); 
	    } 


	    var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX","SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN","FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN","NINETEEN"); 
	var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY","SEVENTY", "EIGHTY", "NINETY"); 

	    if (tn>0 || one>0) 
	    { 
	        if (!(res=="")) 
	        { 
	            res += " AND "; 
	        } 
	        if (tn < 2) 
	        { 
	            res += ones[tn * 10 + one]; 
	        } 
	        else 
	        { 

	            res += tens[tn];
	            if (one>0) 
	            { 
	                res += ("-" + ones[one]); 
	            } 
	        } 
	    }

	    if (res=="")
	    { 
	        res = "zero"; 
	    } 
	    return res;
	}


	// add multiple select / deselect functionality
	$("#chkHead").click(function () {
		  $('.chk').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	function chkSelectAll()
	{
		var x = parseInt($(".chk").length);
		var y = parseInt($(".chk:checked").length);
		if( x == y ) 
		{
			$("#chkHead").prop("checked", true);
		} 
		else 
		{
			$("#chkHead").prop("checked", false);
		}
	}
</script>