<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='AddressBook_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].abRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].abRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].prefixTypeRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].prefixType + " <kbd style='cursor: pointer;'>Print</kbd>";
	          cell.className = "clsPrintData";

	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].name;
	          cell.className = "clsAbName";
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].abType;
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].labelName;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].addr;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].townRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].townName;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].pin;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].landLine1;
	          var cell = row.insertCell(13);
	          cell.innerHTML = records[i].landLine2;
	          var cell = row.insertCell(14);
	          cell.innerHTML = records[i].mobile1;
	          var cell = row.insertCell(15);
	          cell.innerHTML = records[i].mobile2;
	          var cell = row.insertCell(16);
	          cell.innerHTML = records[i].email1;
	          var cell = row.insertCell(17);
	          cell.innerHTML = records[i].email2;
	          var cell = row.insertCell(18);
	          cell.innerHTML = records[i].pan;
	          var cell = row.insertCell(19);
	          cell.innerHTML = records[i].din;
	          var cell = row.insertCell(20);
	          cell.innerHTML = records[i].passport;
	          var cell = row.insertCell(21);
	          if( records[i].passportValidUpto == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].passportValidUpto));
	          }
	          var cell = row.insertCell(22);
	          cell.innerHTML = records[i].drivingLic;
	          var cell = row.insertCell(23);
	          if( records[i].drivingLicValidUpto == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].drivingLicValidUpto));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].drivingLicValidUpto));
	          var cell = row.insertCell(24);
	          cell.innerHTML = records[i].familyCard;
	          var cell = row.insertCell(25);
	          if( records[i].familyCardValidUpto == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].familyCardValidUpto));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].familyCardValidUpto));
	          var cell = row.insertCell(26);
	          if( records[i].dob == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].dob));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].dob));
	          var cell = row.insertCell(27);
	          if( records[i].dom == null)
	          {
	          	cell.innerHTML = "";
	          }
	          else
	          {
	          	cell.innerHTML = dateFormat(new Date(records[i].dom));
	          }
	          // cell.innerHTML = dateFormat(new Date(records[i].dom));
	          var cell = row.insertCell(28);
	          cell.innerHTML = records[i].tinState;
	          var cell = row.insertCell(29);
	          cell.innerHTML = records[i].tinCentre;
	          var cell = row.insertCell(30);
	          cell.innerHTML = records[i].contactPerson;
	          var cell = row.insertCell(31);
	          cell.innerHTML = records[i].contactMobile;
	          var cell = row.insertCell(32);
	          cell.innerHTML = records[i].contactDepartment;
	          var cell = row.insertCell(33);
	          cell.innerHTML = records[i].contactDesignation;
	          var cell = row.insertCell(34);
	          cell.innerHTML = records[i].contactPerson2;
	          var cell = row.insertCell(35);
	          cell.innerHTML = records[i].contactMobile2;
	          var cell = row.insertCell(36);
	          cell.innerHTML = records[i].contactDepartment2;
	          var cell = row.insertCell(37);
	          cell.innerHTML = records[i].contactDesignation2;
	          var cell = row.insertCell(38);
	          cell.innerHTML = records[i].contactPerson3;
	          var cell = row.insertCell(39);
	          cell.innerHTML = records[i].contactMobile3;
	          var cell = row.insertCell(40);
	          cell.innerHTML = records[i].contactDepartment3;
	          var cell = row.insertCell(41);
	          cell.innerHTML = records[i].contactDesignation3;
	          var cell = row.insertCell(42);
	          cell.innerHTML = records[i].contactPriority;
	          var cell = row.insertCell(43);
	          cell.innerHTML = records[i].referenceRowId;
	          cell.style.display = "none"
	          var cell = row.insertCell(44);
	          cell.innerHTML = records[i].refName;
	          var cell = row.insertCell(45);
	          cell.innerHTML = records[i].gstIn;
	          var cell = row.insertCell(46);
	          cell.innerHTML = records[i].godown;

	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
		$(".clsPrintData").on("click", printData);
		// $('#tbl1 tr').each(function(){
		// 	$(this).bind('click', highlightRow);
		// });
			
	}
	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						if( data['dependent'] == "yes" )
						{
							alertPopup('Record can not be deleted... Dependent records exist...', 8000);
						}
						else
						{
							setTable(data['records'])
							alertPopup('Record deleted...', 4000);
							blankControls();
							$("#tblBank").find("tr:gt(0)").remove(); /* empty except 1st (head) */
							$('input:checked').each(function() {
								$(this).removeAttr('checked');
							});
							$("#cboPrefix").focus();
						}
					}
				}
			});
	}
	
	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tblBank tr').each(function(row, tr)
	    {
	    	// if( $(tr).find('td:eq(3)').text() > 0 )
	    	// {
	        	TableData[i]=
	        	{
		            "bankName" : $(tr).find('td:eq(1)').text()
		            , "branchName" :$(tr).find('td:eq(2)').text()
		            , "ifsc" :$(tr).find('td:eq(3)').text()
		            , "acNo" :$(tr).find('td:eq(4)').text()
	        	}   
	        	i++; 
	        // }
	    }); 
	    TableData.shift();  // first row will be heading - so remove
	    tblRowsCount = i;
	    return TableData;
	}

	function saveData()
	{	
		var contactRowId="";
		$('input:checked').each(function() 
		{
			contactRowId = $(this).val() + "," + contactRowId;
		});
		if(contactRowId == "")
		{
			alertPopup("Select contact type...", 8000);
			// $("#cboPrefix").focus();
			return;
		}

		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

		var prefixTypeRowId = $("#cboPrefix").val();
		if(prefixTypeRowId == "-1")
		{
			alertPopup("Select prefix...", 8000);
			$("#cboPrefix").focus();
			return;
		}

		// if($("#cboPrefix option:selected").text() == "M/S" && $("#txtGstIn").val().trim() == "")
		// {
		// 	alertPopup("Enter GSTIN...", 8000);
		// 	$("#txtGstIn").focus();
		// 	return;
		// }

		var name = $("#txtName").val().trim();
		if(name == "")
		{
			alertPopup("Name can not be blank...", 8000);
			$("#txtName").focus();
			return;
		}

		var abType = $("#cboType").val();
		if(abType == "-1")
		{
			alertPopup("Select address book type...", 8000);
			$("#cboType").focus();
			return;
		}

		var contactPriority = $("#cboPriority").val();
		if(contactPriority == "-1")
		{
			alertPopup("Select contact priority...", 8000);
			$("#cboPriority").focus();
			return;
		}

		var godown = $("#cboGodown").val();
		if(godown == "-1")
		{
			alertPopup("Select godown...", 8000);
			$("#cboGodown").focus();
			return;
		}

		var labelName = $("#txtLabel").val().trim();
		var addr = $("#txtAddress").val().trim();

		var townRowId = $("#cboTown").val();
		if(townRowId == "-1")
		{
			alertPopup("Select town...", 8000);
			$("#cboTown").focus();
			return;
		}
		var pin = $("#txtPin").val().trim();
		referenceRowId = $("#cboReference").val();
		if(referenceRowId == "-1")
		{
			alertPopup("Select reference...", 8000);
			$("#cboReference").focus();
			return;
		}
		var landLine1 = $("#txtLandLine1").val().trim();
		var landLine2 = $("#txtLandLine2").val().trim();
		var mobile1 = $("#txtMobile1").val().trim();
		if(mobile1 != "")
		{
			if(mobile1.length<10 || mobile1.length>10)///mobile no. must be 10 digit
			{
				alertPopup("Enter valid mobile no.", 8000);
				$("#txtMobile1").focus();
				return;
			}
			if(isNaN(mobile1)==true)///mobile no. must be numeric
			{
				alertPopup("Enter valid mobile no.", 8000);
				$("#txtMobile1").focus();
				return;
			}
		}
		var mobile2 = $("#txtMobile2").val().trim();
		if(mobile2 != "")
		{
			if(mobile2.length<10 || mobile2.length>10)///mobile no. must be 10 digit
			{
				alertPopup("Enter valid mobile no.", 8000);
				$("#txtMobile2").focus();
				return;
			}
			if(isNaN(mobile2)==true)///mobile no. must be numeric
			{
				alertPopup("Enter valid mobile no.", 8000);
				$("#txtMobile2").focus();
				return;
			}
		}
		var email1 = $("#txtEmail1").val().trim();
		if(email1 !="")
		{
			var resultEmail = validateEmail(email1);
			if(resultEmail == false)
			{
				alertPopup("Pls. enter valid email address...", 8000);
				$("#txtEmail1").focus();
				return;
			}
		}
		var email2 = $("#txtEmail2").val().trim();
		if(email2 !="")
		{
			var resultEmail = validateEmail(email2);
			if(resultEmail == false)
			{
				alertPopup("Pls. enter valid email address...", 8000);
				$("#txtEmail2").focus();
				return;
			}
		}
		var pan = $("#txtPan").val().trim();
		var din = $("#txtDin").val().trim();
		var passport = $("#txtPassportNumber").val().trim();
		var passportValidUpto = $("#dtPassportValidUpto").val().trim();
		if(passportValidUpto !="")
		{
			dtOk = testDate("dtPassportValidUpto");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtPassportValidUpto").focus();
				return;
			}
		}

		var drivingLic = $("#txtDlNumber").val().trim();
		var drivingLicValidUpto = $("#dtDlValidUpto").val().trim();
		if(drivingLicValidUpto !="")
		{
			dtOk = testDate("dtDlValidUpto");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtDlValidUpto").focus();
				return;
			}
		}
		var familyCard = $("#txtFcNumber").val().trim();
		var familyCardValidUpto = $("#dtFcValidUpto").val().trim();
		if(familyCardValidUpto !="")
		{
			dtOk = testDate("dtFcValidUpto");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtFcValidUpto").focus();
				return;
			}
		}
		var dob = $("#dtDob").val().trim();
		if(dob !="")
		{
			dtOk = testDate("dtDob");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtDob").focus();
				return;
			}
		}
		var dom = $("#dtDom").val().trim();
		if(dom !="")
		{
			dtOk = testDate("dtDom");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dtDom").focus();
				return;
			}
		}
		var tinState = $("#txtTinState").val().trim();
		var tinCentre = $("#txtTinCentre").val().trim();
		var contactPerson = $("#txtContactPerson").val().trim();
		var contactMobile = $("#txtContactMobile").val().trim();
		var contactDepartment = $("#txtDepartment").val().trim();
		var contactDesignation = $("#txtDesignation").val().trim();
		var contactPerson2 = $("#txtContactPerson2").val().trim();
		var contactMobile2 = $("#txtContactMobile2").val().trim();
		var contactDepartment2 = $("#txtDepartment2").val().trim();
		var contactDesignation2 = $("#txtDesignation2").val().trim();
		var contactPerson3 = $("#txtContactPerson3").val().trim();
		var contactMobile3 = $("#txtContactMobile3").val().trim();
		var contactDepartment3 = $("#txtDepartment3").val().trim();
		var contactDesignation3 = $("#txtDesignation3").val().trim();
		var gstIn = $("#txtGstIn").val().trim();
		if($("#btnSave").val() == "Save")
		{
			// alert("save");
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'prefixTypeRowId': prefixTypeRowId
								, 'name': name
								, 'abType': abType
								, 'contactPriority': contactPriority
								, 'godown': godown
								, 'labelName': labelName
								, 'addr': addr
								, 'townRowId': townRowId
								, 'pin': pin
								, 'referenceRowId': referenceRowId
								, 'landLine1': landLine1
								, 'landLine2': landLine2
								, 'mobile1': mobile1
								, 'mobile2': mobile2
								, 'email1': email1
								, 'email2': email2
								, 'pan': pan
								, 'din': din
								, 'passport': passport
								, 'passportValidUpto': passportValidUpto
								, 'drivingLic': drivingLic
								, 'drivingLicValidUpto': drivingLicValidUpto
								, 'familyCard': familyCard
								, 'familyCardValidUpto': familyCardValidUpto
								, 'dob': dob
								, 'dom': dom
								, 'tinState': tinState
								, 'tinCentre': tinCentre
								, 'contactPerson': contactPerson
								, 'contactMobile': contactMobile
								, 'contactDepartment': contactDepartment
								, 'contactDesignation': contactDesignation
								, 'contactPerson2': contactPerson2
								, 'contactMobile2': contactMobile2
								, 'contactDepartment2': contactDepartment2
								, 'contactDesignation2': contactDesignation2
								, 'contactPerson3': contactPerson3
								, 'contactMobile3': contactMobile3
								, 'contactDepartment3': contactDepartment3
								, 'contactDesignation3': contactDesignation3
								, 'gstIn': gstIn
								, 'contactRowId': contactRowId
								, 'TableData': TableData
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1

								alertPopup('Record saved...', 4000);
								blankControls();
								$("#tblBank").find("tr:gt(0)").remove(); /* empty except 1st (head) */
								$('input:checked').each(function() {
									$(this).removeAttr('checked');
								});
								
								$("#cboPrefix").focus();
								// location.reload();
							}
						}
							
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert("update");
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'prefixTypeRowId': prefixTypeRowId
								, 'name': name
								, 'abType': abType
								, 'contactPriority': contactPriority
								, 'godown': godown
								, 'labelName': labelName
								, 'addr': addr
								, 'townRowId': townRowId
								, 'pin': pin
								, 'referenceRowId': referenceRowId
								, 'landLine1': landLine1
								, 'landLine2': landLine2
								, 'mobile1': mobile1
								, 'mobile2': mobile2
								, 'email1': email1
								, 'email2': email2
								, 'pan': pan
								, 'din': din
								, 'passport': passport
								, 'passportValidUpto': passportValidUpto
								, 'drivingLic': drivingLic
								, 'drivingLicValidUpto': drivingLicValidUpto
								, 'familyCard': familyCard
								, 'familyCardValidUpto': familyCardValidUpto
								, 'dob': dob
								, 'dom': dom
								, 'tinState': tinState
								, 'tinCentre': tinCentre
								, 'contactPerson': contactPerson
								, 'contactMobile': contactMobile
								, 'contactDepartment': contactDepartment
								, 'contactDesignation': contactDesignation
								, 'contactPerson2': contactPerson2
								, 'contactMobile2': contactMobile2
								, 'contactDepartment2': contactDepartment2
								, 'contactDesignation2': contactDesignation2
								, 'contactPerson3': contactPerson3
								, 'contactMobile3': contactMobile3
								, 'contactDepartment3': contactDepartment3
								, 'contactDesignation3': contactDesignation3
								, 'gstIn': gstIn
								, 'contactRowId': contactRowId
								, 'TableData': TableData	
						},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								blankControls();
								$("#tblBank").find("tr:gt(0)").remove(); /* empty except 1st (head) */
								$('input:checked').each(function() {
									$(this).removeAttr('checked');
								});
								$("#btnSave").val("Save");
								$("#cboPrefix").focus();
							}
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#cboPrefix").focus();
					}
				}
			});
	}
</script>
<div class="container-fluid" style="width: 95%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;margin-left:;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Address Book</h1>
			<div class="row" style="margin-top:5px;">
				<div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
					<h3 style="color:blue;"><u>Basic Information</u></h3>
					<div class="row" style="margin-bottom:0;">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Prefix: <span style='color: red;'>*</span></label>";
								echo form_dropdown('cboPrefix', $prefixes, '-1', "class='form-control' autofocus id='cboPrefix'");
							?> 
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Name: <span style='color: red;'>*</span></label>";
								echo form_input('txtName', '', "class='form-control'  id='txtName' style='text-transform: capitalize;' maxlength=100 autocomplete='off'");
							?> 
						</div>
					</div>

					<div class="row" style="margin-top:10px;">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								$types = array();
								$types['-1'] = '--- Select ---';
								$types['I'] = "Individual";
								$types['N'] = "Non Individual";
								echo "<label style='color: black; font-weight: normal;'>Type: <span style='color: red;'>*</span></label>";
								echo form_dropdown('cboType', $types, '-1', "class='form-control' id='cboType'");
							?> 
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								$types = array();
								$types['-1'] = '--- Select ---';
								$types['H'] = "High";
								$types['N'] = "Normal";
								$types['L'] = "Low";
								echo "<label style='color: black; font-weight: normal;'>Priority: <span style='color: red;'>*</span></label>";
								echo form_dropdown('cboPriority', $types, '-1', "class='form-control' id='cboPriority'");
							?> 
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								$types = array();
								$types['-1'] = '--- Select ---';
								$types['Y'] = "Yes";
								$types['N'] = "No";
								echo "<label style='color: black; font-weight: normal;'>Godown: <span style='color: red;'>*</span></label>";
								echo form_dropdown('cboGodown', $types, '-1', "class='form-control' id='cboGodown'");
							?> 
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								///actually this is staores as label name in database
								echo "<label style='color: black; font-weight: normal;'>Label Name: </label>";
								echo form_input('txtLabel', '', "class='form-control'  id='txtLabel' style='text-transform: capitalize;' maxlength=100 autocomplete='off' placeholder='Fill in case of individual only'");
							?> 
						</div>
					</div>

					<div class="row" style="margin-top:10px;">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Address:</label>";
								echo form_textarea('txtAddress', '', "class='form-control' style='resize:none;height:100px; text-transform: capitalize;' id='txtAddress'  maxlength=255 value=''");
							?>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>&nbsp;</label><br />";
								echo "<label id='lblRegion'  class='blank' style='color: black; font-weight: normal;'>.</label>";
							?> 
						</div>
					</div>

					<div class="row" style="margin-top:10px;">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								$loc = base_url() . "index.php/Towns_Controller";
								echo "<a target='_blank' href=". $loc ."> <label style='color: black; font-weight: normal;'>Town:</a> <span style='color: red;'>*</span></label> &nbsp;&nbsp;<label id='lblRefreshTowns' class='glyphicon glyphicon-refresh' style='color: green;'></label>";
							?>
							<select id="cboTown" class="form-control">
				              <option value="-1" pin="" std="" districtName="" stateName="" countryName="">--- Select ---</option>
				              <?php
				                foreach ($towns as $row) 
				                {
				              ?>
				              <option value=<?php echo $row['townRowId']; ?>  pin="<?php echo $row['pin']; ?>" std="<?php echo $row['std']; ?>"  districtName="<?php echo $row['districtName']; ?>" stateName="<?php echo $row['stateName']; ?>" countryName="<?php echo $row['countryName']; ?>" ><?php echo $row['townName']; ?></option>
				              <?php
				                }
				              ?>
				            </select>
				            <script type="text/javascript">
					            $(document).ready(function()
					            {
					              $("#cboTown").change(function()
					              {
					                // alert($('option:selected', '#cboDistrict').attr('countryName'));
					                $("#lblRegion").text($('option:selected', '#cboTown').attr('districtName'));
					                $("#lblRegion").text( $("#lblRegion").text() + ', ' + $('option:selected', '#cboTown').attr('stateName') );
					                $("#lblRegion").text( $("#lblRegion").text() + ', ' + $('option:selected', '#cboTown').attr('countryName') );
					                $("#txtPin").val($('option:selected', '#cboTown').attr('pin'));
					                // $("#txtStd").val($('option:selected', '#cboTown').attr('pin'));
					              });
					            });


					            $(document).ready(function()
								  {
							        $("#lblRefreshTowns").click(function()
							        {
							          $.ajax({
							              'url': base_url + '/' + controller + '/getTownsRefresh',
							              'type': 'POST',
							              'dataType': 'json',
							              'data': {'x': 'x'},
							              'success': function(data)
							              {
							                if(data)
							                {
							                	// alert( JSON.stringify(data['towns']) );	
							                  $('#cboTown').empty();
							                  if(data['towns'] != null) 
							                  {
							                    var options = "<option value='-1' pin='' std='' districtName='' stateName='' countryName=''>" + "--- Select ---" + "</option>";
							                    for(var i=0; i<data['towns'].length; i++)
							                    {
							                      options += "<option value=" + data['towns'][i].townRowId + " pin=" + data['towns'][i].pin + " std=" + data['towns'][i].std + " districtName=" + data['towns'][i].districtName + " stateName=" + data['towns'][i].stateName + " countryName=" + data['towns'][i].countryName + ">" + data['towns'][i].townName + "</option>";
							                      // options += "<option value=" + data['towns'][i].townRowId + " pin=" + data['towns'][i].pin + ">" + data['towns'][i].townName + "</option>";
							                    }
							                    $('#cboTown').append(options);
							                    $('#cboTown').trigger('change');
							                  }
							                }
							              }
							          });
							        });
							      });
					        </script>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Pin Code:</label>";
								echo form_input('txtPin', '', "class='form-control' placeholder='' id='txtPin' maxlength='10'");
							?>
						</div>

						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Reference:<span style='color: red;'>*</span></label>";
			              	?>
			              	<select id="cboReference" name="cboReference" class="form-control">
				              <option value="-1" addr="" mobile1="" mobile2="" townName="" >--- Select ---</option>
				              <?php
				                foreach ($abList as $row) 
				                {
				              ?>
				              <option value=<?php echo $row['abRowId']; ?> addr="<?php echo $row['addr']; ?>" mobile1="<?php echo $row['mobile1']; ?>" mobile2="<?php echo $row['mobile2']; ?>" townName="<?php echo $row['townName']; ?>" ><?php echo $row['name']; ?></option>
				              <?php
				                }
				              ?>
				            </select>
			          	</div>
			          	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Land Line 1:</label>";
								echo form_input('txtLandLine1', '', "class='form-control' placeholder='' id='txtLandLine1' maxlength='10'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Land Line 2:</label>";
								echo form_input('txtLandLine2', '', "class='form-control' placeholder='' id='txtLandLine2' maxlength='10'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Mobile 1:</label>";
								echo form_input('txtMobile1', '', "class='form-control' placeholder='' id='txtMobile1' maxlength='10'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Mobile 2:</label>";
								echo form_input('txtMobile2', '', "class='form-control' placeholder='' id='txtMobile2' maxlength='10'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Email 1:</label>";
								echo form_input('txtEmail1', '', "class='form-control' placeholder='' id='txtEmail1' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Email 2:</label>";
								echo form_input('txtEmail2', '', "class='form-control' placeholder='' id='txtEmail2' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>PAN:</label>";
								echo form_input('txtPan', '', "class='form-control' placeholder='' style='text-transform: uppercase' id='txtPan' maxlength='20'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>DIN:</label>";
								echo form_input('txtDin', '', "class='form-control' placeholder='' id='txtDin' maxlength='20'");
							?>
						</div>
					</div>

					<h3 style="color:blue;"><u>Extended Information</u></h3>
					
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Passport Number:</label>";
								echo form_input('txtPassportNumber', '', "class='form-control' placeholder='' id='txtPassportNumber' maxlength='20'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Passport Valid Upto:</label>";
								echo form_input('dtPassportValidUpto', '', "class='form-control' placeholder='' id='dtPassportValidUpto' maxlength='10'");
							?>
							<script>
								$( "#dtPassportValidUpto" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    // Set the Current Date as Default
								// $("#dtPassportValidUpto").val(dateFormat(new Date()));
							</script>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Driving License Number:</label>";
								echo form_input('txtDlNumber', '', "class='form-control' placeholder='' id='txtDlNumber' maxlength='20'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Driving License Valid Upto:</label>";
								echo form_input('dtDlValidUpto', '', "class='form-control' placeholder='' id='dtDlValidUpto' maxlength='10'");
							?>
							<script>
								$( "#dtDlValidUpto" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    // Set the Current Date as Default
								// $("#dtDlValidUpto").val(dateFormat(new Date()));
							</script>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Family Card Number:</label>";
								echo form_input('txtFcNumber', '', "class='form-control' placeholder='' id='txtFcNumber' maxlength='20'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Family Card Valid Upto:</label>";
								echo form_input('dtFcValidUpto', '', "class='form-control' placeholder='' id='dtFcValidUpto' maxlength='10'");
							?>
							<script>
								$( "#dtFcValidUpto" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
								});
							    // Set the Current Date as Default
								// $("#dtFcValidUpto").val(dateFormat(new Date()));
							</script>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Date of Birth:</label>";
								echo form_input('dtDob', '', "class='form-control' placeholder='' id='dtDob' maxlength='10'");
							?>
							<script>
								$( "#dtDob" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "1920:2050"
								});
							    // Set the Current Date as Default
								// $("#dtDob").val(dateFormat(new Date()));
							</script>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Date of Marriege:</label>";
								echo form_input('dtDom', '', "class='form-control' placeholder='' id='dtDom' maxlength='10'");
							?>
							<script>
								$( "#dtDom" ).datepicker({
									dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "1940:2050"
								});
							    // Set the Current Date as Default
								// $("#dtDom").val(dateFormat(new Date()));
							</script>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>TIN State:</label>";
								echo form_input('txtTinState', '', "class='form-control' placeholder='' id='txtTinState' maxlength='20'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>TIN Centre:</label>";
								echo form_input('txtTinCentre', '', "class='form-control' placeholder='' id='txtTinCentre' maxlength='20'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>GSTIN:</label>";
								echo form_input('txtGstIn', '', "class='form-control' placeholder='' id='txtGstIn' maxlength='20'");
							?>
						</div>
					</div>

					<h3 style="color:blue;"><u>Contact Persons</u></h3>

					<div class="row" style="margin-top:10px;">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Contact Person:</label>";
								echo form_input('txtContactPerson', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtContactPerson' maxlength='40'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Contact Person Mobile:</label>";
								echo form_input('txtContactMobile', '', "class='form-control' placeholder='' id='txtContactMobile' maxlength='10'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Department:</label>";
								echo form_input('txtDepartment', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtDepartment' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Designation:</label>";
								echo form_input('txtDesignation', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtDesignation' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Contact Person 2:</label>";
								echo form_input('txtContactPerson2', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtContactPerson2' maxlength='40'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Contact Person 2 Mobile:</label>";
								echo form_input('txtContactMobile2', '', "class='form-control' placeholder='' id='txtContactMobile2' maxlength='10'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Department:</label>";
								echo form_input('txtDepartment2', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtDepartment2' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Designation:</label>";
								echo form_input('txtDesignation2', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtDesignation2' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Contact Person 3:</label>";
								echo form_input('txtContactPerson3', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtContactPerson3' maxlength='40'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Contact Person 3 Mobile:</label>";
								echo form_input('txtContactMobile3', '', "class='form-control' placeholder='' id='txtContactMobile3' maxlength='10'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Department:</label>";
								echo form_input('txtDepartment3', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtDepartment3' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Designation:</label>";
								echo form_input('txtDesignation3', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtDesignation3' maxlength='50'");
							?>
						</div>
					</div>

					<h3 style="color:blue;"><u>Bank Details</u></h3>

					<div class="row" style="margin-top:10px;">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Bank Name:</label>";
								echo form_input('txtBankName', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtBankName' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Branch Name:</label>";
								echo form_input('txtBranchName', '', "class='form-control' placeholder='' style='text-transform: capitalize;' id='txtBranchName' maxlength='50'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>IFS Code:</label>";
								echo form_input('txtIfsc', '', "class='form-control' placeholder='' style='text-transform: uppercase;' id='txtIfsc' maxlength='20'");
							?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								echo "<label style='color: black; font-weight: normal;'>Account Number:</label>";
								echo form_input('txtAcNo', '', "class='form-control' placeholder='' id='txtAcNo' maxlength='20'");
							?>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" style="margin-top:10px;">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-top:10px;">
							<?php
								// echo "<label style='color: black; font-weight: normal;'>&nbsp</label>";
								echo "<input type='button' onclick='confirmBankDetail();' value='Confirm Bank Detail' id='btnConfirm' class='btn form-control' style='background-color: #FF5733; color:white;'>";
							?>

						</div>
					</div>
	          	</div>



	          	<!-- Contact Type Div -->
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
	          	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;margin-top:80px;height:895px;overflow:scroll' id="divContact">
					<span class="btn" style='font-weight:900;margin-left:-30px;'>Contact Type</span>
					<hr style='border:1px solid gray;margin-top:-3px; margin-left:-30px;margin-right:-30px;'>
					<?php
						foreach ($contactTypes as $key => $value) 
						{
					?>
							<div class="checkbox">
								<input type="checkbox" class="mycon" id='<?php echo $key;?>' value='<?php echo $key;?>'></input>
								<label  style="color: black;" for='<?php echo $key;?>'><?php echo $value;?></label>
							</div>
					<?php
						}
					?>


	          	</div>
	          	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:rgba(128,128,128,.2);border:1 solid lightgray; padding: 0px;height:235px; overflow:auto;margin-top: 10px;">
						<table class='table table-bordered' id='tblBank'>
							<tr>
							 	<th  width="50" class="text-center">Delete</th>
								<th style='width:0px;display:none;'>rowid</th>
							 	<th>Bank Name</th>
							 	<th>Branch</th>
							 	<th>IFSC</th>
							 	<th>Ac. No.</th>
							</tr>
						</table>
					</div>
				</div>
	          	<!-- END Contact Type Div -->
			</div>
		</div>
		<!-- <div class="col-lg-1 col-sm-1 col-md-1 col-xs-0">
		</div> -->
	</div>



		<div class="row" style="margin-top:15px;">
			<div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
			</div>
			<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				<?php
					echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
					echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
              	?>
          	</div>
			
		</div>



	<div class="row" style="margin-top:20px;" >
		<!-- <div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div> -->

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  width="50" class="text-center">Delete</th>
						<th style='display:none;'>rowid</th>
					 	<th style='display:none;'>PrefixRowId</th>
					 	<th>Prefix</th>
					 	<th>Name</th>
					 	<th>Type</th>
					 	<th>Label Name</th>
					 	<th>Address</th>
					 	<th style='display:none;'>TownRowId</th>
					 	<th>Town</th>
					 	<th>Pin</th>
					 	<th>Land Line1</th>
					 	<th>Land Line2</th>
					 	<th>Mobile1</th>
					 	<th>Mobile2</th>
					 	<th>Email1</th>
					 	<th>Email2</th>
					 	<th>PAN</th>
					 	<th>DIN</th>
					 	<th>Passport#</th>
					 	<th>Passport Dt.</th>
					 	<th>Dl#</th>
					 	<th>Dl Dt.</th>
					 	<th>Family Card#</th>
					 	<th>Famil Card Dt.</th>
					 	<th>DOB</th>
					 	<th>DOM</th>
					 	<th>TIN State</th>
					 	<th>TIN Centre</th>
					 	<th>Contact Person</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Person 2</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Person 3</th>
					 	<th>Contact Mobile</th>
					 	<th>Dept.</th>
					 	<th>Desig.</th>
					 	<th>Contact Priority</th>
					 	<th style='display:none;'>ReferenceRowID</th>
					 	<th>Reference</th>
					 	<th>GSTIN</th>
					 	<th>Godown</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						if($errorfound=="no") ////When Saved Or Updated Successfully
						{
							// echo "<script> blankcontrol(); </script>";
							// echo "<script> document.getElementById('btnSave').value = 'Save'; </script>";
						}
						else 
						{
							// echo $errorfound;
						}


						foreach ($records as $row) 
						{
						 	$rowId = $row['abRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: green;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'green\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="color: red;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'lightgray\';"  onmouseout="this.style.color=\'red\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='width:0px;display:none;'>".$row['abRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['prefixTypeRowId']."</td>";
						 	echo "<td class='clsPrintData'>".$row['prefixType']." <kbd style='cursor: pointer;'>Print</kbd></td>";
						 	echo "<td class='clsAbName'>".$row['name']."</td>";
						 	echo "<td>".$row['abType']."</td>";
						 	echo "<td>".$row['labelName']."</td>";
						 	echo "<td>".$row['addr']."</td>";
						 	echo "<td>".$row['townRowId']."</td>";
						 	echo "<td style='display:none;'>".$row['townName']."</td>";
						 	echo "<td>".$row['pin']."</td>";

						 	echo "<td>".$row['landLine1']."</td>";
						 	echo "<td>".$row['landLine2']."</td>";
						 	echo "<td>".$row['mobile1']."</td>";
						 	echo "<td>".$row['mobile2']."</td>";
						 	echo "<td>".$row['email1']."</td>";
						 	echo "<td>".$row['email2']."</td>";
						 	echo "<td>".$row['pan']."</td>";
						 	echo "<td>".$row['din']."</td>";
						 	echo "<td>".$row['passport']."</td>";
						 	if($row['passportValidUpto'] == "")
						 	{
						 		echo "<td>".''."</td>";
						 	}
						 	else
						 	{
							 	$vdt = strtotime($row['passportValidUpto']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
							}

						 	echo "<td>".$row['drivingLic']."</td>";
						 	if($row['drivingLicValidUpto'] == "")
						 	{
						 		echo "<td>".''."</td>";
						 	}
						 	else
						 	{
							 	$vdt = strtotime($row['drivingLicValidUpto']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
						 	}

						 	echo "<td>".$row['familyCard']."</td>";
						 	if($row['familyCardValidUpto'] == "")
						 	{
						 		echo "<td>".''."</td>";
						 	}
						 	else
						 	{						 	
							 	$vdt = strtotime($row['familyCardValidUpto']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
							}

							if($row['dob'] == "")
						 	{
						 		echo "<td>".''."</td>";
						 	}
						 	else
						 	{
							 	$vdt = strtotime($row['dob']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
							}
						 	if($row['dom'] == "")
						 	{
						 		echo "<td>".''."</td>";
						 	}
						 	else
						 	{							
							 	$vdt = strtotime($row['dom']);
								$vdt = date('d-M-Y', $vdt);
							 	echo "<td>".$vdt."</td>";
							}
						 	echo "<td>".$row['tinState']."</td>";
						 	echo "<td>".$row['tinCentre']."</td>";
						 	echo "<td>".$row['contactPerson']."</td>";
						 	echo "<td>".$row['contactMobile']."</td>";
						 	echo "<td>".$row['contactDepartment']."</td>";
						 	echo "<td>".$row['contactDesignation']."</td>";
						 	echo "<td>".$row['contactPerson2']."</td>";
						 	echo "<td>".$row['contactMobile2']."</td>";
						 	echo "<td>".$row['contactDepartment2']."</td>";
						 	echo "<td>".$row['contactDesignation2']."</td>";
						 	echo "<td>".$row['contactPerson3']."</td>";
						 	echo "<td>".$row['contactMobile3']."</td>";
						 	echo "<td>".$row['contactDepartment3']."</td>";
						 	echo "<td>".$row['contactDesignation3']."</td>";
						 	echo "<td>".$row['contactPriority']."</td>";
						 	echo "<td style='display:none;'>".$row['referenceRowId']."</td>";
						 	echo "<td>".$row['refName']."</td>";
						 	echo "<td>".$row['gstIn']."</td>";
						 	echo "<td>".$row['godown']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<!-- <div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div> -->
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0">
			<!-- <label style="color: red;">Ctrl + Click on name to print data</label> -->
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style:margin-bottom:10%>
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
	</div> 
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#cboPrefix").val($(this).closest('tr').children('td:eq(3)').text());
		$("#txtName").val($(this).closest('tr').children('td:eq(5)').text());
		$("#cboType").val($(this).closest('tr').children('td:eq(6)').text());
		$("#cboGodown").val($(this).closest('tr').children('td:eq(46)').text());
		$("#txtLabel").val($(this).closest('tr').children('td:eq(7)').text());
		$("#txtAddress").val($(this).closest('tr').children('td:eq(8)').text());
		$("#cboTown").val($(this).closest('tr').children('td:eq(9)').text());
		$('#cboTown').trigger('change');
		$("#txtPin").val($(this).closest('tr').children('td:eq(11)').text());
		$("#txtLandLine1").val($(this).closest('tr').children('td:eq(12)').text());
		$("#txtLandLine2").val($(this).closest('tr').children('td:eq(13)').text());
		$("#txtMobile1").val($(this).closest('tr').children('td:eq(14)').text());
		$("#txtMobile2").val($(this).closest('tr').children('td:eq(15)').text());
		$("#txtEmail1").val($(this).closest('tr').children('td:eq(16)').text());
		$("#txtEmail2").val($(this).closest('tr').children('td:eq(17)').text());
		$("#txtPan").val($(this).closest('tr').children('td:eq(18)').text());
		$("#txtDin").val($(this).closest('tr').children('td:eq(19)').text());
		$("#txtPassportNumber").val($(this).closest('tr').children('td:eq(20)').text());
		$("#dtPassportValidUpto").val($(this).closest('tr').children('td:eq(21)').text());
		$("#txtDlNumber").val($(this).closest('tr').children('td:eq(22)').text());
		$("#dtDlValidUpto").val($(this).closest('tr').children('td:eq(23)').text());
		$("#txtFcNumber").val($(this).closest('tr').children('td:eq(24)').text());
		$("#dtFcValidUpto").val($(this).closest('tr').children('td:eq(25)').text());
		$("#dtDob").val($(this).closest('tr').children('td:eq(26)').text());
		$("#dtDom").val($(this).closest('tr').children('td:eq(27)').text());
		$("#txtTinState").val($(this).closest('tr').children('td:eq(28)').text());
		$("#txtTinCentre").val($(this).closest('tr').children('td:eq(29)').text());
		$("#txtContactPerson").val($(this).closest('tr').children('td:eq(30)').text());
		$("#txtContactMobile").val($(this).closest('tr').children('td:eq(31)').text());
		$("#txtDepartment").val($(this).closest('tr').children('td:eq(32)').text());
		$("#txtDesignation").val($(this).closest('tr').children('td:eq(33)').text());
		$("#txtContactPerson2").val($(this).closest('tr').children('td:eq(34)').text());
		$("#txtContactMobile2").val($(this).closest('tr').children('td:eq(35)').text());
		$("#txtDepartment2").val($(this).closest('tr').children('td:eq(36)').text());
		$("#txtDesignation2").val($(this).closest('tr').children('td:eq(37)').text());
		$("#txtContactPerson3").val($(this).closest('tr').children('td:eq(38)').text());
		$("#txtContactMobile3").val($(this).closest('tr').children('td:eq(39)').text());
		$("#txtDepartment3").val($(this).closest('tr').children('td:eq(40)').text());
		$("#txtDesignation3").val($(this).closest('tr').children('td:eq(41)').text());
		$("#cboPriority").val($(this).closest('tr').children('td:eq(42)').text());
		$("#cboReference").val($(this).closest('tr').children('td:eq(43)').text());
		$("#txtGstIn").val($(this).closest('tr').children('td:eq(45)').text());

		/////Setting Contact Types and Bank Detail
		$('input:checked').each(function() {
			$(this).removeAttr('checked');
		});
		$.ajax({
			'url': base_url + '/AddressBook_Controller/getDataForCheckox',
			'type': 'POST', 
			'data':{'rowid':globalrowid},
			'dataType': 'json',
			'success':function(data)
			{
				var arr = data['contacts'].split(",");
				$('.mycon').each(function() {
					for(var i=0; i<arr.length-1; i++)
					{
						if(arr[i].trim() == $(this).val())
						{
							$(this).prop('checked','checked');
						}
					}	
				});

				/////loading Bank Details
				$("#tblBank").find("tr:gt(0)").remove(); //// empty first
		        var table = document.getElementById("tblBank");
		        for(i=0; i<data['bankDetails'].length; i++)
		        {
		          var newRowIndex = table.rows.length;
		          var row = table.insertRow(newRowIndex);
		          var cell = row.insertCell(0);
		          cell.innerHTML =  "<span onClick='delTableRowBank(this);' style='color: lightgray;cursor: pointer;cursor: hand;' onmouseover=\"this.style.color=\'red\';\"  onmouseout=\"this.style.color=\'lightgray\';\" class='glyphicon glyphicon-remove'></span>";
		          var cell = row.insertCell(1);
		          cell.innerHTML = data['bankDetails'][i].bankName;
		          var cell = row.insertCell(2);
		          cell.innerHTML = data['bankDetails'][i].branchName;
		          var cell = row.insertCell(3);
		          cell.innerHTML = data['bankDetails'][i].ifsc;
		          var cell = row.insertCell(4);
		          cell.innerHTML = data['bankDetails'][i].acNo;
		        }	


			}
		});
		/////END - Setting Contact Types

	


		$("#cboPrefix").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );





  function confirmBankDetail() 
  {
      var bankName = $("#txtBankName").val().trim();
      if(bankName == "")
      {
      	alertPopup("Bank name can not be blank...", 4000);
      	$("#txtBankName").focus();
      	return;
      }
      var branchName = $("#txtBranchName").val().trim();
      var ifsc = $("#txtIfsc").val().trim();
      var acNo = $("#txtAcNo").val().trim();
      if(acNo == "")
      {
      	alertPopup("Account numbercan not be blank...", 4000);
      	$("#txtAcNo").focus();
      	return;
      }

      var table = document.getElementById("tblBank");
      var newRowIndex = table.rows.length;
      var row = table.insertRow(newRowIndex);
      var cell0 = row.insertCell(0);
      var cell1 = row.insertCell(1);
      var cell2 = row.insertCell(2);
      var cell3 = row.insertCell(3);
      var cell4 = row.insertCell(4);

      cell0.innerHTML = "<span onClick='delTableRowBank(this);' style='color: lightgray;cursor: pointer;cursor: hand;' onmouseover=\"this.style.color=\'red\';\"  onmouseout=\"this.style.color=\'lightgray\';\" class='glyphicon glyphicon-remove'></span>";
      cell1.innerHTML = bankName;
      cell2.innerHTML = branchName;
      cell3.innerHTML = ifsc;
      cell4.innerHTML = acNo;

      $("#txtBankName").val('');
      $("#txtBranchName").val('');
      $("#txtIfsc").val('');
      $("#txtAcNo").val('');

      $("#txtBankName").focus();
  }

  function delTableRowBank(x)
  {
      var rowToDelete = x.parentNode.parentNode.rowIndex
      var table = document.getElementById("tblBank");
      table.deleteRow(rowToDelete);
  }


  $( "input[type='text']" ).keypress(function(event) 
  {
  		// alert(event.which);
  		if ( event.which == 39 || event.which == 34) 
  		{
  			event.preventDefault();
  			$(this).val($(this).val() + '');
  		}
	});
  $( "#txtAddress" ).keypress(function(event) 
  {
  		if ( event.which == 39 || event.which == 34) 
  		{
  			event.preventDefault();
  			$(this).val($(this).val() + '');
  		}
	});


  $(".clsPrintData").on("click", printData);
  function printData(e)
  {
  	// if ( e.ctrlKey ) 
  	{
		rowId = $(this).closest('tr').children('td:eq(2)').text();
  		// alert(rowId);
  		$.ajax({
			'url': base_url + '/AddressBook_Controller/printData',
			'type': 'POST', 
			'data':{'rowid':rowId},
			// 'dataType': 'json',
			'success':function(data)
			{
				// console.log(data);
				// window.location.href=data;
				var win = window.open(data, '_blank');
  				win.focus();
			},
			'error': function(jqXHR, exception)
			{
				document.write(jqXHR.responseText);
			}
		});
  	}
  }

</script>