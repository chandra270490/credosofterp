<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='RptEsi_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records, employerContri, recordsExecutives)
	{
		 // alert(JSON.stringify(records));
		  // $("#tbl1").empty();
		  $("#txtEmployerPer").val( employerContri[0].esiEmployer );

		  var esiTotal = 0;
		  var grossTotal = 0;
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].empRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(2); ///ESI No.
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].empName;
	          var cell = row.insertCell(4); ///Father NAme
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(5);  /// Days Worked
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(6); ///Sal
	          cell.innerHTML = records[i].gross;
	          grossTotal += parseFloat(records[i].gross);
	          // 
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].esi;
	          esiTotal += parseFloat(records[i].esi);
	  	  }

	  	  ////Loading Executives data
	  	  for(j=0; j<recordsExecutives.length; j++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);

	          var cell = row.insertCell(0);
	          cell.innerHTML = ++i;
	          var cell = row.insertCell(1);
	          cell.innerHTML = recordsExecutives[j].empRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(2); ///ESI No.
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(3);
	          cell.innerHTML = recordsExecutives[j].empName;
	          var cell = row.insertCell(4); ///Father NAme
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(5);  /// Days Worked
	          cell.innerHTML = "";
	          cell.style.display="none";
	          var cell = row.insertCell(6); ///Sal
	          cell.innerHTML = recordsExecutives[j].gross;
	          grossTotal += parseFloat(recordsExecutives[j].gross);
	          // 
	          var cell = row.insertCell(7);
	          cell.innerHTML = recordsExecutives[j].esi;
	          esiTotal += parseFloat(recordsExecutives[j].esi);
	  	  }
	  	  // ///////////// Adding Total Row
	  	  newRowIndex = table.rows.length;
          row = table.insertRow(newRowIndex);
          var cell = row.insertCell(0);
          cell.innerHTML = "";
          var cell = row.insertCell(1);
          cell.innerHTML = "";
          cell.style.display="none";
          var cell = row.insertCell(2);
          cell.innerHTML = "";
          cell.style.display="none";
          var cell = row.insertCell(3);
          cell.innerHTML = "Total";
          cell.style.fontWeight="bold";
          var cell = row.insertCell(4);
          cell.innerHTML = "";
          cell.style.display="none";
          var cell = row.insertCell(5);
          cell.innerHTML = "";
          cell.style.display="none";
          var cell = row.insertCell(6);
          cell.innerHTML = grossTotal.toFixed(2);
          cell.style.fontWeight="bold";
          cell.style.fontWeight="bold";
          var cell = row.insertCell(7);
          cell.innerHTML = esiTotal.toFixed(2);
          cell.style.fontWeight="bold";


          /////Calculating Employer Contri.
          var employerContriPer = $("#txtEmployerPer").val();
          var employerContriAmt = grossTotal * employerContriPer / 100;
          employerContriAmt = employerContriAmt.toFixed(2);
          $("#txtEmployerAmt").val( employerContriAmt );
		
	}

	

	function loadData()
	{	
		// $("#tbl1").find("tr:gt(0)").remove(); /* empty except 1st (head) */	
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		// alert(dtTo);

		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'dtFrom': dtFrom
							, 'dtTo': dtTo
						},
				'success': function(data)
				{
					// alert(JSON.stringify(data['recordsExecutives']));
					// console.log(JSON.stringify(data['recordsExecutives']));
					setTable(data['records'], data['employerContri'], data['recordsExecutives']) 
					alertPopup('Records loaded...', 6000);
				}
		});
		
	}



	var tblRowsCount;
	function storeTblValues()
	{
		var data = Array();
    
		$("#tbl1 tr").each(function(i, v){
		    data[i] = Array();
		    $(this).children('td').each(function(ii, vv){
		        data[i][ii] = $(this).text();
		    }); 
		})

	    return data;
	}

	function exportData()
	{	
		// alert(noOfDays);
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;
		if(tblRowsCount == 0)
		{
			alertPopup("No product selected...", 8000);
			$("#cboProducts").focus();
			return;
		}
		var dtFrom = $("#dtFrom").val().trim();
		dtOk = testDate("dtFrom");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtFrom").focus();
			return;
		}

		var dtTo = $("#dtTo").val().trim();
		dtOk = testDate("dtTo");
		if(dtOk == false)
		{
			alertPopup("Invalid date...", 5000);
			$("#dtTo").focus();
			return;
		}
		var employerContriPer = $("#txtEmployerPer").val();
		var employerContriAmt = $("#txtEmployerAmt").val();

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
							, 'dtFrom': dtFrom
							, 'dtTo': dtTo
							, 'employerContriPer': employerContriPer
							, 'employerContriAmt': employerContriAmt
						},
				'success': function(data)
				{
					if(data)
					{
						window.location.href=data;
					}
				}
		});
		
	}


</script>
<div class="container">
	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>ESI Report</h1>
		<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>From:</label>";
						echo form_input('dtFrom', '', "class='form-control' placeholder='' id='dtFrom' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtFrom" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the 1st of previous month
						var date = new Date();
						var firstDay = new Date(date.getFullYear(), date.getMonth()-1, 1);
						$("#dtFrom").val(dateFormat(firstDay));
						// var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
					</script>					
	          	</div>
	          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>To:</label>";
						echo form_input('dtTo', '', "class='form-control' placeholder='' id='dtTo' maxlength='10'");
	              	?>
	              	<script>
						$( "#dtTo" ).datepicker({
							dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
						});
					    // Set the last of previous month
						var date = new Date();
						var lastDay = new Date(date.getFullYear(), date.getMonth() , 0);
						$("#dtTo").val(dateFormat(lastDay));
					</script>					
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
	          	</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>

			<div class="row" style="margin-top:20px;" >
				<style>
				    /*table, th, td{border:1px solid gray; padding: 7px;}*/
				</style>
				<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
					<div style="height:300px; overflow:auto; border: 1px solid lightgrey;">
					<table style="table-layout: fixed;" id='tbl1' width="100%" class="table table-striped">
						 <tr style="background-color: #B0F0F0;">
						 	<td style='display:none1; font-weight: bold;'>S.N.</td>
						 	<td style='display:none; font-weight: bold;'>Emp Code</td>
						 	<td style='display:none; font-weight: bold;'>ESI No.</td>
						 	<td style='font-weight: bold;'>Emp Name</td>
						 	<td style='display:none;font-weight: bold;'>Father Name</td>
						 	<td style='display:none;font-weight: bold;'>No.Of Days Paid</td>
						 	<td style='font-weight: bold;'>Wages</td>
						 	<td style='display:none1;font-weight: bold;' >ESI</td>
						 </tr>
					</table>
					</div>
				</div>
			</div>
		</form>
	</div>



	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
		<div class="row" style="margin-top:10px;" >
			<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Employer Contri.(%)</label>";
					echo form_input('txtEmployerPer', '', "class='form-control' placeholder='' id='txtEmployerPer' maxlength='15' disabled='yes'");
	          	?>
			</div>
			<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				<?php
					echo "<label style='color: black; font-weight: normal;'>Employer Contri.(Rs.)</label>";
					echo form_input('txtEmployerAmt', '0', "class='form-control' placeholder='' id='txtEmployerAmt' maxlength='15' disabled='yes'");
	          	?>
			</div>
			<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			</div>

			<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				<?php
					echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
					echo "<input type='button' onclick='exportData();' value='Export Data' id='btnSaveData' class='btn form-control' style='background-color: lightgray;'>";
					echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
		      	?>
			</div>
		</div>
	</div>
</div>





<script type="text/javascript">


</script>