<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>CREDO: ERP</title>
	<script type="text/javascript" src="<?php echo base_url(); ?>bootstrap/js/jquery.js"></script>
	<link rel='stylesheet' href='<?php  echo base_url();  ?>bootstrap/css/bootstrap.css'>
	<link rel='stylesheet' href='<?php  echo base_url();  ?>bootstrap/css/bootstrap.min.css'>
	<link href="<?php echo base_url(); ?>bootstrap/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />





	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/datatable/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/datatable/dataTables.tableTools.min.css">
	<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>bootstrap/datatable/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>bootstrap/datatable/dataTables.tableTools.min.js"></script>

	<!--Checkbox Tree-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/checktree/css/jquery-checktree.css">


	<!-- UI like dialog date picker (like alert) -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/ui/jquery-ui.css" />
	<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>bootstrap/ui/jquery-ui.js"></script>
	<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>bootstrap/ui/jquery-ui.min.js"></script>

	<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>

	
</head>
<body>
	<!-- <div class="jumbotron row">
		<div class="col-lg-2">
			<img src="<?php echo base_url(); ?>bootstrap/images/mbglogomini.png" class="img-responsive"> </img>
		</div> 
		<div class="col-lg-9">
			<h1 style="font-family:tahoma;font-size:30pt;">Ajmer Blood Donors</h1>
		</div>
	</div> -->
	<script src="<?php echo base_url(); ?>bootstrap/ckeditor/ckeditor.js"></script>
	