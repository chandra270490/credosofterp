<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='Allowances_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  $("#tbl1").empty();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = "<span class='glyphicon glyphicon-pencil'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='green'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.className = "editRecord";

	          var cell = row.insertCell(1);
				  cell.innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
	          cell.style.textAlign = "center";
	          cell.style.color='lightgray';
	          cell.setAttribute("onmouseover", "this.style.color='red'");
	          cell.setAttribute("onmouseout", "this.style.color='lightgray'");
	          cell.setAttribute("onclick", "delrowid(" + records[i].allowRowId +")");
	          // data-toggle="modal" data-target="#myModal"
	          cell.setAttribute("data-toggle", "modal");
	          cell.setAttribute("data-target", "#myModal");

	          var cell = row.insertCell(2);
	          // cell.style.display="none";
	          cell.innerHTML = records[i].allowRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].designationRowId;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].designationType;
	          // cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].nightAllow;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].midNightAllow;
	          // cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].tourAllow;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].attendanceAllow;
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].breakfastAllow;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].lunchAllow;
	  	  }


	  	$('.editRecord').bind('click', editThis);

		myDataTable.destroy();
		$(document).ready( function () {
	    myDataTable=$('#tbl1').DataTable({
		    paging: false,
		    iDisplayLength: -1,
		    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

		});
		} );

		$("#tbl1 tr").on("click", highlightRow);
			
	}
	function deleteRecord(rowId)
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/delete',
				'type': 'POST',
				'dataType': 'json',
				'data': {'rowId': rowId},
				'success': function(data){
					if(data)
					{
						if( data['dependent'] == "yes" )
						{
							alertPopup('Record can not be deleted... Dependent records exist...', 8000);
						}
						else
						{
							setTable(data['records'])
							alertPopup('Record deleted...', 4000);
							blankControls();
							// $("#txtTownName").focus();
						}
					}
				}
			});
	}
	
	function saveData()
	{	
		designationRowId = $("#cboDesignation").val().trim();
		if(designationRowId == "-1")
		{
			alertPopup("Select designation...", 8000);
			$("#cboDesignation").focus();
			return;
		}
		nightAllow = $("#txtNight").val().trim();
		midNightAllow = $("#txtMidNight").val().trim();
		tourAllow = $("#txtTour").val().trim();
		attendanceAllow = $("#txtAttendance").val().trim();
		breakfastAllow = $("#txtBreakfast").val().trim();
		lunchAllow = $("#txtLunch").val().trim();

		if($("#btnSave").val() == "Save")
		{
			// alert("save");
			$.ajax({
					'url': base_url + '/' + controller + '/insert',
					'type': 'POST',
					'dataType': 'json',
					'data': {
								'designationRowId': designationRowId
								, 'nightAllow': nightAllow
								, 'midNightAllow': midNightAllow
								, 'tourAllow': tourAllow
								, 'attendanceAllow': attendanceAllow
								, 'breakfastAllow': breakfastAllow
								, 'lunchAllow': lunchAllow
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								// $("#txtTownName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1

								alertPopup('Record saved...', 4000);
								blankControls();
								// $("#txtTownName").focus();
								// location.reload();
							}
						}
							
					}
			});
		}
		else if($("#btnSave").val() == "Update")
		{
			// alert("update");
			$.ajax({
					'url': base_url + '/' + controller + '/update',
					'type': 'POST',
					'dataType': 'json',
					'data': {'globalrowid': globalrowid
								, 'designationRowId': designationRowId
								, 'nightAllow': nightAllow
								, 'midNightAllow': midNightAllow
								, 'tourAllow': tourAllow
								, 'attendanceAllow': attendanceAllow
								, 'breakfastAllow': breakfastAllow
								, 'lunchAllow': lunchAllow
							},
					'success': function(data)
					{
						if(data)
						{
							// alert(data);
							if(data == "Duplicate record...")
							{
								alertPopup("Duplicate record...", 4000);
								$("#txtTownName").focus();
							}
							else
							{
								setTable(data['records']) ///loading records in tbl1
								alertPopup('Record updated...', 4000);
								blankControls();
								$("#btnSave").val("Save");
								// $("#txtTownName").focus();
							}
						}
							
					}
			});
		}
	}

	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records'])
						alertPopup('Records loaded...', 4000);
						blankControls();
						// $("#txtTownName").focus();
					}
				}
			});
	}
</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:10px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px;font-size:3vw'>Allowances</h1>
			<div class="row" style="margin-top:25px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Designation:</label>";
						echo form_dropdown('cboDesignation',$designations, '-1',"class='form-control' id='cboDesignation'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Night Allow.</label>";
						echo form_input('txtNight', '0', "class='form-control' id='txtNight' style='' maxlength=10 autocomplete='off'");
	              	?>

	          	</div>
			</div>

			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Mid-Night Allow.</label>";
						echo form_input('txtMidNight', '0', "class='form-control' id='txtMidNight' style='' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Tour Allow.:</label>";
						echo form_input('txtTour', '0', "class='form-control' id='txtTour' style='' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Attendance Allow.:</label>";
						echo form_input('txtAttendance', '0', "class='form-control' id='txtAttendance' style='' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Breakfast Allow.:</label>";
						echo form_input('txtBreakfast', '0', "class='form-control' id='txtBreakfast' style='' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
			</div>
			<div class="row" style="margin-top:15px;">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>Lunch Allow.:</label>";
						echo form_input('txtLunch', '0', "class='form-control' id='txtLunch' style='' maxlength=10 autocomplete='off'");
	              	?>
	          	</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<?php
						echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
						echo "<input type='button' onclick='saveData();' value='Save' id='btnSave' class='btn btn-primary form-control'>";
	              	?>
	          	</div>
			</div>


		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow: 5px 5px #d3d3d3;">
				<table class='table table-hover' id='tbl1'>
				 <thead>
					 <tr>
					 	<th  width="50" class="editRecord text-center">Edit</th>
					 	<th  width="50" class="text-center">Delete</th>
						<th style='display:none1;'>allowRowId</th>
					 	<th>DesigRowId</th>
						<th style='display:none1;'>Designation</th>
					 	<th>Night Allow.</th>
						<th style='display:none1;'>Mid-Night Allow.</th>
					 	<th>Tour Allow</th>
					 	<th>Att. Allow</th>
					 	<th>Breakfast Allow</th>
					 	<th>Lunch Allow</th>
					 </tr>
				 </thead>
				 <tbody>
					 <?php 
						foreach ($records as $row) 
						{
						 	$rowId = $row['allowRowId'];
						 	echo "<tr>";						//onClick="editThis(this);
							echo '<td style="color: lightgray;cursor: pointer;cursor: hand;" class="editRecord text-center" onmouseover="this.style.color=\'green\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-pencil"></span></td>
								   <td style="color: lightgray;cursor: pointer;cursor: hand;" class="text-center" onclick="delrowid('.$rowId.');" data-toggle="modal" data-target="#myModal" onmouseover="this.style.color=\'red\';"  onmouseout="this.style.color=\'lightgray\';"><span class="glyphicon glyphicon-remove"></span></td>';
						 	echo "<td style='display:none1;'>".$row['allowRowId']."</td>";
						 	echo "<td>".$row['designationRowId']."</td>";
						 	echo "<td style='display:none1;'>".$row['designationType']."</td>";
						 	echo "<td>".$row['nightAllow']."</td>";
						 	echo "<td style='display:none1;'>".$row['midNightAllow']."</td>";
						 	echo "<td>".$row['tourAllow']."</td>";
						 	echo "<td>".$row['attendanceAllow']."</td>";
						 	echo "<td>".$row['breakfastAllow']."</td>";
						 	echo "<td>".$row['lunchAllow']."</td>";
							echo "</tr>";
						}
					 ?>
				 </tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-0">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:5%">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-2 col-sm-2 col-md-2 col-xs-0">
		</div>

	</div>
</div>



		  <div class="modal" id="myModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">WSS</h4>
		        </div>
		        <div class="modal-body">
		          <p>Are you sure <br /> Delete this record..?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" onclick="deleteRecord(globalrowid);" class="btn btn-danger" data-dismiss="modal">Yes</button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		        </div>
		      </div>
		    </div>
		  </div>


<script type="text/javascript">
	var globalrowid;
	function delrowid(rowid)
	{
		globalrowid = rowid;
	}

	$('.editRecord').bind('click', editThis);
	function editThis(jhanda)
	{
		rowIndex = $(this).parent().index();
		colIndex = $(this).index();
		townName = 
		districtRowId = $(this).closest('tr').children('td:eq(4)').text();
		pin = $(this).closest('tr').children('td:eq(10)').text();
		std = $(this).closest('tr').children('td:eq(11)').text();
		globalrowid = $(this).closest('tr').children('td:eq(2)').text();

		$("#cboDesignation").val( $(this).closest('tr').children('td:eq(3)').text() );
		$("#txtNight").val( $(this).closest('tr').children('td:eq(5)').text() );
		$("#txtMidNight").val( $(this).closest('tr').children('td:eq(6)').text() );
		$("#txtTour").val( $(this).closest('tr').children('td:eq(7)').text() );
		$("#txtAttendance").val( $(this).closest('tr').children('td:eq(8)').text() );
		$("#txtBreakfast").val( $(this).closest('tr').children('td:eq(9)').text() );
		$("#txtLunch").val( $(this).closest('tr').children('td:eq(10)').text() );
		$("#cboDistrict").focus();
		$("#btnSave").val("Update");
	}


		$(document).ready( function () {
		    myDataTable = $('#tbl1').DataTable({
			    paging: false,
			    iDisplayLength: -1,
			    aLengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],

			});
		} );

</script>