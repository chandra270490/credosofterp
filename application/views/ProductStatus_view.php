<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='ProductStatus_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		// alert(totalStages);
		var burl = '<?php echo base_url();?>';
          $("#tbl1").find("tr:gt(0)").remove();
         //  if(records != null) 
         //  {
          	var table = document.getElementById("tbl1");

         //  	var tot = 0;
            for(var i=0; i<records.length; i++)
            {
            	newRowIndex = table.rows.length;
	        	row = table.insertRow(newRowIndex);
            	var cell = row.insertCell(0);
	        	cell.innerHTML = records[i].rowId;
	        	cell.style.display="none";

		        var cell = row.insertCell(1);
		        cell.innerHTML = records[i].productRowId;
		        cell.style.display="none";

		        var cell = row.insertCell(2);
		        cell.innerHTML = records[i].productName; 
		        // cell.style.display="none";
		        
		        var tot = 0;
		        for(var j=1; j<=totalStages; j++)
		        {
		        	var cell = row.insertCell(j+2);
		        	var fn = "records[i].S"+(j+12); ///coz first stage is 13
		        	// console.log(eval(fn));
		        	cell.innerHTML = eval(fn);
		        	if(j>1)  ///excluding no stage
			        {
			        	if( isNaN(parseFloat(eval(fn))) == false )
			        	{
			        		tot += parseInt(eval(fn));
			        	}
			        }
		        }
		        var cell = row.insertCell(j+2);
		        cell.innerHTML = tot; 
		        cell.style.color="red";

		        var cell = row.insertCell(j+3);
		        cell.innerHTML = 0;
		        if( records[i].pendingQty > 0)
		        {
		        	cell.innerHTML = records[i].pendingQty; 
		        }
		        cell.style.color="blue";

		        var cell = row.insertCell(j+4);
		        cell.innerHTML = tot - records[i].pendingQty; 
		        cell.style.color="green";

		        var cell = row.insertCell(j+5);
		        cell.innerHTML = records[i].imagePathThumb; 
		        cell.style.display="none";

		        var cell = row.insertCell(j+6);
				if( records[i].imagePathThumb != "N" )
		        {
					var pic = burl + "bootstrap/images/products/" + records[i].imagePathThumb;
			        cell.innerHTML = "<img id=\'" + records[i].productRowId + "\' src=\'" + pic + "\' width='60px' alt='' />";
		      	}
		      	else
		      	{
		      	  	cell.innerHTML = "<img src='' width='60px' alt='' />";
		      	}		        
            } ///For loop ends here

			//////Delete rows With 0 total or 0 order
			var i=0;
			// totalTdInRow = $('#tbl1').find('tr:eq(1)').children('td').length;
			// totalQty = 
			// alert(  );
			$('#tbl1 tr').each(function(row, tr)
	    	{
	    		if( i > 0 )  ///store headings
	    		{
	    			if( $(tr).find('td:eq(17)').text() <= 0) 
	    			{
	    				$(this).remove();
	    			}
	    		}
	    		i++; 
	    	});
	    	if($('#tbl1').find('tr:eq(1)').length <= 0)
	    	{
	    		alert("Zero total qty in selected products...");
	    	}
	    	// alert( $('#tbl1').find('tr:eq(1)').length );
	}

	function loadData()
	{	
		
		var productRowId="";
		$('input:checked.chkProducts').each(function() 
		{
			// alert($(this).val());
			productRowId = $(this).val() + "," + productRowId;
		});
		if(productRowId == "")
		{
			alertPopup("Select product...", 8000);
			return;
		}
		productRowId = productRowId.substr(0, productRowId.length-1);
		// alert(productRowId);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/getData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productRowId': productRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
						setTable(data['records']);
						alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}

	var tblRowsCount;
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	if( i == 0 )  ///store headings
	    	{
	        	TableData[i]=
	        	{
		            "sn" : $(tr).find('th:eq(0)').text()
		            , "productName" : $(tr).find('th:eq(2)').text()
		            , "S01" :$(tr).find('th:eq(3)').text()
		            , "S02" :$(tr).find('th:eq(4)').text()
		            , "S03" :$(tr).find('th:eq(5)').text()
		            , "S04" :$(tr).find('th:eq(6)').text()
		            , "S05" :$(tr).find('th:eq(7)').text()
		            , "S06" :$(tr).find('th:eq(8)').text()
		            , "S07" :$(tr).find('th:eq(9)').text()
		            , "S08" :$(tr).find('th:eq(10)').text()
		            , "S09" :$(tr).find('th:eq(11)').text()
		            , "S10" :$(tr).find('th:eq(12)').text()
		            , "S11" :$(tr).find('th:eq(13)').text()
		            , "S12" :$(tr).find('th:eq(14)').text()
		            , "S13" :$(tr).find('th:eq(15)').text()
		            , "S14" :$(tr).find('th:eq(16)').text()
		            , "total" :$(tr).find('th:eq(17)').text()
		            , "orders" :$(tr).find('th:eq(18)').text()
		            , "diff" : 'Diff.'
		            , "imagePathThumb" :$(tr).find('th:eq(20)').text()
	        	}   
	        	i++; 
	        }
	        else if( i == 1 )  ///dont store stageRowIDs
	    	{
	    		i++;
	    	}
	        else /// Storing table detail
	    	{
	    		if( parseInt($(tr).find('td:eq(17)').text()) > 0 || parseInt($(tr).find('td:eq(18)').text()) > 0 ) ///total or Order >0
	    		{
		        	TableData[i]=
		        	{
			            "sn" : $(tr).find('td:eq(0)').text()
			            , "productName" : $(tr).find('td:eq(2)').text()
			            , "S01" :$(tr).find('td:eq(3)').text()
			            , "S02" :$(tr).find('td:eq(4)').text()
			            , "S03" :$(tr).find('td:eq(5)').text()
			            , "S04" :$(tr).find('td:eq(6)').text()
			            , "S05" :$(tr).find('td:eq(7)').text()
			            , "S06" :$(tr).find('td:eq(8)').text()
			            , "S07" :$(tr).find('td:eq(9)').text()
			            , "S08" :$(tr).find('td:eq(10)').text()
			            , "S09" :$(tr).find('td:eq(11)').text()
			            , "S10" :$(tr).find('td:eq(12)').text()
			            , "S11" :$(tr).find('td:eq(13)').text()
			            , "S12" :$(tr).find('td:eq(14)').text()
			            , "S13" :$(tr).find('td:eq(15)').text()
			            , "S14" :$(tr).find('td:eq(16)').text()
			            , "total" :$(tr).find('td:eq(17)').text()
			            , "orders" :$(tr).find('td:eq(18)').text()
			            , "diff" :$(tr).find('td:eq(19)').text()
			            , "imagePathThumb" :$(tr).find('td:eq(20)').text()
		        	}   
		        	i++;
	        	} 
	        }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function exportData()
	{	
		// // alert();
		// // return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		

		$.ajax({
				'url': base_url + '/' + controller + '/exportData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
							window.location.href=data;
					}
				}
		});
		
	}

	function exportDataExcel()
	{	
		// // alert();
		// // return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(TableData);
		$.ajax({
				'url': base_url + '/' + controller + '/exportDataExcel',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
							window.location.href=data;
					}
				}
		});
		
	}
	function exportDataExternal()
	{	
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// console.log(TableData);
		// return;

		$.ajax({
				'url': base_url + '/' + controller + '/exportDataExternal',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
						},
				'success': function(data)
				{
					// alert(data);
					if(data)
					{
							window.location.href=data;
					}
				}
		});
		
	}

</script>
<div class="container-fluid" style="width:95%;">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Product Status</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Category:</label>";
							echo form_dropdown('cboProductCategories',$productCategories, '-1',"class='form-control' id='cboProductCategories'");
		              	?>
		          	</div>					
					<div id="style-1" class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style='background:rgba(128,128,128,.2);padding:0 50px;border-radius:5px;height:250px;overflow:auto;margin: 10px;'>
						<span class="btn" style='margin-left:-42px;'>
							<input type="checkbox" id="chkAllProducts" />
							<label for="chkAllProducts" style="font-weight:bold;color:black;">All Products</label>
						</span>

					</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Load Detail' id='btnShow' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:350px; overflow:auto;box-shadow:5px 5px #d3d3d3;border-radius:25px">
				<table class='table table-hover' id='tbl1'>
					 <tr>
						<th style='display:none;'>S.N.</th>
						<th style='display:none;'>ProductRowId</th>
						<th style='display:none1;'>Product</th>
						<?php
							foreach ($stages4table as $row) 
							{
								echo "<th>".$row['stageName']."</th>";
							}
						?>
						<th style='display:none1;'>Total</th>
						<th style='display:none1;'>Orders</th>
						<th style='display:none1;'>Diff</th>
						<th style='display:none;'>ImagePathThumb</th>
						<th style='display:none1;'>Image</th>
					 </tr>
				 	<tr id="trStageRowId" style='display:none;'>
						<td style='display:none1;'></td>
						<td style='display:none;'></td>
						<td style='display:none1;'></td>
						
						<?php
							$totalStages = 0;
							foreach ($stages4table as $row) 
							{
								echo "<td>".$row['stageRowId']."</td>";
								$totalStages++;
							}
						?>
						<td style='display:none1;'></td>
						<td style='display:none1;'></td>
						<td style='display:none1;'></td>
						<td style='display:none1;'></td>
					 </tr>
				</table>
			</div>
		</div>

		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="row" style="margin-top:20px;margin-bottom:10%" >
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportData();' value='Export Data - PDF (Internal Use)' id='btnSaveChanges' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportDataExcel();' value='Export Data - Excel (Internal Use)' id='btnSaveChanges' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
			<?php
				echo "<input type='button' onclick='exportDataExternal();' value='Export Data (External Use)' id='btnExportExternal' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
		</div>

	</div>
</div>

<!-- <div class="container text-center" style="background: lightyellow;" >  
<h4 align="center">MySql Table to 2D array(PHP,JS)</h4>
<button id='btnTableToArray' onclick='tableToArray();'>Bring and Show</button>
<div class="text-left" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;">
	<table id="tblSql" class="table table-bordered">
		<tr>
			<th>SN</th>
			<th>purchaseDetailRowId</th>
			<th>itemRowId</th>
			<th>itemName</th>
			<th>customerRowId</th>
			<th>customerName</th>
		</tr>
	</table>
</div>
</div>   -->



<script type="text/javascript">
var totalStages ='<?php echo $totalStages; ?>';


	// add multiple select / deselect functionality
	$("#chkAllProducts").click(function () {
		// alert();
		  $('.chkProducts').prop('checked', this.checked);
	});	


	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$('.chkProducts').bind('click', chkSelectAllProducts);
	function chkSelectAllProducts()
	{
		var x = parseInt($(".chkProducts").length);
		var y = parseInt($(".chkProducts:checked").length);
		if( x == y ) 
		{
			$("#chkAllProducts").prop("checked", true);
		} 
		else 
		{
			$("#chkAllProducts").prop("checked", false);
		}
	}	



	$(document).ready(function(){
        $("#cboProductCategories").change(function(){
          var productCategoryRowId = $("#cboProductCategories").val();
          if(productCategoryRowId == "-1")
          {
            return;
          }   
          // alert('taxtyperowid');
          $.ajax({
              'url': base_url + '/' + controller + '/getProductList',
              'type': 'POST',
              'dataType': 'json',
              'data': {'productCategoryRowId': productCategoryRowId},
              'success': function(data)
              {
                // var container = $('#container');
                if(data)
                {
                	$("#style-1 input:gt(0)").remove();
                	$("#style-1 label:gt(0)").remove();
                	$("#style-1 br").remove();
                	for(var i=0; i<data['products'].length; i++)
                    {
                    	$("#style-1").append("<br />");
                    	$("#style-1").append("<input type='checkbox' class='chkProducts' txt='"+ data['products'][i].productName +"' id="+data['products'][i].productRowId+" value="+data['products'][i].productRowId+">" );
                    	$("#style-1").append("<label  style='color: black;margin-left:10px;' for="+data['products'][i].productRowId+">"+ data['products'][i].productName +"</label>" );
                  	}

                }
              }
          });
        });
      });
</script>