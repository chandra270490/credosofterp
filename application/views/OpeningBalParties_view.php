<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='OpeningBalParties_Controller';
	var base_url='<?php echo site_url();?>';


	function setTable(records)
	{
		 // alert(JSON.stringify(records));
		  
		 // setHeadings();
		  $("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      // alert(noOfDays);
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);


	          var cell = row.insertCell(0);
	          cell.innerHTML = i+1;
	          cell.style.backgroundColor="#F0F0F0";
	          var cell = row.insertCell(1);
	          cell.innerHTML = records[i].partyRowId;
	          cell.style.backgroundColor="#F0F0F0";
	          cell.style.display="none";

	          var cell = row.insertCell(2);
	          cell.innerHTML = records[i].name;
	          cell.style.backgroundColor="#F0F0F0";
	          cell.className="clsPartyName";

	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].paid;
	          cell.setAttribute("contentEditable", true);
	          cell.style.textAlign="right";

	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].recd;
	          cell.setAttribute("contentEditable", true);
	          cell.style.textAlign="right";

	          var cell = row.insertCell(5);
	          cell.innerHTML = "0";
	          cell.style.textAlign="center";
	  	  }

	  	  ///////Following function to add select TD text on FOCUS
			  	$("#tbl1 tr td").on("focus", function(){
			  		// alert($(this).text());
			  		 var range, selection;
					  if (document.body.createTextRange) {
					    range = document.body.createTextRange();
					    range.moveToElementText(this);
					    range.select();
					  } else if (window.getSelection) {
					    selection = window.getSelection();
					    range = document.createRange();
					    range.selectNodeContents(this);
					    selection.removeAllRanges();
					    selection.addRange(range);
					  }
			  	}); 

			  	// $("#tbl1 tr").on("focus", function(){
			  	// 	// alert($(this).text());
			  	// 	var $nameTd = $(this).prev();
			  	// 	$nameTd.css( "background", "yellow" );
			  	// }); 



		///////////
		$("#tbl1 tr td").on("keyup", function(e){
	  	  	if ( (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) ) 
	  	  	{
	  	  		var rowIndex = $(this).parent().index();
	  	  		$("#tbl1").find("tr:eq("+ rowIndex + ")").find("td:eq("+ 5 +")").text(1);
	  	  		$("#tbl1").find("tr:eq("+ rowIndex + ")").find("td:eq("+ 5 +")").css({'color':'red', 'background':'white'});
	  	  		rowCount();
	  	  	}

	  	  	// $(".clsPartyName").css( "color", "black" );
	  	  	// $(this).parent().find("td:eq("+ 2 +")").css( "color", "red" );
	  	  	$("#tbl1 tr").css( "color", "black" );
	  	  	$(this).parent().css( "color", "red" );

	  	  });


		// $("#tbl1 tr").on("click", highlightRowAlag);
	}
	function rowCount()
	{
		var c=0;
		$("#tbl1 tr").each(function(i, v){
		    if($(this).find("td:eq("+ 5 +")").text() == '1' )
		    {
		    	c++;
		    }	
		});
		// alert(c);
		if(c>=299)
		{
			alert("Enough changes done for this time...");
		}
	}

	function loadData()
	{	
		$.ajax({
				'url': base_url + '/' + controller + '/showData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productCategoryRowId': 'productCategoryRowId'
							
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
							setTable(data['records']);
							alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}

	
	function storeTblValues()
	{
	    var TableData = new Array();
	    var i=0;
	    $('#tbl1 tr').each(function(row, tr)
	    {
	    	if($(this).find("td:eq("+ 5 +")").text() == '1' )
		    {
	        	TableData[i]=
	        	{
		            "partyRowId" : $(tr).find('td:eq(1)').text()
		            , "paid" :$(tr).find('td:eq(3)').text()
		            , "recd" :$(tr).find('td:eq(4)').text()
	        	}   
	        	i++; 
	        }
	    }); 
	    // TableData.shift();  // NOT first row will be heading - so remove COZ its dataTable
	    tblRowsCount = i-1;
	    return TableData;
	}

	function saveData()
	{	
		// alert();
		// return;
		var TableData;
		TableData = storeTblValues();
		TableData = JSON.stringify(TableData);
		// alert(JSON.stringify(TableData));
		// return;

	
		$.ajax({
				'url': base_url + '/' + controller + '/saveData',
				'type': 'POST',
				// 'dataType': 'json',
				'data': {
							'TableData': TableData
						},
				'success': function(data)
				{
					alertPopup('Changes saved...', 4000);
					// location.reload();
					$("#btnShow").trigger("click");
				}
		});
		
	}
	
</script>

<div class="container">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="">
			<h3 class="text-center" style='margin-top:-20px'>Opening Balance (Purchase)</h3>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='loadData();' value='Show Data' id='btnShow' class='btn form-control' style='background-color: lightgray;'>";
		              	?>
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">						
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
		          	</div>
				</div>

				<div class="row" style="margin-top:20px;" >
					<style>
					    table, th, td{border:1px solid gray; padding: 7px;}
					</style>
					<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:400px; overflow:auto;">
						<table style="table-layout: fixed;" id='tbl1' width="100%">
							 <tr style="background-color: #F0F0F0;">
							 	<th width="40" style='display:none1;'>S.N.</th>
							 	<th width="40" style='display:none;'>partyRowId</th>
							 	<th width="150" >Party Name</th>
							 	<th width="80" >Paid</th>
							 	<th width="80" >Recd</th>
							 	<th width="80" style='text-align: center;'>Flag</th>
							 </tr>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>

	<div class="container">
		<div class="row" style="margin-top:20px;" >
			<div class="col-lg-9 col-sm-9 col-md-9 col-xs-0">
			</div>

			<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
				<?php
					echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
					echo "<input type='button' onclick='saveData();' value='Save Changes' id='btnSave' class='btn btn-primary form-control'>";
		      	?>
			</div>
		</div>
	</div>
</div>
