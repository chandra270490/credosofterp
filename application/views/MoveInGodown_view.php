<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	var controller='MoveInGodown_Controller';
	var base_url='<?php echo site_url();?>';

	function setTable(records)
	{
		// alert(JSON.stringify(records));
		// $("#tbl1").empty();
		$("#tbl1").find("tr:gt(0)").remove();
	      var table = document.getElementById("tbl1");
	      for(i=0; i<records.length; i++)
	      {
	          newRowIndex = table.rows.length;
	          row = table.insertRow(newRowIndex);

	          var cell = row.insertCell(0);
	          cell.innerHTML = records[i].rowId;
	          cell.style.display="none";
	          var cell = row.insertCell(1);
			  cell.innerHTML = dateFormat(new Date(records[i].dt));
	          var cell = row.insertCell(2);
	          cell.style.display="none";
	          cell.innerHTML = records[i].productRowId;
	          var cell = row.insertCell(3);
	          cell.innerHTML = records[i].productName;
	          var cell = row.insertCell(4);
	          cell.innerHTML = records[i].colourRowId;
	          cell.style.display="none";
	          var cell = row.insertCell(5);
	          cell.innerHTML = records[i].colourName;
	          var cell = row.insertCell(6);
	          cell.innerHTML = records[i].fromStage;
	          cell.style.display="none";
	          var cell = row.insertCell(7);
	          cell.innerHTML = records[i].fromStageName;
	          var cell = row.insertCell(8);
	          cell.innerHTML = records[i].toStage;
	          cell.style.display="none";
	          var cell = row.insertCell(9);
	          cell.innerHTML = records[i].toStageName;
	          var cell = row.insertCell(10);
	          cell.innerHTML = records[i].qty;
	          var cell = row.insertCell(11);
	          cell.innerHTML = records[i].remarks;
	          var cell = row.insertCell(12);
	          cell.innerHTML = records[i].authorised;
	       }
	}

	function moveData()
	{	
		var dt = $("#dt").val().trim();
		// if(vDt !="")
		// {
			dtOk = testDate("dt");
			if(dtOk == false)
			{
				alertPopup("Invalid date...", 5000);
				$("#dt").focus();
				return;
			}
		// }

		// empRowId = $("#cboEmp").val();
		// if(empRowId == "-1")
		// {
		// 	alertPopup("Select employee...", 8000);
		// 	$("#cboEmp").focus();
		// 	return;
		// }

		productRowId = $("#cboProduct").val();
		if(productRowId == "-1")
		{
			alertPopup("Select product...", 8000);
			$("#cboProduct").focus();
			return;
		}

		colourRowId = $("#cboColour").val();
		if(colourRowId == "-1")
		{
			alertPopup("Select colour...", 8000);
			$("#cboColour").focus();
			return;
		}


		fromStage = $("#cboFromStage").val();
		if(fromStage == "-1")
		{
			alertPopup("Select form stage...", 8000);
			$("#cboFromStage").focus();
			return;
		}


		qty = parseInt($("#txtMove").val());
		if( qty<=0 )
		{
			alertPopup("QTy can not be zero...", 8000);
			$("#txtMove").focus();
			return;
		}
		///////avlQty must be more than moving qty
		avlQty = $("#lblFromAvailable").text();
		if(qty>avlQty)
		{
			msgBoxError("Error", "Can not move more than available...");
	        return;
		}
		

		toStage = $("#cboToStage").val();
		if(toStage == "-1")
		{
			alertPopup("Select to stage...", 8000);
			$("#cboToStage").focus();
			return;
		}

		colourRowId = $("#cboColour").val();
		if( colourRowId == "-1" )  ///if desp stage and color not selected
		{
			msgBoxError("Error", "Select Colour...");
			return;
		}
		


		if(fromStage == toStage)
		{
			msgBoxError("Error", "Both stages can not be same...");
			return;
		}
		
		remarks = $("#txtRemarks").val();

		$.ajax({
				'url': base_url + '/' + controller + '/moveData',
				'type': 'POST',
				'dataType': 'json',
				'data': {
							'productRowId': productRowId
							, 'colourRowId': colourRowId
							, 'dt': dt
							, 'fromStage': fromStage
							, 'toStage': toStage
							, 'qty': qty
							, 'remarks': remarks
							, 'colourRowId': colourRowId
						},
				'success': function(data)
				{
					if(data)
					{
						// alert(JSON.stringify(data));
						setTable(data['records']);
						$("#cboProduct").val("-1");
						$("#cboColour").val("-1");
						$("#cboFromStage").val("-1");
						$("#cboToStage").val("-1");
						$("#txtMove").val("");
						$("#txtRemarks").val("");
						$("#lblFromAvailable").text("");
						alertPopup('Records loaded...', 4000);
					}
				}
		});
		
	}


</script>
<div class="container">
	<div class="row" style="border: 1px solid lightgray; padding-top:25px;padding-bottom:40px;box-shadow: 5px 5px #d3d3d3;border-radius:25px;background-color:#fffaf0">
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<h1 class="text-center" style='margin-top:-20px'>Move in Godown</h1>
			<form name='frm' id='frm' method='post' enctype='multipart/form-data' action="">
				<div class="row" style="margin-top:25px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
						echo "<label style='color: black; font-weight: normal;'>Date:</label>";
						echo form_input('dt', '', "class='form-control' placeholder='' id='dt' maxlength='10'");
		              	?>
		              	<script>
							$( "#dt" ).datepicker({
								dateFormat: "dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2010:2050"
							});
						    // Set the Current Date as Default
							$("#dt").val(dateFormat(new Date()));
						</script>
		          	</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Product Name:</label>";
							echo form_dropdown('cboProduct',$products, '-1',"class='form-control' id='cboProduct'");
		              	?>
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black;'>Colour:</label>";
							echo form_dropdown('cboColour',$colours, '-1', "class='form-control' id='cboColour'");
		              	?>
		          	</div>
				</div>

				<div class="row" style="margin-top:15px;">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>From Stage:</label>";
							echo "<label style='color: red; font-weight: normal;' id='lblFromAvailable'>&nbsp; Avl. Qty</label>";
							echo form_dropdown('cboFromStage',$stagesFrom, '-1',"class='form-control' id='cboFromStage'");
		              	?>
		              	
		          	</div>
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>To Stage:</label>";
							// echo "<label style='color: red; font-weight: normal;' id='lblToAvailable'>&nbsp; Avl. Qty</label>";
							echo form_dropdown('cboToStage',$stagesFrom, '-1',"class='form-control' id='cboToStage'");
		              	?>
		          	</div>
		          	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Move Qty.:</label>";
							echo '<input type="number" step="1" name="txtMove" value="" class="form-control" maxlength="10" id="txtMove" />';
		              	?>
		          	</div>
		          	
				</div>

				<div class="row" style="margin-top:10px;">					
					<div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>Remarks:</label>";
							echo '<input type="text" name="txtRemarks" value="" class="form-control" maxlength="250" id="txtRemarks" />';
		              	?>
		          	</div>
		          	
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
						<?php
							echo "<label style='color: black; font-weight: normal;'>&nbsp;	</label>";
							echo "<input type='button' onclick='moveData();' value='Move Now' id='btnMove' class='btn btn-primary form-control'>";
		              	?>
		          	</div>
				</div>
			</form>
		</div>
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>


	<div class="row" style="margin-top:20px;" >
		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<div id="divTable" class="divTable col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid lightgray; padding: 10px;height:300px; overflow:auto;border-radius:25px;box-shadow:5px 5px #d3d3d3">
				<table class='table table-bordered' id='tbl1'>
					<thead>
					 <tr>
					 	<th style='display:none;'>rowId</th>
					 	<th style='display:none1;'>Date</th>
						<th style='display:none;'>ProductRowId</th>
						<th style='display:none1;'>Product</th>
						<th style='display:none;'>ColourRowId</th>
						<th style='display:none1;'>Colour</th>
						<th style='display:none;'>From</th>
						<th style='display:none1;'>From</th>
						<th style='display:none;'>To</th>
						<th style='display:none1;'>To</th>
						<th style='display:none1;'>Qty</th>
						<th style='display:none1;'>Rem</th>
						<th style='display:none1;'>Auth.</th>
					 </tr>
					</thead>
					 <tbody>
					 <?php 
					 	// print_r($records);
						foreach ($records as $row) 
						{
						 	$rowId = $row['rowId'];
						 	echo "<tr>";						
							
						 	echo "<td style='display:none;'>".$row['rowId']."</td>";
						 	$vdt = strtotime($row['dt']);
							$vdt = date('d-M-Y', $vdt);
						 	echo "<td>".$vdt."</td>";
						 	echo "<td style='display:none;'>".$row['productRowId']."</td>";
						 	echo "<td>".$row['productName']."</td>";
						 	echo "<td style='display:none;'>".$row['colourRowId']."</td>";
						 	echo "<td>".$row['colourName']."</td>";
						 	echo "<td style='display:none;'>".$row['fromStage']."</td>";
						 	echo "<td>".$row['fromStageName']."</td>";
						 	echo "<td style='display:none;'>".$row['toStage']."</td>";
						 	echo "<td>".$row['toStageName']."</td>";
						 	echo "<td>".$row['qty']."</td>";
						 	echo "<td>".$row['remarks']."</td>";
						 	echo "<td>".$row['authorised']."</td>";
							echo "</tr>";
						}
					 ?>
				 	</tbody>
				</table>
			</div>
		</div>


		<div class="col-lg-8 col-sm-8 col-md-8 col-xs-0" style="margin-bottom: 20px; margin-top: 20px;">
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom: 20px; margin-top: 20px;">
			<?php
				echo "<input type='button' onclick='loadAllRecords();' value='Load All Records' id='btnLoadAll' class='btn form-control' style='background-color: lightgray;'>";
	      	?>
		</div>
		


		<div class="col-lg-0 col-sm-0 col-md-0 col-xs-0">
		</div>
	</div>
</div>





<script type="text/javascript">
	function loadAllRecords()
	{
		// alert(rowId);
		$.ajax({
				'url': base_url + '/' + controller + '/loadAllRecords',
				'type': 'POST',
				'dataType': 'json',
				'success': function(data)
				{
					if(data)
					{
						setTable(data['records']);
						alertPopup('Records loaded...', 4000);
					}
				}
			});
	}

$(document).ready(function(){
        $("#cboFromStage").change(function(){
        	 var productRowId = $("#cboProduct").val();
	         if(productRowId == "-1")
	         {
	         	msgBoxError("Error", "Select product...");
	            return;
	         }   
	         var colourRowId = $("#cboColour").val();
	         if(colourRowId == "-1")
	         {
	         	msgBoxError("Error", "Select Colour...");
	            return;
	         }   
        	fromStage = parseInt($("#cboFromStage").val());
        	if(fromStage == "-1")
	         {
	         	msgBoxError("Error", "Select Stage...");
	            return;
	         }   
	        $.ajax({
	              'url': base_url + '/' + controller + '/getCurrentQty',
	              'type': 'POST',
	              'dataType': 'json',
	              'data': {'productRowId': productRowId, 'stage': fromStage, 'colourRowId': colourRowId},
	              'success': function(data)
	              {
	                if(data)
	                {
	                	// alert(JSON.stringify(data));
	                	// setTable(data['ob'], data['pendingOrders'], data['despStageQty'])
	                	$("#lblFromAvailable").text(data['currentQty'][0]['avlQty']);
	                }
	              }
	          });
	        });
       
        });
</script>