<?php
class Openingbalpurchase_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataForReport()
    {
        $this->db->select('products.*, ledgerp.receive');
        $this->db->from('products');
        $this->db->where('products.deleted', 'N');
        $this->db->where('products.productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->join('ledgerp','ledgerp.productRowId = products.productRowId AND vType="OB" AND ledgerp.orgRowId='. $this->session->orgRowId, 'left');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function insert()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);


        for ($i=0; $i < $myTableRows; $i++) 
        {
            $this->db->select('productRowId');
            $this->db->from('ledgerp');
            $this->db->where('productRowId', $TableData[$i]['productRowId']);
            $this->db->where('orgRowId', $this->session->orgRowId);
            $this->db->where('ledgerp.vType', 'OB');
            $query = $this->db->get();
            if ($query->num_rows() > 0) ////if product OB is already in Ledger
            {
                $this->db->select_max('ledgerPRowId');
                $query = $this->db->get('ledgerp');
                $row = $query->row_array();
                $ledgerPRowId = $row['ledgerPRowId']+1;

                $data = array(
                    'receive' => (float)$TableData[$i]['receive']
                    , 'receiveBal' => (float)$TableData[$i]['receive']
                );
                $this->db->where('vType', 'OB');
                $this->db->where('productRowId', $TableData[$i]['productRowId']);
                $this->db->where('orgRowId', $this->session->orgRowId);
                $this->db->update('ledgerp', $data);
            }
            else /// product first time in ledger
            {
                $this->db->select_max('ledgerPRowId');
                $query = $this->db->get('ledgerp');
                $row = $query->row_array();
                $ledgerPRowId = $row['ledgerPRowId']+1;

                $data = array(
                    'ledgerPRowId' => $ledgerPRowId
                    , 'orgRowId' => $this->session->orgRowId
                    , 'vType' => 'OB'
                    , 'vNo' => 1
                    , 'dt' => date('Y-m-d')
                    , 'productCategoryRowId' => $this->input->post('productCategoryRowId')
                    , 'productRowId' => $TableData[$i]['productRowId']
                    , 'receive' => (float)$TableData[$i]['receive']
                    , 'receiveBal' => (float)$TableData[$i]['receive']
                    , 'createdBy' => $this->session->userRowId
                );
                $this->db->set('createdStamp', 'NOW()', FALSE);
                $this->db->insert('ledgerp', $data);
            }


        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}