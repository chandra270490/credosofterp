<?php
class Designationtypes_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('designationTypeRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('designationtypes');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('designationTypeRowId');
		$query = $this->db->get('designationtypes');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('designationType');
		$this->db->where('designationType', $this->input->post('designationType'));
		$query = $this->db->get('designationtypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('designationTypeRowId');
		$query = $this->db->get('designationtypes');
        $row = $query->row_array();

        $current_row = $row['designationTypeRowId']+1;

		$data = array(
	        'designationTypeRowId' => $current_row
	        , 'designationType' => ucwords($this->input->post('designationType'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('designationtypes', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('designationType');
		$this->db->where('designationType', $this->input->post('designationType'));
		$this->db->where('designationTypeRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('designationtypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'designationType' => ucwords($this->input->post('designationType'))
		);
		$this->db->where('designationTypeRowId', $this->input->post('globalrowid'));
		$this->db->update('designationtypes', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('designationTypeRowId', $this->input->post('rowId'));
		$this->db->delete('designationtypes');
	}

	public function getDesignationList()
	{
		$this->db->select('designationTypeRowId, designationType');
		$this->db->where('deleted', 'N');
		$this->db->order_by('designationType');
		$query = $this->db->get('designationtypes');
		$arr = array();
		$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['designationTypeRowId']]= $row['designationType'];
		}
		return $arr;
	}

}