<?php
class Moveinstage_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getProductList()
    {
        $this->db->select('productRowId, productName, productcategories.productCategory');
        $this->db->where('products.deleted', 'N');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('productName');
        $query = $this->db->get('products');

        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['productRowId']] = $row['productName'] . '  ---  ['. $row['productCategory'] . ' ]';
        }

        return $arr;
    }

    public function getEmpList()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees',"employees.abRowId = addressbook.abRowId AND employees.discontinue='N'");
        $this->db->where('employees.deleted', 'N');
        // $this->db->where('employees.discontinue', 'N');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getStagesTo()
    {
        $this->db->select('stages.*');
        $this->db->from('stages');
        $this->db->where('stages.deleted', 'N');
        $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('stages.odr');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['stageRowId']]= $row['stageName'];
        }
        return $arr;
    }

    public function getStagesFrom()
    {
        $this->db->select('stages.*');
        $this->db->from('stages');
        $this->db->where('stages.deleted', 'N');
        $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('stages.odr');
        $query = $this->db->get();

        return($query->result_array());
    }

    public function getStages4table()
    {
        $this->db->select('*');
        $this->db->where('deleted', 'N');
        $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('odr');
        $query = $this->db->get('stages');

        return($query->result_array());
    }

    public function getProductStatus()
    {
        $this->db->select('openingbal.*, odr');
        $this->db->where('productRowId',  $this->input->post('productRowId'));
        $this->db->where('openingbal.stageRowId <',  26);
        $this->db->order_by('odr');
        $this->db->from('openingbal');
        $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
        $query = $this->db->get();

        return($query->result_array());
    }

    public function getDespStageQty()
    {
        $this->db->select_sum('avlQty');
        $this->db->where('productRowId',  $this->input->post('productRowId'));
        $this->db->where('openingbal.stageRowId',  26);
        $this->db->order_by('odr');
        $this->db->from('openingbal');
        $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
        $query = $this->db->get();

        return($query->result_array());
    }


    public function getPendingOrders()
    {
        $this->db->select_sum('pendingQty');
        $this->db->where('productRowId',  $this->input->post('productRowId'));
        $this->db->where('qpo.orgRowId', $this->session->orgRowId);
        $this->db->join('qpo',"qpo.qpoRowId = qpodetail.qpoRowId AND qpo.deleted='N'");
        $this->db->from('qpodetail');

        // $this->db->where('productRowId',  $this->input->post('productRowId'));
        // $this->db->order_by('odr');
        // $this->db->from('qpodetail');
        // $this->db->join('stages','stages.stageRowId = openingbal.stageRowId');
        $query = $this->db->get();

        return($query->result_array());
    }

    public function moveData()
    {
        set_time_limit(0);
        $this->db->trans_start();

        $this->db->select_max('rowId');
        $query = $this->db->get('movestage');
        $row = $query->row_array();

        $current_row = $row['rowId']+1;

        if($this->input->post('dt') == '')
        {
            $dt = null;
        }
        else
        {
            $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        }
        $data = array(
            'rowId' => $current_row
            , 'productRowId' => $this->input->post('productRowId')
            , 'dt' => $dt
            , 'empRowId' => $this->input->post('empRowId')
            , 'fromStage' => $this->input->post('fromStage')
            , 'toStage' => $this->input->post('toStage')
            , 'qty' => $this->input->post('qty')
            , 'weightage' => $this->input->post('weightage')
            , 'direction' => $this->input->post('direction')
            , 'remarks' => $this->input->post('remarks')
            , 'orgRowId' => $this->session->orgRowId
            , 'createdBy' => $this->session->userRowId
            , 'colourRowId' => $this->input->post('colourRowId')
        );
        $this->db->set('createdStamp', 'NOW()', FALSE);
        $this->db->insert('movestage', $data); 


        ////////////// Adjusting Avl Qty of product
        ////updating fromStage QTy
        if($this->input->post('fromStage') == "26") ///despatch stage
        {
            $this->db->select('colourRowId');
            $this->db->from('openingbal');
            $this->db->where('openingbal.productRowId', $this->input->post('productRowId'));
            $this->db->where('openingbal.colourRowId', $this->input->post('colourRowId'));
            $this->db->where('openingbal.stageRowId', 26);
            $query = $this->db->get();
            if ($query->num_rows() > 0) ///this color already exists or not
            {
                $found = 1;
            }
            else
            {
                $found = 0;
            }
            if( $found == 1 )
            {
                $q="Update openingbal set avlQty = avlQty - " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND stageRowId=". $this->input->post('fromStage') . " AND colourRowId=". $this->input->post('colourRowId');
                $this->db->query($q);
            }
            else
            {
                
            }
        }
        else   /// not desp stage
        {
            $q="Update openingbal set avlQty = avlQty - " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND stageRowId=". $this->input->post('fromStage');
            $this->db->query($q);
        }
        


        ////updating toStage QTy
        if($this->input->post('toStage') == "26") ///despatch stage
        {
            $this->db->select('colourRowId');
            $this->db->from('openingbal');
            $this->db->where('openingbal.productRowId', $this->input->post('productRowId'));
            $this->db->where('openingbal.colourRowId', $this->input->post('colourRowId'));
            $this->db->where('openingbal.stageRowId', 26);
            // $this->db->order_by('stages.odr');
            $query = $this->db->get();
            if ($query->num_rows() > 0) ///this color already exists or not
            {
                // $row = $query->row_array();
                //  $lastStageRowId = $row['stageRowId'];
                $found = 1;
            }
            else
            {
                $found = 0;
            }

           if( $found == 1 )
           {
                $q="Update openingbal set avlQty = avlQty + " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND stageRowId=". $this->input->post('toStage'). " AND colourRowId=". $this->input->post('colourRowId');
                $this->db->query($q);
            }
            else
            {
                $this->db->select_max('rowId');
                $query = $this->db->get('openingbal');
                $row = $query->row_array();

                $opBalRowId = $row['rowId']+1;

                 $data = array(
                    'rowId' => $opBalRowId
                    , 'productRowId' => $this->input->post('productRowId')
                    , 'dt' => $dt
                    , 'stageRowId' => 26
                    , 'opQty' => 0
                    , 'avlQty' => $this->input->post('qty')
                    , 'orgRowId' => $this->session->orgRowId
                    , 'colourRowId' => $this->input->post('colourRowId')
                );
                $this->db->set('createdStamp', 'NOW()', FALSE);
                $this->db->insert('openingbal', $data); 
            }
            
        }
        else //// not desp stage
        {
            $q="Update openingbal set avlQty = avlQty + " . $this->input->post('qty') . " WHERE productRowId=" . $this->input->post('productRowId') . " AND stageRowId=". $this->input->post('toStage');
            $this->db->query($q);
        }

        $this->db->trans_complete();
    }

}