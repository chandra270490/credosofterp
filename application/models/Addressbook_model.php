<?php
class Addressbook_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('addressbook.*, prefixtypes.prefixType, towns.townName, AB.name as refName');
			$this->db->from('addressbook');
			$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
			$this->db->join('towns','towns.townRowId = addressbook.townRowId');
			$this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');
			$this->db->where('addressbook.deleted', 'N');
			// $this->db->where('addressbook.abRowId', 592);
			// $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
			$this->db->order_by('addressbook.abRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('addressbook.*, prefixtypes.prefixType, towns.townName, AB.name as refName');
			$this->db->from('addressbook');
			$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
			$this->db->join('towns','towns.townRowId = addressbook.townRowId');
			$this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');
			$this->db->where('addressbook.deleted', 'N');
			// $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('addressbook.createdBy', $abAccessIn);
			$this->db->order_by('addressbook.abRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}

	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('addressbook.*, prefixtypes.prefixType, towns.townName, AB.name as refName');
			$this->db->from('addressbook');
			$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
			$this->db->join('towns','towns.townRowId = addressbook.townRowId');
			$this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');
			$this->db->where('addressbook.deleted', 'N');
			// $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
			$this->db->order_by('addressbook.abRowId');
			$query = $this->db->get();
			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('addressbook.*, prefixtypes.prefixType, towns.townName, AB.name as refName');
			$this->db->from('addressbook');
			$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
			$this->db->join('towns','towns.townRowId = addressbook.townRowId');
			$this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');
			$this->db->where('addressbook.deleted', 'N');
			// $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('addressbook.createdBy', $abAccessIn);
			$this->db->order_by('addressbook.abRowId');
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    

	public function checkDuplicate()
    {
		$this->db->select('name');
		$this->db->where('name', $this->input->post('name'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('addressbook');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_start();

		$this->db->select_max('abRowId');
		$query = $this->db->get('addressbook');
        $row = $query->row_array();
        $current_row = $row['abRowId']+1;

        if($this->input->post('passportValidUpto') == '')
        {
        	$passportValidUpto = null;
        }
        else
        {
        	$passportValidUpto = date('Y-m-d', strtotime($this->input->post('passportValidUpto')));
        }
        if($this->input->post('drivingLicValidUpto') == '')
        {
        	$drivingLicValidUpto = null;
        }
        else
        {
        	$drivingLicValidUpto = date('Y-m-d', strtotime($this->input->post('drivingLicValidUpto')));
        }
        if($this->input->post('familyCardValidUpto') == '')
        {
        	$familyCardValidUpto = null;
        }
        else
        {
        	$familyCardValidUpto = date('Y-m-d', strtotime($this->input->post('familyCardValidUpto')));
        }
        if($this->input->post('dob') == '')
        {
        	$dob = null;
        }
        else
        {
        	$dob = date('Y-m-d', strtotime($this->input->post('dob')));
        }
        if($this->input->post('dom') == '')
        {
        	$dom = null;
        }
        else
        {
        	$dom = date('Y-m-d', strtotime($this->input->post('dom')));
        }

		$data = array(
	        'abRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'prefixTypeRowId' => $this->input->post('prefixTypeRowId')
	        , 'name' => ucwords($this->input->post('name'))
	        , 'abType' => $this->input->post('abType')
	        , 'contactPriority' => $this->input->post('contactPriority')
	        , 'godown' => $this->input->post('godown')
	        , 'labelName' => ucwords($this->input->post('labelName'))
	        , 'addr' => ucwords($this->input->post('addr'))
	        , 'townRowId' => $this->input->post('townRowId')
	        , 'pin' => $this->input->post('pin')
	        , 'referenceRowId' => $this->input->post('referenceRowId')
	        , 'landLine1' => $this->input->post('landLine1')
	        , 'landLine2' => $this->input->post('landLine2')
	        , 'mobile1' => $this->input->post('mobile1')
	        , 'mobile2' => $this->input->post('mobile2')
	        , 'email1' => $this->input->post('email1')
	        , 'email2' => $this->input->post('email2')
	        , 'pan' => strtoupper($this->input->post('pan'))
	        , 'din' => $this->input->post('din')
	        , 'passport' => $this->input->post('passport')
	        , 'passportValidUpto' => $passportValidUpto
	        , 'drivingLic' => $this->input->post('drivingLic')
	        , 'drivingLicValidUpto' => $drivingLicValidUpto
	        , 'familyCard' => $this->input->post('familyCard')
	        , 'familyCardValidUpto' => $familyCardValidUpto
	        , 'dob' => $dob
	        , 'dom' => $dom
	        , 'tinState' => $this->input->post('tinState')
	        , 'tinCentre' => $this->input->post('tinCentre')
	        , 'contactPerson' => ucwords($this->input->post('contactPerson'))
	        , 'contactMobile' => $this->input->post('contactMobile')
	        , 'contactDepartment' => ucwords($this->input->post('contactDepartment'))
	        , 'contactDesignation' => $this->input->post('contactDesignation')
	        , 'contactPerson2' => ucwords($this->input->post('contactPerson2'))
	        , 'contactMobile2' => $this->input->post('contactMobile2')
	        , 'contactDepartment2' => ucwords($this->input->post('contactDepartment2'))
	        , 'contactDesignation2' => $this->input->post('contactDesignation2')
	        , 'contactPerson3' => ucwords($this->input->post('contactPerson3'))
	        , 'contactMobile3' => $this->input->post('contactMobile3')
	        , 'contactDepartment3' => ucwords($this->input->post('contactDepartment3'))
	        , 'contactDesignation3' => $this->input->post('contactDesignation3')
	        , 'gstIn' => $this->input->post('gstIn')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('addressbook', $data);	

		/////Saving Contact Types
		$contactRowId = explode(",", $this->input->post('contactRowId'));
		for ($i=0; $i <count($contactRowId)-1 ; $i++)
       	{
			$data1 = array(
	        'abRowId' => $current_row,
	        'contactTypeRowId' =>$contactRowId[$i],
			);
			$this->db->insert('addressbookcontacttypes', $data1);
		}
		/////END - Saving Contact Types

		/////Saving Bank Details
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        

        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('rowId');
			$query = $this->db->get('addressbookbankdetails');
	        $row = $query->row_array();
	        $bankRowId = $row['rowId']+1;

			$data = array(
			        'rowId' => $bankRowId
			        , 'abRowId' => $current_row
			        , 'bankName' => ucwords($TableData[$i]['bankName'])
			        , 'branchName' => ucwords($TableData[$i]['branchName'])
			        , 'ifsc' => strtoupper($TableData[$i]['ifsc'])
			        , 'acNo' => $TableData[$i]['acNo']
			);
			$this->db->insert('addressbookbankdetails', $data);	  
        }
        /////END - Saving Bank Details


       $this->db->trans_complete();

	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('name');
		$this->db->where('name', $this->input->post('name'));
		$this->db->where('abRowId !=', $this->input->post('globalrowid'));
		$this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('addressbook');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_start();

        if($this->input->post('passportValidUpto') == null)
        {
        	$passportValidUpto = null;
        }
        else
        {
        	$passportValidUpto = date('Y-m-d', strtotime($this->input->post('passportValidUpto')));
        }
        if($this->input->post('drivingLicValidUpto') == '')
        {
        	$drivingLicValidUpto = null;
        }
        else
        {
        	$drivingLicValidUpto = date('Y-m-d', strtotime($this->input->post('drivingLicValidUpto')));
        }
        if($this->input->post('familyCardValidUpto') == '')
        {
        	$familyCardValidUpto = null;
        }
        else
        {
        	$familyCardValidUpto = date('Y-m-d', strtotime($this->input->post('familyCardValidUpto')));
        }
        if($this->input->post('dob') == '')
        {
        	$dob = null;
        }
        else
        {
        	$dob = date('Y-m-d', strtotime($this->input->post('dob')));
        }
        if($this->input->post('dom') == '')
        {
        	$dom = null;
        }
        else
        {
        	$dom = date('Y-m-d', strtotime($this->input->post('dom')));
        }    	
		$data = array(
	        'prefixTypeRowId' => $this->input->post('prefixTypeRowId')
	        , 'orgRowId' => $this->session->orgRowId
	        , 'name' => ucwords($this->input->post('name'))
	        , 'abType' => $this->input->post('abType')
	        , 'contactPriority' => $this->input->post('contactPriority')
	        , 'godown' => $this->input->post('godown')
	        , 'labelName' => ucwords($this->input->post('labelName'))
	        , 'addr' => ucwords($this->input->post('addr'))
	        , 'townRowId' => $this->input->post('townRowId')
	        , 'pin' => $this->input->post('pin')
	        , 'referenceRowId' => $this->input->post('referenceRowId')	        
	        , 'landLine1' => $this->input->post('landLine1')
	        , 'landLine2' => $this->input->post('landLine2')
	        , 'mobile1' => $this->input->post('mobile1')
	        , 'mobile2' => $this->input->post('mobile2')
	        , 'email1' => $this->input->post('email1')
	        , 'email2' => $this->input->post('email2')
	        , 'pan' => strtoupper($this->input->post('pan'))
	        , 'din' => $this->input->post('din')
	        , 'passport' => $this->input->post('passport')
	        , 'passportValidUpto' => $passportValidUpto
	        , 'drivingLic' => $this->input->post('drivingLic')
	        , 'drivingLicValidUpto' => $drivingLicValidUpto
	        , 'familyCard' => $this->input->post('familyCard')
	        , 'familyCardValidUpto' => $familyCardValidUpto
	        , 'dob' => $dob
	        , 'dom' => $dom
	        , 'tinState' => $this->input->post('tinState')
	        , 'tinCentre' => $this->input->post('tinCentre')
	        , 'contactPerson' => ucwords($this->input->post('contactPerson'))
	        , 'contactMobile' => $this->input->post('contactMobile')
	        , 'contactDepartment' => ucwords($this->input->post('contactDepartment'))
	        , 'contactDesignation' => $this->input->post('contactDesignation')
	        , 'contactPerson2' => ucwords($this->input->post('contactPerson2'))
	        , 'contactMobile2' => $this->input->post('contactMobile2')
	        , 'contactDepartment2' => ucwords($this->input->post('contactDepartment2'))
	        , 'contactDesignation2' => $this->input->post('contactDesignation2')
	        , 'contactPerson3' => ucwords($this->input->post('contactPerson3'))
	        , 'contactMobile3' => $this->input->post('contactMobile3')
	        , 'contactDepartment3' => ucwords($this->input->post('contactDepartment3'))
	        , 'contactDesignation3' => $this->input->post('contactDesignation3')
	        , 'gstIn' => $this->input->post('gstIn')

	    );
		$this->db->where('abRowId', $this->input->post('globalrowid'));
		$this->db->update('addressbook', $data);	

		/////Saving Contact Types
	    $this->db->where('abRowId', $this->input->post('globalrowid'));
        $this->db->delete('addressbookcontacttypes');


		$contactRowId = explode(",", $this->input->post('contactRowId'));
		for ($i=0; $i <count($contactRowId)-1 ; $i++)
       	{
			$data1 = array(
	        'abRowId' => $this->input->post('globalrowid'),
	        'contactTypeRowId' =>$contactRowId[$i],
			);
			$this->db->insert('addressbookcontacttypes', $data1);
		}
		/////END - Saving Contact Types

		/////Saving Bank Details
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        $this->db->where('abRowId', $this->input->post('globalrowid'));
        $this->db->delete('addressbookbankdetails');

        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('rowId');
			$query = $this->db->get('addressbookbankdetails');
	        $row = $query->row_array();
	        $bankRowId = $row['rowId']+1;

			$data = array(
			        'rowId' => $bankRowId
			        , 'abRowId' => $this->input->post('globalrowid')
			        , 'bankName' => ucwords($TableData[$i]['bankName'])
			        , 'branchName' => ucwords($TableData[$i]['branchName'])
			        , 'ifsc' => strtoupper($TableData[$i]['ifsc'])
			        , 'acNo' => $TableData[$i]['acNo']
			);
			$this->db->insert('addressbookbankdetails', $data);	  
        }
        /////END - Saving Bank Details		

        $this->db->trans_complete();
        		
	}

	public function delete()
	{
		$data = array(
		        'deleted' => 'Y',
		        'deletedBy' => $this->session->userRowId

		);
		$this->db->set('deletedStamp', 'NOW()', FALSE);
		$this->db->where('abRowId', $this->input->post('rowId'));
		$this->db->update('addressbook', $data);

		// $this->db->where('abRowId', $this->input->post('rowId'));
		// $this->db->delete('addressbook');
	}

	public function getAllContacts()
	{
		$this->db->select('contactTypeRowId');
		$this->db->where('abRowId',$this->input->post('rowid'));
		$query = $this->db->get('addressbookcontacttypes');
		$msg ="";

		foreach ($query->result_array() as $row)
		{
    		$msg = $row['contactTypeRowId'].",".$msg;
			}
		return $msg;
	}

	public function getbankDetails()
    {
        $this->db->select('addressbookbankdetails.*');
        $this->db->where('addressbookbankdetails.abRowId', $this->input->post('rowid'));
        $this->db->from('addressbookbankdetails');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getAbData()
	{
		$this->db->select('addressbook.*, towns.townName, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('addressbook');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('addressbook.deleted', 'N');
		$this->db->where('addressbook.abRowId', $this->input->post('rowid'));
		$this->db->order_by('addressbook.name');
		$query = $this->db->get();

		return($query->result_array());
	}
	public function getPrefixType()
	{
		$this->db->select('addressbook.name, prefixtypes.prefixType');
		$this->db->from('addressbook');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->where('addressbook.abRowId', $this->input->post('rowid'));
		$query = $this->db->get();
		return($query->result_array());
	}
	public function getReferencerName()
	{
		$this->db->select('addressbook.name, AB.name as refName');
		$this->db->from('addressbook');
		$this->db->join('addressbook AB','AB.abRowId = addressbook.referenceRowId', 'left');
		$this->db->where('addressbook.abRowId', $this->input->post('rowid'));
		$this->db->where('addressbook.deleted', 'N');
		$query = $this->db->get();
		return($query->result_array());
	}
	public function getContactTypes()
	{
		$this->db->select('contactType');
		$this->db->where('abRowId',$this->input->post('rowid'));
		$this->db->join('contacttypes','contacttypes.contactTypeRowId = addressbookcontacttypes.contactTypeRowId');
		$query = $this->db->get('addressbookcontacttypes');
		$msg ="";

		foreach ($query->result_array() as $row)
		{
    		$msg = $row['contactType'].",".$msg;
			}
		return $msg;
	}
}