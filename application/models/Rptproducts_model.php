<?php
class Rptproducts_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getImageName($rowId)
    {
        $this->db->select('products.productRowId, products.imagePathThumb');
		$this->db->from('products');
		$this->db->where('products.productRowId', $rowId);
		// $this->db->order_by('qpo.qpoRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }
}