<?php
class Salaryotherinc_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function showData()
    {
        //////Checking if already stored on that date
        $this->db->select('dt');
        $this->db->where('dt =', date('Y-m-d', strtotime($this->input->post('dt'))));
        $query = $this->db->get('salaryinc');
        if ($query->num_rows() > 0) /////////Already Marked
        {
            $this->db->select('addressbook.name, salaryinc.*');
            $this->db->from('salaryinc');
            $this->db->join('employees','employees.empRowId = salaryinc.empRowId');
            $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
            $this->db->where('salaryinc.dt =', date('Y-m-d', strtotime($this->input->post('dt'))));
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();
            return($query->result_array());
        }
        else
        {
            $this->db->select('addressbook.name, employees.empRowId');
            $this->db->from('addressbook');
            $this->db->join('employees','employees.abRowId = addressbook.abRowId');
            $this->db->where('employees.deleted', 'N');
            $this->db->where('employees.salType', 'M');
            $this->db->where('employees.discontinue', 'N');
            $this->db->where('employees.orgRowId', $this->session->orgRowId);
            $this->db->order_by('addressbook.name');
            $query = $this->db->get();
            return($query->result_array());
        }
    }


	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        // echo $myTableRows;

        $this->db->where('dt =', date('Y-m-d', strtotime($this->input->post('dt'))));
        $this->db->delete('salaryinc');	
        
        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('siRowId');
			$query = $this->db->get('salaryinc');
	        $row = $query->row_array();
	        $siRowId = $row['siRowId']+1;

			$data = array(
		        'siRowId' => $siRowId
		        , 'dt' => $dt
		        , 'empRowId' => $TableData[$i]['empRowId']
		        , 'amt' => (float)$TableData[$i]['amt']
		        , 'remarks' => $TableData[$i]['remarks']
		        , 'orgRowId' => $this->session->orgRowId
                , 'createdBy' => $this->session->userRowId
			);
            $this->db->set('createdStamp', 'NOW()', FALSE);
			$this->db->insert('salaryinc', $data);	
		}

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}

}