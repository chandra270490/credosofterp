<?php
class Districts_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('districts.*, states.stateName, countries.countryRowId, countries.countryName');
		$this->db->from('districts');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('districts.deleted', 'N');
		$this->db->order_by('districts.districtRowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('districts.*, states.stateName, countries.countryRowId, countries.countryName');
		$this->db->from('districts');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('districts.deleted', 'N');
		$this->db->order_by('districts.districtRowId');
		$query = $this->db->get();

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('districtName');
		$this->db->where('districtName', $this->input->post('districtName'));
		$query = $this->db->get('districts');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('districtRowId');
		$query = $this->db->get('districts');
        $row = $query->row_array();

        $current_row = $row['districtRowId']+1;
		$data = array(
	        'districtRowId' => $current_row
	        , 'districtName' => ucwords($this->input->post('districtName'))
	        , 'stateRowId' => $this->input->post('stateRowId')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('districts', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('districtName');
		$this->db->where('districtName', $this->input->post('districtName'));
		$this->db->where('districtRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('districts');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'districtName' => ucwords($this->input->post('districtName'))
	        , 'stateRowId' => $this->input->post('stateRowId')	        
		);
		$this->db->where('districtRowId', $this->input->post('globalrowid'));
		$this->db->update('districts', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('districtRowId', $this->input->post('rowId'));
		$this->db->delete('districts');
	}

	public function getDistrictList()
	{
		$this->db->select('districts.*, states.stateName, countries.countryName');
		$this->db->from('districts');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('districts.deleted', 'N');
		$this->db->order_by('districts.districtName');
		$query = $this->db->get();

		return($query->result_array());

	}
}