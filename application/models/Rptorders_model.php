<?php
class Rptorders_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getUserList()
    {
        $this->db->select('users.rowid, users.uid');
        $this->db->from('users');
        $this->db->where('users.deleted', 'N');
        $this->db->order_by('users.uid');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['rowid']]= $row['uid'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        if ( $this->input->post('userRowId') == "-1" )
        {
            if( $this->input->post('partyRowId') == "-1" && $this->input->post('status') == "-1" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0', 'LEFT');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') == "-1" && $this->input->post('status') == "P" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') == "-1" && $this->input->post('status') == "C" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.qpoRowId, qpo.vType, qpo.vNo, qpo.vDt, qpo.partyRowID, qpo.orderTypeRowId, qpo.commitmentDate, qpo.net, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, "-22" as qri, users.uid');
                 $this->db->select_sum('qpodetail.pendingQty');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->group_by('qpo.qpoRowId, qpo.vType, qpo.vNo');
                 $this->db->having('sum(qpodetail.pendingQty)', 0);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') != "-1" && $this->input->post('status') == "-1" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0', 'LEFT');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') != "-1" && $this->input->post('status') == "P" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                  $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') != "-1" && $this->input->post('status') == "C" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.qpoRowId, qpo.vType, qpo.vNo, qpo.vDt, qpo.partyRowID, qpo.orderTypeRowId, qpo.commitmentDate, qpo.net, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, "-22" as qri, users.uid');
                 $this->db->select_sum('qpodetail.pendingQty');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                  $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->group_by('qpo.qpoRowId, qpo.vType, qpo.vNo');
                 $this->db->having('sum(qpodetail.pendingQty)', 0);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }        
        }
        else if ( $this->input->post('userRowId') != "-1" )
        {
            if( $this->input->post('partyRowId') == "-1" && $this->input->post('status') == "-1" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0', 'LEFT');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.createdBy', $this->input->post('userRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') == "-1" && $this->input->post('status') == "P" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.createdBy', $this->input->post('userRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') == "-1" && $this->input->post('status') == "C" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.qpoRowId, qpo.vType, qpo.vNo, qpo.vDt, qpo.partyRowID, qpo.orderTypeRowId, qpo.commitmentDate, qpo.net, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, "-22" as qri, users.uid');
                 $this->db->select_sum('qpodetail.pendingQty');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.createdBy', $this->input->post('userRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->group_by('qpo.qpoRowId, qpo.vType, qpo.vNo');
                 $this->db->having('sum(qpodetail.pendingQty)', 0);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') != "-1" && $this->input->post('status') == "-1" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0', 'LEFT');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                 $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.createdBy', $this->input->post('userRowId'));
                 $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') != "-1" && $this->input->post('status') == "P" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.*, qpodetail.qpoRowId as qri, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, users.uid');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId AND qpodetail.pendingQty>0');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                  $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.createdBy', $this->input->post('userRowId'));
                 $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }
            else if( $this->input->post('partyRowId') != "-1" && $this->input->post('status') == "C" )
            {
                 $this->db->distinct();
                 $this->db->select('qpo.qpoRowId, qpo.vType, qpo.vNo, qpo.vDt, qpo.partyRowID, qpo.orderTypeRowId, qpo.commitmentDate, qpo.net, addressbook.name, addressbook.addr, ordertypes.orderType, ab.name as reference, "-22" as qri, users.uid');
                 $this->db->select_sum('qpodetail.pendingQty');
                 $this->db->from('qpo');
                 $this->db->join('qpodetail','qpodetail.qpoRowId = qpo.qpoRowId');
                 $this->db->join('parties','parties.partyRowId = qpo.partyRowId');
                 $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
                 $this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
                 $this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
                  $this->db->join('users', 'users.rowid = qpo.createdBy');
                 $this->db->where('qpo.vType', 'O');
                 $this->db->where('qpo.createdBy', $this->input->post('userRowId'));
                 $this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
                 $this->db->where('qpo.vDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                 $this->db->where('qpo.vDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                 $this->db->where('qpo.deleted', 'N');
                 $this->db->where('qpo.orgRowId', $this->session->orgRowId);
                 $this->db->group_by('qpo.qpoRowId, qpo.vType, qpo.vNo');
                 $this->db->having('sum(qpodetail.pendingQty)', 0);
                 $this->db->order_by('qpo.qpoRowId');
                 $query = $this->db->get();
                 return($query->result_array());
            }        

        }
    }



    public function getProducts()
    {
        $this->db->select('qpodetail.*, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight, colours.colourName');
        $this->db->where('qpodetail.qpoRowId', $this->input->post('rowid'));
        $this->db->from('qpodetail');
        $this->db->join('products','products.productRowId = qpodetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        // $this->db->join('placements','placements.placementRowId = qpodetail.placementRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());
    }
}