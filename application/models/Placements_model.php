<?php
class Placements_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('placementRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('placements');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('placementRowId');
		$query = $this->db->get('placements');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('placement');
		$this->db->where('placement', $this->input->post('placement'));
		$query = $this->db->get('placements');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('placementRowId');
		$query = $this->db->get('placements');
        $row = $query->row_array();

        $current_row = $row['placementRowId']+1;

		$data = array(
	        'placementRowId' => $current_row
	        , 'placement' => ucwords($this->input->post('placement'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('placements', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('placement');
		$this->db->where('placement', $this->input->post('placement'));
		$this->db->where('placementRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('placements');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'placement' => ucwords($this->input->post('placement'))
		);
		$this->db->where('placementRowId', $this->input->post('globalrowid'));
		$this->db->update('placements', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('placementRowId', $this->input->post('rowId'));
		$this->db->delete('placements');
	}

	public function getPlacementList()
	{
		$this->db->select('placementRowId, placement');
		$this->db->where('deleted', 'N');
		$this->db->order_by('placement');
		$query = $this->db->get('placements');
		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['placementRowId']]= $row['placement'];
		}
		return $arr;
	}

	public function getPlacementListRefresh()
	{
		$this->db->select('placementRowId, placement');
		$this->db->where('deleted', 'N');
		$this->db->order_by('placement');
		$query = $this->db->get('placements');
		return $query->result_array();
		// $arr["-1"] = '--- SELECT ---';
		// foreach ($query->result_array() as $row)
		// {
  //   		$arr[$row['placementRowId']]= $row['placement'];
		// }
		// return $arr;
	}
}