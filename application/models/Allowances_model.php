<?php
class Allowances_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDesignationList()
	{
		$this->db->select('designationTypeRowId, designationType');
		$this->db->where('deleted', 'N');
		$this->db->order_by('designationType');
		$query = $this->db->get('designationtypes');
		$arr = array();
		$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['designationTypeRowId']]= $row['designationType'];
		}
		return $arr;
	}

    public function getDataLimit()
	{
		$this->db->select('allowances.*, designationtypes.designationType');
		$this->db->from('allowances');
		$this->db->where('allowances.deleted', 'N');
		$this->db->join('designationtypes','designationtypes.designationTypeRowId = allowances.designationRowId');
		$this->db->order_by('allowances.allowRowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('allowances.*, designationtypes.designationType');
		$this->db->from('allowances');
		$this->db->where('allowances.deleted', 'N');
		$this->db->join('designationtypes','designationtypes.designationTypeRowId = allowances.designationRowId');
		$this->db->order_by('allowances.allowRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('allowRowId');
		$this->db->where('designationRowId', $this->input->post('designationRowId'));
		$query = $this->db->get('allowances');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('allowRowId');
		$query = $this->db->get('allowances');
        $row = $query->row_array();

        $current_row = $row['allowRowId']+1;
		$data = array(
	        'allowRowId' => $current_row
	        , 'designationRowId' => ucwords($this->input->post('designationRowId'))
	        , 'nightAllow' => $this->input->post('nightAllow')
	        , 'midNightAllow' => $this->input->post('midNightAllow')
	        , 'tourAllow' => $this->input->post('tourAllow')
	        , 'attendanceAllow' => $this->input->post('attendanceAllow')
	        , 'breakfastAllow' => $this->input->post('breakfastAllow')
	        , 'lunchAllow' => $this->input->post('lunchAllow')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('allowances', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('allowRowId');
		$this->db->where('designationRowId', $this->input->post('designationRowId'));
		$this->db->where('allowRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('allowances');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'designationRowId' => ucwords($this->input->post('designationRowId'))
	        , 'nightAllow' => $this->input->post('nightAllow')
	        , 'midNightAllow' => $this->input->post('midNightAllow')
	        , 'tourAllow' => $this->input->post('tourAllow')
	        , 'attendanceAllow' => $this->input->post('attendanceAllow')
	        , 'breakfastAllow' => $this->input->post('breakfastAllow')
	        , 'lunchAllow' => $this->input->post('lunchAllow')
		);
		$this->db->where('allowRowId', $this->input->post('globalrowid'));
		$this->db->update('allowances', $data);			
	}

	public function delete()
	{
		$data = array(
		        'deleted' => 'Y',
		        'deletedBy' => $this->session->userRowId

		);
		$this->db->set('deletedStamp', 'NOW()', FALSE);
		$this->db->where('allowRowId', $this->input->post('rowId'));
		$this->db->update('allowances', $data);

		// $this->db->where('townRowId', $this->input->post('rowId'));
		// $this->db->delete('towns');
	}

	// public function getTownList()
	// {
	// 	$this->db->select('towns.*, districts.districtName, states.stateName, countries.countryName');
	// 	$this->db->from('towns');
	// 	$this->db->join('districts','districts.districtRowId = towns.districtRowId');
	// 	$this->db->join('states','states.stateRowId = districts.stateRowId');
	// 	$this->db->join('countries','countries.countryRowId = states.countryRowId');
	// 	$this->db->where('towns.deleted', 'N');
	// 	$this->db->order_by('towns.townName');
	// 	$query = $this->db->get();

	// 	return($query->result_array());
	// }

	// public function getTownListRefresh()
	// {
	// 	$this->db->select('towns.*, districts.districtName, states.stateName, countries.countryName');
	// 	$this->db->from('towns');
	// 	$this->db->join('districts','districts.districtRowId = towns.districtRowId');
	// 	$this->db->join('states','states.stateRowId = districts.stateRowId');
	// 	$this->db->join('countries','countries.countryRowId = states.countryRowId');
	// 	$this->db->where('towns.deleted', 'N');
	// 	$this->db->order_by('towns.townName');
	// 	$query = $this->db->get();

	// 	return($query->result_array());
	// }
}