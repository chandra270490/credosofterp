<?php
class Openingballeaves_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataForReport()
    {
        $this->db->select('employees.*, addressbook.name, users.uid');
        $this->db->from('employees');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.salType', 'M' );
        $this->db->where('employees.userRowId >', 0 );
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->join('users','users.rowid = employees.userRowId');
        $this->db->order_by('name');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function insert()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);


        for ($i=0; $i < $myTableRows; $i++) 
        {
            $data = array(
                'leavesOpBal' => $TableData[$i]['opBal']
            );
            $this->db->where('empRowId', $TableData[$i]['empRowId']);
            $this->db->update('employees', $data);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}