<?php
class Backupdata_model extends CI_Model 
{

        public function __construct()
        {
                $this->load->database('');
        }
        public function fromExcel()
        {
        	$this->load->library('Excel');
			$objPHPExcel = PHPExcel_IOFactory::load('temp1.xls');
			$objPHPExcel->setActiveSheetIndex(0);
			$maxRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			// $this->db->empty_table('productgroups');
			for($r=2; $r<=$maxRow; $r++)
			{
				$data = array(
			        'rowId' => $objPHPExcel->getActiveSheet()->getCell('A'.$r),
			        'name' => $objPHPExcel->getActiveSheet()->getCell('B'.$r),
			        'fname' => $objPHPExcel->getActiveSheet()->getCell('C'.$r),
			        'gender' => $objPHPExcel->getActiveSheet()->getCell('D'.$r),
			        'street' => $objPHPExcel->getActiveSheet()->getCell('E'.$r),
			        'locality' => $objPHPExcel->getActiveSheet()->getCell('F'.$r),
			        'city' => $objPHPExcel->getActiveSheet()->getCell('G'.$r),
			        'mobile1' => $objPHPExcel->getActiveSheet()->getCell('I'.$r),
			        'mobile2' => $objPHPExcel->getActiveSheet()->getCell('H'.$r),
			        'bloodGroup' => $objPHPExcel->getActiveSheet()->getCell('J'.$r),
			        'dataSource' => 'Mahavir Int.',
			        'createdBy' => 1,
			        'authorised' => 'Y'

				);
				$this->db->set('createdStamp', 'NOW()', FALSE);				
				$this->db->insert('donors', $data);
				echo $objPHPExcel->getActiveSheet()->getCell('B'.$r);
			}
			echo $maxRow;


			//// to import products table
   //      	$this->load->library('Excel');
			// $objPHPExcel = PHPExcel_IOFactory::load('temp.xls');
			// $objPHPExcel->setActiveSheetIndex(0);
			// $maxRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			// $this->db->empty_table('products');
			// for($r=2; $r<=$maxRow; $r++)
			// {
			// 	$data = array(
			//         'productrowid' => $objPHPExcel->getActiveSheet()->getCell('A'.$r),
			//         'productname' => $objPHPExcel->getActiveSheet()->getCell('B'.$r),
			//         'pgrowid' => $objPHPExcel->getActiveSheet()->getCell('C'.$r)

			// 	);
			// 	$this->db->insert('products', $data);
			// }
			// echo $maxRow;
        }
}   