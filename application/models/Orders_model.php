<?php
class Orders_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

    public function getReferenceList()
	{
		set_time_limit(0);
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('addressbook.abRowId, addressbook.name');
			$this->db->from('addressbook');
			$this->db->where('addressbook.deleted', 'N');
			$this->db->where('length(addressbook.name)>', 0);
			// $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
			$this->db->order_by('addressbook.name');
			$query = $this->db->get();

			$arr = array();
			$arr["-1"] = '--- SELECT ---';
			foreach ($query->result_array() as $row)
			{
	    		$arr[$row['abRowId']]= $row['name'];
			}
			return $arr;
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('addressbook.abRowId, addressbook.name');
			$this->db->from('addressbook');
			$this->db->where('addressbook.deleted', 'N');
			$this->db->where('length(addressbook.name)>', 0);
			// $this->db->where('addressbook.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('addressbook.createdBy', $abAccessIn);
			$this->db->order_by('addressbook.name');
			$query = $this->db->get();

			$arr = array();
			$arr["-1"] = '--- SELECT ---';
			foreach ($query->result_array() as $row)
			{
	    		$arr[$row['abRowId']]= $row['name'];
			}
			return $arr;
		}
	}

    public function getOrderTypeList()
	{
		set_time_limit(0);
		$this->db->select('ordertypes.orderTypeRowId, ordertypes.orderType');
		$this->db->from('ordertypes');
		$this->db->where('ordertypes.deleted', 'N');
		$this->db->order_by('ordertypes.orderType');
		$query = $this->db->get();

		$arr = array();
		$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['orderTypeRowId']]= $row['orderType'];
		}
		return $arr;
	}

	public function getPreviousDiscount()
    {
    	set_time_limit(0);
		$this->db->select('qpo.discountPer');
		$this->db->from('qpo');
		$this->db->where('qpo.vType', 'O');
		$this->db->where('qpo.deleted', 'N');
		$this->db->where('qpo.partyRowId', $this->input->post('partyRowId'));
		$this->db->where('qpo.orgRowId', $this->session->orgRowId);
		$this->db->order_by('qpo.qpoRowId desc');
		$this->db->limit(2);
		$query = $this->db->get();

		return($query->result_array());    
	}

	public function getProductList()
    {
    	set_time_limit(0);
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('qpo.*, addressbook.name, ordertypes.orderType, ab.name as reference');
			$this->db->from('qpo');
			$this->db->join('parties','parties.partyRowId = qpo.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
			$this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
			$this->db->where('qpo.vType', 'O');
			$this->db->where('qpo.deleted', 'N');
			$this->db->where('qpo.orgRowId', $this->session->orgRowId);
			$this->db->order_by('qpo.qpoRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('qpo.*, addressbook.name, ordertypes.orderType, ab.name as reference');
			$this->db->from('qpo');
			$this->db->join('parties','parties.partyRowId = qpo.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
			$this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
			$this->db->where('qpo.vType', 'O');
			$this->db->where('qpo.deleted', 'N');
			$this->db->where('qpo.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('qpo.createdBy', $abAccessIn);
			$this->db->where_in('qpo.createdBy', $this->session->userRowId);
			$this->db->order_by('qpo.qpoRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('qpo.*, addressbook.name, ordertypes.orderType, ab.name as reference');
			$this->db->from('qpo');
			$this->db->join('parties','parties.partyRowId = qpo.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
			$this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
			$this->db->where('qpo.vType', 'O');
			$this->db->where('qpo.deleted', 'N');
			$this->db->where('qpo.orgRowId', $this->session->orgRowId);
			$this->db->order_by('qpo.qpoRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('qpo.*, addressbook.name, ordertypes.orderType, ab.name as reference');
			$this->db->from('qpo');
			$this->db->join('parties','parties.partyRowId = qpo.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->join('addressbook as ab', 'ab.abRowId = qpo.referenceRowId');
			$this->db->join('ordertypes', 'ordertypes.orderTypeRowId = qpo.orderTypeRowId');
			$this->db->where('qpo.vType', 'O');
			$this->db->where('qpo.deleted', 'N');
			$this->db->where('qpo.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('qpo.createdBy', $abAccessIn);
			$this->db->where_in('qpo.createdBy', $this->session->userRowId);
			$this->db->order_by('qpo.qpoRowId');
			// $this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    
	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_start();

        $this->db->query('LOCK TABLE qpo WRITE, qpodetail WRITE');

		$this->db->select_max('qpoRowId');
		$query = $this->db->get('qpo');
        $row = $query->row_array();

        $current_row = $row['qpoRowId']+1;
		$this->globalInvNo = $current_row;
		
        $this->db->select_max('vNo');
        $this->db->where('vType', 'O');
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('qpo');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;

        if($this->input->post('vDt') == '')
        {
        	$vDt = null;
        }
        else
        {
        	$vDt = date('Y-m-d', strtotime($this->input->post('vDt')));
        }

        if($this->input->post('commitmentDate') == '')
        {
        	$commitmentDate = null;
        }
        else
        {
        	$commitmentDate = date('Y-m-d', strtotime($this->input->post('commitmentDate')));
        }

		$data = array(
	        'qpoRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'vType' => 'O'
	        , 'vNo' => $vNo 
	        , 'vDt' => $vDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'letterNo' => $this->input->post('letterNo')
	        , 'referenceRowId' => $this->input->post('referenceRowId')
	        , 'orderTypeRowId' => $this->input->post('orderTypeRowId')
	        , 'commitmentDate' => $commitmentDate
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'discountPer' => (float)$this->input->post('discountPer')
	        , 'discountAmt' => (float)$this->input->post('discountAmt')
	        , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
	        , 'vatPer' => (float)$this->input->post('vatPer')
	        , 'vatAmt' => (float)$this->input->post('vatAmt')
            , 'sgstPer' => (float)$this->input->post('sgstPer')
            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
            , 'cgstPer' => (float)$this->input->post('cgstPer')
            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
            , 'igstPer' => (float)$this->input->post('igstPer')
            , 'igstAmt' => (float)$this->input->post('igstAmt')
	        , 'net' => (float)$this->input->post('net')
	        , 'advance' => (float)$this->input->post('advance')
	        , 'balance' => (float)$this->input->post('balance')
	        , 'deliveryAddress' => $this->input->post('deliveryAddress')	        
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('qpo', $data);	


		/////Saving Products
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('rowId');
			$query = $this->db->get('qpodetail');
	        $row = $query->row_array();
	        $rowId = $row['rowId']+1;

			$data = array(
			        'rowId' => $rowId
			        , 'qpoRowId' => $current_row
			        , 'productRowId' => $TableData[$i]['productRowId']
			        , 'rate' => (float) $TableData[$i]['productRate']
			        , 'qty' => (float) $TableData[$i]['productQty']
			        , 'pendingQty' => (float) $TableData[$i]['productQty']
			        , 'amt' => (float) $TableData[$i]['productAmt']
			        , 'colourRowId' => $TableData[$i]['colourRowId']
			        , 'remarks' => $TableData[$i]['remarks']
			);
			$this->db->insert('qpodetail', $data);	  
        }
        /////END - Saving Products

        $this->db->query('UNLOCK TABLES');

		$this->db->trans_complete();
        return $this->db->trans_status() === FALSE ? "error" : "Noerror" ;
	}


	public function update()
    {
    	set_time_limit(0);
        $this->db->trans_start();

        $this->db->query('LOCK TABLE qpo WRITE, qpodetail WRITE');

    	if($this->input->post('vDt') == '')
        {
        	$vDt = null;
        }
        else
        {
        	$vDt = date('Y-m-d', strtotime($this->input->post('vDt')));
        }

        if($this->input->post('commitmentDate') == '')
        {
        	$commitmentDate = null;
        }
        else
        {
        	$commitmentDate = date('Y-m-d', strtotime($this->input->post('commitmentDate')));
        }
		$data = array(
	        'vDt' => $vDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'letterNo' => $this->input->post('letterNo')
	        , 'referenceRowId' => $this->input->post('referenceRowId')
	        , 'orderTypeRowId' => $this->input->post('orderTypeRowId')
	        , 'commitmentDate' => $commitmentDate
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'discountPer' => (float)$this->input->post('discountPer')
	        , 'discountAmt' => (float)$this->input->post('discountAmt')
	        , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
	        , 'vatPer' => (float)$this->input->post('vatPer')
	        , 'vatAmt' => (float)$this->input->post('vatAmt')
            , 'sgstPer' => (float)$this->input->post('sgstPer')
            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
            , 'cgstPer' => (float)$this->input->post('cgstPer')
            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
            , 'igstPer' => (float)$this->input->post('igstPer')
            , 'igstAmt' => (float)$this->input->post('igstAmt')
	        , 'net' => (float)$this->input->post('net')
	        , 'advance' => (float)$this->input->post('advance')
	        , 'balance' => (float)$this->input->post('balance')	    
	        , 'deliveryAddress' => $this->input->post('deliveryAddress')    
		);
		$this->db->where('qpoRowId', $this->input->post('globalrowid'));
		$this->db->update('qpo', $data);	

		/////Saving Products
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        // $this->db->where('qpoRowId', $this->input->post('globalrowid'));
        // $this->db->delete('qpodetail');

        for ($i=0; $i < $myTableRows; $i++) 
        {
   //      	$this->db->select_max('rowId');
			// $query = $this->db->get('qpodetail');
	  //       $row = $query->row_array();
	  //       $rowId = $row['rowId']+1;

        	////////// Pending Qty Arrangment
        	$this->db->select('qty, pendingQty');
        	$this->db->where('qpoRowId',  $this->input->post('globalrowid'));
			$this->db->where('productRowId', $TableData[$i]['productRowId']);
			$this->db->where('colourRowId', $TableData[$i]['colourRowId']);
			$query = $this->db->get('qpodetail');
	        $row = $query->row_array();
	        $qty = $row['qty'];
	        $pendingQty = $row['pendingQty'];

	        $qtyDifference = $qty - $TableData[$i]['productQty'];
	        $newPendingQty = $pendingQty - $qtyDifference;
        	//////////END - Pending Qty Arrangment

			$data = array(
			        // 'rowId' => $rowId
			        // , 'qpoRowId' => $this->input->post('globalrowid')
			        // , 'productRowId' => $TableData[$i]['productRowId']
			        'rate' => (float) $TableData[$i]['productRate']
			        , 'qty' => (float) $TableData[$i]['productQty']
			        , 'pendingQty' => (float) $newPendingQty
			        , 'amt' => (float) $TableData[$i]['productAmt']
			        // , 'colourRowId' => $TableData[$i]['colourRowId']
			        , 'remarks' => $TableData[$i]['remarks']
			);
			$this->db->where('qpoRowId',  $this->input->post('globalrowid'));
			$this->db->where('productRowId', $TableData[$i]['productRowId']);
			$this->db->where('colourRowId', $TableData[$i]['colourRowId']);
			$this->db->update('qpodetail', $data);			


        }
        /////END - Saving Products

        $this->db->query('UNLOCK TABLES');
        
        $this->db->trans_complete();
        return $this->db->trans_status() === FALSE ? "error" : "Noerror" ;

	}

	public function checkDependency()
	{
		set_time_limit(0);
		$this->db->select('qpodetail.rowId');
        $this->db->where('qpodetail.qpoRowId', $this->input->post('rowId'));
        $this->db->from('qpodetail');
        $query = $this->db->get();
        $qpoDetailRowId='';
        foreach ($query->result() as $row)
		{
			$qpoDetailRowId = $qpoDetailRowId .$row->rowId.",";
		}
		$qpoDetailRowId = substr($qpoDetailRowId, 0, strlen($qpoDetailRowId)-1);
     	// return $qpoDetailRowId;

     	$this->db->select('despatchdetail.rowId');
        $this->db->where_in('despatchdetail.qpoDetailRowId', $qpoDetailRowId);
        // $this->db->where('cidetail.despatchDetailRowId', $despathDetailRowId);
        $this->db->from('despatchdetail');
        $this->db->join('despatch','despatch.despatchRowId = despatchdetail.despatchRowId AND despatch.deleted="N"');
        // $this->db->where('despatch.deleted','N');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
		{
			return 1;
		}
	}

	public function delete()
	{
		$data = array(
		    'deleted' => 'Y'
		);
		$this->db->where('qpoRowId',  $this->input->post('rowId'));
		$this->db->update('qpo', $data);
	}


	public function getProducts()
    {
    	set_time_limit(0);
        $this->db->select('qpodetail.*, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight, colours.colourName');
        $this->db->where('qpodetail.qpoRowId', $this->input->post('rowid'));
        $this->db->from('qpodetail');
        $this->db->join('products','products.productRowId = qpodetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        // $this->db->join('placements','placements.placementRowId = qpodetail.placementRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());
    }

	public function getInvNo()
    {
        return $this->globalInvNo;
    }

	public function getImageName($rowId)
    {
    	set_time_limit(0);
        $this->db->select('products.productRowId, products.imagePathThumb');
		$this->db->from('products');
		$this->db->where('products.productRowId', $rowId);
		// $this->db->order_by('qpo.qpoRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }

    public function getQpo($rowId)
    {
    	set_time_limit(0);
        $this->db->select('qpo.*, addressbook.name, addressbook.addr, addressbook.pin, prefixtypes.prefixType, towns.townName');
		$this->db->from('qpo');
		$this->db->join('parties','parties.partyRowId = qpo.partyRowId');
		$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('qpo.qpoRowId', $rowId);
		$this->db->order_by('qpo.qpoRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());

    }

   public function getQuotationProducts()
    {
    	set_time_limit(0);
        $this->db->select('qpodetail.*, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight, placements.placement, colours.colourName');
        $this->db->where('qpodetail.qpoRowId', $this->input->post('qpoRowId'));
        $this->db->from('qpodetail');
        $this->db->join('products','products.productRowId = qpodetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->join('placements','placements.placementRowId = qpodetail.placementRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->order_by('rowId');
        $query = $this->db->get();
        return($query->result_array());
    }
    
}