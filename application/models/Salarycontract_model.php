<?php
class Salarycontract_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getEmployee4CheckBox()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.salType', 'C');
        $this->db->where('employees.discontinue', 'N');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        // $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'];
        }
        return $arr;
    }


    public function saveData()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        // $this->db->query('LOCK TABLE qpo WRITE, qpodetail WRITE');

        

        if($this->input->post('dt') == '')
        {
            $dt = null;
        }
        else
        {
            $dt = date('Y-m-d', strtotime($this->input->post('dt')));
        }

        /////Saving salry contract
        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {

            /////updateing calculated Y in move stages
            $data = array(
                        'calculated' => 'Y'
                );
                $this->db->where('empRowId', $TableData[$i]['empRowId']);
                $this->db->where('movestage.dt <=', date('Y-m-d', strtotime($this->input->post('dt'))));
                $this->db->update('movestage', $data);    
            /////END - updateing calculated Y in move stages

            /////updateing calculated Y in Payments
            $data = array(
                        'calculated' => 'Y'
                );
                $this->db->where('empRowId', $TableData[$i]['empRowId']);
                $this->db->update('payments', $data);    
            /////END - updateing calculated Y in Payments

            /////updateing calculated Y in SalaryContract
            $data = array(
                        'calculated' => 'Y'
                );
                $this->db->where('empRowId', $TableData[$i]['empRowId']);
                $this->db->update('salarycontract', $data);    
            /////END - updateing calculated Y in SalaryContract


            /////////// Inserting Current Data
            $this->db->select_max('scRowId');
            $query = $this->db->get('salarycontract');
            $row = $query->row_array();
            $scRowId = $row['scRowId'] + 1;

            $data = array(
                    'scRowId' => $scRowId
                    , 'dt' => $dt
                    , 'orgRowId' => $this->session->orgRowId
                    , 'empRowId' => $TableData[$i]['empRowId']
                    , 'amtCalculated' => (float) $TableData[$i]['amtCalculated']
                    , 'prevDues' => (float) $TableData[$i]['prevDues']
                    , 'alreadyPaid' => (float) $TableData[$i]['alreadyPaid']
                    , 'bal' => (float) $TableData[$i]['bal']
                    , 'payingNow' => $TableData[$i]['payingNow']
                    , 'dueNow' => $TableData[$i]['dueNow']
                    , 'createdBy' => $this->session->userRowId
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('salarycontract', $data);    
        }
        /////END - salry contract



        // $this->db->query('UNLOCK TABLES');

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function getDataForReport()
    {
         set_time_limit(0);
        //////creating tmpsal table
        $this->load->dbforge();
        if($this->db->table_exists('tmpsal'))
        {
            $this->dbforge->drop_table('tmpsal');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'dt' => array(
                                     'type' => 'DATE',
                                ),
                    'productRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'productName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'productWeightage' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,2',
                                     'default' => '0',
                                ),
                    'stageRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'stageWeight' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,2',
                                     'default' => '0',
                                ),
                    'moveQty' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                ),
                    'direction' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '1',
                                ),
                    'remarks' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '255',
                                ),
                    'contractRate' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'amt' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'due' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'paid' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'bal' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                )
            );  
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('tmpsal');

        ///////////////////// loading moveStage data
        // $this->db->select('movestage.*, addressbook.name, employeecontract.contractRate');
        // $this->db->from('movestage');
        // $empRowId = explode(",", $this->input->post('empRowId'));
        // $this->db->where_in('movestage.empRowId', $empRowId );
        // $this->db->where('movestage.calculated', 'N' );
        // $this->db->where('movestage.dt <=', date('Y-m-d', strtotime($this->input->post('dt'))));
        // $this->db->join('employees','employees.empRowId = movestage.empRowId');
        // $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        // $this->db->join('employeecontract','employeecontract.empRowId = movestage.empRowId AND employeecontract.productRowId = movestage.productRowId');
        // $this->db->order_by('productRowId');
        $this->db->select('employees.*, addressbook.name');
        $this->db->from('employees');
        $empRowId = explode(",", $this->input->post('empRowId'));
        $this->db->where_in('employees.empRowId', $empRowId );
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->order_by('name');
        $query = $this->db->get();
        $rowId = 1;
        foreach ($query->result() as $row)
        {
            $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                );
            $this->db->insert('tmpsal', $data);               
            $rowId++;

                         ////// Storing Prev. Dues
            $this->db->select_sum('salarycontract.dueNow');
            $this->db->from('salarycontract');
            $this->db->where('salarycontract.empRowId', $row->empRowId );
            $this->db->where('salarycontract.calculated', 'N' );
            $queryDue = $this->db->get();
            if ($queryDue->num_rows() > 0)
            {
                $rowDue = $queryDue->row_array();
                $due = $rowDue['dueNow'];
                $data = array(
                    'due' => $due
                    );
                $this->db->where('empRowId', $row->empRowId);
                $this->db->update('tmpsal', $data);
            }

             ////// Storing already paid
            $this->db->select_sum('payments.amt');
            $this->db->from('payments');
            $this->db->where('payments.empRowId', $row->empRowId );
            $this->db->where('payments.calculated', 'N' );
            $queryPaid = $this->db->get();
            $rowPaid = $queryPaid->row_array();
            $paid = $rowPaid['amt'];
            $data = array(
                'paid' => $paid
                );
            $this->db->where('empRowId', $row->empRowId);
            $this->db->update('tmpsal', $data);
        }

        $this->db->select('movestage.*, addressbook.name, employeecontract.contractRate');
        // $this->db->select('movestage.*, addressbook.name');
        $this->db->from('movestage');
        $empRowId = explode(",", $this->input->post('empRowId'));
        $this->db->where_in('movestage.empRowId', $empRowId );
        $this->db->where('movestage.calculated', 'N' );
        $this->db->where('movestage.dt <=', date('Y-m-d', strtotime($this->input->post('dt'))));
        $this->db->join('employees','employees.empRowId = movestage.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->join('employeecontract','employeecontract.empRowId = movestage.empRowId AND employeecontract.productRowId = movestage.productRowId');
        $this->db->order_by('productRowId');
        $query = $this->db->get();
        $rowId = 1;
        foreach ($query->result() as $row)
        {
            if( $row->direction == "F" ) ////Fwd Stage movement
            {
                $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'dt' => $row->dt,
                        'productRowId' => $row->productRowId,
                        'stageRowId' => $row->fromStage,
                        'stageWeight' => $row->weightage,
                        'moveQty' => $row->qty,
                        'direction' => $row->direction,
                        'contractRate' => $row->contractRate,
                        'amt' => ($row->qty * $row->contractRate),
                        // 'amt' => ($row->qty *10),
                        'remarks' => 'From Movement Table'
                );
            }
            else if( $row->direction == "B" ) ////Back Stage movement
            {
                $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'dt' => $row->dt,
                        'productRowId' => $row->productRowId,
                        'stageRowId' => $row->toStage,
                        'stageWeight' => $row->weightage,
                        'moveQty' => $row->qty,
                        'direction' => $row->direction,
                        'contractRate' => $row->contractRate,
                        'amt' => ($row->qty * $row->contractRate * -1),
                        // 'amt' => ($row->qty * -10),
                        'remarks' => 'From Movement Table'
                );
            }
            $this->db->insert('tmpsal', $data);               
            $rowId++;

             ////// Storing Prev. Dues
            $this->db->select_sum('salarycontract.dueNow');
            $this->db->from('salarycontract');
            $this->db->where('salarycontract.empRowId', $row->empRowId );
            $this->db->where('salarycontract.calculated', 'N' );
            $queryDue = $this->db->get();
            if ($queryDue->num_rows() > 0)
            {
                $rowDue = $queryDue->row_array();
                $due = $rowDue['dueNow'];
                $data = array(
                    'due' => $due
                    );
                $this->db->where('empRowId', $row->empRowId);
                $this->db->update('tmpsal', $data);
            }

             ////// Storing already paid
            $this->db->select_sum('payments.amt');
            $this->db->from('payments');
            $this->db->where('payments.empRowId', $row->empRowId );
            $this->db->where('payments.calculated', 'N' );
            $queryPaid = $this->db->get();
            $rowPaid = $queryPaid->row_array();
            $paid = $rowPaid['amt'];
            $data = array(
                'paid' => $paid
                );
            $this->db->where('empRowId', $row->empRowId);
            $this->db->update('tmpsal', $data);
        }



        /// now sending data to client
        $this->db->select_sum('tmpsal.amt');
        $this->db->select('tmpsal.empRowId, tmpsal.empName, tmpsal.paid, tmpsal.due');
        $this->db->order_by('tmpsal.empName');
        $this->db->group_by('tmpsal.empRowId');
        $this->db->from('tmpsal');
        $query = $this->db->get();
        return($query->result_array());
    }





    public function getDetail()
    {
         set_time_limit(0);
        //////creating tmpsal table
        $this->load->dbforge();
        if($this->db->table_exists('tmpsal'))
        {
            $this->dbforge->drop_table('tmpsal');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'dt' => array(
                                     'type' => 'DATE',
                                ),
                    'productRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'productName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'productWeightage' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,2',
                                ),
                    'stageRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'stageWeight' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '10,2',
                                ),
                    'moveQty' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'direction' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '1',
                                ),
                    'remarks' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '255',
                                ),
                    'contractRate' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0'
                                ),
                    'amt' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                ),
                    'paid' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                ),
                    'bal' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                )
            );  
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('tmpsal');

        ///////////////////// loading moveStage data
        $this->db->select('movestage.*, addressbook.name, products.productName, products.weightage productWeightage, employeecontract.contractRate');
        // $this->db->select('movestage.*, addressbook.name, products.productName, products.weightage productWeightage');
        $this->db->from('movestage');
        $this->db->where('movestage.empRowId', $this->input->post('globalEmpRowId') );
        $this->db->where('movestage.calculated', 'N' );
        $this->db->where('movestage.dt <=', date('Y-m-d', strtotime($this->input->post('dt'))));
        // $this->db->where('movestage.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->join('products','products.productRowId = movestage.productRowId');
        $this->db->join('employees','employees.empRowId = movestage.empRowId');
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->join('employeecontract','employeecontract.empRowId = movestage.empRowId AND employeecontract.productRowId = movestage.productRowId', 'left');
        $this->db->order_by('productRowId');
        $query = $this->db->get();
        $rowId = 1;
        foreach ($query->result() as $row)
        {
            if($row->contractRate == null)
            {
                // $contractRate = $row->contractRate;
                $contractRate = 0;

            }
            else
            {
                $contractRate = $row->contractRate;
            }
            if( $row->direction == "F" ) ////Fwd Stage movement
            {
                $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'dt' => $row->dt,
                        'productRowId' => $row->productRowId,
                        'productName' => $row->productName,
                        'productWeightage' => $row->productWeightage,
                        'stageRowId' => $row->fromStage,
                        'stageWeight' => $row->weightage,
                        'moveQty' => $row->qty,
                        'direction' => $row->direction,
                        'contractRate' => $contractRate,
                        'amt' => ($row->qty * $contractRate),
                        'remarks' => 'From Movement Table'
                );
            }
            else if( $row->direction == "B" ) ////Back Stage movement
            {
                $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'dt' => $row->dt,
                        'productRowId' => $row->productRowId,
                        'productName' => $row->productName,
                        'productWeightage' => $row->productWeightage,
                        'stageRowId' => $row->toStage,
                        'stageWeight' => $row->weightage,
                        'moveQty' => $row->qty,
                        'direction' => $row->direction,
                        'contractRate' => $contractRate,
                        'amt' => ($row->qty * $contractRate * -1),
                        'remarks' => 'From Movement Table'
                );
            }
            $this->db->insert('tmpsal', $data);               
            $rowId++;

           

        }



        /// now sending data to client
        // $this->db->select_sum('tmpsal.amt');
        $this->db->select('tmpsal.*');
        $this->db->order_by('tmpsal.dt');
        // $this->db->group_by('tmpsal.empRowId');
        $this->db->from('tmpsal');
        $query = $this->db->get();
        return($query->result_array());
    }

}