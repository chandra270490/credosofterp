<?php
class States_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('states.*, countries.countryName');
		$this->db->from('states');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('states.deleted', 'N');
		$this->db->order_by('states.stateRowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('states.*, countries.countryName');
		$this->db->from('states');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('states.deleted', 'N');
		$this->db->order_by('states.stateRowId');
		$query = $this->db->get();

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('stateName');
		$this->db->where('stateName', $this->input->post('stateName'));
		$query = $this->db->get('states');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('stateRowId');
		$query = $this->db->get('states');
        $row = $query->row_array();

        $current_row = $row['stateRowId']+1;

		$data = array(
	        'stateRowId' => $current_row
	        , 'stateName' => ucwords($this->input->post('stateName'))
	        , 'countryRowId' => $this->input->post('countryRowId')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('states', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('stateName');
		$this->db->where('stateName', $this->input->post('stateName'));
		$this->db->where('stateRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('states');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'stateName' => ucwords($this->input->post('stateName'))
	        , 'countryRowId' => $this->input->post('countryRowId')	        
		);
		$this->db->where('stateRowId', $this->input->post('globalrowid'));
		$this->db->update('states', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('stateRowId', $this->input->post('rowId'));
		$this->db->delete('states');
	}

	public function getStateList()
	{
		// $this->db->select('stateRowId, stateName');
		// $this->db->where('deleted', 'N');
		// $this->db->order_by('stateName');
		// $query = $this->db->get('states');
		// return($query->result_array());

		$this->db->select('states.*, countries.countryName');
		$this->db->from('states');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('states.deleted', 'N');
		$this->db->order_by('states.stateName');
		$query = $this->db->get();

		return($query->result_array());

	}
}