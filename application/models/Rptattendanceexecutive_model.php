<?php
class Rptattendanceexecutive_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataForReport()
    {
        if ( $this->session->userRowId == 1 || $this->session->userRowId == 15) ////if admin or AvantikaJi
        {
            $this->db->distinct();
            $this->db->select('attendanceexecutives.userRowId, users.uid');
            $this->db->from('attendanceexecutives');
            $this->db->where('attendanceexecutives.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            $this->db->where('attendanceexecutives.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('users.uid !=', 'admin');
            $this->db->where('attendanceexecutives.orgRowId', $this->session->orgRowId);
            $this->db->join('users', 'users.rowid = attendanceexecutives.userRowId');
            $this->db->order_by('uid');
            $query = $this->db->get();
        }
        else
        {
            $this->db->distinct();
            $this->db->select('attendanceexecutives.userRowId, users.uid');
            $this->db->from('attendanceexecutives');
            $this->db->where('attendanceexecutives.userRowId', $this->session->userRowId); ///Current user
            $this->db->where('attendanceexecutives.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
            $this->db->where('attendanceexecutives.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
            $this->db->where('users.uid !=', 'admin');
            $this->db->where('attendanceexecutives.orgRowId', $this->session->orgRowId);
            $this->db->join('users', 'users.rowid = attendanceexecutives.userRowId');
            $this->db->order_by('uid');
            $query = $this->db->get();
        }
        // return($query->result_array());

        //////creating tmpatt table
        $this->load->dbforge();
        if($this->db->table_exists('tmpatt'))
        {
            $this->dbforge->drop_table('tmpatt');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'userRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'userName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                )
            );  
        $this->dbforge->add_field($fields);


        $begin = new DateTime( $this->input->post('dtFrom') );
        $end = new DateTime( $this->input->post('dtTo') );
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $totalDays = 0;
        foreach ( $period as $dt )
        {
          $f = $dt->format( "Y-m-d" );
          $fields = array(
                        'd-'.$f => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => '100', 
                                          )
                                    );
                    $this->dbforge->add_field($fields);
                    $totalDays++;
        }


        $this->dbforge->create_table('tmpatt');

        $rowId = 1;
        foreach ($query->result() as $row)
        {

            $data = array(
                        'rowId' => $rowId,
                        'userRowId' => $row->userRowId,
                        'userName' => $row->uid
                    );
            $this->db->insert('tmpatt', $data);               

            //// fetching duty
            foreach ( $period as $dt )
            {
                $f = $dt->format( "Y-m-d" );
                $this->db->select('*');
                $this->db->from('attendanceexecutives');
                $this->db->where('attendanceexecutives.userRowId', $row->userRowId );
                $this->db->where('attendanceexecutives.orgRowId', $this->session->orgRowId);
                $this->db->where('attendanceexecutives.dt', $f);
                // $this->db->where('attendanceexecutives.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                $queryDuty = $this->db->get();        
                if ($queryDuty->num_rows() > 0)
                {
                    $rowDuty = $queryDuty->row_array();
                    $duty = $rowDuty['attendance'] ;
                }    
                else
                {
                    // $duty = 'AB';
                    $duty = 'NA';
                }
                $data = array(
                        'd-'.$f => $duty
                    );
                $this->db->where('userRowId', $row->userRowId );
                $this->db->update('tmpatt', $data);
            }

            $rowId++;
        }

        ///////// returning data
        $this->db->select('tmpatt.*');
        $this->db->order_by('tmpatt.userName');
        $this->db->from('tmpatt');
        $query = $this->db->get();
        return($query->result_array());
    }

}