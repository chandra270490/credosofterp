<?php
class Rptinvoice_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getPartyList()
    {
        $this->db->select('addressbook.name, parties.partyRowId');
        $this->db->from('addressbook');
        $this->db->join('parties','parties.abRowId = addressbook.abRowId');
        $this->db->where('parties.deleted', 'N');
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- ALL ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['partyRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataForReport()
    {
        if( $this->input->post('partyRowId') == "-1" )
        {
             $this->db->select('ci.*, addressbook.name, users.uid');
             $this->db->from('ci');
             $this->db->join('parties','parties.partyRowId = ci.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('users','users.rowid = ci.createdBy');
             $this->db->group_start();
                  $this->db->or_where('ci.vType', 'I');
                  $this->db->or_where('ci.vType', 'R');
             $this->db->group_end();
             $this->db->where('ci.ciDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('ci.ciDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('ci.deleted', 'N');
             $this->db->where('ci.orgRowId', $this->session->orgRowId);
             $this->db->order_by('ci.ciRowId');
             $query = $this->db->get();
             return($query->result_array());
        }
        else
        {
             $this->db->select('ci.*, addressbook.name, users.uid');
             $this->db->from('ci');
             $this->db->join('parties','parties.partyRowId = ci.partyRowId');
             $this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
             $this->db->join('users','users.rowid = ci.createdBy');
             $this->db->group_start();
                  $this->db->or_where('ci.vType', 'I');
                  $this->db->or_where('ci.vType', 'R');
             $this->db->group_end();
             $this->db->where('ci.partyRowId', $this->input->post('partyRowId'));
             $this->db->where('ci.ciDt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
             $this->db->where('ci.ciDt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
             $this->db->where('ci.deleted', 'N');
             $this->db->where('ci.orgRowId', $this->session->orgRowId);
             
             $this->db->order_by('ci.ciRowId');
             $query = $this->db->get();
             return($query->result_array());
        } 
    }



    public function getProducts()
    {   //, colours.colourName
        $this->db->select('cidetail.*, ordertypes.orderType, qpo.vType, qpo.vNo, qpo.vDt, qpo.commitmentDate, despatch.despatchRowId, despatch.despatchDt, qpodetail.remarks, colours.colourName, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
        $this->db->where('cidetail.ciRowId', $this->input->post('rowid'));
        $this->db->from('cidetail');
        $this->db->join('despatchdetail','despatchdetail.rowId = cidetail.despatchDetailRowId');
        $this->db->join('despatch','despatch.despatchRowId = despatchdetail.despatchRowId');
        $this->db->join('qpodetail','qpodetail.rowId = despatchdetail.qpoDetailRowId');
        $this->db->join('qpo','qpo.qpoRowId = qpodetail.qpoRowId');
        $this->db->join('colours','colours.colourRowId = qpodetail.colourRowId');
        $this->db->join('ordertypes','ordertypes.orderTypeRowId = qpo.orderTypeRowId');
        $this->db->join('products','products.productRowId = despatchdetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        // $this->db->join('colours','colours.colourRowId = cidetail.colourRowId');
        // $this->db->order_by('ciDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }
}