<?php
class Leaveapproval_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

   
	public function getUsers()
	{
		$this->db->distinct();
		$this->db->select('leaveapplication.leaveApplicationRowId, users.uid');
		$this->db->from('leaveapplication');
		$this->db->join('users', 'users.rowid = leaveapplication.userRowId');
		$this->db->where('leaveapplication.orgRowId', $this->session->orgRowId);
		$this->db->where('leaveapplication.approved', 'N');
		$this->db->order_by('uid');
		// $this->db->limit(5);
		$query = $this->db->get();

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['leaveApplicationRowId']]= $row['uid'];
		}
		return $arr;

	}
	
	public function getDetail()
    {
        $this->db->select('leaveapplication.*');
        $this->db->where('leaveApplicationRowId',  $this->input->post('leaveApplicationRowId'));
        $this->db->from('leaveapplication');
        $query = $this->db->get();
        return($query->result_array());
    }

	public function getLastApproved($userRowId)
    {
        $this->db->select('leaveapplication.*, users.uid');
        $this->db->where('userRowId',  $userRowId);
        $this->db->where('leaveapplication.approved', 'Y');
        $this->db->from('leaveapplication');
        $this->db->join('users', 'users.rowid = leaveapplication.userRowId');
        $this->db->order_by('leaveapplication.leaveApplicationRowId desc');
        $query = $this->db->get();
        return($query->result_array());
    }



	public function approve()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$dt = date('Y-m-d', strtotime($this->input->post('dt')));
    	$data = array(
				 'approvalDt' => $dt
		        , 'approved' => 'Y'
		        , 'approvalBy' => $this->session->userRowId
			);

		$this->db->where('leaveApplicationRowId', $this->input->post('leaveApplicationRowId'));
		$this->db->update('leaveapplication', $data);	


		///////////// Marking Attendance
		$this->db->select('*');
		$this->db->where('leaveApplicationRowId', $this->input->post('leaveApplicationRowId'));
		$query = $this->db->get('leaveapplication');
        $row = $query->row_array();

        $begin = new DateTime( $row['dtFrom'] );
        $end = new DateTime( $row['dtTo'] );
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $totalDays = 0;
        foreach ( $period as $dt )
        {
          	$f = $dt->format( "Y-m-d" );
	        
	        ///Agar already marked h to purani att delete // 08 june 2019
	        $this->db->where('orgRowId', $this->session->orgRowId);
	        $this->db->where('dt', $f);
	        $this->db->where('userRowId', $row['userRowId']);
			$this->db->delete('attendanceexecutives');
	        ///END - Agar already marked h to purani att delete

			$this->db->select_max('aeRowId');
			$queryA = $this->db->get('attendanceexecutives');
	        $rowA = $queryA->row_array();
	        $aeRowId = $rowA['aeRowId']+1;

	        $att = "LE";


	        $data = array(
				        'aeRowId' => $aeRowId
				        , 'refRowId' => $this->input->post('leaveApplicationRowId')
				        , 'orgRowId' => $this->session->orgRowId
				        , 'dt' => $f
				        , 'userRowId' => $row['userRowId']
				        , 'attendance' => $att
				        , 'createdBy' => $this->session->userRowId
					);
	        $this->db->set('createdStamp', 'NOW()', FALSE);
			$this->db->insert('attendanceexecutives', $data);
		}
		///////////// END - Marking Attendance
		
		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}


	public function reject()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

		$dt = date('Y-m-d', strtotime($this->input->post('dt')));
		

    	$data = array(
				 'approvalDt' => $dt
		        , 'approved' => 'N'
		        , 'rejectReason' => $this->input->post('rejectReason')
		        , 'approvalBy' => $this->session->userRowId
			);

		$this->db->where('leaveApplicationRowId', $this->input->post('leaveApplicationRowId'));
		$this->db->update('leaveapplication', $data);		

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }	
	}
	// public function delete()
	// {
	// 	$data = array(
	// 	        'deleted' => 'Y',
	// 	        'deletedBy' => $this->session->userRowId

	// 	);
	// 	$this->db->set('deletedStamp', 'NOW()', FALSE);
	// 	$this->db->where('dsrRowId', $this->input->post('rowId'));
	// 	$this->db->update('leaveapplication', $data);

	// }


}