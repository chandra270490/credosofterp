<?php
class Holidays_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('holidayDt');
		// $this->db->limit(5);
		$query = $this->db->get('holidays');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('holidayRowId');
		$query = $this->db->get('holidays');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
    	$holidayDt = date('Y-m-d', strtotime($this->input->post('holidayDt')));
		$this->db->select('holidayName');
		$this->db->where('holidayDt', $holidayDt);
		$query = $this->db->get('holidays');
		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
    	$holidayDt = date('Y-m-d', strtotime($this->input->post('holidayDt')));
		$this->db->select_max('holidayRowId');
		$query = $this->db->get('holidays');
        $row = $query->row_array();

        $current_row = $row['holidayRowId']+1;

		$data = array(
	        'holidayRowId' => $current_row
            , 'holidayDt' => $holidayDt
	        , 'holidayName' => ucwords($this->input->post('holidayName'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('holidays', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	$holidayDt = date('Y-m-d', strtotime($this->input->post('holidayDt')));
		$this->db->select('holidayName');
		$this->db->where('holidayDt', $holidayDt);
		$this->db->where('holidayRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('holidays');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
    	$holidayDt = date('Y-m-d', strtotime($this->input->post('holidayDt')));

		$data = array(
	        'holidayDt' => $holidayDt
	        , 'holidayName' => ucwords($this->input->post('holidayName'))
		);
		$this->db->where('holidayRowId', $this->input->post('globalrowid'));
		$this->db->update('holidays', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('holidayRowId', $this->input->post('rowId'));
		$this->db->delete('holidays');
	}
}