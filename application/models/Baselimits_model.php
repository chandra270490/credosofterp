<?php
class Baselimits_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getBaseLimits()
	{
		$this->db->select('baselimits.*');
		$this->db->from('baselimits');
		$query = $this->db->get();
		return($query->result_array());
	}



	public function update()
    {
    	$this->db->select('*');
		$query = $this->db->get('baselimits');
		if ($query->num_rows() > 0)
		{
			$data = array(
		        'pfBase' => (float)$this->input->post('pfBase')
		        , 'pfEmployee' => (float)$this->input->post('pfEmployee')	        
		        , 'pfEmployer' => (float)$this->input->post('pfEmployer')
		        , 'esiBase' => (float)$this->input->post('esiBase')
		        , 'esiEmployee' => (float)$this->input->post('esiEmployee')
		        , 'esiEmployer' => (float)$this->input->post('esiEmployer')
			);
			$this->db->update('baselimits', $data);			
		}
		else
		{
			$data = array(
		        'pfBase' => (float)$this->input->post('pfBase')
		        , 'pfEmployee' => (float)$this->input->post('pfEmployee')	        
		        , 'pfEmployer' => (float)$this->input->post('pfEmployer')
		        , 'esiBase' => (float)$this->input->post('esiBase')
		        , 'esiEmployee' => (float)$this->input->post('esiEmployee')
		        , 'esiEmployer' => (float)$this->input->post('esiEmployer')
			);
			$this->db->insert('baselimits', $data);	
		}
	}

}