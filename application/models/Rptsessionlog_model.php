<?php
class Rptsessionlog_model extends CI_Model {

	public function __construct()
	{
	        $this->load->database('');
	}


	public function getData()
	{
		$this->db->select('sessionlog.*, users.uid');
		$this->db->where('CAST(sessionlog.loginstamp AS DATE) between STR_TO_DATE("'.$this->input->post('dtFrom').'","%d-%M-%Y") and STR_TO_DATE("'.$this->input->post('dtTo').'","%d-%M-%Y")');
		$this->db->from('sessionlog');
		$this->db->join('users', 'users.rowid = sessionlog.userrowid');
		$query = $this->db->get();
		return($query->result_array());
	}
}