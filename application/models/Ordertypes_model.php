<?php
class Ordertypes_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('orderTypeRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('ordertypes');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('orderTypeRowId');
		$query = $this->db->get('ordertypes');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('orderType');
		$this->db->where('orderType', $this->input->post('orderType'));
		$query = $this->db->get('ordertypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('orderTypeRowId');
		$query = $this->db->get('ordertypes');
        $row = $query->row_array();

        $current_row = $row['orderTypeRowId']+1;

		$data = array(
	        'orderTypeRowId' => $current_row
	        , 'orderType' => ucwords($this->input->post('orderType'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('ordertypes', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('orderType');
		$this->db->where('orderType', $this->input->post('orderType'));
		$this->db->where('orderTypeRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('ordertypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'orderType' => ucwords($this->input->post('orderType'))
		);
		$this->db->where('orderTypeRowId', $this->input->post('globalrowid'));
		$this->db->update('ordertypes', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('orderTypeRowId', $this->input->post('rowId'));
		$this->db->delete('ordertypes');
	}


}