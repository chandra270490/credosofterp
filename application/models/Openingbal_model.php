<?php
class Openingbal_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function totalStages()
    {
        $this->db->select('stageRowId');
        $this->db->where('deleted', 'N');
        $query = $this->db->get('stages');
        return($query->num_rows());
        // return($query->result_array());
    }

    public function getStages()
    {
        $this->db->select('*');
        $this->db->where('deleted', 'N');
        $this->db->order_by('odr');
        $query = $this->db->get('stages');
        return($query->result_array());
    }

    public function getDataForProductCategories()
    {
        $this->db->select('productCategoryRowId, productCategory');
        $this->db->where('deleted', 'N');
        $this->db->order_by('productCategory');
        $query = $this->db->get('productcategories');

        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['productCategoryRowId']]= $row['productCategory'];
        }

        return $arr;
    }


    public function loadData()
    {
        set_time_limit(0);
        /////////////////creating new tmp table
        $this->load->dbforge();
        if($this->db->table_exists('openingbaltmp'))
        {
            $this->dbforge->drop_table('openingbaltmp');
        }
        $fields = array(
                    'productRowId' => array(
                                             'type' => 'INT',
                                             'constraint' => 5 
                                             
                                      ),
                    'productName' => array(
                                             'type' => 'VARCHAR',
                                             'constraint' => '50',
                                      ),
                    'productCategoryRowId' => array(
                                             'type' => 'INT',
                                             'constraint' => 5, 
                                             'unsigned' => False
                                      ),
                    'productCategory' => array(
                                             'type' => 'VARCHAR',
                                             'constraint' => '50',
                                      )

            );  

        $this->dbforge->add_field($fields);     
        $this->db->select('stageRowId, stageName');
        $this->db->where('deleted', 'N');
        $this->db->order_by('odr');
        $query = $this->db->get('stages');

        $stageNames = array();
        foreach ($query->result() as $row)
        {
            array_push($stageNames, $row->stageName);
            $fields = array(
                        'F'.$row->stageRowId => array(
                                                 'type' => 'INT',
                                                 'default' => 0,
                                          )
                );  

            $this->dbforge->add_field($fields);     
        }
        $this->dbforge->create_table('openingbaltmp');
        /////////////////  END - creating new tmp table


        ////// Creating array of stageRowId (like 1,2,3,4)
        $fields = $this->db->list_fields('openingbaltmp');
        $totalStages = count($fields)-4;    //coz intial 4 fields are fix
        $stagesArray = array('');
        foreach ($fields as $field)
        {
            array_push($stagesArray, substr($field, 1, strlen($field)));
        }
        array_shift($stagesArray);  //removing first 4 fields (productrowid,name,group, grouprowid)
        array_shift($stagesArray);
        array_shift($stagesArray);
        array_shift($stagesArray);
        array_shift($stagesArray);

        /////////1st row for stageRowId
            $data = array(
                    'productRowId' => 0,
                    'productName' => '    x',
                    'productCategoryRowId' => 0,
                    'productCategory' => 'y'
            );
            $this->db->insert('openingbaltmp', $data);               

            for($i=0; $i<count($stagesArray); $i++)
            {

                $data = array(
                    'F'.$stagesArray[$i] => $stagesArray[$i]
                );
                $this->db->where('productRowId', 0);
                $this->db->update('openingbaltmp', $data);
            }   
        ///////// END - 1st row for stageRowId     

        ///////////////////// loading products in tmp table
        $this->db->select('products.*, productcategories.productCategory');
        $this->db->where('products.deleted', 'N');
        $this->db->where('products.productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->join('productcategories', 'productcategories.productCategoryRowId = products.productCategoryRowId');
        $this->db->order_by('productName');
        $query = $this->db->get('products');
        foreach ($query->result() as $row)
        {
            $data = array(
                    'productRowId' => $row->productRowId,
                    'productName' => $row->productName,
                    'productCategoryRowId' => $row->productCategoryRowId,
                    'productCategory' => $row->productCategory,
            );
            $this->db->insert('openingbaltmp', $data);               

            for($i=0; $i<count($stagesArray); $i++)
            {
                // echo 'F'.$priceTypesArray[$i];
                $this->db->select('opQty');
                $this->db->where('productRowId', $row->productRowId);
                $this->db->where('stageRowid', $stagesArray[$i]);
                $query1 = $this->db->get('openingbal');
                if ($query1->num_rows() > 0)
                {
                    $row1 = $query1->row(); 
                    $opQty = $row1->opQty;
                }
                else
                {
                    $opQty=0;
                }

                $data = array(
                    'F'.$stagesArray[$i] => $opQty
                );
                $this->db->where('productRowId', $row->productRowId);
                $this->db->update('openingbaltmp', $data);
            }
        }
        ///////////////////// END -loading products in tmp table


        ///////////////returning data to controller
        $this->db->select('*');
        $this->db->order_by('productName');
        $query = $this->db->get('openingbaltmp');
        return($query->result_array());
    }

    public function saveChanges()
    {
        set_time_limit(0);
        $this->db->trans_begin();
        $this->db->query('LOCK TABLE openingbal WRITE, stages WRITE');

        $TableData = $this->input->post('TableData');
        // $TableData = stripcslashes($TableData);
        // $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);
        $tmp = $this->totalStages()+5;
        // echo $myTableRows;
        for ($i=1; $i < $myTableRows; $i++) 
        {
            $this->db->where('productRowId', $TableData[$i]['1']);
            $this->db->delete('openingbal');
            for ($j=5; $j < $tmp; $j++) 
            {

                $this->db->select_max('rowId');
                $query = $this->db->get('openingbal');
                $row = $query->row_array();
                $current_row = $row['rowId']+1;

                $data = array(
                    'rowId' => $current_row
                    , 'productRowId' => $TableData[$i][1]
                    , 'stageRowId' => $TableData[0][$j]
                    , 'opQty' => $TableData[$i][$j]
                    , 'avlQty' => $TableData[$i][$j]
                    , 'orgRowId' => $this->session->orgRowId
                );
                $this->db->set('createdStamp', 'NOW()', FALSE);
                $this->db->insert('openingbal', $data);  
            }  
        }

        $this->db->query('UNLOCK TABLES');
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    // public function getProductList()
    // {
    //     $this->db->select('productRowId, productName, productcategories.productCategory');
    //     $this->db->where('products.deleted', 'N');
    //     $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
    //     $this->db->order_by('productName');
    //     $query = $this->db->get('products');

    //     $arr = array();
    //     $arr["-1"] = '--- Select ---';
    //     foreach ($query->result_array() as $row)
    //     {
    //         $arr[$row['productRowId']] = $row['productName'] . '  ---  ['. $row['productCategory'] . ' ]';
    //     }

    //     return $arr;
    // }

    // public function getData()
    // {
    //     //////creating tmp table
    //     $this->load->dbforge();
    //     if($this->db->table_exists('tmp'))
    //     {
    //         $this->dbforge->drop_table('tmp');
    //     }
    //     $fields = array(
    //                 'productRowId' => array(
    //                                  'type' => 'INT',
    //                                  'constraint' => '5',
                                             
    //                             ),
    //                 'stageRowId' => array(
    //                                  'type' => 'INT',
    //                                  'constraint' => '5',
                                             
    //                             ),
    //                 'stageName' => array(
    //                                  'type' => 'VARCHAR',
    //                                  'constraint' => '40',
                                             
    //                             ),
    //                 'opQty' => array(
    //                                  'type' => 'INT',
    //                                  'constraint' => '5',
    //                                  'default' => 0
    //                             )

    //         );  
    //     $this->dbforge->add_field($fields);
    //     $this->dbforge->create_table('tmp');
    //     ////// END - creating tmp table

    //     //// storing opBal 0 for all stages
    //     $this->db->select('stageRowId, stageName');
    //     $this->db->where('deleted', 'N');
    //     $this->db->where('stages.orgRowId', $this->session->orgRowId);
    //     $this->db->order_by('stageRowId');
    //     $query = $this->db->get('stages');
    //     foreach ($query->result() as $row)
    //     {
    //         $data = array(
    //             'productRowId' => $this->input->post('productRowId')
    //             , 'stageRowId' => $row->stageRowId
    //             , 'stageName' => $row->stageName
    //             , 'opQty' => 0
    //             );
    //         $this->db->insert('tmp', $data);    
    //     }
    //     ////END - storing contractRate 0 for all products

    //     //// Now setting already saved opBal
    //     $this->db->select('tmp.*');
    //     $this->db->order_by('stageRowId');
    //     $this->db->from('tmp');
    //     $query = $this->db->get();
    //     foreach ($query->result() as $row)
    //     {
    //         $this->db->select('opQty');
    //         $this->db->where('productRowId',  $this->input->post('productRowId'));
    //         $this->db->where('stageRowId', $row->stageRowId);
    //         $this->db->from('openingbal');
    //         $query1 = $this->db->get();
    //         if ($query1->num_rows() > 0)
    //         {
    //             $row1 = $query1->row(); 
                
    //             $data = array(
    //                 'opQty' => $row1->opQty
    //                 );
    //             $this->db->where('productRowId', $this->input->post('productRowId'));
    //             $this->db->where('stageRowId', $row->stageRowId);
    //             $this->db->update('tmp', $data);   
    //         }
    //     }
    //     //// END - Now setting already saved opBal

    //     /// now sending data to client
    //     $this->db->select('tmp.*');
    //     $this->db->order_by('stageRowId');
    //     $this->db->from('tmp');
    //     $query = $this->db->get();

    //     return($query->result_array());
    // }

    // public function saveChanges()
    // {
    //     $this->db->where('productRowId', $this->input->post('productRowId'));
    //     $this->db->delete('openingbal');


    //     $TableData = $this->input->post('TableData');
    //     $TableData = stripcslashes($TableData);
    //     $TableData = json_decode($TableData,TRUE);
    //     $myTableRows = count($TableData);
    //     for ($i=0; $i < $myTableRows; $i++) 
    //     {
    //         $this->db->select_max('rowId');
    //         $query = $this->db->get('openingbal');
    //         $row = $query->row_array();

    //         $current_row = $row['rowId']+1;

    //         $data = array(
    //             'rowId' => $current_row
    //             , 'productRowId' => $this->input->post('productRowId')
    //             , 'stageRowId' => $TableData[$i]['stageRowId']
    //             , 'opQty' => $TableData[$i]['opQty']
    //             , 'avlQty' => $TableData[$i]['opQty']
    //             , 'orgRowId' => $this->session->orgRowId
    //         );
    //         $this->db->insert('openingbal', $data);    
    //     }

    // }

}