<?php
class Salesreturn_model extends CI_Model 
{
	public $globalInvNo = 0;
    public function __construct()
    {
            $this->load->database('');
    }

	public function getProductList()
    {
        $this->db->select('*');
        $this->db->from('products');
        // $this->db->where('states.staterowid', 1);
        $this->db->where('productCategoryRowId', $this->input->post('productCategoryRowId'));
        $this->db->where('deleted', 'N');
        $this->db->order_by('productName');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('ci.*, addressbook.name');
			$this->db->from('ci');
			$this->db->join('parties','parties.partyRowId = ci.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('ci.vType', 'R');
			$this->db->where('ci.deleted', 'N');
			$this->db->where('ci.orgRowId', $this->session->orgRowId);
			$this->db->order_by('ci.ciRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('ci.*, addressbook.name');
			$this->db->from('ci');
			$this->db->join('parties','parties.partyRowId = ci.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('ci.vType', 'R');
			$this->db->where('ci.deleted', 'N');
			$this->db->where('ci.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('ci.createdBy', $abAccessIn);
			$this->db->where_in('ci.createdBy', $this->session->userRowId);
			$this->db->order_by('ci.ciRowId desc');
			$this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    public function getDataAll()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('ci.*, addressbook.name');
			$this->db->from('ci');
			$this->db->join('parties','parties.partyRowId = ci.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('ci.vType', 'R');
			$this->db->where('ci.deleted', 'N');
			$this->db->where('ci.orgRowId', $this->session->orgRowId);
			$this->db->order_by('ci.ciRowId');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('ci.*, addressbook.name');
			$this->db->from('ci');
			$this->db->join('parties','parties.partyRowId = ci.partyRowId');
			$this->db->join('addressbook', 'addressbook.abRowId = parties.abRowId');
			$this->db->where('ci.vType', 'R');
			$this->db->where('ci.deleted', 'N');
			$this->db->where('ci.orgRowId', $this->session->orgRowId);
			// $abAccessIn = explode(",", $this->session->abAccessIn);
			// $this->db->where_in('ci.createdBy', $abAccessIn);
			$this->db->where_in('ci.createdBy', $this->session->userRowId);
			$this->db->order_by('ci.ciRowId');
			// $this->db->limit(5);
			$query = $this->db->get();
			return($query->result_array());
		}
	}

    public function getOrderTypeList()
	{
		$this->db->select('ordertypes.orderTypeRowId, ordertypes.orderType');
		$this->db->from('ordertypes');
		$this->db->where('ordertypes.deleted', 'N');
		$this->db->order_by('ordertypes.orderType');
		$query = $this->db->get();

		$arr = array();
		$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['orderTypeRowId']]= $row['orderType'];
		}
		return $arr;
	}

	public function getStages()
    {
        $this->db->select('stages.*');
        $this->db->from('stages');
        $this->db->where('stages.deleted', 'N');
        $this->db->where('stages.orgRowId', $this->session->orgRowId);
        $this->db->order_by('stages.odr');
        $query = $this->db->get();

        return($query->result_array());
    }

	public function insert()
    {
    	set_time_limit(0);
        $this->db->trans_begin();

        $this->db->query('LOCK TABLE ci WRITE, cidetail WRITE, despatchdetail WRITE, openingbal WRITE, ledgerp WRITE');

		$this->db->select_max('ciRowId');
		$query = $this->db->get('ci');
        $row = $query->row_array();

        $current_row = $row['ciRowId']+1;
		$this->globalInvNo = $current_row;
		
		$this->db->select_max('vNo');
        $this->db->where('vType', 'R');
        $this->db->where('orgRowId', $this->session->orgRowId);
		$query = $this->db->get('ci');
        $row = $query->row_array();
        $vNo = $row['vNo']+1;

        $ciDt = date('Y-m-d', strtotime($this->input->post('vDt')));

		$data = array(
	        'ciRowId' => $current_row
	        , 'orgRowId' => $this->session->orgRowId
	        , 'vType' => 'R'
	        , 'vNo' => $vNo 
	        , 'ciDt' => $ciDt
	        , 'partyRowId' => $this->input->post('partyRowId')
	        , 'returnType' => $this->input->post('returnType')
	        , 'totalQty' => (float)$this->input->post('totalQty')
	        , 'totalAmt' => (float)$this->input->post('totalAmt')
	        , 'discountPer' => (float)$this->input->post('discountPer')
	        , 'discountAmt' => (float)$this->input->post('discountAmt')
	        , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
	        , 'vatPer' => (float)$this->input->post('vatPer')
	        , 'vatAmt' => (float)$this->input->post('vatAmt')
            , 'sgstPer' => (float)$this->input->post('sgstPer')
            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
            , 'cgstPer' => (float)$this->input->post('cgstPer')
            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
            , 'igstPer' => (float)$this->input->post('igstPer')
            , 'igstAmt' => (float)$this->input->post('igstAmt')
	        , 'net' => (float)$this->input->post('net')
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('ci', $data);	


		/////Saving ciDetail
		$TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        // echo $myTableRows;
        for ($i=0; $i < $myTableRows; $i++) 
        {
        	$this->db->select_max('ciDetailRowId');
			$query = $this->db->get('cidetail');
	        $row = $query->row_array();
	        $ciDetailRowId = $row['ciDetailRowId']+1;

			$data = array(
			        'ciDetailRowId' => $ciDetailRowId
			        , 'ciRowId' => $current_row
			        , 'rate' => (float) $TableData[$i]['rate']
			        , 'qty' => (float) $TableData[$i]['qty']
			        , 'amt' => (float) $TableData[$i]['amt']
			        , 'colourRowId' => (float) $TableData[$i]['colourRowId']
			        , 'stageRowId' => (float) $TableData[$i]['stageRowId']
			        , 'productRowId' => (float) $TableData[$i]['productRowId']
			        , 'remarks' => $TableData[$i]['remarks']
			);
			$this->db->insert('cidetail', $data);	 
			///////////////////////////////////////////////////////////////////////////Updating in STAGE
			if( $TableData[$i]['stageRowId'] == 26 )
		    {
		        $this->db->select('avlQty');
		        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
		        $this->db->where('colourRowId',  (float) $TableData[$i]['colourRowId']);
		        $this->db->where('stageRowId',  $TableData[$i]['stageRowId']);
		        $query = $this->db->get('openingbal');
		        if($query->num_rows() > 0)
		        {
			        $row = $query->row_array();
			        $abhiKiQty = $row['avlQty'];
			        $data = array(
			            'avlQty' => $abhiKiQty + (float) $TableData[$i]['qty']
			        );
			        $this->db->where('productRowId',  (float) $TableData[$i]['productRowId']);
			        $this->db->where('colourRowId',  (float) $TableData[$i]['colourRowId']);
			        $this->db->where('stageRowId',  (float) $TableData[$i]['stageRowId']);
			        $this->db->where('orgRowId',  $this->session->orgRowId);
			        $this->db->update('openingbal', $data); 
			    }
			    else  /// agar ye wala colour already nahi h to naya rec insert karenge
			    {
					$this->db->select_max('rowId');
	                $query = $this->db->get('openingbal');
	                $row = $query->row_array();
	                $opBalRowId = $row['rowId']+1;
	                 $data = array(
	                    'rowId' => $opBalRowId
	                    , 'productRowId' => $TableData[$i]['productRowId']
	                    , 'dt' => $ciDt
	                    , 'stageRowId' => 26
	                    , 'opQty' => 0
	                    , 'avlQty' => $TableData[$i]['qty']
	                    , 'orgRowId' => $this->session->orgRowId
	                    , 'colourRowId' => $TableData[$i]['colourRowId']
	                );
	                $this->db->set('createdStamp', 'NOW()', FALSE);
	                $this->db->insert('openingbal', $data); 
			    }
		    }
		    else /// Agar desp stage k alawa koi stage h
		    {
		    	$this->db->select('avlQty');
		        $this->db->where('productRowId',  $TableData[$i]['productRowId']);
		        // $this->db->where('colourRowId',  (float) $TableData[$i]['colourRowId']);
		        $this->db->where('stageRowId',  $TableData[$i]['stageRowId']);
		        $query = $this->db->get('openingbal');
		        if($query->num_rows() > 0)
		        {
			        $row = $query->row_array();
			        $abhiKiQty = $row['avlQty'];
			        $data = array(
			            'avlQty' => $abhiKiQty + (float) $TableData[$i]['qty']
			        );
			        $this->db->where('productRowId',  (float) $TableData[$i]['productRowId']);
			        // $this->db->where('colourRowId', (float) $TableData[$i]['colourRowId']);
			        $this->db->where('stageRowId',  (float) $TableData[$i]['stageRowId']);
			        $this->db->where('orgRowId',  $this->session->orgRowId);
			        $this->db->update('openingbal', $data); 
			    }
		    }

		    // ////////////inserting in ledgerP////////////19 Nov 2018
		    if ($TableData[$i]['stageRowId'] == -4)
		    {
	            $this->db->select_max('ledgerPRowId');
	            $query = $this->db->get('ledgerp');
	            $row = $query->row_array();
	            $ledgerPRowId = $row['ledgerPRowId']+1;

	            $data = array(
	                'ledgerPRowId' => $ledgerPRowId
	                , 'orgRowId' => $this->session->orgRowId
	                , 'vType' => 'R'
	                , 'vNo' => $current_row  /// storing ciRowId, NOT vNo (warna deletion m dikkat aayegi)
	                , 'dt' => $ciDt
	                , 'productRowId' => $TableData[$i]['productRowId']
	                , 'receive' => (float)$TableData[$i]['qty']
	                , 'receiveBal' => (float)$TableData[$i]['qty']
	                , 'createdBy' => $this->session->userRowId
	            );
	            $this->db->set('createdStamp', 'NOW()', FALSE);
	            $this->db->insert('ledgerp', $data);
        	}
		    // //////////// END - inserting in ledgerP////////////19 Nov 2018

		} 

        /////END - Saving Products

        $this->db->query('UNLOCK TABLES');

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
	}


	// public function update()
 //    {
 //    	set_time_limit(0);
 //        $this->db->trans_begin();

 //        $this->db->query('LOCK TABLE ci WRITE, cidetail WRITE, openingbal WRITE');

 //        $ciDt = date('Y-m-d', strtotime($this->input->post('vDt')));

	// 	$data = array(
	//         'orgRowId' => $this->session->orgRowId
	//         , 'ciDt' => $ciDt
	//         , 'partyRowId' => $this->input->post('partyRowId')
	//         , 'returnType' => $this->input->post('returnType')
	//         , 'totalQty' => (float)$this->input->post('totalQty')
	//         , 'totalAmt' => (float)$this->input->post('totalAmt')
	//         , 'discountPer' => (float)$this->input->post('discountPer')
	//         , 'discountAmt' => (float)$this->input->post('discountAmt')
	//         , 'totalAfterDiscount' => (float)$this->input->post('totalAfterDiscount')
	//         , 'vatPer' => (float)$this->input->post('vatPer')
	//         , 'vatAmt' => (float)$this->input->post('vatAmt')
 //            , 'sgstPer' => (float)$this->input->post('sgstPer')
 //            , 'sgstAmt' => (float)$this->input->post('sgstAmt')
 //            , 'cgstPer' => (float)$this->input->post('cgstPer')
 //            , 'cgstAmt' => (float)$this->input->post('cgstAmt')
 //            , 'igstPer' => (float)$this->input->post('igstPer')
 //            , 'igstAmt' => (float)$this->input->post('igstAmt')
	//         , 'net' => (float)$this->input->post('net')
	// 	);
	// 	$this->db->where('ciRowId', $this->input->post('globalrowid'));
	// 	$this->db->update('ci', $data);	

		
	// 	/////Updating CiDetail
	// 	$TableData = $this->input->post('TableData');
 //        $TableData = stripcslashes($TableData);
 //        $TableData = json_decode($TableData,TRUE);
 //        $myTableRows = count($TableData);

 //        for ($i=0; $i < $myTableRows; $i++) 
 //        {
	// 		/////////Purani Qty 
 //        	$this->db->select('qty');
	//         $this->db->where('productRowId',  $TableData[$i]['productRowId']);
	//         $this->db->where('ciRowId', $this->input->post('globalrowid'));
	//         $query = $this->db->get('cidetail');
	//         if($query->num_rows() > 0)
	//         {
	// 	        $row = $query->row_array();
	// 	        $pichliQty = $row['qty'];
	// 	    }
	// 	    $this->db->select('avlQty');
	//         $this->db->where('productRowId',  $TableData[$i]['productRowId']);
	//         $this->db->where('stageRowId',  $TableData[$i]['stageRowId']);
	//         $this->db->where('orgRowId',  $this->session->orgRowId);
	//         $query = $this->db->get('openingbal');
	//         if($query->num_rows() > 0)
	//         {
	// 	        $row = $query->row_array();
	// 	        $abhiKiQty = $row['avlQty'];
		        
	// 	        $data = array(
	// 	            'avlQty' => $abhiKiQty - $pichliQty + (float) $TableData[$i]['qty']
	// 	        );
	// 	        $this->db->where('productRowId',  (float) $TableData[$i]['productRowId']);
	// 	        $this->db->where('stageRowId',  (float) $TableData[$i]['stageRowId']);
	// 	        $this->db->where('orgRowId',  $this->session->orgRowId);
	// 	        $this->db->update('openingbal', $data); 
	// 	    }


	// 	    ///////update CiDetail
	// 		$data = array(
	// 		        'rate' => (float) $TableData[$i]['rate']
	// 		        , 'qty' => (float) $TableData[$i]['qty']
	// 		        , 'amt' => (float) $TableData[$i]['amt']
	// 		        , 'colourRowId' => (float) $TableData[$i]['colourRowId']
	// 		        , 'stageRowId' => (float) $TableData[$i]['stageRowId']
	// 		        , 'remarks' => $TableData[$i]['remarks']
	// 		);
	// 		$this->db->where('productRowId', $TableData[$i]['productRowId']);
	// 		$this->db->where('ciRowId', $this->input->post('globalrowid'));
	// 		$this->db->update('cidetail', $data);			
	// 		///////Updating Stage Qty

			

 //        }
 //        /////END - Saving Products

 //        $this->db->query('UNLOCK TABLES');

 //        if ($this->db->trans_status() === FALSE)
 //        {
 //            $this->db->trans_rollback();
 //        }
 //        else
 //        {
 //            $this->db->trans_commit();
 //        }

	// }


	public function delete()
	{
		$data = array(
		    'deleted' => 'Y'
		);
		$this->db->where('ciRowId',  $this->input->post('rowId'));
		$this->db->update('ci', $data);

		if ($this->session->orgRowId == 2) ///Agar kinny se kr rahe h
		{
			$this->db->select('*');
	        $this->db->where('ciRowId', $this->input->post('rowId'));
	        $query = $this->db->get('cidetail');
	        foreach ($query->result() as $row)
	        {
	        	$productRowId = $row->productRowId;
	        	$colourRowId = $row->colourRowId;
	        	$stageRowId = $row->stageRowId;
	        	$pichliQty = $row->qty;

	        	$this->db->select('avlQty');
		        $this->db->where('productRowId',  $productRowId);
		        $this->db->where('colourRowId',  $colourRowId);
		        $this->db->where('stageRowId',  $stageRowId);
		        $this->db->where('orgRowId',  $this->session->orgRowId);
		        $query = $this->db->get('openingbal');
		        if($query->num_rows() > 0)
		        {
			        $row = $query->row_array();
			        $abhiKiQty = $row['avlQty'];
			        
			        $data = array(
			            'avlQty' => $abhiKiQty - $pichliQty
			        );
			        $this->db->where('productRowId', $productRowId);
			        $this->db->where('colourRowId', $colourRowId);
			        $this->db->where('stageRowId',  $stageRowId);
			        $this->db->where('orgRowId',  $this->session->orgRowId);
			        $this->db->update('openingbal', $data); 
			    }

	        }
	    }
	    else  ////Agar credo se kr rahe h
	    {
	    	$data = array(
	                'receive' => 0
	                , 'receiveBal' => 0
	            );
	            $this->db->where('vType', 'R');
		        $this->db->where('vNo',  $this->input->post('rowId'));
		        $this->db->where('orgRowId',  $this->session->orgRowId);
		        $this->db->update('ledgerp', $data);
	    }
		
	}


	public function getProducts()
    {
        $this->db->select('cidetail.*, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight, colours.colourName, stageName');
        $this->db->where('cidetail.ciRowId', $this->input->post('rowid'));
        $this->db->from('cidetail');
        $this->db->join('products','products.productRowId = cidetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        // $this->db->join('placements','placements.placementRowId = qpodetail.placementRowId');
        $this->db->join('colours','colours.colourRowId = cidetail.colourRowId');
        $this->db->join('stages','stages.stageRowId = cidetail.stageRowId');

        $this->db->order_by('ciDetailRowId');
        $query = $this->db->get();
        return($query->result_array());
    }

	public function getInvNo()
    {
        return $this->globalInvNo;
    }

	public function getImageName($rowId)
    {
        $this->db->select('products.productRowId, products.imagePathThumb');
		$this->db->from('products');
		$this->db->where('products.productRowId', $rowId);
		// $this->db->order_by('qpo.qpoRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
    }

    public function getCi($rowId)
    {
        $this->db->select('ci.*, addressbook.name, addressbook.addr, addressbook.pin, addressbook.tinCentre, prefixtypes.prefixType, towns.townName');
		$this->db->from('ci');
		$this->db->join('parties','parties.partyRowId = ci.partyRowId');
		$this->db->join('addressbook','addressbook.abRowId = parties.abRowId');
		$this->db->join('prefixtypes','prefixtypes.prefixTypeRowId = addressbook.prefixTypeRowId');
		$this->db->join('towns','towns.townRowId = addressbook.townRowId');
		$this->db->join('districts','districts.districtRowId = towns.districtRowId');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('ci.ciRowId', $rowId);
		$this->db->order_by('ci.ciRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());

    }

    public function getReturnDetial()
    {
    	// $this->db->select('cidetail.*, productcategories.productCategory, productcategories.productCategoryRowId, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight');
    	$this->db->select('cidetail.*, productcategories.productCategory, products.productName, products.productLength, products.productWidth, products.uom, products.productHeight, colours.colourName, stageName');
        $this->db->where('cidetail.ciRowId', $this->input->post('ciRowId'));
        $this->db->from('cidetail');
        $this->db->join('products','products.productRowId = cidetail.productRowId');
        $this->db->join('productcategories','productcategories.productCategoryRowId = products.productCategoryRowId');
        // $this->db->join('placements','placements.placementRowId = qpodetail.placementRowId');
        $this->db->join('colours','colours.colourRowId = cidetail.colourRowId');
        $this->db->join('stages','stages.stageRowId = cidetail.stageRowId', "left");
        $query = $this->db->get();
        return($query->result_array());
    }
    
}