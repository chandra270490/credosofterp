<?php
class Pointstoemployee_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getEmpList()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        // $this->db->where('employees.salType', 'C');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'];
        }
        return $arr;
    }

    public function getDataLimit()
	{
		$this->db->select('pointstoemployee.*, addressbook.name');
		$this->db->from('pointstoemployee');
		$this->db->join('employees','employees.empRowId = pointstoemployee.empRowId');
		$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
		$this->db->order_by('pointstoemployee.rowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('pointstoemployee.*, addressbook.name');
		$this->db->from('pointstoemployee');
		$this->db->join('employees','employees.empRowId = pointstoemployee.empRowId');
		$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
		$this->db->order_by('pointstoemployee.rowId desc');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    
	public function insert()
    {
		$this->db->select_max('rowId');
		$query = $this->db->get('pointstoemployee');
        $row = $query->row_array();

        $current_row = $row['rowId']+1;

        $dt = date('Y-m-d', strtotime($this->input->post('dt')));
		$data = array(
	        'rowId' => $current_row
	        , 'dt' => $dt
	        , 'empRowId' => $this->input->post('empRowId')
	        , 'points' => $this->input->post('points')
	        , 'remarks' => $this->input->post('remarks')
	        , 'orgRowId' => $this->session->orgRowId
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('pointstoemployee', $data);	
	}


	public function update()
    {
    	$dt = date('Y-m-d', strtotime($this->input->post('dt')));
		$data = array(
	        'dt' => $dt
	        , 'empRowId' => $this->input->post('empRowId')
	        , 'points' => $this->input->post('points')
	        , 'remarks' => $this->input->post('remarks')
		);
		$this->db->where('rowId', $this->input->post('globalrowid'));
		$this->db->update('pointstoemployee', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('rowId', $this->input->post('rowId'));
		$this->db->delete('pointstoemployee');
	}

	public function getTownList()
	{
		$this->db->select('pointstoemployee.*, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('pointstoemployee');
		$this->db->join('districts','districts.points = pointstoemployee.points');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('pointstoemployee.deleted', 'N');
		$this->db->order_by('pointstoemployee.empRowId');
		$query = $this->db->get();

		return($query->result_array());
	}

	public function getTownListRefresh()
	{
		$this->db->select('pointstoemployee.*, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('pointstoemployee');
		$this->db->join('districts','districts.points = pointstoemployee.points');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('pointstoemployee.deleted', 'N');
		$this->db->order_by('pointstoemployee.empRowId');
		$query = $this->db->get();

		return($query->result_array());
	}
}