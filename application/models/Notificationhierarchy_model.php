<?php
class Notificationhierarchy_model extends CI_Model 
{
	public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{	
		$this->db->select('notificationhierarchy.*, users.uid as userName, managers.uid as mgr');
		$this->db->from('notificationhierarchy');
		$this->db->join('users','users.rowid = notificationhierarchy.userRowId');
		$this->db->join('users managers','managers.rowid = notificationhierarchy.userRowIdMgr');
		$this->db->order_by('notificationhierarchy.rowId desc');
		$query = $this->db->get();

		return($query->result_array());
	}

	public function deleteRecord()
	{
		$this->db->where('userRowId', $this->input->post('userRowId'));
		$this->db->where('forModule', $this->input->post('forModule'));
		$this->db->delete('notificationhierarchy');
	}

 	public function insertAjax()
    {
		$this->db->select_max('rowId');
		$query = $this->db->get('notificationhierarchy');
		// echo "fff";
		$this->deleteRecord();
		// echo console.log('ff');
		if ($query->num_rows() > 0)
		{
	        $row = $query->row_array();
        	$userRowId  = $this->input->post('userRowId');
			$data = explode(",", $_POST['users4notification']);


         	$current_row = $row['rowId']+1;
         	echo $current_row;
	       	for ($i=0; $i <count($data) ; $i++)
	       	{
				$data1 = array(
		        'rowId' => $current_row,
		        'userRowId' =>$userRowId,
		        'userRowIdMgr' => $data[$i],
		        'forModule' => $this->input->post('forModule')
				);
				
				$this->db->insert('notificationhierarchy', $data1);
				$current_row++;
			}
		}


		
	}



	public function getRights($uid)
    {
		// $username = $uid;
		$this->db->select('userRowIdMgr');
		$this->db->where('userRowId', $uid);
		$this->db->where('forModule', $this->input->post('forModule'));
		$this->db->order_by('');
		$query = $this->db->get('notificationhierarchy');
		return($query->result_array());
    }
}