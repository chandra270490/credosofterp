<?php
class Leaveapplication_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

   
	public function getDataLimit()
	{
		if( $this->session->abAccess == "C" )
		{
			$this->db->select('leaveapplication.*, users.uid');
			$this->db->from('leaveapplication');
			$this->db->join('users', 'users.rowid = leaveapplication.userRowId');
			$this->db->where('leaveapplication.approved', 'N');
			$this->db->where('leaveapplication.orgRowId', $this->session->orgRowId);
			$this->db->order_by('leaveapplication.leaveApplicationRowId desc');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
		else if( $this->session->abAccess == "L" )
		{
			$this->db->select('leaveapplication.*, users.uid');
			$this->db->from('leaveapplication');
			$this->db->join('users', 'users.rowid = leaveapplication.userRowId');
			$this->db->where('leaveapplication.approved', 'N');
			$this->db->where('leaveapplication.orgRowId', $this->session->orgRowId);
			$abAccessIn = explode(",", $this->session->abAccessIn);
			$this->db->where_in('leaveapplication.createdBy', $abAccessIn);
			$this->db->order_by('leaveapplication.leaveApplicationRowId desc');
			// $this->db->limit(5);
			$query = $this->db->get();

			return($query->result_array());
		}
	}
	


	public function checkDuplicate()
    {
		$this->db->select('reason');
		$this->db->where('dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('userRowId', $this->session->userRowId );
        $this->db->from('leaveapplication');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return 1;
        }
    }

	public function insert()
    {
		$this->db->select_max('leaveApplicationRowId');
		$query = $this->db->get('leaveapplication');
        $row = $query->row_array();

        $current_row = $row['leaveApplicationRowId']+1;
        $dtFrom = date('Y-m-d', strtotime($this->input->post('dtFrom')));
        $dtTo = date('Y-m-d', strtotime($this->input->post('dtTo')));
        
    	$data = array(
		        'leaveApplicationRowId' => $current_row
		        , 'orgRowId' => $this->session->orgRowId
		        , 'userRowId' => $this->session->userRowId
		        , 'dtFrom' => $dtFrom
		        , 'dtTo' => $dtTo
		        , 'totalDays' => (int)$this->input->post('totalDays')
		        , 'reason' => $this->input->post('reason')
		        , 'createdBy' => $this->session->userRowId
			);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('leaveapplication', $data);	


		////////////////////Notification
		$this->db->select('userRowIdMgr');
		$this->db->where('userRowId', $this->session->userRowId );
		$this->db->where('forModule', 'Leave' );
		$queryNh = $this->db->get('notificationhierarchy');
		foreach ($queryNh->result() as $rowNh)
        {
        	$userRowIdMgr = $rowNh->userRowIdMgr;


			$this->db->select_max('notificationRowId');
			$queryN = $this->db->get('notifications');
	        $rowN = $queryN->row_array();

	        $notificationRowId = $rowN['notificationRowId']+1;
		    $data = array(
		        'notificationRowId' => $notificationRowId
		        , 'orgRowId' => $this->session->orgRowId
		        , 'vType' => 'Leave'
		        , 'vNo' => $current_row
		        , 'notification' => $this->session->userid . " applied for leave."
		        , 'userRowIdFor' => $userRowIdMgr
		        , 'createdBy' => $this->session->userRowId
			);
			$this->db->set('createdStamp', 'NOW()', FALSE);
			$this->db->insert('notifications', $data);	
		}
	}

	public function checkDuplicateOnUpdate()
    {
		$this->db->select('reason');
		$this->db->where('dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->where('userRowId', $this->session->userRowId );
        $this->db->where('leaveApplicationRowId !=', $this->input->post('globalrowid'));
        $this->db->from('leaveapplication');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return 1;
        }
    }

	public function update()
    {
		$dtFrom = date('Y-m-d', strtotime($this->input->post('dtFrom')));
        $dtTo = date('Y-m-d', strtotime($this->input->post('dtTo')));
        
    	$data = array(
		         'orgRowId' => $this->session->orgRowId
		        , 'dtFrom' => $dtFrom
		        , 'dtTo' => $dtTo
		        , 'totalDays' => (int)$this->input->post('totalDays')
		        , 'reason' => $this->input->post('reason')
			);

		$this->db->where('leaveApplicationRowId', $this->input->post('globalrowid'));
		$this->db->update('leaveapplication', $data);			
	}

	public function delete()
	{
		$this->db->where('leaveApplicationRowId', $this->input->post('rowId'));
		$this->db->delete('leaveapplication');

	}


}