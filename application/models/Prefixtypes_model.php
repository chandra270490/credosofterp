<?php
class Prefixtypes_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('prefixTypeRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('prefixtypes');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('prefixTypeRowId');
		$query = $this->db->get('prefixtypes');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('prefixType');
		$this->db->where('prefixType', $this->input->post('prefixType'));
		$query = $this->db->get('prefixtypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('prefixTypeRowId');
		$query = $this->db->get('prefixtypes');
        $row = $query->row_array();

        $current_row = $row['prefixTypeRowId']+1;

		$data = array(
	        'prefixTypeRowId' => $current_row
	        , 'prefixType' => ucwords($this->input->post('prefixType'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('prefixtypes', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('prefixType');
		$this->db->where('prefixType', $this->input->post('prefixType'));
		$this->db->where('prefixTypeRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('prefixtypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'prefixType' => ucwords($this->input->post('prefixType'))
		);
		$this->db->where('prefixTypeRowId', $this->input->post('globalrowid'));
		$this->db->update('prefixtypes', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('prefixTypeRowId', $this->input->post('rowId'));
		$this->db->delete('prefixtypes');
	}

	public function getPrefixes()
	{
		$this->db->select('prefixTypeRowId, prefixType');
		$this->db->where('deleted', 'N');
		$this->db->order_by('prefixType');
		$query = $this->db->get('prefixtypes');

		$arr = array();
		$arr["-1"] = '--- Select ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['prefixTypeRowId']]= $row['prefixType'];
		}

		return $arr;
	}
}