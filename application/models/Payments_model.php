<?php
class Payments_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getEmpList()
    {
        $this->db->select('addressbook.name, employees.empRowId');
        $this->db->from('addressbook');
        $this->db->join('employees','employees.abRowId = addressbook.abRowId');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.discontinue', 'N');
        // $this->db->where('employees.salType', 'C');
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->order_by('addressbook.name');
        $query = $this->db->get();
        $arr = array();
        $arr["-1"] = '--- Select ---';
        foreach ($query->result_array() as $row)
        {
            $arr[$row['empRowId']]= $row['name'].' ('. $row['empRowId'] .')';
        }
        return $arr;
    }

    public function getDataLimit()
	{
		$this->db->select('payments.*, addressbook.name');
		$this->db->from('payments');
		$this->db->join('employees','employees.empRowId = payments.empRowId');
		$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
		$this->db->order_by('payments.paymentRowId desc');
		$this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('payments.*, addressbook.name');
		$this->db->from('payments');
		$this->db->join('employees','employees.empRowId = payments.empRowId');
		$this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
		$this->db->order_by('payments.paymentRowId');
		// $this->db->limit(5);
		$query = $this->db->get();

		return($query->result_array());
	}

    
	public function insert()
    {
		$this->db->select_max('paymentRowId');
		$query = $this->db->get('payments');
        $row = $query->row_array();

        $current_row = $row['paymentRowId']+1;

        $dt = date('Y-m-d', strtotime($this->input->post('dt')));
		$data = array(
	        'paymentRowId' => $current_row
	        , 'dt' => $dt
	        , 'empRowId' => $this->input->post('empRowId')
	        , 'amt' => $this->input->post('amt')
	        , 'remarks' => $this->input->post('remarks')
	        , 'orgRowId' => $this->session->orgRowId
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('payments', $data);	
	}


	public function update()
    {
    	$dt = date('Y-m-d', strtotime($this->input->post('dt')));
		$data = array(
	        'dt' => $dt
	        , 'empRowId' => $this->input->post('empRowId')
	        , 'amt' => $this->input->post('amt')
	        , 'remarks' => $this->input->post('remarks')
		);
		$this->db->where('paymentRowId', $this->input->post('globalrowid'));
		$this->db->update('payments', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('paymentRowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('paymentRowId', $this->input->post('paymentRowId'));
		$this->db->delete('payments');
	}

	public function getTownList()
	{
		$this->db->select('payments.*, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('payments');
		$this->db->join('districts','districts.amt = payments.amt');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('payments.deleted', 'N');
		$this->db->order_by('payments.empRowId');
		$query = $this->db->get();

		return($query->result_array());
	}

	public function getTownListRefresh()
	{
		$this->db->select('payments.*, districts.districtName, states.stateName, countries.countryName');
		$this->db->from('payments');
		$this->db->join('districts','districts.amt = payments.amt');
		$this->db->join('states','states.stateRowId = districts.stateRowId');
		$this->db->join('countries','countries.countryRowId = states.countryRowId');
		$this->db->where('payments.deleted', 'N');
		$this->db->order_by('payments.empRowId');
		$query = $this->db->get();

		return($query->result_array());
	}
}