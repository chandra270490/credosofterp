<?php
class Salaryexecutives_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function PooreMahineKiAttendanceMarkHoGayi()
    {
        // $days = date('d', strtotime($this->input->post('dtTo'))) - date('d', strtotime($this->input->post('dtFrom'))) + 1;
        // $this->db->distinct();
        // $this->db->select('attendanceexecutives.dt');
        // $this->db->from('attendanceexecutives');
        // $this->db->where('attendanceexecutives.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        // $this->db->where('attendanceexecutives.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        // $query = $this->db->get();
        // if ($query->num_rows() > 0)
        // {
        //     if( $query->num_rows() == $days )
        //     {
        //         return 0;
        //     }
        //     else
        //     {
        //         return 1;
        //     }
        // }
        // //////// END - checking if already calculated
    }

    public function getDataForReport()
    {
         set_time_limit(0);
         ////// ESI base limit and emp contri
        $this->db->select('baselimits.*');
        $this->db->from('baselimits');
        $queryBaseLimits = $this->db->get();
        if ($queryBaseLimits->num_rows() > 0)
        {
            $rowBaseLimits = $queryBaseLimits->row(); 
            $esiBase = $rowBaseLimits->esiBase;
            $esiEmployee = $rowBaseLimits->esiEmployee;
            $pfBase = $rowBaseLimits->pfBase;
            $pfEmployee = $rowBaseLimits->pfEmployee;
        }
        ////// END - ESI base limit and emp contri

        //////creating tmpsale table
        $this->load->dbforge();
        if($this->db->table_exists('tmpsale'))
        {
            $this->dbforge->drop_table('tmpsale');
        }
        $fields = array(
                    'rowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'empName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'userRowId' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                ),
                    'userName' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '100',
                                ),
                    'presents' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                ),
                    'holidays' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                ),
                    'tours' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                ),
                    'leavesTaken' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'leBf' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'leavesEarned' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'leCf' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'absents' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                ),
                    'countDays' => array(
                                     'type' => 'INT',
                                     'constraint' => '5',
                                     'default' => '0',
                                ),
                    'salPerMonth' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                     'default' => '0',
                                ),
                    'hra' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'ta' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'ca' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'ma' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'salCalculated' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'inc' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'gross' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'pf' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'esi' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'deductionUnderIt' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'tds' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'net' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'prevDues' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,0',
                                     'default' => '0',
                                ),
                    'adv' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'netPayble' => array(
                                     'type' => 'DECIMAL',
                                     'constraint' => '12,2',
                                     'default' => '0',
                                ),
                    'showInAttendance' => array(
                                     'type' => 'VARCHAR',
                                     'constraint' => '1',
                                     'default' => 'Y',
                                )
            );  
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('tmpsale');


        $this->db->select('employees.*, addressbook.name, users.uid');
        $this->db->from('employees');
        $this->db->where('employees.deleted', 'N');
        $this->db->where('employees.salType', 'M' );
        // $this->db->where('employees.discontinue', 'N' );
        $this->db->group_start();
            $this->db->or_where('employees.discontinueDt IS NULL');
            $this->db->or_where('employees.discontinueDt > ', date('Y-m-d', strtotime($this->input->post('dtTo'))) );
        $this->db->group_end();
        $this->db->where('employees.userRowId >', 0 );
        $this->db->where('employees.orgRowId', $this->session->orgRowId);
        $this->db->join('addressbook','addressbook.abRowId = employees.abRowId');
        $this->db->join('users','users.rowid = employees.userRowId');
        $this->db->order_by('name');
        $query = $this->db->get();

        $rowId = 1;
        foreach ($query->result() as $row)
        {
            $leBf = $row->leavesOpBal;
            $data = array(
                        'rowId' => $rowId,
                        'empRowId' => $row->empRowId,
                        'empName' => $row->name,
                        'userRowId' => $row->userRowId,
                        'userName' => $row->uid,
                        'salPerMonth' => $row->basicSal,
                        'hra' => $row->hra,
                        'ta' => $row->ta,
                        'ca' => $row->ca,
                        'ma' => $row->ma,
                        'leBf' => $row->leavesOpBal,
                        'showInAttendance' => $row->showInAttendance,
                );
            $this->db->insert('tmpsale', $data);               
            $rowId++;

            $vShowInAttendance = $row->showInAttendance;
            if($vShowInAttendance == "Y")   ///for regular employees
            {
                //////////////////////// Present Count
                $this->db->distinct();
                $this->db->select('attendanceexecutives.dt, attendanceexecutives.attendance, attendanceexecutives.userRowId, attendanceexecutives.orgRowId');
                $this->db->from('attendanceexecutives');
                $this->db->where('attendanceexecutives.userRowId', $row->userRowId );
                $this->db->where('attendanceexecutives.dt <=', date('Y-m-d', strtotime($this->input->post('dtTo'))));
                $this->db->where('attendanceexecutives.dt >=', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
                $this->db->where('attendanceexecutives.orgRowId', $this->session->orgRowId);
                $queryAttendance = $this->db->get();
                $PRcount = 0;
                $HOcount = 0;
                $TOcount = 0;
                $LEcount = 0;
                $ABcount = 0;

                $leavesEarned = 0;

                if ($queryAttendance->num_rows() > 0)
                {
                    foreach ($queryAttendance->result() as $rowAttendance)
                    {
                        $duty = $rowAttendance->attendance;
                        if( $duty == "PR" )
                        {
                            // $PRcount += $nightAll;
                            $PRcount++;
                        }
                        else if( $duty == "HO" )
                        {
                            $HOcount++;
                        }
                        else if( $duty == "TO" )
                        {
                            $TOcount++;
                        }
                        else if( $duty == "LE" )
                        {
                            $LEcount++;
                        }
                        else if( $duty == "AB" )
                        {
                            $ABcount++;
                        }
                    }
                    $leavesOn = $row->leavesOn;
                    $totalWorkingDays = $PRcount + $TOcount;

                    if( $leavesOn == 0)
                    {
                        $leavesEarned = 0;
                    }
                    else
                    {
                        $leavesEarned = $totalWorkingDays / $leavesOn;
                    }
                    $countDays = $PRcount + $TOcount + $HOcount; ///days without AB


                    $date1 = $this->input->post('dtFrom');
                    $date2 = $this->input->post('dtTo');
                    $dpm = (abs(strtotime($date2) - strtotime($date1))) / (60*60*24)  +  1;
                    $salCalculated = ($row->basicSal+$row->hra+$row->ta+$row->ca+$row->ma) / $dpm * $countDays;
                    $gross = $salCalculated;

                    ////////////// Calc. PF
                    $pf = 0;
                    if( $row->deductPf == 'Y' && $row->basicSal <= $pfBase )
                    {
                        $pf = $gross * $pfEmployee / 100;
                        // $pf =99;
                    }
                    //////////// END - Calc. PF  

                    ////////////// Calc. Esi
                    $esi = 0;   
                    if( $row->deductEsi == 'Y' && $row->basicSal <= $esiBase )
                    {
                        $esi = $gross * $esiEmployee / 100;
                    }
                    ////////////// END - Calc. Esi      

                    // ////////////// Calc. TDS
                    // $annualSal = $row->basicSal * 12;
                    // $this->db->select('incometaxslabs.*');
                    // $this->db->from('incometaxslabs');
                    // $this->db->where('LowerRange<=', $annualSal);
                    // $this->db->order_by('incometaxslabs.lowerRange desc');
                    // $queryIT = $this->db->get();
                    $tds = 0;
                    // $rowIT =0;
                    //  $surcharge=0;
                    // if ($queryIT->num_rows() > 0)
                    // {
                    //     $i=0;
                    //     $rowIT = $queryIT->row($i);
                    //     $surcharge = $rowIT->surcharge;
                    //     $cess = $rowIT->cess;
                    //     $Tax = 0;
                    //     while($annualSal > 0 )
                    //     {
                    //         if($annualSal > $rowIT->lowerRange)
                    //         {
                    //             // $surcharge=$queryIT->row(0)->lowerRange;
                    //             $x = $annualSal - ($rowIT->lowerRange - 1);
                    //             $Tax = $Tax + ($x * $rowIT->rate / 100);
                    //             $annualSal = $annualSal - $x;
                    //         }
                    //         else
                    //         {
                    //             break;
                    //         }
                    //         $i++;
                    //         $rowIT = $queryIT->row($i);
                    //     }
                    //     $tds = $Tax / 12;                    
                    // }
                    // ////////////// END - Calc. TDS
                    // $net = $gross - $pf - $esi - $tds;
                    $net = $gross - $pf - $esi;

                    $data = array(
                        'presents' => $PRcount
                        , 'holidays' => $HOcount
                        , 'tours' => $TOcount
                        , 'leavesTaken' => $LEcount
                        , 'absents' => $ABcount
                        , 'leavesEarned' => $leavesEarned
                        , 'countDays' => $countDays
                        , 'salCalculated' => $salCalculated
                        , 'gross' => $gross
                        , 'pf' => $pf
                        , 'esi' => $esi
                        , 'tds' =>  $tds
                        , 'net' => $net
                        );
                    $this->db->where('userRowId', $row->userRowId);
                    $this->db->update('tmpsale', $data);
                }

                //////////////////////// Leaves Carry Fwd (LeCF)
                $this->db->select('salaryexecutives.leCf');
                $this->db->from('salaryexecutives');
                $this->db->where('salaryexecutives.userRowId', $row->userRowId );
                $this->db->order_by('salaryexecutives.seRowId desc');
                $queryLeBf = $this->db->get();
                
                if ($queryLeBf->num_rows() > 0)
                {
                    $rowLastCfLe = $queryLeBf->row(1);
                    $leBf = $rowLastCfLe->leCf;
                }
                $data = array(
                    'leBf' => $leBf
                    );
                $this->db->where('userRowId', $row->userRowId);
                $this->db->update('tmpsale', $data);

                ////////// Leaves Carry Fwd AND Sal Calculated

                $leCf = $leBf + $leavesEarned;
                $data = array(
                    'leCf' => $leCf
                    );
                $this->db->where('userRowId', $row->userRowId);
                $this->db->update('tmpsale', $data);
            }
            else  ///agar management ka banda h (eg. Avantika etc.)
            {
                $salCalculated = ($row->basicSal+$row->hra+$row->ta+$row->ca+$row->ma);
                $gross = $salCalculated;

            
                // ////////////// Calc. TDS
                // $annualSal = $row->basicSal * 12;
                // $this->db->select('incometaxslabs.*');
                // $this->db->from('incometaxslabs');
                // $this->db->where('LowerRange<=', $annualSal);
                // $this->db->order_by('incometaxslabs.lowerRange desc');
                // $queryIT = $this->db->get();
                $tds = 0;
                // $rowIT =0;
                //  $surcharge=0;
                // if ($queryIT->num_rows() > 0)
                // {
                //     $i=0;
                //     $rowIT = $queryIT->row($i);
                //     $surcharge = $rowIT->surcharge;
                //     $cess = $rowIT->cess;
                //     $Tax = 0;
                //     while($annualSal > 0 )
                //     {
                //         if($annualSal > $rowIT->lowerRange)
                //         {
                //             // $surcharge=$queryIT->row(0)->lowerRange;
                //             $x = $annualSal - ($rowIT->lowerRange - 1);
                //             $Tax = $Tax + ($x * $rowIT->rate / 100);
                //             $annualSal = $annualSal - $x;
                //         }
                //         else
                //         {
                //             break;
                //         }
                //         $i++;
                //         $rowIT = $queryIT->row($i);
                //     }
                //     $tds = $Tax / 12;                    
                // }
                // ////////////// END - Calc. TDS
                // $net = $gross - $tds;
                $net = $gross;

                $data = array(
                     'salCalculated' => $salCalculated
                    , 'gross' => $gross
                    , 'tds' =>  $tds
                    , 'net' => $net
                    );
                $this->db->where('userRowId', $row->userRowId);
                $this->db->update('tmpsale', $data);
            }
        }


        /// now sending data to client
        // $this->db->select_sum('tmpsale.amt');
        $this->db->select('tmpsale.*');
        $this->db->order_by('tmpsale.empName');
        // $this->db->group_by('tmpsale.empRowId');
        $this->db->from('tmpsale');
        $query = $this->db->get();
        return($query->result_array());
    }

    public function checkDuplicate()
    {
        //////// checking if already calculated
        $this->db->select('salaryexecutives.empRowId');
        $this->db->where('dtFrom =', date('Y-m-d', strtotime($this->input->post('dtFrom'))));
        $this->db->where('dtTo =', date('Y-m-d', strtotime($this->input->post('dtTo'))));
        $this->db->from('salaryexecutives');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return 1;
        }
        //////// END - checking if already calculated
    }

    public function saveData()
    {
        set_time_limit(0);
        $this->db->trans_begin();

        // $this->db->query('LOCK TABLE qpo WRITE, qpodetail WRITE');

        


        $dtFrom = date('Y-m-d', strtotime($this->input->post('dtFrom')));
        $dtTo = date('Y-m-d', strtotime($this->input->post('dtTo')));
        $dt = date('Y-m-d', strtotime($this->input->post('dt')));

        /////Saving salry 
        $TableData = $this->input->post('TableData');
        $TableData = stripcslashes($TableData);
        $TableData = json_decode($TableData,TRUE);
        $myTableRows = count($TableData);

        for ($i=0; $i < $myTableRows; $i++) 
        {
            /////updateing calculated Y in Payments
            // $data = array(
            //             'calculated' => 'Y'
            //     );
            //     $this->db->where('empRowId', $TableData[$i]['empRowId']);
            //     $this->db->update('payments', $data);    
            /////END - updateing calculated Y in Payments

            /////updateing calculated Y in SalaryMonthly
            $data = array(
                        'calculated' => 'Y'
                );
                $this->db->where('empRowId', $TableData[$i]['empRowId']);
                $this->db->update('salaryexecutives', $data);    
            /////END - updateing calculated Y in SalaryContract


            /////////// Inserting Current Data
            $this->db->select_max('seRowId');
            $query = $this->db->get('salaryexecutives');
            $row = $query->row_array();
            $seRowId = $row['seRowId'] + 1;

            $data = array(
                    'seRowId' => $seRowId
                    , 'dtFrom' => $dtFrom
                    , 'dtTo' => $dtTo
                    , 'orgRowId' => $this->session->orgRowId
                    , 'empRowId' => $TableData[$i]['empRowId']
                    , 'userRowId' => $TableData[$i]['userRowId']
                    , 'presents' =>  $TableData[$i]['presents']
                    , 'holidays' =>  $TableData[$i]['holidays']
                    , 'tours' => $TableData[$i]['tours']
                    , 'leavesTaken' =>  $TableData[$i]['leavesTaken']
                    , 'leBf' => $TableData[$i]['leBf']
                    , 'leavesEarned' => $TableData[$i]['leavesEarned']
                    , 'leCf' => $TableData[$i]['leCf']
                    , 'absents' => $TableData[$i]['absents']
                    , 'countDays' => (float) $TableData[$i]['countDays']
                    , 'salPerMonth' => (float) $TableData[$i]['salPerMonth']
                    , 'hra' => (float) $TableData[$i]['hra']
                    , 'ta' => (float) $TableData[$i]['ta']
                    , 'ca' => (float) $TableData[$i]['ca']
                    , 'ma' => (float) $TableData[$i]['ma']
                    , 'salCalculated' => (float) $TableData[$i]['salCalculated']
                    , 'gross' => (float) $TableData[$i]['gross']
                    , 'pf' => (float) $TableData[$i]['pf']
                    , 'esi' => (float) $TableData[$i]['esi']
                    , 'tds' => (float) $TableData[$i]['tds']
                    , 'net' => (float) $TableData[$i]['net']
                    , 'createdBy' => $this->session->userRowId
            );
            $this->db->set('createdStamp', 'NOW()', FALSE);
            $this->db->insert('salaryexecutives', $data);    
        }
        /////END - salry contract



        // $this->db->query('UNLOCK TABLES');

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}