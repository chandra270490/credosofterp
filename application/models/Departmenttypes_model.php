<?php
class Departmenttypes_model extends CI_Model 
{
    public function __construct()
    {
            $this->load->database('');
    }

    public function getDataLimit()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('departmentTypeRowId desc');
		$this->db->limit(5);
		$query = $this->db->get('departmenttypes');

		return($query->result_array());
	}

    public function getDataAll()
	{
		$this->db->select('*');
		$this->db->where('deleted', 'N');
		$this->db->order_by('departmentTypeRowId');
		$query = $this->db->get('departmenttypes');

		return($query->result_array());
	}

    

	public function checkDuplicate()
    {
		$this->db->select('departmentType');
		$this->db->where('departmentType', $this->input->post('departmentType'));
		$query = $this->db->get('departmenttypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function insert()
    {
		$this->db->select_max('departmentTypeRowId');
		$query = $this->db->get('departmenttypes');
        $row = $query->row_array();

        $current_row = $row['departmentTypeRowId']+1;

		$data = array(
	        'departmentTypeRowId' => $current_row
	        , 'departmentType' => ucwords($this->input->post('departmentType'))
	        , 'createdBy' => $this->session->userRowId
		);
		$this->db->set('createdStamp', 'NOW()', FALSE);
		$this->db->insert('departmenttypes', $data);	
	}

	public function checkDuplicateOnUpdate()
    {
    	// echo "chk";
		$this->db->select('departmentType');
		$this->db->where('departmentType', $this->input->post('departmentType'));
		$this->db->where('departmentTypeRowId !=', $this->input->post('globalrowid'));
		$query = $this->db->get('departmenttypes');

		if ($query->num_rows() > 0)
		{
			return 1;
		}
    }

	public function update()
    {
		$data = array(
	        'departmentType' => ucwords($this->input->post('departmentType'))
		);
		$this->db->where('departmentTypeRowId', $this->input->post('globalrowid'));
		$this->db->update('departmenttypes', $data);			
	}

	public function delete()
	{
		// $data = array(
		//         'deleted' => 'Y',
		//         'deletedBy' => $this->session->userRowId

		// );
		// $this->db->set('deletedStamp', 'NOW()', FALSE);
		// $this->db->where('vehicleRowId', $this->input->post('rowId'));
		// $this->db->update('vehicles', $data);

		$this->db->where('departmentTypeRowId', $this->input->post('rowId'));
		$this->db->delete('departmenttypes');
	}

	public function getDepartmentList()
	{
		$this->db->select('departmentTypeRowId, departmentType');
		$this->db->where('deleted', 'N');
		$this->db->order_by('departmentType');
		$query = $this->db->get('departmenttypes');
		$arr = array();
		$arr["-1"] = '--- SELECT ---';
		foreach ($query->result_array() as $row)
		{
    		$arr[$row['departmentTypeRowId']]= $row['departmentType'];
		}
		return $arr;
	}
}